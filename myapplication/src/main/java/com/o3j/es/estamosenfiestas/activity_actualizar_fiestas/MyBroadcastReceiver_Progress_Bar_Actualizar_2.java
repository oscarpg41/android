package com.o3j.es.estamosenfiestas.activity_actualizar_fiestas;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;

/**
 * Created by Juan on 31/08/2015.
 */
public class MyBroadcastReceiver_Progress_Bar_Actualizar_2 extends BroadcastReceiver {
    private static String xxx;

    //No hago nada en este broadcast
    @Override
    public void onReceive(Context context, Intent intent) {
        //**********************************************************************************
        // acquire the wake lock como lo explican en android developers
        //para el caso de que el movil este dormido
        PowerManager powerManager = (PowerManager) context.getSystemService(context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "MyWakelockTag");
        wakeLock.acquire();

        //release the wake lock como lo explican en android developers
//        wakeLock.release();
        //**********************************************************************************
        xxx = this.getClass().getSimpleName();

        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
        {
            Log.e(xxx, " en metodo onReceive de MyBroadcastReceiver_Progress_Bar_Actualizar_2");
        }

        Bundle bundle = intent.getExtras();
        if (bundle != null) {

        }

        //**********************************************************************************
        // acquire the wake lock como lo explican en android developers
        //para el caso de que el movil este dormido
//        PowerManager powerManager = (PowerManager) context.getSystemService(context.POWER_SERVICE);
//        PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
//                "MyWakelockTag");
//        wakeLock.acquire();

        //release the wake lock como lo explican en android developers
        wakeLock.release();
        //**********************************************************************************
    }
}
