/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.o3j.es.estamosenfiestas.display_frame_layout;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eef.data.dataelements.Event;
import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper;
import com.o3j.es.estamosenfiestas.dialogs.ClaseDialogEventoGuardado;
import com.o3j.es.estamosenfiestas.display_vista_deslizante_2.Activity_Fragment_SwipeView_EventoDetalle_BackToParent;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Provide views to RecyclerView with data from dataSet.
 */
//Juan, 4 Junio 2015. Uso este adpater para gestionar el evento como una lista con recycle view,
//    y mantener el mismo manejo que para la lista de fiestas, dias de la fiesta y lista de eventos
//    Se muestran los detalles del evento en la lista List_De_Eventos_Del_Dia


    //Juan 10 julio 2015, he puesto aqui el manejo de la contextual action bar como me pidio oscar
public class CustomAdapter_RecyclerView_Evento_Detalle extends RecyclerView.Adapter<CustomAdapter_RecyclerView_Evento_Detalle.ViewHolder>
        implements ClaseDialogEventoGuardado.InterfaceClaseDialogEventoGuardado {
//    private static final String TAG = "CustomAdapter";
    private static  String TAG;



//    private static FragmentActivity actividad;


    //Original
//    private String[] mDataSet;


    private List<Event> List_De_Eventos_Del_Dia; //Solo tiene el evento seleccionado con sus detalles

//    Agrego la interface para recoger los clicks en la actividad
    private OnItemClickListenerEventoDetalle mListener;

    // BEGIN_INCLUDE(recyclerViewSampleViewHolder)
    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */


    public static class ViewHolder extends RecyclerView.ViewHolder {
        //Los nombres como en Class_Evento_1
        private final TextView textView_Titulo;
        private final TextView textView_Descripcion;
        private final TextView textView_Descripcion2;
        private final ImageView imageView;
        private final ImageView imageViewGrande;
        private final View m_View;

        private final Toolbar toolbar_acciones_del_detailed_view;



        public TextView getTextView_Titulo() {

            return textView_Titulo;
        }

        public TextView getTextView_Descripcion() {

            return textView_Descripcion;
        }

        public TextView getTextView_Descripcion2() {

            return textView_Descripcion2;
        }

        public ImageView getImageView() {
            return imageView;
        }

        public ImageView getImageView_Grande() {
            return imageViewGrande;
        }


        public View getView() {
            return m_View;
        }


        public Toolbar getContextualActionBar() {
            return toolbar_acciones_del_detailed_view;
        }



        public ViewHolder(View v) {
            super(v);
            // Define click listener for the ViewHolder's View.


//            OJO: ver en http://www.truiton.com/2015/02/android-recyclerview-tutorial/
//            como poner el listener en el fragment

//        Juan, 3 Junio 2015, pongo los onclick en onBindViewHolder
//        (como en el proyecto NavigationDrawer) y no en el constructor del viewHolder
//            Comento estos que son los originales

//            v.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
//                        Log.d(TAG, "Element " + getPosition() + " clicked.");
//                    }
//
//
//                    //23 Feb 2015: Llamo a la actividad que muestra el detalle del evento
//                    // Do something in response to button
//
//
//                }
//            });


            // Juan, 26 Mayo 2015: Solo detecto el click, por ahora
//            v.setOnLongClickListener(new View.OnLongClickListener(){
//
//                @Override
//                public boolean onLongClick(View v) {
//                    if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
//                        Log.d(TAG, "Element " + getPosition() + " LONG clicked.");
//                    }
////                    OJO: Si retorno false, entonces se dispara el onClick!!
////                    return false;
////                    Por eso si solo quiero detectar el long click, retorno true
//                    return true;
//                }
//            });



            m_View = v;
            textView_Titulo = (TextView) v.findViewById(R.id.textView6);
            textView_Descripcion = (TextView) v.findViewById(R.id.textView7);
            textView_Descripcion2 = (TextView) v.findViewById(R.id.textView8);
            imageView = (ImageView) v.findViewById(R.id.imageView);
            imageViewGrande = (ImageView) v.findViewById(R.id.imageView_foto_Grande);

            toolbar_acciones_del_detailed_view = (Toolbar) v.findViewById(R.id.tollbar_contextual_action_bar);
            toolbar_acciones_del_detailed_view.setVisibility(View.VISIBLE);


        }

    }//Fin de la clase ViewHolder
    // END_INCLUDE(recyclerViewSampleViewHolder)

    private  void method_generar_toolbar_para_contextual_action_mode(Toolbar toolbar_acciones_del_detailed_view) {

        // Set an OnMenuItemClickListener to handle menu item clicks
        toolbar_acciones_del_detailed_view.setOnMenuItemClickListener(
                new Toolbar.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // Handle the menu item

                        int id = item.getItemId();

                        switch (id)

                        {
                            case R.id.action_agregar_a_favoritos:
                                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                                    Log.d(TAG, TAG + " action_agregar_a_favorito ");
                                }

                                //Juan 19 agosto 2015: metodo fake
//                                methodFake_Add_To_List_Of_Favorites();


                                //19 agosto 2015: Uso la clase Clase_Add_To_List_Of_Favorites que tiene el metodo method_Add_To_List_Of_Favorites
                                Clase_Add_To_List_Of_Favorites clase_add_to_list_of_favorites =
                                        Clase_Add_To_List_Of_Favorites.newInstance(m_Event_Poner_en_favoritos,
                                        activity_Fragment_SwipeView_EventoDetalle_BackToParent);
                                clase_add_to_list_of_favorites.method_Add_To_List_Of_Favorites();

                                //Juan, 4 oct 2015: Redibujo el evento para resaltar que ha sido puesto en favoritos
                                miFragment_Evento.methodResaltarEventoFavorito(m_Event_Poner_en_favoritos);

                                return true;


                            case R.id.action_enviar_mail:
                                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                                    Log.d(TAG, TAG + " action_enviar_mail ");
                                }

                                methodFake_Enviar_Mail();


                                return true;


                            default:
                                return false;
                        }
                        //*******************************************************************************
                    }
                });

        // Inflate a menu to be displayed in the toolbar
        toolbar_acciones_del_detailed_view.inflateMenu(R.menu.menu_contextual_action_mode_evento_de_la_lista);

    }//Fin de method_generar_toolbar_para_contextual_action_mode





    //    12 junio 2015 metodo fake para agregar elementos a la lista de favoritos
    //    Este metodo no lo uso cuando la api de JL este terminada

    Event m_Event_Poner_en_favoritos;
    private void methodFake_Add_To_List_Of_Favorites() {

        Class_Modelo_De_Datos_Lista_Favoritos m_Class_Modelo_De_Datos_Lista_Favoritos =
                new Class_Modelo_De_Datos_Lista_Favoritos();

        m_Class_Modelo_De_Datos_Lista_Favoritos.setStringTituloDeLaFiesta(Class_ConstantsHelper.titulo_De_La_Fiesta);
        m_Class_Modelo_De_Datos_Lista_Favoritos.setStringFechaDelEvento(Class_ConstantsHelper.fecha_Del_Dia);
        m_Class_Modelo_De_Datos_Lista_Favoritos.setEventEventoFavorito(m_Event_Poner_en_favoritos);
        Class_ConstantsHelper.arraylist_Datos_Lista_Favoritos.add(m_Class_Modelo_De_Datos_Lista_Favoritos);

        //*************************************************************************************************
//        Mostrar el dialogo de has guardado en favoritos
        boolean boolean_evento_guardado = true;

        if (boolean_evento_guardado) {

            // Create an instance of the dialog fragment and show it
            DialogFragment dialog =  ClaseDialogEventoGuardado.newInstance
                    (R.string.titulo_dialog_evento_guardado, R.string.message_dialog_evento_guardado, R.string.positiveButton,
                            R.string.negativeButton, 1, R.string.boton_neutral_evento_guardado);
            dialog.show(miFragment_Evento.getFragmentManager(), "ClaseDialogGenericaDosTresBotones");
//            dialog.show(getSupportFragmentManager(), "ClaseDialogGenericaDosTresBotones");
        }else{
            DialogFragment dialog =  ClaseDialogEventoGuardado.newInstance
                    (R.string.titulo_dialog_evento_guardado, R.string.message_dialog_evento_guardado_Error, R.string.positiveButton,
                            R.string.negativeButton, 1, R.string.boton_neutral_evento_guardado);
            dialog.show(miFragment_Evento.getFragmentManager(), "ClaseDialogGenericaDosTresBotones");
        }

        //*************************************************************************************************

    }//methodFake_Add_To_List_Of_Favorites

    private void methodFake_Enviar_Mail() {

//        String to = textTo.getText().toString();
//        String subject = textSubject.getText().toString();
//        String message = textMessage.getText().toString();

        String to = "";
        String subject = Class_ConstantsHelper.titulo_De_La_Fiesta;




        //Escribo la fecha de la fiesta
        SimpleDateFormat sfd = new SimpleDateFormat("EEEE dd LLLL yyyy");
//        Class_ConstantsHelper.fecha_Del_Dia = sfd.format(m_Event_Poner_en_favoritos.getStartDate());




        StringBuilder strBuilder = new StringBuilder(m_Event_Poner_en_favoritos.getSortText());
        strBuilder.append("\n");
//        strBuilder.append(Class_ConstantsHelper.fecha_Del_Dia);
        strBuilder.append(sfd.format(m_Event_Poner_en_favoritos.getStartDate()));
        strBuilder.append("\n");
        strBuilder.append("Hora del evento: ");

        DateFormat df2 = new SimpleDateFormat("HH:mm");
//        df2.format(m_Event_Poner_en_favoritos.getStartDate());
        strBuilder.append(df2.format(m_Event_Poner_en_favoritos.getStartDate()));


        strBuilder.append("\n");
        strBuilder.append(m_Event_Poner_en_favoritos.getLongText());
        String message = strBuilder.toString();


        //10 julio 2015, original que solo enviaba e-mail
//        Intent intent_email = new Intent(Intent.ACTION_SEND);
//        intent_email.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
//        intent_email.putExtra(Intent.EXTRA_SUBJECT, subject);
//        intent_email.putExtra(Intent.EXTRA_TEXT, message);
//        //need this to prompts email client only
//        intent_email.setType("message/rfc822");
//        if (intent_email.resolveActivity(miFragment_Evento.getActivity().getPackageManager()) != null) {
//            miFragment_Evento.startActivity(Intent.createChooser(intent_email, "Choose an Email client :"));
//        }


        //10 julio 2015, primera version para usar facebook, twitter, etc
        Intent intent_Generico = new Intent(Intent.ACTION_SEND);
        intent_Generico.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent_Generico.putExtra(Intent.EXTRA_TEXT, message);
        //need this to prompts email client only
        intent_Generico.setType("text/plain");

        if (intent_Generico.resolveActivity(miFragment_Evento.getActivity().getPackageManager()) != null) {
            miFragment_Evento.startActivity(Intent.createChooser(intent_Generico, miFragment_Evento.getResources().getString(R.string.selecciona_una_aplicacion)));

        }
        //**************************************************************************************

    }//Fin de methodFake_Enviar_Mail


    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used by RecyclerView.
     */
    //Original
//    public CustomAdapter_RecyclerView_3(String[] dataSet) {

    //21 Feb 2015 modificado para funcionar con el array list
//    public CustomAdapter_RecyclerView_3(ArrayList<Class_Evento_1> dataSet) {
    //Original
//    public CustomAdapter_RecyclerView_3(ArrayList<Class_Evento_1> dataSet, String intNumeroAdapter) {

    //Le paso el context para poder llamar a la actividad que muestra el detalle

    MiFragment_Evento miFragment_Evento;
    public CustomAdapter_RecyclerView_Evento_Detalle(List<Event> dataSet,
                                                     String intNumeroAdapter,
                                                     OnItemClickListenerEventoDetalle mListener,
                                                     MiFragment_Evento miFragment_Evento,
                                                     Activity_Fragment_SwipeView_EventoDetalle_BackToParent
                                                             activity_Fragment_SwipeView_EventoDetalle_BackToParent) {
//        mDataSet = dataSet;
        List_De_Eventos_Del_Dia = dataSet;
//        TAG = this.getClass().getName();
        //Con simple name por que con name el string es muy largo por que nombra el paquete tambien
        TAG = this.getClass().getSimpleName();
            this.intNumeroAdapter = intNumeroAdapter;
            this.stringNumeroAdapter = TAG +" " + this.intNumeroAdapter +", ";

        this.mListener = mListener;

        //10 julio 2015, para gestionar el contextual action bar de agregar y enviar
        this.miFragment_Evento = miFragment_Evento;

        this.m_Event_Poner_en_favoritos = dataSet.get(0);

        this.activity_Fragment_SwipeView_EventoDetalle_BackToParent = activity_Fragment_SwipeView_EventoDetalle_BackToParent;

    }//Fin del constructor

    Activity_Fragment_SwipeView_EventoDetalle_BackToParent activity_Fragment_SwipeView_EventoDetalle_BackToParent;



    //Uso eete integer para saber que instancia estoy viendo en el log
    private String intNumeroAdapter;
    private String stringNumeroAdapter;

    // BEGIN_INCLUDE(recyclerViewOnCreateViewHolder)
    // Create new views (invoked by the layout manager)
    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
//        View v = LayoutInflater.from(viewGroup.getContext())
//        .inflate(R.layout.fila_info_detail_card_view, viewGroup, false);

        //10 julio 2015: prueba con fila_info_detail_card_view_con_action_bar para poner el contextual
        //action bar al lado del titulo del evento
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fila_info_detail_card_view_con_action_bar, viewGroup, false);

//        NOTA IMPORTANTE: ver selectableItemBackground en fila_info_eventos_1
//        To display visual responses like ripples on screen when a click event is detected add selectableItemBackground
//        resource in the layout (highlighted above).



//        return new ViewHolder(v);
        return new ViewHolder(v);

    }
    // END_INCLUDE(recyclerViewOnCreateViewHolder)

    // BEGIN_INCLUDE(recyclerViewOnBindViewHolder)
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(TAG, stringNumeroAdapter +"Element " + position + " set.");
        }

        //Prueba de uso de loader
        //Hago un chequeo de null, y regreso sin imprimir nada
        if(List_De_Eventos_Del_Dia == null) return;


//        Juan, 26 Mayo 2015: Relleno el ViewHolder con los datos de los dias:

        // Get element from your dataset at this position and replace the contents of the view
        // with that element
//        Por ahora, solo muestro el string con el dia

        viewHolder.getTextView_Titulo().
                setText(List_De_Eventos_Del_Dia.get(position).getSortText());

//        viewHolder.getTextView_Titulo().
//                setText(List_De_Eventos_Del_Dia.get(position).toString());

        //            4 Junio 2015: uso este getTextView_Descripcion para mostrar la hora del evento
//        DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        DateFormat df2 = new SimpleDateFormat("HH:mm");
        viewHolder.getTextView_Descripcion().
//                setText(R.string.hora_del_evento +" " +df2.format(List_De_Eventos_Del_Dia.get(position).getStartDate()));
        setText(R.string.hora_del_evento);
        viewHolder.getTextView_Descripcion().
                append(" " + df2.format(List_De_Eventos_Del_Dia.get(position).getStartDate()));

        viewHolder.getTextView_Descripcion2().
                setText(List_De_Eventos_Del_Dia.get(position).getLongText().trim());




//        viewHolder.getImageView().
//                setImageResource(R.drawable.ic_launcher);


        //Esta imagen no hay que cargarla, lo dejo comentado, 18 agosto 2015
//        viewHolder.getImageView().
//                setImageResource(R.drawable.icon72_2xestamosenfiestas);


        //Voy a usar el metodo method_Descarga_Imagen_De_Internet
//        viewHolder.getImageView_Grande().setImageResource(R.drawable.mercaobarroco);

        //************************************************************************************
        //Juan 18 agosto 2015: presento la imagen como en:
        //http://themakeinfo.com/2015/04/android-retrofit-images-tutorial/
        //Con esta instruccion:
        //Picasso.with(context).load("http://i.imgur.com/DvpvklR.png").into(imageView);
        method_Descarga_Imagen_De_Internet(viewHolder.getImageView_Grande(),
                List_De_Eventos_Del_Dia.get(position));
        //*************************************************************************************

//        Juan, 3 Junio 2015, pongo los onclick aqui (como en el proyecto NavigationDrawer) y no en el constructor del viewHolder
        viewHolder.getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(TAG, "Element " + position + " clicked.");
                }


                //23 Feb 2015: Llamo a la actividad que muestra el detalle del evento
                // Do something in response to button

//                Esta llamada obtiene un objeto Events que tiene todos los datos de ese evento
                mListener.onClickEventoDetalle(List_De_Eventos_Del_Dia.get(position));

            }
        });


        viewHolder.getView().setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(TAG, "Element " + position + " LONG clicked.");
                }
//                    OJO: Si retorno false, entonces se dispara el onClick!!
//                    return false;
//                    Por eso si solo quiero detectar el long click, retorno true
                return true;
            }
        });

        // 10 julio 2015: he pasado todo el manejo de la contextual tool bar a CustomAdapter_RecyclerView_Evento_Detalle
        //como me pidio oscar. Ahora agregar a favoritos y enviar se gestiona en este  adapter.
        method_generar_toolbar_para_contextual_action_mode(viewHolder.getContextualActionBar());

        //Juan 26 agosto 2015: metodo para resaltar un evento que ha sido agregado a la lista de favoritos
        methodResaltarEventoSiEstaEnEventoDetalle(viewHolder, position);
    }//Fin de onBindViewHolder
    // END_INCLUDE(recyclerViewOnBindViewHolder)

    private void methodResaltarEventoSiEstaEnEventoDetalle(ViewHolder viewHolder, final int position) {

        if (List_De_Eventos_Del_Dia.get(position).getAddedToFavourites()) {
            //Cambio el color del texto
            viewHolder.getTextView_Titulo().
                    setTextColor(Color.parseColor("#F06292"));

            //Pongo el texto resaltado en negrita
//            viewHolder.getTextView_Titulo().
//                    setTypeface(null, Typeface.BOLD);

        } else {
            //No hago nada
        }
    }


    public void method_Descarga_Imagen_De_Internet(ImageView imageView, Event event) {
        //Juan 17 agosto 2015: presento la imagen como en:
        //http://themakeinfo.com/2015/04/android-retrofit-images-tutorial/
        //Con esta instruccion:
        //Picasso.with(context).load("http://i.imgur.com/DvpvklR.png").into(imageView);
        //O esta:
        //Picasso.with(getContext()).load(url+flower.getPhoto()).resize(100,100).into(img);

        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(TAG, ": En metodo method_Descarga_Imagen_De_Internet");
        }

        String url = null;

        url = event.getMultimediaElementList().get(0).getMultimediaElementLocation();
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, ": En metodo method_Descarga_Imagen_De_Internet, url: " +url);
        }
        if(!Class_ConstantsHelper.methodStaticChequeaURL(url)){//url invalida
//            if(url == null || url.isEmpty()){
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG, ": En metodo method_Descarga_Imagen_De_Internet: url es NULL o esta VACIO");
            }
            //Oscar quiere que si no hay imagen, se vea mas pequena la cardview
            imageView.setVisibility(View.GONE);
//            imageView.setVisibility(View.INVISIBLE);
        }else{//url es valido, invoca la descarga de la imagen
            //Asi se ven las imagenes deformadas
//            Picasso.with(activity_Fragment_SwipeView_EventoDetalle_BackToParent).load(url).resize(400, 400).into(imageView);


            //Asi se ven bien, aunque algo recortadas, depende del tamaño
            //todas las imagene se ven del mismo tamaño, pero recortadas
            //Entonces un cartel que tenga texto, se puede ver fatal

            //En la version 13.0.10 estaba asi, hasta el 27-6-16
            //Juan 27-6-16, Osacr me pidio que las imagenes se vean con sus proporciones correctas
            //27-6-16: quito esta que muestra la imagen centrada
//            Picasso.with(activity_Fragment_SwipeView_EventoDetalle_BackToParent).load(url).fit().centerCrop().into(imageView);

            //asi se ve bien, la imagen sale entera pero dependiendo de la imagen, saldra mas vertical,
            //O mas horizontal, y cada imagen, de diferente tamaño.
            //O sea, que muestra toda la imagen respetando sus proporciones
            //Juan 27-6-16, Osacr me pidio que las imagenes se vean con sus proporciones correctas
            Picasso.with(activity_Fragment_SwipeView_EventoDetalle_BackToParent).load(url).fit().centerInside().into(imageView);
        }


    }




    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
//        return mDataSet.length;

        if (List_De_Eventos_Del_Dia == null) {
            return 0;
        }

        return List_De_Eventos_Del_Dia.size();
    }

    /**
     * Interface para recibir en Activity_Fragment_FrameLayout_WithNavDrawer_2 el click de la fiesta seleccionada
     */
    public interface OnItemClickListenerEventoDetalle {
        public void onClickEventoDetalle(Event m_Event);
    }

    //Interface de dialogo de evento guardado. No hago nada en estos metodos

    @Override
    public void onDialogPositiveClick_ClaseDialogEventoGuardado(DialogFragment dialog) {

    }

    @Override
    public void onDialogNegativeClick_ClaseDialogEventoGuardado(DialogFragment dialog) {

    }

    @Override
    public void onDialogNeutralClick_ClaseDialogEventoGuardado(DialogFragment dialog) {

    }

    public void methodResaltarEventoFavorito(Event m_Event) {

        //Marca el evento favorito en el array
        methodMarcaElEventoFavoritoEnElArray (m_Event);


//        notifyDataSetChanged();
    }

    public void methodMarcaElEventoFavoritoEnElArray(Event m_Event) {

        //Marca el evento favorito en el array

        for (int i = 0; i < List_De_Eventos_Del_Dia.size(); i++) {

            if (List_De_Eventos_Del_Dia.get(i).getEventId() == m_Event.getEventId()) {
                m_Event.setAddedToFavourites(true);
                //Guardo el m_Event.getEventId() en el array estaico para que lo usen los fragments de
                //MiFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView y puedan redibujar resaltado el
                //evento agregado a favoriros al hacer el back to parent
                Class_ConstantsHelper.arraylist_Eventos_Favoritos.add(m_Event.getEventId());
            }
        }

        notifyDataSetChanged();
    }
}//Fin de la clase
