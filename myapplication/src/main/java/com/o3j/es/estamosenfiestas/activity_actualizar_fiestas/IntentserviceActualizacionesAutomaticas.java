package com.o3j.es.estamosenfiestas.activity_actualizar_fiestas;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;

import com.eef.data.eefExceptions.eefException;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.ClaseChequeaAcceso_A_Internet;

import managers.DownloadInfoFromServerForUpdateManager;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * helper methods.
 */
public class IntentserviceActualizacionesAutomaticas extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    public static final String ACTION_FOO = "com.o3j.es.estamosenfiestas.action.FOO";
    public static final String ACTION_BAZ = "com.o3j.es.estamosenfiestas.action.BAZ";

    // TODO: Rename parameters
    public static final String EXTRA_PARAM1 = "com.o3j.es.estamosenfiestas.extra.PARAM1";
    public static final String EXTRA_PARAM2 = "com.o3j.es.estamosenfiestas.extra.PARAM2";

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionFoo(Context context, String param1, String param2) {
        Intent intent = new Intent(context, IntentserviceActualizacionesAutomaticas.class);
        intent.setAction(ACTION_FOO);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionBaz(Context context, String param1, String param2) {
        Intent intent = new Intent(context, IntentserviceActualizacionesAutomaticas.class);
        intent.setAction(ACTION_BAZ);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    public IntentserviceActualizacionesAutomaticas() {
        super("IntentserviceActualizacionesAutomaticas");
        xxx = this.getClass().getSimpleName();

    }
    private static String xxx;




    @Override
    protected void onHandleIntent(Intent intent) {
        //**********************************************************************************
        // acquire the wake lock como lo explican en android developers
        //para el caso de que el movil este dormido
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "MyWakelockTag");
        wakeLock.acquire();

        //release the wake lock como lo explican en android developers
//        wakeLock.release();
        //**********************************************************************************
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_FOO.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                //Comento handleActionFoo para que no casque debido a la exception "Not yet implemented"
//                handleActionFoo(param1, param2);

                //**********************************************************************************
                // acquire the wake lock como lo explican en android developers
                //para el caso de que el movil este dormido
//                PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
//                PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
//                        "MyWakelockTag");
//                wakeLock.acquire();


//                methodInicializarBroadcastreceivers();
                method_Fuera_Del_On_Create_Chequea_Acceso_A_Internet();

                //release the wake lock como lo explican en android developers
//                wakeLock.release();
                //**********************************************************************************



            } else if (ACTION_BAZ.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                //Comento handleActionFoo para que no casque debido a la exception "Not yet implemented"
//                handleActionBaz(param1, param2);
            }
        }

        //**********************************************************************************
        // acquire the wake lock como lo explican en android developers
        //para el caso de que el movil este dormido
//        PowerManager powerManager = (PowerManager) context.getSystemService(context.POWER_SERVICE);
//        PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
//                "MyWakelockTag");
//        wakeLock.acquire();

        //release the wake lock como lo explican en android developers
        wakeLock.release();
        //**********************************************************************************
    }






    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFoo(String param1, String param2) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionBaz(String param1, String param2) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    //***********************************************************************************
    //***********************************************************************************
    //     21 agosto 2015, Actualizacion
    //     Inicio de todo lo relacionado con los broadcast receivers y progress bar
    //***********************************************************************************
    //***********************************************************************************
    //ID DEL BROADCAST PARA INDICAR EL PROGRESO DE LA DESCARGA DE PROGRAMAS Y EVENTOS
    String broadcast_Progreso_Actualizar = "broadcast_Progreso_Actualizar_Automatico";
    //ID DEL BROADCAST PARA INDICAR EL FIN DE LA DESCARGA DE PROGRAMAS Y EVENTOS
    String broadcast_Fin_De_La_Auctualizacion = "broadcast_Fin_De_La_Auctualizacion_Automatico";



    public void method_Fuera_Del_On_Create_Chequea_Acceso_A_Internet() {
        //*************************************************************************************************
        if(method_Dime_Si_Hay_Red()) {//Chequeamos si hay red
            //Si hay red, sigue ejecutando
            //24 julio 2015, nuevo metodo, ahora usamos retrofit y receivers
            method_Actualiza();

        }else {//NO hay red
            // No hago nada, espera a la siguiente alarma, y si ya hay red, se lanzara
        }
        //*************************************************************************************************
    }



    public void method_Actualiza() {
        //**********************************************************************************
        DownloadInfoFromServerForUpdateManager downloadInfoFromServerForUpdateManager = new DownloadInfoFromServerForUpdateManager();
        downloadInfoFromServerForUpdateManager.initManager(this, broadcast_Progreso_Actualizar, broadcast_Fin_De_La_Auctualizacion);
        //**********************************************************************************

        try {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(xxx, xxx +"En metodo  method_Actualiza dentro del try" );
            }
            //Actualiza la info de las fiestas, recojo el resultado en los broadcast receivers
            downloadInfoFromServerForUpdateManager.updateProgramsFromServer();

        } catch (eefException eefe) {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(xxx, xxx +"En metodo  method_Actualiza: " +"catch, ERROR ACTUALIZANDO FIESTAS: " + eefe.getLocalizedMessage());
            }
            //Muestr dialogo de error
            methodErrorDeActualizacionEn_method_Actualiza();//No hago nada, espera a la siguiente alarma
        }
    }//Fin de method_Actualiza

    public void methodErrorDeActualizacionEn_method_Actualiza() {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, xxx +"En metodo  methodErrorDeActualizacionEn_method_Actualiza " );
        }
    }

    public boolean method_Dime_Si_Hay_Red() {

        //antes de lanzar cualquier servicio web al server, verifico si hay acceso a internet
        ClaseChequeaAcceso_A_Internet claseChequeaAcceso_A_Internet = new ClaseChequeaAcceso_A_Internet(this);
        if(claseChequeaAcceso_A_Internet.isOnline()) {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(xxx, xxx +"En metodo  method_Dime_Si_Hay_Red: SI" );
            }
            return true;
        }else {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(xxx, xxx +"En metodo  method_Dime_Si_Hay_Red: NO" );
            }
            return false;
        }
    }

}
