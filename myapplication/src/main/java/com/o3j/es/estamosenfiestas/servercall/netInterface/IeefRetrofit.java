package com.o3j.es.estamosenfiestas.servercall.netInterface;

import com.o3j.es.estamosenfiestas.servercall.pojos.InfoOrganizerPojo;
import com.o3j.es.estamosenfiestas.servercall.pojos.OrganizerPojo;
import com.o3j.es.estamosenfiestas.servercall.pojos.ProgramEventPojo;
import com.o3j.es.estamosenfiestas.servercall.pojos.ProgramModifiedPojo;
import com.o3j.es.estamosenfiestas.servercall.pojos.ProgramPojo;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by jluis on 23/07/15.
 */
public interface IeefRetrofit {
    @GET("/{idccaa}")
    void getOrganizerByCcAa(@Path("idccaa") Integer idCcaa , Callback<List<OrganizerPojo>> response);

    @GET("/{idprogram}")

    void getprogramEvents(@Path("idprogram") Integer idProgram , Callback<List<ProgramEventPojo>> response);

     @GET("/{idorganizer}")
    void getOrganizerInfo(@Path("idorganizer") Integer idOrganizer, Callback<List<InfoOrganizerPojo>> response);

    @GET("/{idorganizer}")
    void getOrganizerProgramsListInfo(@Path("idorganizer") Integer idOrganizer, Callback<List<ProgramPojo>> response);

    @GET("/{idprogram}")
    void checkProgramModified(@Path("idprogram") Integer idProgram , Callback<List<ProgramModifiedPojo>> response);
}
