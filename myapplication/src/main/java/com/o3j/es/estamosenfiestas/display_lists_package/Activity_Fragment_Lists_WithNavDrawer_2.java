package com.o3j.es.estamosenfiestas.display_lists_package;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.eef.data.dataelements.DayEvents;
import com.eef.data.dataelements.Event;
import com.eef.data.dataelements.Program;
import com.eef.data.eefExceptions.eefException;
import com.eef.data.eeftypes.TypeConfiguration;
import com.eef.data.eeftypes.TypeState;
import com.eef.data.managers.IConfigurationManager;
import com.eef.data.managers.impl.ProgamListManager;
import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.factory.impl.ConfigurationFactory;
import com.o3j.es.estamosenfiestas.nav_drawer_utilities.ClassMenuItemsNavigationDrawer;
import com.o3j.es.estamosenfiestas.nav_drawer_utilities.CustomAdapter_NavigationDrawer;
import com.o3j.es.estamosenfiestas.toolbar_con_tabs_helpers.SlidingTabLayout;

import java.util.ArrayList;
import java.util.List;

//Carga el fragment con loader
public class Activity_Fragment_Lists_WithNavDrawer_2 extends ActionBarActivity implements MiFragment_Con_Loader_3.OnFragmentInteractionListener {
    //Original con fragment sin loader
//    public class Activity_Fragment_Base_2 extends ActionBarActivity implements MiFragment_1.OnFragmentInteractionListener {
    AdaptadorDeSwipeViews_2 adaptadorDeSwipeViews;
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        xxx = this.getClass().getSimpleName();

        setContentView(R.layout.activity_fragment_lists_with_nav_drawer_2);

//        Toolbar toolBar_Actionbar = (Toolbar) findViewById (R.id.activity_my_toolbar);
        Toolbar toolBar_Actionbar = (Toolbar) findViewById(R.id.view);
        //Toolbar will now take on default Action Bar characteristics
        toolBar_Actionbar.setTitle(getResources().getString(R.string.title_main));
        toolBar_Actionbar.setSubtitle(getResources().getString(R.string.sub_titulo_4));
        setSupportActionBar(toolBar_Actionbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Toolbar bottom_toolbar = (Toolbar) findViewById(R.id.bottom_toolbar);
        bottom_toolbar.setLogo(R.drawable.ic_launcher);
        bottom_toolbar.setTitle(getResources().getString(R.string.texto_1));
        bottom_toolbar.setSubtitle(getResources().getString(R.string.texto_2));
        bottom_toolbar.setNavigationIcon(R.drawable.ic_launcher);
        //Toolbar que pongo abajo

        // Set an OnMenuItemClickListener to handle menu item clicks
        bottom_toolbar.setOnMenuItemClickListener(
                new Toolbar.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // Handle the menu item

                        int id = item.getItemId();

                        //noinspection SimplifiableIfStatement
                        if (id == R.id.action_settings) {
                            Toast.makeText(Activity_Fragment_Lists_WithNavDrawer_2.this, "Bottom toolbar pressed: ", Toast.LENGTH_LONG).show();
                            return true;
                        }
                        return true;
                    }
                });


        bottom_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Activity_Fragment_Lists_WithNavDrawer_2.this, "Navigation Icon pressed: ", Toast.LENGTH_LONG).show();
                // Para esta practica, al presionar el navigation icon lo quito.
                bottom_toolbar.setVisibility(View.GONE);
            }
        });

        // Inflate a menu to be displayed in the toolbar
        bottom_toolbar.inflateMenu(R.menu.menu_main);



        //Por ahora utilizo este metodo para jugar con el API de Jose Luis, 21 Mayo 2015
        method_Data_To_Show_In_Fragments();




        // ViewPager and its adapters use support library
        // fragments, so use getSupportFragmentManager.
        adaptadorDeSwipeViews =
                new AdaptadorDeSwipeViews_2(
                        getSupportFragmentManager(), int_Number_Of_Fragments_To_Show, List_Programs);
        mViewPager = (ViewPager) findViewById(R.id.viewpager_1);
        mViewPager.setAdapter(adaptadorDeSwipeViews);

        //Poner tabs del swipe view
        metodoPonerTabs_1(mViewPager);



        //Inicializar el nav drawer, 21 Mayo 2015
        method_Init_NavigationDrawer();

    }//Fin del onCreate

    private void metodoPonerTabs_1(ViewPager mViewPager) {
//        Hecho como en: http://guides.codepath.com/android/Google-Play-Style-Tabs-using-SlidingTabLayout
// Give the SlidingTabLayout the ViewPager
        SlidingTabLayout slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        // Center the tabs in the layout
        slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setViewPager(mViewPager);
        //Colorear el tab
        slidingTabLayout.setBackgroundColor(getResources().getColor(R.color.primary_color));
        // Customize tab indicator color
        slidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.accent_color);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this. 21 Mayo 2015
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }//Codigo del nav drawer


        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(Activity_Fragment_Lists_WithNavDrawer_2.this, "Bottom toolbar pressed: ", Toast.LENGTH_LONG).show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Implementa la interfaz
    public void onFragmentInteraction(String string) {

    }



    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //  Inicio de Codigo relacionado con el navigation drawer, 21 Mayo 2015
    //****************************************************************
    //****************************************************************
    private void method_Init_NavigationDrawer() {

        //****************************************************************
        //   Codigo relacionado con recycler view del navigation drawer
        //****************************************************************


        //Inicializar los datos del array del menu a mostrar en el navigationDrawer
        init_arrayList_NavigationDrawer();

        navigationDrawerRecyclerView = (RecyclerView) findViewById(R.id.left_drawer_recycle_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        navigationDrawerRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        navigationDrawerLayoutManager = new LinearLayoutManager(this);
        navigationDrawerRecyclerView.setLayoutManager(navigationDrawerLayoutManager);
        navigationDrawerRecyclerView.setAdapter(new CustomAdapter_NavigationDrawer(
                arrayListItemsNavigationDrawer, "0", this));

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_1);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        //Este objeto permite que el navigation Drawer interactue correctamente con el action bar (la toolbar en mi caso)
        //Tengo que usar el actionBarDrawerToggle de V7 support por que el de V4 esta deprecated
        actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                drawerLayout,         /* DrawerLayout object */
                //Deprecated en V7, esto era valido en V4
//                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(getResources().getString(R.string.title_activity_main_activity_navigation_drawer));
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()

            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                getActionBar().setTitle(mDrawerTitle);
//                getSupportActionBar().setTitle(getTitle());
                getSupportActionBar().setTitle(getResources().getString(R.string.titulo_navigation_drawer));
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()


            }
        };//Fin de actionBarDrawerToggle

        // Set the drawer toggle as the DrawerListener
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //****************************************************************
        //  FIN Codigo relacionado con recycler view del navigation drawer
        //****************************************************************
    }//Fin de method_Init_NavigationDrawer


    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = drawerLayout.isDrawerOpen(navigationDrawerRecyclerView);
        menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }// Fin de onPrepareOptionsMenu


    //Datos del menu del navigation drawer
    private void init_arrayList_NavigationDrawer() {//Relacionado con recycler view
        arrayListItemsNavigationDrawer = new ArrayList<ClassMenuItemsNavigationDrawer>();
        for (int i = 0; i < DATASET_COUNT; i++) {
            arrayListItemsNavigationDrawer.add(
                    ClassMenuItemsNavigationDrawer.newInstance("Titulo # " + i,
                            "Descripcion #" + i,
                            R.drawable.ic_launcher));
        }
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    //Si estos metodos no se llaman, no aparece la hamburguesa
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    //Variables para el recycler view
    private DrawerLayout drawerLayout;
    private RecyclerView navigationDrawerRecyclerView;
    private RecyclerView.Adapter navigationDrawerAdapter;
    private RecyclerView.LayoutManager navigationDrawerLayoutManager;
    protected ArrayList<ClassMenuItemsNavigationDrawer> arrayListItemsNavigationDrawer = null;
    private static final int DATASET_COUNT = 60;
    //Este objeto permite que el navigation Drawer interactue correctamente con el action bar (la toolbar en mi caso)
    private ActionBarDrawerToggle actionBarDrawerToggle;
    //FIN Variables para el recycler view

    //****************************************************************
    //****************************************************************
    //     FIN del Codigo relacionado con el navigation drawer
    //****************************************************************
    //****************************************************************




    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //  Inicio de Codigo relacionado con el API de JL, 21 Mayo 2015
    //****************************************************************
    //****************************************************************
    private void method_Data_To_Show_In_Fragments() {
//        ESte metodo obtiene datos del api de JL, pero habra que cambiar cosas.

        ConfigurationFactory cmTFactory = new ConfigurationFactory();

        try {
            myCmT = cmTFactory.getConfigurationManager(TypeConfiguration.INSTALLATION_DEFAULT_WITH_AUTONOMOUS_COMMUNITY);

            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(xxx, xxx +" Esta bien configurado [" + myCmT.isRightConfigured() + "]");
                Log.d(xxx, xxx +" El cmt esta configurado[" + myCmT.isRightConfigured() + "] numero AU[" + myCmT.getAutonomousCommunityList().size() + "]");
                Log.d(xxx, xxx +" La comunidad Autonoma Configurada[" + myCmT.getAutonomousCommunityConfigured().getAutonomousCommunityName()
                        + "] y el pueblo configurado[" + myCmT.getEventPlannerConfigured().getName() + "]");
            }

            ProgamListManager pmT = new ProgamListManager();
            pmT.loadData(myCmT.getAutonomousCommunityConfigured(),myCmT.getEventPlannerConfigured());
            if(pmT.existCurrentProgram()){
                Program p = pmT.getCurrentProgram();

                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(xxx, xxx +"HAY PROGRAMA EN MARCHA ["+p.getProgramSortDescription()+"]");

                }
                //Obtener la lista de eventos del programa por dias
                List<DayEvents> ltd = p.getEventByDay();
                //Obtener la lista de eventos completa
                List<Event> ltf =p.getFullList();
            }



            if(pmT.existCurrentProgram() || pmT.existFuturetPrograms() || pmT.existPastPrograms()) {
                 List_Programs = pmT.getFullProgramList();


                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(xxx, "HAY  [" + List_Programs.size() + "]" +" PROGRAMAS EN MARCHA");
                    for (int i=0; i<List_Programs.size(); i++){
                        Log.d(xxx, xxx +"Codigo del programa numero  [" +i + ": "
                                +List_Programs.get(i).getState().getCode() + "]" +" PROGRAMAS EN MARCHA");
                        if(List_Programs.get(i).getState().getCode().equals(TypeState.ENDED)){
                            int_Number_Of_Fragments_To_Show++;
                            List_Programs.remove(i);
                        }

                    }
                }

            }


        }
        catch(eefException eef)
        {
            Log.e("onCreate","Error inciando la conficuracion");
        }
        catch (Exception ex){
            Log.e("onCreate","Excepotion no eef inciando la configuracion");
        }


    }//Fin de method_Data_To_Show_In_Fragments
    IConfigurationManager myCmT=null;
    private static String xxx;
    List<Program> List_Programs;
    int int_Number_Of_Fragments_To_Show = 0;


    //****************************************************************
    //****************************************************************
    //     FIN del Codigo relacionado con el API de JL
    //****************************************************************
    //****************************************************************
}//Fin de la clase
