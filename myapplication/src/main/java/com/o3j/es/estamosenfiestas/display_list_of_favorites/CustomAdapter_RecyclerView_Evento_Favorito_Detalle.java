/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.o3j.es.estamosenfiestas.display_list_of_favorites;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eef.data.dataelements.Event;
import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper;
import com.o3j.es.estamosenfiestas.display_frame_layout.Clase_Descarga_Imagen_De_Internet_De_Un_Evento;
import com.o3j.es.estamosenfiestas.display_frame_layout.Class_Modelo_De_Datos_Lista_Favoritos;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Provide views to RecyclerView with data from dataSet.
 */
//Juan, 4 Junio 2015. Uso este adpater para gestionar el evento como una lista con recycle view,
//    y mantener el mismo manejo que para la lista de fiestas, dias de la fiesta y lista de eventos
//    Se muestran los detalles del evento en la lista arrayList_Lista_De_Favoritos
public class CustomAdapter_RecyclerView_Evento_Favorito_Detalle extends RecyclerView.Adapter<CustomAdapter_RecyclerView_Evento_Favorito_Detalle.ViewHolder> {
//    private static final String TAG = "CustomAdapter";
    private static  String TAG;



//    private static FragmentActivity actividad;


    //Original
//    private String[] mDataSet;


    private List<Event> arrayList_Lista_De_Favoritos; //Solo tiene el evento seleccionado con sus detalles

    private List<Class_Modelo_De_Datos_Lista_Favoritos> list_De_Eventos_Favoritos;

    private int int_Evento_Favorito_Seleccionado;


    //    Agrego la interface para recoger los clicks en la actividad
    private OnItemClickListenerEventoFavoritoDetalle mListener;

    // BEGIN_INCLUDE(recyclerViewSampleViewHolder)
    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */


    public static class ViewHolder extends RecyclerView.ViewHolder {
        //Los nombres como en Class_Evento_1
        private final TextView textView_Titulo;
        private final TextView textView_Descripcion;
        private final TextView textView_Descripcion2;
        private final ImageView imageView;
        private final ImageView imageViewGrande;
        private final View m_View;



        public TextView getTextView_Titulo() {

            return textView_Titulo;
        }

        public TextView getTextView_Descripcion() {

            return textView_Descripcion;
        }

        public TextView getTextView_Descripcion2() {

            return textView_Descripcion2;
        }

        public ImageView getImageView() {
            return imageView;
        }

        public ImageView getImageView_Grande() {
            return imageViewGrande;
        }


        public View getView() {
            return m_View;
        }



        public ViewHolder(View v) {
            super(v);
            // Define click listener for the ViewHolder's View.


//            OJO: ver en http://www.truiton.com/2015/02/android-recyclerview-tutorial/
//            como poner el listener en el fragment

//        Juan, 3 Junio 2015, pongo los onclick en onBindViewHolder
//        (como en el proyecto NavigationDrawer) y no en el constructor del viewHolder
//            Comento estos que son los originales

//            v.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
//                        Log.d(TAG, "Element " + getPosition() + " clicked.");
//                    }
//
//
//                    //23 Feb 2015: Llamo a la actividad que muestra el detalle del evento
//                    // Do something in response to button
//
//
//                }
//            });


            // Juan, 26 Mayo 2015: Solo detecto el click, por ahora
//            v.setOnLongClickListener(new View.OnLongClickListener(){
//
//                @Override
//                public boolean onLongClick(View v) {
//                    if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
//                        Log.d(TAG, "Element " + getPosition() + " LONG clicked.");
//                    }
////                    OJO: Si retorno false, entonces se dispara el onClick!!
////                    return false;
////                    Por eso si solo quiero detectar el long click, retorno true
//                    return true;
//                }
//            });



            m_View = v;
            textView_Titulo = (TextView) v.findViewById(R.id.textView6);
            textView_Descripcion = (TextView) v.findViewById(R.id.textView7);
            textView_Descripcion2 = (TextView) v.findViewById(R.id.textView8);
            imageView = (ImageView) v.findViewById(R.id.imageView);
            imageViewGrande = (ImageView) v.findViewById(R.id.imageView_foto_Grande);


        }

    }//Fin de la clase ViewHolder
    // END_INCLUDE(recyclerViewSampleViewHolder)


    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used by RecyclerView.
     */
    //Original
//    public CustomAdapter_RecyclerView_3(String[] dataSet) {

    //21 Feb 2015 modificado para funcionar con el array list
//    public CustomAdapter_RecyclerView_3(ArrayList<Class_Evento_1> dataSet) {
    //Original
//    public CustomAdapter_RecyclerView_3(ArrayList<Class_Evento_1> dataSet, String intNumeroAdapter) {

    //Le paso el context para poder llamar a la actividad que muestra el detalle
    public CustomAdapter_RecyclerView_Evento_Favorito_Detalle(List<Event> dataSet,
                                                              String intNumeroAdapter,
                                                              OnItemClickListenerEventoFavoritoDetalle mListener,
                                                              List<Class_Modelo_De_Datos_Lista_Favoritos> list_De_Eventos_Favoritos,
                                                              int int_Evento_Favorito_Seleccionado,
                                                              Context context) {
//        mDataSet = dataSet;
        arrayList_Lista_De_Favoritos = dataSet;
        this.list_De_Eventos_Favoritos = list_De_Eventos_Favoritos;
        this.int_Evento_Favorito_Seleccionado = int_Evento_Favorito_Seleccionado;
//        TAG = this.getClass().getName();
        //Con simple name por que con name el string es muy largo por que nombra el paquete tambien
        TAG = this.getClass().getSimpleName();
            this.intNumeroAdapter = intNumeroAdapter;
            this.stringNumeroAdapter = TAG +" " + this.intNumeroAdapter +", ";

        this.mListener = mListener;

        this.context = context;
    }//Fin del constructor


    //Uso eete integer para saber que instancia estoy viendo en el log
    private String intNumeroAdapter;
    private String stringNumeroAdapter;
    private Context context;

    // BEGIN_INCLUDE(recyclerViewOnCreateViewHolder)
    // Create new views (invoked by the layout manager)
    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
//                .inflate(R.layout.fila_info_eventos_1, viewGroup, false);
        .inflate(R.layout.fila_info_detail_card_view, viewGroup, false);

//        NOTA IMPORTANTE: ver selectableItemBackground en fila_info_eventos_1
//        To display visual responses like ripples on screen when a click event is detected add selectableItemBackground
//        resource in the layout (highlighted above).



//        return new ViewHolder(v);
        return new ViewHolder(v);

    }
    // END_INCLUDE(recyclerViewOnCreateViewHolder)

    // BEGIN_INCLUDE(recyclerViewOnBindViewHolder)
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(TAG, stringNumeroAdapter +"Element " + position + " set.");
        }

        //Prueba de uso de loader
        //Hago un chequeo de null, y regreso sin imprimir nada
        if(arrayList_Lista_De_Favoritos == null) return;


//        Juan, 26 Mayo 2015: Relleno el ViewHolder con los datos de los dias:

        // Get element from your dataset at this position and replace the contents of the view
        // with that element
//        Por ahora, solo muestro el string con el dia

        viewHolder.getTextView_Titulo().
                setText(arrayList_Lista_De_Favoritos.get(position).getSortText());

//        viewHolder.getTextView_Titulo().
//                setText(arrayList_Lista_De_Favoritos.get(position).toString());

        //            4 Junio 2015: uso este getTextView_Descripcion para mostrar la hora del evento
//        DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        DateFormat df2 = new SimpleDateFormat("HH:mm");
        viewHolder.getTextView_Descripcion().
//                setText(R.string.hora_del_evento +" " +df2.format(arrayList_Lista_De_Favoritos.get(position).getStartDate()));
        setText(R.string.hora_del_evento);
        viewHolder.getTextView_Descripcion().
                append(" " + df2.format(arrayList_Lista_De_Favoritos.get(position).getStartDate()));

        viewHolder.getTextView_Descripcion2().
                setText(arrayList_Lista_De_Favoritos.get(position).getLongText().trim());


//        viewHolder.getImageView().
//                setImageResource(R.drawable.ic_launcher);

        viewHolder.getImageView().
                setImageResource(R.drawable.icon72_2xestamosenfiestas);
//        No muestro esta imagen
        viewHolder.getImageView().setVisibility(View.GONE);


//        viewHolder.getImageView_Grande().
//                setImageResource(R.drawable.mercaobarroco);
//        viewHolder.getImageView_Grande().setVisibility(View.VISIBLE);

        //Descargo la imagen con Picasso
        Clase_Descarga_Imagen_De_Internet_De_Un_Evento clase_descarga_imagen_de_internet =
                Clase_Descarga_Imagen_De_Internet_De_Un_Evento.newInstance(viewHolder.getImageView_Grande(),
                        arrayList_Lista_De_Favoritos.get(position),
                        context);

        clase_descarga_imagen_de_internet.method_Descarga_Imagen_De_Internet();

//        Juan, 3 Junio 2015, pongo los onclick aqui (como en el proyecto NavigationDrawer) y no en el constructor del viewHolder
        viewHolder.getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(TAG, "Element " + position + " clicked.");
                }


                //23 Feb 2015: Llamo a la actividad que muestra el detalle del evento
                // Do something in response to button

//                Esta llamada obtiene un objeto Events que tiene todos los datos de ese evento
                mListener.onClickEventoFavoritoDetalle(arrayList_Lista_De_Favoritos.get(position));

            }
        });


        viewHolder.getView().setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(TAG, "Element " + position + " LONG clicked.");
                }
//                    OJO: Si retorno false, entonces se dispara el onClick!!
//                    return false;
//                    Por eso si solo quiero detectar el long click, retorno true
                return true;
            }
        });
    }
    // END_INCLUDE(recyclerViewOnBindViewHolder)

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
//        return mDataSet.length;

        if (arrayList_Lista_De_Favoritos == null) {
            return 0;
        }

        return arrayList_Lista_De_Favoritos.size();
    }

    /**
     * Interface para recibir en Activity_Fragment_FrameLayout_WithNavDrawer_2 el click de la fiesta seleccionada
     */
    public interface OnItemClickListenerEventoFavoritoDetalle {
        public void onClickEventoFavoritoDetalle(Event m_Event);
    }
}//Fin de la clase
