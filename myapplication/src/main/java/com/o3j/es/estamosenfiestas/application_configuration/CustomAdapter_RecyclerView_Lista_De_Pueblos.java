/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.o3j.es.estamosenfiestas.application_configuration;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eef.data.dataelements.EventPlanner;
import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;


public class CustomAdapter_RecyclerView_Lista_De_Pueblos extends RecyclerView.Adapter<CustomAdapter_RecyclerView_Lista_De_Pueblos.ViewHolder> {
//    private static final String TAG = "CustomAdapter";
    private static  String TAG;



//    private static FragmentActivity actividad;


    //Original
//    private String[] mDataSet;

    //21 Feb 2015 modificado para funcionar con el array list
//    private ArrayList<Class_Evento_1> arrayList_De_Pueblos;
    private static  List<EventPlanner> arrayList_De_Pueblos;

//    Agrego la interface para recoger los clicks en la actividad
    private OnItemClickListenerPuebloSeleccionado mListener;

    // BEGIN_INCLUDE(recyclerViewSampleViewHolder)
    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */


    public static class ViewHolder extends RecyclerView.ViewHolder {
        //Los nombres como en Class_Evento_1
        private final TextView textView_Titulo;
        private final TextView textView_Descripcion;
        private final TextView textView_Descripcion2;
        private final ImageView imageView;
        private final View m_View;



        public TextView getTextView_Titulo() {

            return textView_Titulo;
        }

        public TextView getTextView_Descripcion() {

            return textView_Descripcion;
        }

        public TextView getTextView_Descripcion2() {

            return textView_Descripcion2;
        }

        public ImageView getImageView() {
            return imageView;
        }

        public View getView() {
            return m_View;
        }



        public ViewHolder(View v) {
            super(v);
            // Define click listener for the ViewHolder's View.


//            OJO: ver en http://www.truiton.com/2015/02/android-recyclerview-tutorial/
//            como poner el listener en el fragment

//        Juan, 3 Junio 2015, pongo los onclick en onBindViewHolder
//        (como en el proyecto NavigationDrawer) y no en el constructor del viewHolder
//            Comento estos que son los originales

//            v.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
//                        Log.d(TAG, "Element " + getPosition() + " clicked.");
//                    }
//
//
//                    //23 Feb 2015: Llamo a la actividad que muestra el detalle del evento
//                    // Do something in response to button
//
//
//                }
//            });


            // Juan, 26 Mayo 2015: Solo detecto el click, por ahora
//            v.setOnLongClickListener(new View.OnLongClickListener(){
//
//                @Override
//                public boolean onLongClick(View v) {
//                    if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
//                        Log.d(TAG, "Element " + getPosition() + " LONG clicked.");
//                    }
////                    OJO: Si retorno false, entonces se dispara el onClick!!
////                    return false;
////                    Por eso si solo quiero detectar el long click, retorno true
//                    return true;
//                }
//            });



            m_View = v;
            textView_Titulo = (TextView) v.findViewById(R.id.textView6);
            textView_Descripcion = (TextView) v.findViewById(R.id.textView7);
            textView_Descripcion2 = (TextView) v.findViewById(R.id.textView8);
            imageView = (ImageView) v.findViewById(R.id.imageView);

//            Juan, 26 Mayo 2015: Este adapter solo muestra la fecha que esta en el textView_Titulo
//            y las fechas de inicio y fin
//            El resto de controles los oculto por ahora
            textView_Descripcion.setVisibility(View.GONE);
            textView_Descripcion2.setVisibility(View.GONE);
//            imageView.setVisibility(View.GONE);
        }

    }//Fin de la clase ViewHolder
    // END_INCLUDE(recyclerViewSampleViewHolder)


    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used by RecyclerView.
     */
    //Original
//    public CustomAdapter_RecyclerView_3(String[] dataSet) {

    //21 Feb 2015 modificado para funcionar con el array list
//    public CustomAdapter_RecyclerView_3(ArrayList<Class_Evento_1> dataSet) {
    //Original
//    public CustomAdapter_RecyclerView_3(ArrayList<Class_Evento_1> dataSet, String intNumeroAdapter) {

    //Le paso el context para poder llamar a la actividad que muestra el detalle
    public CustomAdapter_RecyclerView_Lista_De_Pueblos(List<EventPlanner> dataSet,
                                                       String intNumeroAdapter,
                                                       OnItemClickListenerPuebloSeleccionado mListener) {
//        mDataSet = dataSet;
        arrayList_De_Pueblos = dataSet;
//        TAG = this.getClass().getName();
        //Con simple name por que con name el string es muy largo por que nombra el paquete tambien
        TAG = this.getClass().getSimpleName();
            this.intNumeroAdapter = intNumeroAdapter;
            this.stringNumeroAdapter = TAG +" " + this.intNumeroAdapter +", ";

        this.mListener = mListener;

        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(TAG,  "estoy en el constructor ");
        }
    }//Fin del constructor


    //Uso eete integer para saber que instancia estoy viendo en el log
    private String intNumeroAdapter;
    private String stringNumeroAdapter;

    // BEGIN_INCLUDE(recyclerViewOnCreateViewHolder)
    // Create new views (invoked by the layout manager)
    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
//                .inflate(R.layout.text_row_item, viewGroup, false);
                  .inflate(R.layout.fila_info_eventos_1, viewGroup, false);

//        NOTA IMPORTANTE: ver selectableItemBackground en fila_info_eventos_1
//        To display visual responses like ripples on screen when a click event is detected add selectableItemBackground
//        resource in the layout (highlighted above).



//        return new ViewHolder(v);
        return new ViewHolder(v);

    }
    // END_INCLUDE(recyclerViewOnCreateViewHolder)

    // BEGIN_INCLUDE(recyclerViewOnBindViewHolder)
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(TAG, stringNumeroAdapter +"Element " + position + " set.");
        }

        //Prueba de uso de loader
        //Hago un chequeo de null, y regreso sin imprimir nada
        if(arrayList_De_Pueblos == null) return;


//        Juan, 26 Mayo 2015: Relleno el ViewHolder con los datos de los dias:

        // Get element from your dataset at this position and replace the contents of the view
        // with that element
//        Por ahora, solo muestro el string con el nombre de la fiesta

//        viewHolder.getTextView_Titulo().setText(arrayList_De_Pueblos.get(position).getSortDescription());
        viewHolder.getTextView_Titulo().setText(arrayList_De_Pueblos.get(position).getName());

        //Este metodo no lo uso en este adapter
//        method_Insertar_Fechas_De_La_Fiesta(viewHolder, position );

//        viewHolder.getTextView_Descripcion().
//                setText(arrayList_De_Pueblos.get(position).getDescripcion());
//        viewHolder.getTextView_Descripcion2().
//                setText(arrayList_De_Pueblos.get(position).getDescripcion2());
        viewHolder.getImageView().
                setImageResource(R.drawable.ic_launcher);

//        Juan, 3 Junio 2015, pongo los onclick aqui (como en el proyecto NavigationDrawer) y no en el constructor del viewHolder
        viewHolder.getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(TAG, "Element " + position + " clicked.");
                }


                //23 Feb 2015: Llamo a la actividad que muestra el detalle del evento
                // Do something in response to button








                mListener.onClickPuebloSeleccionado(arrayList_De_Pueblos.get(position));


            }
        });


        viewHolder.getView().setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(TAG, "Element " + position + " LONG clicked.");
                }
//                    OJO: Si retorno false, entonces se dispara el onClick!!
//                    return false;
//                    Por eso si solo quiero detectar el long click, retorno true
                return true;
            }
        });
    }
    // END_INCLUDE(recyclerViewOnBindViewHolder)

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
//        return mDataSet.length;

        if (arrayList_De_Pueblos == null) {
            return 0;
        }

        return arrayList_De_Pueblos.size();
    }

    /**
     * Interface para recibir en Activity_Fragment_FrameLayout_WithNavDrawer_2 el click de la fiesta seleccionada
     */
    public interface OnItemClickListenerPuebloSeleccionado {
        public void onClickPuebloSeleccionado(EventPlanner m_EventPlanner);
    }


    // Pone las fechas de inicio y fin de las fiestas
    public void method_Insertar_Fechas_De_La_Fiesta(ViewHolder viewHolder, int position) {

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        DateFormat df3 = new SimpleDateFormat("dd-MMM-yyyy");
        DateFormat df4 = new SimpleDateFormat("MMMM dd, yyyy");
        DateFormat df5 = new SimpleDateFormat("E, MMM dd yyyy");
        DateFormat df6 = new SimpleDateFormat("E, MMM dd yyyy HH:mm:ss");


//        viewHolder.getTextView_Descripcion().
//                setText(df4.format(arrayList_De_Pueblos.get(position).getProgramStartDate()) +" a ");
//
//        viewHolder.getTextView_Descripcion().
//                append(df4.format(arrayList_De_Pueblos.get(position).getProgramEndDate()));
    }
}//Fin de la clase
