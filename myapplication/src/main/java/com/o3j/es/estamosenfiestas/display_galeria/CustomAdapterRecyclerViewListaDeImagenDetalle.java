/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.o3j.es.estamosenfiestas.display_galeria;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.eef.data.dataelements.Event;
import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Provide views to RecyclerView with data from mDataSet.
 */
//creado el 21 feb 2014  para nuevo layout fila_info_eventos
public class CustomAdapterRecyclerViewListaDeImagenDetalle extends RecyclerView.Adapter<CustomAdapterRecyclerViewListaDeImagenDetalle.ViewHolder> {
//    private static final String TAG = "CustomAdapter";
    private static  String TAG;



//    private static FragmentActivity actividad;


    //Original
//    private String[] mDataSet;


    private List<Event> List_De_Eventos;

//    Agrego la interface para recoger los clicks en la actividad
    private OnItemClickListenerImagenSeleccionada mListener;

    // BEGIN_INCLUDE(recyclerViewSampleViewHolder)
    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */


    public static class ViewHolder extends RecyclerView.ViewHolder {
        //Los nombres como en Class_Evento_1
//        private final TextView textView_Titulo;
//        private final TextView textView_Descripcion;
//        private final TextView textView_Descripcion2;
        private final ImageView imageView;
        private final View m_View;

//        4 Junio 2015: uso este textview para mostrar la hora del evento
//        private final TextView textView_Hora_Del_Evento;


//        public TextView getTextView_Titulo() {
//
//            return textView_Titulo;
//        }
//
//        public TextView getTextView_Descripcion() {
//
//            return textView_Descripcion;
//        }
//
//        public TextView getTextView_Descripcion2() {
//
//            return textView_Descripcion2;
//        }
//
//        public TextView getTextView_Hora_Del_Evento() {
//            return textView_Hora_Del_Evento;
//        }


        public ImageView getImageView() {
            return imageView;
        }

        public View getView() {
            return m_View;
        }




        public ViewHolder(View v) {
            super(v);


            m_View = v;


//            textView_Titulo = (TextView) v.findViewById(R.id.textView6);
//            textView_Descripcion = (TextView) v.findViewById(R.id.textView7);
//            textView_Descripcion2 = (TextView) v.findViewById(R.id.textView8);


            imageView = (ImageView) v.findViewById(R.id.imageView);
            imageView.setVisibility(View.VISIBLE);

//            Juan, 26 Mayo 2015: Este adapter solo muestra la fecha que esta en el textView_Titulo
//            El resto de controles los oculto por ahora
//            textView_Titulo.setVisibility(View.GONE);
//            textView_Descripcion.setVisibility(View.GONE);
//            textView_Descripcion2.setVisibility(View.GONE);
//            imageView.setVisibility(View.GONE);

//            4 Junio 2015: uso este textview para mostrar la hora del evento
//            textView_Hora_Del_Evento = (TextView) v.findViewById(R.id.textView_hora_del_evento);
//            textView_Hora_Del_Evento.setVisibility(View.GONE);

        }

    }//Fin de la clase ViewHolder
    // END_INCLUDE(recyclerViewSampleViewHolder)



    //3 oct 2015, nuevo constructor 2 para recibir el fragment que llama para podel resaltar el evento
    //agregado a favoritos
    FragmentMuestraImagenDetalleConRecycleView miFragmentMuestraGaleriaConRecycleView;
    public CustomAdapterRecyclerViewListaDeImagenDetalle(List<Event> dataSet,
                                                         String intNumeroAdapter,
                                                         OnItemClickListenerImagenSeleccionada mListener,
                                                         FragmentMuestraImagenDetalleConRecycleView miFragmentMuestraGaleriaConRecycleView
    ) {
//        mDataSet = dataSet;
        List_De_Eventos = dataSet;
//        TAG = this.getClass().getName();
        //Con simple name por que con name el string es muy largo por que nombra el paquete tambien
        TAG = this.getClass().getSimpleName();
        this.intNumeroAdapter = intNumeroAdapter;
        this.stringNumeroAdapter = TAG +" " + this.intNumeroAdapter +", ";

        this.mListener = mListener;

        this.miFragmentMuestraGaleriaConRecycleView = miFragmentMuestraGaleriaConRecycleView;
    }//Fin del constructor 2

    //Uso eete integer para saber que instancia estoy viendo en el log
    private String intNumeroAdapter;
    private String stringNumeroAdapter;

    // BEGIN_INCLUDE(recyclerViewOnCreateViewHolder)
    // Create new views (invoked by the layout manager)
    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
//                .inflate(R.layout.text_row_item, viewGroup, false);
//                .inflate(R.layout.fila_info_eventos_1, viewGroup, false);
//                .inflate(R.layout.galeria_de_imagenes, viewGroup, false);
//                .inflate(R.layout.imagen_detalle, viewGroup, false);
        .inflate(R.layout.imagen_detalle_2, viewGroup, false);

//        NOTA IMPORTANTE: ver selectableItemBackground en fila_info_eventos_1
//        To display visual responses like ripples on screen when a click event is detected add selectableItemBackground
//        resource in the layout (highlighted above).



//        return new ViewHolder(v);
        return new ViewHolder(v);

    }
    // END_INCLUDE(recyclerViewOnCreateViewHolder)

    // BEGIN_INCLUDE(recyclerViewOnBindViewHolder)
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder( ViewHolder viewHolder, final int position) {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(TAG, stringNumeroAdapter +"Element " + position + " set.");
        }
        //Hago un chequeo de null, y regreso sin imprimir nada
        if(List_De_Eventos == null) return;


//        viewHolder.getTextView_Titulo().
//                setText(List_De_Eventos.get(position).getSortText());

        viewHolder.getImageView().
                setImageResource(R.drawable.ic_launcher);

        method_Descarga_Imagen_De_Internet(viewHolder.getImageView(),
                List_De_Eventos.get(position));

        //            4 Junio 2015: uso este textview para mostrar la hora del evento
//        DateFormat df2 = new SimpleDateFormat("HH:mm");
//
//        viewHolder.getTextView_Hora_Del_Evento().
//                setText(df2.format(List_De_Eventos.get(position).getStartDate()));
//
//        viewHolder.getTextView_Hora_Del_Evento().
//                append("  ");//Hago esto para despegar la hora del texto

//        Juan, 3 Junio 2015, pongo los onclick aqui (como en el proyecto NavigationDrawer) y no en el constructor del viewHolder
        viewHolder.getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(TAG, "Element " + position + " clicked.");
                }


                //23 Feb 2015: Llamo a la actividad que muestra el detalle del evento
                // Do something in response to button

//                Esta llamada obtiene un objeto Events que tiene todos los datos de ese evento
                mListener.onClickImagen(List_De_Eventos.get(position));

            }
        });


        int int_Id_De_La_View_Clickada = viewHolder.getView().getId();

        viewHolder.getView().setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(TAG, "Element " + position + " LONG clicked.");
                }


//                    OJO: Si retorno false, entonces se dispara el onClick!!
//                    return false;
//                    Por eso si solo quiero detectar el long click, retorno true
                return true;
            }
        });

        //Juan 26 agosto 2015: metodo para resaltar un evento que ha sido agregado a la lista de favoritos

    }
    // END_INCLUDE(recyclerViewOnBindViewHolder)

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
//        return mDataSet.length;

        if (List_De_Eventos == null) {
            return 0;
        }

        return List_De_Eventos.size();
    }

    /**
     * Interface para recibir en Activity_Fragment_FrameLayout_WithNavDrawer_2 el click de la fiesta seleccionada
     */
    public interface OnItemClickListenerImagenSeleccionada {
//        public interface OnItemClickListenerEventoSeleccionado {

        public void onClickImagen(Event m_Event);

        public boolean onLongClickImagen
                (Event m_Event, View m_View_Del_Evento);

    }


    public void method_Descarga_Imagen_De_Internet(ImageView imageView, Event event) {
        //Juan 17 agosto 2015: presento la imagen como en:
        //http://themakeinfo.com/2015/04/android-retrofit-images-tutorial/
        //Con esta instruccion:
        //Picasso.with(context).load("http://i.imgur.com/DvpvklR.png").into(imageView);
        //O esta:
        //Picasso.with(getContext()).load(url+flower.getPhoto()).resize(100,100).into(img);

        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(TAG, ": En metodo method_Descarga_Imagen_De_Internet");
        }

        String url = null;

        url = event.getMultimediaElementList().get(0).getMultimediaElementLocation();
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, ": En metodo method_Descarga_Imagen_De_Internet, url: " +url);
        }
        if(!Class_ConstantsHelper.methodStaticChequeaURL(url)){//url invalida
//            if(url == null || url.isEmpty()){
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG, ": En metodo method_Descarga_Imagen_De_Internet: url es NULL o esta VACIO");
            }
            //Oscar quiere que si no hay imagen, se vea mas pequena la cardview
            imageView.setVisibility(View.GONE);
//            imageView.setVisibility(View.INVISIBLE);
        }else{//url es valido, invoca la descarga de la imagen
            //Asi se ven las imagenes deformadas
//            Picasso.with(activity_Fragment_SwipeView_EventoDetalle_BackToParent).load(url).resize(400, 400).into(imageView);
            //Asi se ven bien, aunque algo recortadas, depende del tamaño
            //todas las imagene se ven del mismo tamaño, pero recortadas
            //Entonces un cartel que tenga texto, se puede ver fatal
//            Picasso.with(miFragmentMuestraGaleriaConRecycleView.getActivity()).load(url).fit().centerCrop().into(imageView);

            //asi se ve bien, la imagen sale entera pero dependiendo de la imagen, saldra mas vertical,
            //O mas horizontal, y cada imagen, de diferente tamaño.
            //O sea, que muestra toda la imagen respetando sus proporciones
            Picasso.with(miFragmentMuestraGaleriaConRecycleView.getActivity()).load(url).fit().centerInside().into(imageView);
        }


    }


}//Fin de la clase
