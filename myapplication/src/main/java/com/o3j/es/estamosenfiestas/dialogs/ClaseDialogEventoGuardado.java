package com.o3j.es.estamosenfiestas.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

/**
 * Created by Juan on 15/08/2015.
 */
public class ClaseDialogEventoGuardado extends DialogFragment {
    int title;
    int description;
    int positiveButton;
    int negativeButton;
    int numero_De_Botones;
    int boton_neutral;

    public static ClaseDialogEventoGuardado newInstance(int title, int description, int positiveButton, int negativeButton,
                                                            int numero_De_Botones, int boton_neutral) {
        ClaseDialogEventoGuardado frag = new ClaseDialogEventoGuardado();
        Bundle args = new Bundle();
        args.putInt("title", title);
        args.putInt("description", description);
        args.putInt("positiveButton", positiveButton);
        args.putInt("negativeButton", negativeButton);
        args.putInt("numero_De_Botones", numero_De_Botones);
        args.putInt("boton_neutral", boton_neutral);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        title = getArguments().getInt("title");
        description = getArguments().getInt("description");
        positiveButton = getArguments().getInt("positiveButton");
        negativeButton = getArguments().getInt("negativeButton");
        numero_De_Botones = getArguments().getInt("numero_De_Botones");
        boton_neutral = getArguments().getInt("boton_neutral");
        // Use the Builder class for convenient dialog construction

        AlertDialog.Builder builder = null;

        if(numero_De_Botones == 1) {
            builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(description)
                    .setTitle(title)
                    .setNeutralButton(boton_neutral, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // Send the negative button event back to the host activity
                            mListener.onDialogNeutralClick_ClaseDialogEventoGuardado
                                    (ClaseDialogEventoGuardado.this);
                        }
                    });
            // Create the AlertDialog object and return it
//            return builder.create();
        }

        if(numero_De_Botones == 2) {
            builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(description)
                    .setTitle(title)
                    .setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // Send the positive button event back to the host activity
                            mListener.onDialogPositiveClick_ClaseDialogEventoGuardado
                                    (ClaseDialogEventoGuardado.this);                        }
                    })
                    .setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // Send the negative button event back to the host activity
                            mListener.onDialogNegativeClick_ClaseDialogEventoGuardado
                                    (ClaseDialogEventoGuardado.this);                        }
                    });
            // Create the AlertDialog object and return it
//            return builder.create();
        }

        if(numero_De_Botones == 3) {
            builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(description)
                    .setTitle(title)
                    .setNeutralButton(boton_neutral, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // Send the negative button event back to the host activity
                            mListener.onDialogNeutralClick_ClaseDialogEventoGuardado
                                    (ClaseDialogEventoGuardado.this);
                        }
                    })
                    .setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // Send the positive button event back to the host activity
                            mListener.onDialogPositiveClick_ClaseDialogEventoGuardado
                                    (ClaseDialogEventoGuardado.this);
                        }
                    })
                    .setNegativeButton(negativeButton, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // Send the negative button event back to the host activity
                            mListener.onDialogNegativeClick_ClaseDialogEventoGuardado
                                    (ClaseDialogEventoGuardado.this);
                        }
                    });
            // Create the AlertDialog object and return it
//            return builder.create();
        }
        return builder.create();

    }

    //******************************************************************************************************

    //Interfaces y attach dce la activity que llama

    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface InterfaceClaseDialogEventoGuardado {
        public void onDialogPositiveClick_ClaseDialogEventoGuardado(DialogFragment dialog);
        public void onDialogNegativeClick_ClaseDialogEventoGuardado(DialogFragment dialog);
        public void onDialogNeutralClick_ClaseDialogEventoGuardado(DialogFragment dialog);
    }

    // Use this instance of the interface to deliver action events
    InterfaceClaseDialogEventoGuardado mListener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (InterfaceClaseDialogEventoGuardado) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    //******************************************************************************************************

}
