package com.o3j.es.estamosenfiestas.display_frame_layout;

import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

import com.eef.data.dataelements.Event;
import com.eef.data.eefExceptions.eefException;
import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.dialogs.ClaseDialogEventoGuardado;

import managers.DatabaseServerManager;



//Juan, 19 agosto 2015, clase para guardar favoritos. es usada por:
//CustomAdapter_RecyclerView_Evento_Detalle
//Activity_Fragment_FrameLayout_WithNavDrawer_2
//
public class Clase_Add_To_List_Of_Favorites extends DialogFragment {

    private static  String TAG;
    Event m_Event_Poner_en_favoritos;
    ActionBarActivity actionBarActivity;

    public static Clase_Add_To_List_Of_Favorites newInstance(Event m_Event_Poner_en_favoritos,
                                                             ActionBarActivity actionBarActivity) {



        Clase_Add_To_List_Of_Favorites frag = new Clase_Add_To_List_Of_Favorites();

        frag.m_Event_Poner_en_favoritos = m_Event_Poner_en_favoritos;
        frag.actionBarActivity = actionBarActivity;
        TAG = frag.getClass().getSimpleName();

        return frag;
    }//Fin de newInstance




    //******************************************************************************************************
    //19 agosto 2015: metodo definitivo para guardar evento en favoritos
    public void method_Add_To_List_Of_Favorites() {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, " En metodo method_Add_To_List_Of_Favorites:  ");
        }

        boolean boolean_evento_guardado = true;

        DatabaseServerManager databaseServerManager = new DatabaseServerManager();
        databaseServerManager.initManager(actionBarActivity);
        try {
            databaseServerManager.grabarEventoComoFavorito(m_Event_Poner_en_favoritos);
        } catch (eefException eef) {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG,  " En metodo method_Add_To_List_Of_Favorites Exception:  " +eef.getLocalizedMessage());
            }
            boolean_evento_guardado = false;
        }


        //*************************************************************************************************
//        Mostrar el dialogo de has guardado en favoritos

        if (boolean_evento_guardado) {

            // Create an instance of the dialog fragment and show it
            DialogFragment dialog =  ClaseDialogEventoGuardado.newInstance
                    (R.string.titulo_dialog_evento_guardado, R.string.message_dialog_evento_guardado, R.string.positiveButton,
                            R.string.negativeButton, 1, R.string.boton_neutral_evento_guardado);
            dialog.show(actionBarActivity.getSupportFragmentManager(), "ClaseDialogGenericaDosTresBotones");
//            dialog.show(getSupportFragmentManager(), "ClaseDialogGenericaDosTresBotones");
        }else{
            DialogFragment dialog =  ClaseDialogEventoGuardado.newInstance
                    (R.string.titulo_dialog_evento_guardado, R.string.message_dialog_evento_guardado_Error, R.string.positiveButton,
                            R.string.negativeButton, 1, R.string.boton_neutral_evento_guardado);
            dialog.show(actionBarActivity.getSupportFragmentManager(), "ClaseDialogGenericaDosTresBotones");
        }

        //*************************************************************************************************

    }//method_Add_To_List_Of_Favorites
}
