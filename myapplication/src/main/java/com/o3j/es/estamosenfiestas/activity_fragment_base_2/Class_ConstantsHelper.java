package com.o3j.es.estamosenfiestas.activity_fragment_base_2;

import android.util.Patterns;

import com.eef.data.dataelements.EventPlanner;
import com.eef.data.managers.IConfigurationManager;
import com.o3j.es.estamosenfiestas.display_frame_layout.Class_Modelo_De_Datos_Lista_Favoritos;

import java.util.ArrayList;

/**
 * Created by Juan on 20/02/2015.
 */
public class Class_ConstantsHelper {

    //Juan, 4 oct 2015: Este array lo escriben MiFragment_Evento para que al hacer el back to parent,
    //MiFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView sepa si tiene que resaltar un evento como favorito
    public static ArrayList<Integer> arraylist_Eventos_Favoritos =
            new ArrayList<Integer>();


    //Juan 3 oct 2015: boolean para actualizar un evento agregado a favoritos en Lista de eventos en los programas:
    //Esta variable se usa en act de programa para actualizar los datos en el onResume cuando hago back to parent
    //desde Activity_Fragment_SwipeView_EventosDelDia_BackToParent
    public static boolean boolean_Actualiza_Datos_Del_Programa_Con_Favoritos= false;

    //Juan, 26 agosto 2015: variable para diferenciar cuando estoy instalando la app o estoy reconfigurando
    public static boolean boolean_estoy_instalando= false;


    public static final boolean BOOLEAN_SHOW_LOG_D = true;

    public static String titulo_De_La_Fiesta = "";

    public static String fecha_Del_Dia = "";


    public static String string_Nombre_Del_Pueblo_O_Ayuntamiento = "";

    public static IConfigurationManager m_IConfigurationManager;

    public static boolean boolean_Hay_Una_nueva_configuracion = false;


    //Juan 18 agosto 2015: Static EventPlanner para pintar el logo en la toolbar de las vistas deslizantes

    public static EventPlanner eventPlanner = null;

    //    Estas Variables son para el fake de lista de favoritos, 12 junio 2015
    public static ArrayList<Class_Modelo_De_Datos_Lista_Favoritos> arraylist_Datos_Lista_Favoritos =
            new ArrayList<Class_Modelo_De_Datos_Lista_Favoritos>();
    public static boolean boolean_Arraylist_Datos_Lista_Favoritos_Tiene_Datos = false;
    public static String string_NombreDeLaFiesta;
    public static String string_FechaDelEvento;
//  FIN de  Estas Variables son para el fake de lista de favoritos, 12 junio 2015

    public static boolean methodStaticChequeaURL(String string) {
        //9 septiembre 2015: nuevo chequeo de url valida
        //Como en: http://stackoverflow.com/questions/4905075/how-to-check-if-url-is-valid-in-android
        //Use WEB_URL pattern in Patterns Class:  Patterns.WEB_URL.matcher(potentialUrl).matches()
        //It will return True if URL is valid and false if URL is invalid.
        return Patterns.WEB_URL.matcher(string).matches();
    }

}
