package com.o3j.es.estamosenfiestas.display_informacion;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.o3j.es.estamosenfiestas.R;

/**
 * A fragment with a Google +1 button.
 * Activities that contain this fragment must implement the
 * {@link MiFragment_Show_Developer_Web.OnFragmentInteractionListener_Info} interface
 * to handle interaction events.
 * Use the {@link MiFragment_Show_Developer_Web#newInstance} factory method to
 * create an instance of this fragment.
 */




//Hacerlo lo del mail como en
//        http://www.mkyong.com/android/how-to-send-email-in-android/
//de ahi saque el xml


public class MiFragment_Show_Developer_Web extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    // The URL to +1.  Must be a valid URL.
    private final String PLUS_ONE_URL = "http://developer.android.com";

    // The request code must be 0 or greater.
    private static final int PLUS_ONE_REQUEST_CODE = 0;


    private OnFragmentInteractionListener_Info mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MiFragment_Developer_Information.
     */
    // TODO: Rename and change types and number of parameters
    public static MiFragment_Show_Developer_Web newInstance(String param1, String param2) {
        MiFragment_Show_Developer_Web fragment = new MiFragment_Show_Developer_Web();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public MiFragment_Show_Developer_Web() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_display_developer_information, container, false);


//        method_Seleccionar_Accion(view);
        method_Mostrar_web_Del_Desarrollador(view);
//        method_Enviar_Email(view);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction_show_developer_web_site();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener_Info) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener_Info");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener_Info {
        public void onFragmentInteraction_show_developer_web_site();
        public void onFragmentInteraction_send_mail_to_developer();
    }



    public void method_Enviar_Email(View view) {

//        ver intents en android developers
//        http://developer.android.com/training/basics/intents/sending.html
//        http://developer.android.com/guide/components/intents-common.html


//        poner en google search: email intent android tutorial y round button android

//    Como en: De ahi saque el codigo de este metodo. Usarlo en el contextual action mode de la lista de eventos
//    http://www.mkyong.com/android/how-to-send-email-in-android/
//        y
//    ver tambien: a ver que puedo mejorar
//    http://examples.javacodegeeks.com/android/core/email/android-sending-email-example/

        linear_email_compose = (LinearLayout) view.findViewById(R.id.linear_email_compose);
        buttonSend = (Button) view.findViewById(R.id.buttonSend);
        textTo = (EditText) view.findViewById(R.id.editTextTo);
        textTo.requestFocus();
        textSubject = (EditText) view.findViewById(R.id.editTextSubject);
        textMessage = (EditText) view.findViewById(R.id.editTextMessage);

        linear_email_compose.setVisibility(View.VISIBLE);

        buttonSend.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String to = textTo.getText().toString();
                String subject = textSubject.getText().toString();
                String message = textMessage.getText().toString();

                Intent intent_email = new Intent(Intent.ACTION_SEND);
                intent_email.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
                //email.putExtra(Intent.EXTRA_CC, new String[]{ to});
                //email.putExtra(Intent.EXTRA_BCC, new String[]{to});
                intent_email.putExtra(Intent.EXTRA_SUBJECT, subject);
                intent_email.putExtra(Intent.EXTRA_TEXT, message);

                //need this to prompts email client only
                intent_email.setType("message/rfc822");

//                startActivity(Intent.createChooser(intent_email, "Choose an Email client :"));

                if (intent_email.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(Intent.createChooser(intent_email, "Choose an Email client :"));
                }

            }
        });

    }//Fin de method_Enviar_Email

    LinearLayout linear_email_compose;
    Button buttonSend;
    EditText textTo;
    EditText textSubject;
    EditText textMessage;

    public void method_Seleccionar_Accion(View view) {

        imageView = (ImageView) view.findViewById(R.id.imageView_foto_Grande);
        string_selecciona_una_accion = (TextView) view.findViewById(R.id.titulo_del_fragment_info_developer);
        button_mostrar_web_del_desarrollador = (Button) view.findViewById(R.id.button_mostrar_web_del_desarrollador);
        button_enviar_mail_al_desarrollador = (Button) view.findViewById(R.id.button_enviar_mail_al_desarrollador);

        imageView.setVisibility(View.VISIBLE);
        string_selecciona_una_accion.setVisibility(View.VISIBLE);
        button_mostrar_web_del_desarrollador.setVisibility(View.VISIBLE);
        button_enviar_mail_al_desarrollador.setVisibility(View.VISIBLE);

        button_mostrar_web_del_desarrollador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteraction_show_developer_web_site();
            }
        });

        button_enviar_mail_al_desarrollador.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteraction_send_mail_to_developer();
            }
        });

    }//Fin de method_Seleccionar_Accion

    ImageView imageView;
    TextView string_selecciona_una_accion;
    Button button_mostrar_web_del_desarrollador;
    Button button_enviar_mail_al_desarrollador;


    public void method_Mostrar_web_Del_Desarrollador(View view) {
        linear_web_page = (LinearLayout) view.findViewById(R.id.linear_web_page);
        linear_web_page.setVisibility(View.VISIBLE);
//        para la web, poner en google seach: show webpage in android app
        myWebView = (WebView) view.findViewById(R.id.webview);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setBuiltInZoomControls(true);
        myWebView.getSettings().setDefaultTextEncodingName("UTF-8");

        //Carga desde esta url en la web
//        myWebView.loadUrl("http://estamosenfiestas.es/");
        //Carga fichero html en carpeta assets
//        myWebView.loadUrl("file:///android_asset/acerca_de.html");

        //8 septiembre 2015, la 2 no tiene el envio de correo
        myWebView.loadUrl("file:///android_asset/acerca_de_2.html");

    }//Fin de method_Mostrar_web_Del_Desarrollador

    LinearLayout linear_web_page;
    WebView myWebView;

}//Fin de la clase
