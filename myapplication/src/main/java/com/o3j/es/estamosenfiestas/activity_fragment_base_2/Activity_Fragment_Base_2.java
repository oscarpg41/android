package com.o3j.es.estamosenfiestas.activity_fragment_base_2;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.toolbar_con_tabs_helpers.SlidingTabLayout;

//Carga el fragment con loader
public class Activity_Fragment_Base_2 extends ActionBarActivity implements com.o3j.es.estamosenfiestas.activity_fragment_base_2.MiFragment_Con_Loader_1.OnFragmentInteractionListener {
    //Original con fragment sin loader
//    public class Activity_Fragment_Base_2 extends ActionBarActivity implements MiFragment_1.OnFragmentInteractionListener {
    com.o3j.es.estamosenfiestas.activity_fragment_base_2.AdaptadorDeSwipeViews adaptadorDeSwipeViews;
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_base_2);

//        Toolbar toolBar_Actionbar = (Toolbar) findViewById (R.id.activity_my_toolbar);
        Toolbar toolBar_Actionbar = (Toolbar) findViewById(R.id.view);
        //Toolbar will now take on default Action Bar characteristics
        toolBar_Actionbar.setTitle(getResources().getString(R.string.title_main));
        toolBar_Actionbar.setSubtitle(getResources().getString(R.string.sub_titulo_2));
        setSupportActionBar(toolBar_Actionbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Toolbar bottom_toolbar = (Toolbar) findViewById(R.id.bottom_toolbar);
        bottom_toolbar.setLogo(R.drawable.ic_launcher);
        bottom_toolbar.setTitle(getResources().getString(R.string.texto_1));
        bottom_toolbar.setSubtitle(getResources().getString(R.string.texto_2));
        bottom_toolbar.setNavigationIcon(R.drawable.ic_launcher);
        //Toolbar que pongo abajo

        // Set an OnMenuItemClickListener to handle menu item clicks
        bottom_toolbar.setOnMenuItemClickListener(
                new Toolbar.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // Handle the menu item

                        int id = item.getItemId();

                        //noinspection SimplifiableIfStatement
                        if (id == R.id.action_settings) {
                            Toast.makeText(com.o3j.es.estamosenfiestas.activity_fragment_base_2.Activity_Fragment_Base_2.this, "Bottom toolbar pressed: ", Toast.LENGTH_LONG).show();
                            return true;
                        }
                        return true;
                    }
                });


        bottom_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(com.o3j.es.estamosenfiestas.activity_fragment_base_2.Activity_Fragment_Base_2.this, "Navigation Icon pressed: ", Toast.LENGTH_LONG).show();
                // Para esta practica, al presionar el navigation icon lo quito.
                bottom_toolbar.setVisibility(View.GONE);
            }
        });

        // Inflate a menu to be displayed in the toolbar
        bottom_toolbar.inflateMenu(R.menu.menu_main);

        // ViewPager and its adapters use support library
        // fragments, so use getSupportFragmentManager.
        adaptadorDeSwipeViews =
                new com.o3j.es.estamosenfiestas.activity_fragment_base_2.AdaptadorDeSwipeViews(
                        getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.viewpager_1);
        mViewPager.setAdapter(adaptadorDeSwipeViews);

        //Poner tabs del swipe view
        metodoPonerTabs_1(mViewPager);





    }//Fin del onCreate

    private void metodoPonerTabs_1(ViewPager mViewPager) {
//        Hecho como en: http://guides.codepath.com/android/Google-Play-Style-Tabs-using-SlidingTabLayout
// Give the SlidingTabLayout the ViewPager
        SlidingTabLayout slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        // Center the tabs in the layout
        slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setViewPager(mViewPager);
        //Colorear el tab
        slidingTabLayout.setBackgroundColor(getResources().getColor(R.color.primary_color));
        // Customize tab indicator color
        slidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.accent_color);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(com.o3j.es.estamosenfiestas.activity_fragment_base_2.Activity_Fragment_Base_2.this, "Bottom toolbar pressed: ", Toast.LENGTH_LONG).show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Implementa la interfaz
    public void onFragmentInteraction(String string) {

    }


}//Fin de la clase
