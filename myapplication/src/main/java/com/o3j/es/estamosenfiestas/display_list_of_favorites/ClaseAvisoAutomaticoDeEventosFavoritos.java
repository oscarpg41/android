package com.o3j.es.estamosenfiestas.display_list_of_favorites;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.o3j.es.estamosenfiestas.activity_actualizar_fiestas.IntentserviceActualizacionesAutomaticas;

import java.util.Calendar;

/**
 * Created by Juan on 24/08/2015.
 */
public class ClaseAvisoAutomaticoDeEventosFavoritos {
    //Esta clase es para actualizar de forma automatica los datos de las fiestas
    private String TAG;
    private Context context;
    private String fechaInicial = "fechaInicial";
    private int intMinutosElapse = 5;



    public static ClaseAvisoAutomaticoDeEventosFavoritos newInstance(Context context) {

        ClaseAvisoAutomaticoDeEventosFavoritos claseAvisoAutomaticoDeEventosFavoritos =
                new ClaseAvisoAutomaticoDeEventosFavoritos();


        claseAvisoAutomaticoDeEventosFavoritos.TAG = claseAvisoAutomaticoDeEventosFavoritos.getClass().getSimpleName();
        claseAvisoAutomaticoDeEventosFavoritos.context = context;

        return claseAvisoAutomaticoDeEventosFavoritos;
    }//Fin de newInstance


    //************************************************************************************
    //10 sept 2015: aqui empieza lo nuevo de aviso automatico de favoritos
    //************************************************************************************

    public void methodDetenerArrancarChequeoAutomaticodeFavoritos() {
        //
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, " En metodo methodDetenerArrancarChequeoAutomaticodeFavoritos  :  "  );
        }

        //Siempre detengo el chequeo
        methodDetenerChequeoAutomaticoDeFavoritos("param1",  "param2");

        //Chequo si hay favoritos.
        //Si los hay, arranco el chequeo


        //OJO: CREO QUE NO ME HACE FALTA USAR EL XML DE PREFERENCIAS
//        if (methodGetBooleanHayFavoritos()) {//Arranco el chequeo


        if (true) {//Arranco el chequeo
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG, " En metodo methodDetenerArrancarChequeoAutomaticodeFavoritos, INICIANDO CHEQUEO AUTOMATICO  :  ");
            }
            methodIniciaChequeoAutomaticoDeFavoritos("param1", "param2");

        } else {//No hago nada
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG, " En metodo methodDetenerArrancarChequeoAutomaticodeFavoritos, NO INICIO CHEQUEO AUTOMATICO  :  ");
            }

        }




    }//Fin de methodDetenerArrancarChequeoAutomaticodeFavoritos




    public void methodSetHayFavoritos(boolean myBoolean) {
        //Metodo para indicar que hay favoritos o no
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, " En metodo methodSetHayFavoritos, myBoolean:  " +myBoolean );
        }

        SharedPreferences prefs_Favoritos = context.getSharedPreferences("prefs_Favoritos", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs_Favoritos.edit();
        editor.putBoolean("BooleanHayFavoritos", myBoolean);
        editor.commit();
    }//Fin de methodSetHayFavoritos

    public boolean methodGetBooleanHayFavoritos() {
        //Metodo para averiguar el estado del checkbox
        SharedPreferences prefs_Favoritos = context.getSharedPreferences("prefs_Favoritos", Context.MODE_PRIVATE);
        boolean BooleanHayFavoritos = prefs_Favoritos.getBoolean("BooleanHayFavoritos", false);
        if (BooleanHayFavoritos) {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG, " En metodo methodGetBooleanHayFavoritos, BooleanHayFavoritos :  " +BooleanHayFavoritos);
            }
            return true;
        }else{
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG, " En metodo methodGetBooleanHayFavoritos, BooleanHayFavoritos :  " +BooleanHayFavoritos);
            }
            return false;
        }
    }//Fin de methodGetBooleanHayFavoritos

    public void methodDetenerChequeoAutomaticoDeFavoritos(String param1, String param2) {
        //Metodo para cancelar la alarma de chequear favoritos

        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, " En metodo methodDetenerChequeoAutomaticoDeFavoritos:  ");
        }
        AlarmManager alarmMgr;
        PendingIntent alarmIntent;

        alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, IntentserviceChequeoAutomaticoDeFavoritos.class);
        intent.setAction(IntentserviceActualizacionesAutomaticas.ACTION_FOO);
        intent.putExtra(IntentserviceActualizacionesAutomaticas.EXTRA_PARAM1, param1);
        intent.putExtra(IntentserviceActualizacionesAutomaticas.EXTRA_PARAM2, param2);
        alarmIntent = PendingIntent.getService(context, 194650, intent, 0);

        if (alarmMgr!= null) {
            alarmMgr.cancel(alarmIntent);
        }
    }//Fin de methodDetenerChequeoAutomaticoDeFavoritos


    public void methodIniciaChequeoAutomaticoDeFavoritos(String param1, String param2) {
        //Juan 31 agosto 2015: metodo para inicializar actualizacion automatica
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, " En metodo methodIniciaChequeoAutomaticoDeFavoritos:  ");
        }

        //Por fin, MI ALARMA, repite cada minuto
        AlarmManager alarmMgr;
        PendingIntent alarmIntent;

        alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, IntentserviceChequeoAutomaticoDeFavoritos.class);
        intent.setAction(IntentserviceActualizacionesAutomaticas.ACTION_FOO);
        intent.putExtra(IntentserviceActualizacionesAutomaticas.EXTRA_PARAM1, param1);
        intent.putExtra(IntentserviceActualizacionesAutomaticas.EXTRA_PARAM2, param2);
        alarmIntent = PendingIntent.getService(context, 194650, intent, 0);

// Set the alarm to start at 8:30 a.m.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.MINUTE, 1);

// setRepeating() lets you specify a precise custom interval--in this case,
// 60 minutes.
        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                1000 * 60 * 60, alarmIntent);

    }//FIN de methodIniciaChequeoAutomaticoDeFavoritos

    //************************************************************************************
    //FIN DE 10 sept 2015: aqui empieza lo nuevo de aviso automatico de favoritos
    //************************************************************************************
}
