package com.o3j.es.estamosenfiestas.servercall.pojos;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by jluis on 28/07/15.
 */
public class InfoOrganizerPojo implements Parcelable {

    @Expose
    private String idOrganizer;
    @Expose
    private String name;
    @Expose
    private String town;
    @Expose
    private String info;
    @Expose
    private String idccaa;
    @SerializedName("ccaa_name")
    @Expose
    private String ccaaName;
    @SerializedName("ff_paid")
    @Expose
    private String ffPaid;
    @SerializedName("ff_validity")
    @Expose
    private String ffValidity;
    @Expose
    private String logo;

    /**
     *
     * @return
     * The idOrganizer
     */
    public String getIdOrganizer() {
        return idOrganizer;
    }

    /**
     *
     * @param idOrganizer
     * The idOrganizer
     */
    public void setIdOrganizer(String idOrganizer) {
        this.idOrganizer = idOrganizer;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The town
     */
    public String getTown() {
        return town;
    }

    /**
     *
     * @param town
     * The town
     */
    public void setTown(String town) {
        this.town = town;
    }

    /**
     *
     * @return
     * The info
     */
    public String getInfo() {
        return info;
    }

    /**
     *
     * @param info
     * The info
     */
    public void setInfo(String info) {
        this.info = info;
    }

    /**
     *
     * @return
     * The idccaa
     */
    public String getIdccaa() {
        return idccaa;
    }

    /**
     *
     * @param idccaa
     * The idccaa
     */
    public void setIdccaa(String idccaa) {
        this.idccaa = idccaa;
    }

    /**
     *
     * @return
     * The ccaaName
     */
    public String getCcaaName() {
        return ccaaName;
    }

    /**
     *
     * @param ccaaName
     * The ccaa_name
     */
    public void setCcaaName(String ccaaName) {
        this.ccaaName = ccaaName;
    }

    /**
     *
     * @return
     * The ffPaid
     */
    public String getFfPaid() {
        return ffPaid;
    }

    /**
     *
     * @param ffPaid
     * The ff_paid
     */
    public void setFfPaid(String ffPaid) {
        this.ffPaid = ffPaid;
    }

    /**
     *
     * @return
     * The ffValidity
     */
    public String getFfValidity() {
        return ffValidity;
    }

    /**
     *
     * @param ffValidity
     * The ff_validity
     */
    public void setFfValidity(String ffValidity) {
        this.ffValidity = ffValidity;
    }

    /**
     *
     * @return
     * The logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     *
     * @param logo
     * The logo
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.idOrganizer);
        dest.writeString(this.name);
        dest.writeString(this.town);
        dest.writeString(this.info);
        dest.writeString(this.idccaa);
        dest.writeString(this.ccaaName);
        dest.writeString(this.ffPaid);
        dest.writeString(this.ffValidity);
        dest.writeString(this.logo);
    }

    public InfoOrganizerPojo() {
    }

    protected InfoOrganizerPojo(Parcel in) {
        this.idOrganizer = in.readString();
        this.name = in.readString();
        this.town = in.readString();
        this.info = in.readString();
        this.idccaa = in.readString();
        this.ccaaName = in.readString();
        this.ffPaid = in.readString();
        this.ffValidity = in.readString();
        this.logo = in.readString();
    }

    public static final Creator<InfoOrganizerPojo> CREATOR = new Creator<InfoOrganizerPojo>() {
        public InfoOrganizerPojo createFromParcel(Parcel source) {
            return new InfoOrganizerPojo(source);
        }

        public InfoOrganizerPojo[] newArray(int size) {
            return new InfoOrganizerPojo[size];
        }
    };
}
