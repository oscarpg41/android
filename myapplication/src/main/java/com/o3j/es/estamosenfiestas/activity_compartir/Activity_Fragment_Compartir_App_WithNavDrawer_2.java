package com.o3j.es.estamosenfiestas.activity_compartir;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ScrollView;

import com.eef.data.dataelements.DayEvents;
import com.eef.data.dataelements.Event;
import com.eef.data.dataelements.Program;
import com.eef.data.managers.IConfigurationManager;
import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.ClaseChequeaAcceso_A_Internet;
import com.o3j.es.estamosenfiestas.dialogs.ClaseDialogGenericaDosTresBotones;
import com.o3j.es.estamosenfiestas.dialogs.ClaseDialogNoHayInternet;
import com.o3j.es.estamosenfiestas.display_frame_layout.CustomAdapter_RecyclerView_Evento_Detalle;
import com.o3j.es.estamosenfiestas.display_frame_layout.CustomAdapter_RecyclerView_Fiesta_List_Por_Dias;
import com.o3j.es.estamosenfiestas.display_frame_layout.CustomAdapter_RecyclerView_Fiestas;
import com.o3j.es.estamosenfiestas.display_frame_layout.CustomAdapter_RecyclerView_List_De_Eventos;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Evento;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Fiesta_Seleccionada_Con_RecycleView;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Fiestas_Con_RecycleView;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView;
import com.o3j.es.estamosenfiestas.display_lists_package.AdaptadorDeSwipeViews_2;
import com.o3j.es.estamosenfiestas.nav_drawer_utilities.ClassMenuItemsNavigationDrawer;
import com.o3j.es.estamosenfiestas.nav_drawer_utilities.CustomAdapter_NavigationDrawer_2;

import java.util.ArrayList;
import java.util.List;


// Juan 26 agosto 2015: Esta clase permite enviar por whatsapp, gmail, etc el link de eef en la play store.

public class Activity_Fragment_Compartir_App_WithNavDrawer_2 extends ActionBarActivity
        implements CustomAdapter_RecyclerView_Fiestas.OnItemClickListenerFiestaSeleccionada,
        CustomAdapter_RecyclerView_Fiesta_List_Por_Dias.OnItemClickListenerDiaSeleccionado,
        CustomAdapter_RecyclerView_List_De_Eventos.OnItemClickListenerEventoSeleccionado,
        CustomAdapter_RecyclerView_Evento_Detalle.OnItemClickListenerEventoDetalle,
        CustomAdapter_NavigationDrawer_2.OnItemClickListenerElementoDeMenuSeccionado,
        MiFragment_Fiestas_Con_RecycleView.OnFragmentInteractionListener,
        MiFragment_Fiesta_Seleccionada_Con_RecycleView.OnFragmentInteractionListener,
        MiFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView.OnFragmentInteractionListener,
        MiFragment_Evento.OnFragmentInteractionListener,
        ClaseDialogGenericaDosTresBotones.InterfaceClaseDialogGenericaDosTresBotonesListener,
        ClaseDialogNoHayInternet.InterfaceClaseDialogNoHayInternet{
    //Original con fragment sin loader
//    public class Activity_Fragment_Base_2 extends ActionBarActivity implements MiFragment_1.OnFragmentInteractionListener {
    AdaptadorDeSwipeViews_2 adaptadorDeSwipeViews;
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        xxx = this.getClass().getSimpleName();

        setContentView(R.layout.activity_under_construction);

        //Oculto el scroll view del menu actualizar, que comparten el layout
        ScrollView ScrollViewActualizacion = (ScrollView) findViewById(R.id.ScrollViewActualizacion);
        ScrollViewActualizacion.setVisibility(View.GONE);



        Toolbar toolBar_Actionbar = (Toolbar) findViewById(R.id.view);

        toolBar_Actionbar.setTitle(getResources().getString(R.string.menu_7));

//        No uso el subtitulo
//        toolBar_Actionbar.setSubtitle(getResources().getString(R.string.sub_titulo_4));


        setSupportActionBar(toolBar_Actionbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        //Inicializar el nav drawer, 21 Mayo 2015
        method_Init_NavigationDrawer();


        // Juan, 21 agosto 2015: Muestra el dialogo de inicio de compartir
        DialogFragment dialog =  ClaseDialogGenericaDosTresBotones.newInstance
                (R.string.titulo_dialog_compartir, R.string.message_dialog_compartir, R.string.positiveButton_dialog_compartir,
                        R.string.negativeButton_dialog_compartir, 2, R.string.boton_neutral);
        dialog.show(getSupportFragmentManager(), "ClaseDialogGenericaDosTresBotones");

    }//Fin del onCreate


    //**************************************************************************
    //**************************************************************************
    //  24 junio 2015: Setting correcto para no mostrar menu en la toolbar
    //  24 junio 2015: Solo se muestra el Nav Drawer
    //**************************************************************************
    //**************************************************************************
//    Me ayude con:
//    https://github.com/codepath/android_guides/wiki/Fragment-Navigation-Drawer

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;

        // Uncomment to inflate menu items to Action Bar
        // inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = drawerLayout.isDrawerOpen(navigationDrawerRecyclerView);

        //No he inflado el menu, lo dejo comentado
//        menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }// Fin de onPrepareOptionsMenu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this. 21 Mayo 2015
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }//Codigo del nav drawer


        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        if (id == R.id.action_settings) {
//            Toast.makeText(Activity_Fragment_FrameLayout_WithNavDrawer_2.this, " toolbar pressed: ", Toast.LENGTH_LONG).show();
//
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }
    //**************************************************************************
    //**************************************************************************
    //  FIN DE 24 junio 2015: Setting correcto para no mostrar menu en la toolbar
    //  24 junio 2015: Solo se muestra el Nav Drawer
    //**************************************************************************
    //**************************************************************************

    //Implementa la interfaz de la clase MiFragment_Fiestas_Con_RecycleView
//    y la clase MiFragment_Fiesta_Seleccionada_Con_RecycleView

//    OJO: NO ESTOY HACIENDO USO DE ESTA INTERFAZ
    public void onFragmentInteraction(String string) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"Fragmento activo: " +string);

        }
    }



    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //  Inicio de Codigo relacionado con el navigation drawer, 21 Mayo 2015
    //****************************************************************
    //****************************************************************
    private void method_Init_NavigationDrawer() {

        //****************************************************************
        //   Codigo relacionado con recycler view del navigation drawer
        //****************************************************************


        //Inicializar los datos del array del menu a mostrar en el navigationDrawer
        init_arrayList_NavigationDrawer();

        navigationDrawerRecyclerView = (RecyclerView) findViewById(R.id.left_drawer_recycle_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        navigationDrawerRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        navigationDrawerLayoutManager = new LinearLayoutManager(this);
        navigationDrawerRecyclerView.setLayoutManager(navigationDrawerLayoutManager);
//        navigationDrawerRecyclerView.setAdapter(new CustomAdapter_NavigationDrawer(

//        Adapter del navigation drawer con la cabecera
                navigationDrawerRecyclerView.setAdapter(new CustomAdapter_NavigationDrawer_2(
                arrayListItemsNavigationDrawer, "0",
                        (CustomAdapter_NavigationDrawer_2.OnItemClickListenerElementoDeMenuSeccionado) this));

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_1);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        //Este objeto permite que el navigation Drawer interactue correctamente con el action bar (la toolbar en mi caso)
        //Tengo que usar el actionBarDrawerToggle de V7 support por que el de V4 esta deprecated
        actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                drawerLayout,         /* DrawerLayout object */
                //Deprecated en V7, esto era valido en V4
//                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
//                getSupportActionBar().setTitle(getResources().getString(R.string.title_activity_main_activity_navigation_drawer));
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()

            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                getActionBar().setTitle(mDrawerTitle);
//                getSupportActionBar().setTitle(getTitle());
//                getSupportActionBar().setTitle(getResources().getString(R.string.titulo_navigation_drawer));
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()


            }
        };//Fin de actionBarDrawerToggle

        // Set the drawer toggle as the DrawerListener
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //****************************************************************
        //  FIN Codigo relacionado con recycler view del navigation drawer
        //****************************************************************
    }//Fin de method_Init_NavigationDrawer





    //Datos del menu del navigation drawer
    private void init_arrayList_NavigationDrawer() {//Relacionado con recycler view
        String[] array_menu = {getString(R.string.menu_1),getString(R.string.menu_2),getString(R.string.menu_3)
                ,getString(R.string.menu_4),getString(R.string.menu_6),getString(R.string.menu_7), getString(R.string.menu_5)};
        arrayListItemsNavigationDrawer = new ArrayList<ClassMenuItemsNavigationDrawer>();
        for (int i = 0; i < DATASET_COUNT; i++) {
            arrayListItemsNavigationDrawer.add(
                    ClassMenuItemsNavigationDrawer.newInstance(array_menu[i],
                            " ",
                            R.drawable.ic_launcher));
        }
    }


    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    //Si estos metodos no se llaman, no aparece la hamburguesa
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    //Variables para el recycler view
    private DrawerLayout drawerLayout;
    private RecyclerView navigationDrawerRecyclerView;
    private RecyclerView.Adapter navigationDrawerAdapter;
    private RecyclerView.LayoutManager navigationDrawerLayoutManager;
    protected ArrayList<ClassMenuItemsNavigationDrawer> arrayListItemsNavigationDrawer = null;
    private static final int DATASET_COUNT = 7;
    //Este objeto permite que el navigation Drawer interactue correctamente con el action bar (la toolbar en mi caso)
    private ActionBarDrawerToggle actionBarDrawerToggle;
    //FIN Variables para el recycler view

    //****************************************************************
    //****************************************************************
    //     FIN del Codigo relacionado con el navigation drawer
    //****************************************************************
    //****************************************************************


    IConfigurationManager myCmT=null;
    private static String xxx;
    List<Program> List_Programs;
    int int_Number_Of_Fragments_To_Show = 0;
    ArrayList<Program> arrayList_Lista_De_Fiesta_Serializable = new ArrayList<Program>();

    int int_Nivel_De_Info_Fiestas = 1;
    int int_Nivel_De_Info_Fiesta_Por_Dias = 2;
    int int_Nivel_De_Info_Fiesta_Eventos_Del_Dia = 3;
    int int_Nivel_De_Info_Fiesta_Evento_Detalle = 4;



    //    Interfaz de la clase CustomAdapter_RecyclerView_Fiestas.
    @Override
    public void onClick(Program m_Programa_seleccionado) {
        Fragment newFragment = getNewFragmentForFiesta(m_Programa_seleccionado);
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack

        transaction.replace(R.id.details, newFragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();

    }

    public Fragment getNewFragmentForFiesta(Program m_Programa_seleccionado) {
        Fragment fragment = new MiFragment_Fiesta_Seleccionada_Con_RecycleView();
        Bundle args = new Bundle();
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "fragment#" +(int_Nivel_De_Info_Fiesta_Por_Dias + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, String.valueOf(int_Nivel_De_Info_Fiesta_Por_Dias + 1));

        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, int_Nivel_De_Info_Fiesta_Por_Dias);


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
        args.putSerializable("Programa Serializado", m_Programa_seleccionado);


        fragment.setArguments(args);

        return fragment;
    }


//    Interfaz de la clase CustomAdapter_RecyclerView_Fiesta_List_Por_Dias.
    @Override
    public void onClickDia(DayEvents m_DayEvents) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"He llegado a onClickDia" );
        }
        Fragment newFragment = getNewFragmentForDayEvents(m_DayEvents);
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack

        transaction.replace(R.id.details, newFragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }

    public Fragment getNewFragmentForDayEvents(DayEvents m_DayEvents) {
        Fragment fragment = new MiFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView();
        Bundle args = new Bundle();
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "fragment#" +(int_Nivel_De_Info_Fiesta_Eventos_Del_Dia + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, String.valueOf(int_Nivel_De_Info_Fiesta_Eventos_Del_Dia + 1));

        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, int_Nivel_De_Info_Fiesta_Eventos_Del_Dia);

//        Hago esto por que la lista de eventos no es serializable, asi que lo pongo en array para
//                pasarlo al fragment con la lista de los eventos del dia seleccionado
        ArrayList<Event> arrayList_Lista_De_Eventos_Del_Dia = new ArrayList<Event>();
        for (int i=0; i<m_DayEvents.getListaEventos().size(); i++){
            arrayList_Lista_De_Eventos_Del_Dia.add(m_DayEvents.getListaEventos().get(i));
        }


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
        args.putSerializable("Eventos del dia Serializado", arrayList_Lista_De_Eventos_Del_Dia);


        fragment.setArguments(args);

        return fragment;
    }

    //    Interfaz de la clase CustomAdapter_RecyclerView_List_De_Eventos.
    @Override
    public void onClickEvento(Event m_Event) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"He llegado a onClickEvento" );
        }
        Fragment newFragment = getNewFragmentToShowEvent(m_Event);
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack

        transaction.replace(R.id.details, newFragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }

    public Fragment getNewFragmentToShowEvent(Event m_Event) {
        Fragment fragment = new MiFragment_Evento();
        Bundle args = new Bundle();
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "fragment#" + (int_Nivel_De_Info_Fiesta_Evento_Detalle + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, String.valueOf(int_Nivel_De_Info_Fiesta_Evento_Detalle + 1));
        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, int_Nivel_De_Info_Fiesta_Evento_Detalle);

        args.putSerializable("Datos del Evento Serializado", m_Event);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onClickEventoDetalle(Event m_Event) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"He llegado a onClickEventoDetalle" );
        }
    }





    @Override
    public void onClickMenuItenDelNavDrawer(int position_Del_Menu_Seleccionada) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"He llegado a onClickMenuItenDelNavDrawer  con click: " +position_Del_Menu_Seleccionada );
        }

//        Intent intent = new Intent(this, Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
//
//        startActivity(intent);



        Intent intent;


        switch (position_Del_Menu_Seleccionada)
        {
            case 0:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_frame_layout.
                        Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 1://Mis Eventos
                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_list_of_favorites.
                        Activity_Fragment_List_Of_Favorites_With_Nav_Drawer.class);
                startActivity(intent);
                break;
            case 2:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.activity_actualizar_fiestas.
                        Activity_Fragment_Actualizar_Fiesta_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 3:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.application_configuration.
                        Activity_Application_Configuration_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 4:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_event_planner_information.
                        Activity_Display_Event_Planner_Information_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 5:
                drawerLayout.closeDrawer(Gravity.LEFT);

                break;
            case 6:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_informacion.
                        Activity_Display_Developer_Information_WithNavDrawer_2.class);
                startActivity(intent);
                break;
        }
        if (position_Del_Menu_Seleccionada != 5) finish();
//        if(position_Del_Menu_Seleccionada == 2){
//            //Solo muestra el toast
//            Toast.makeText(this, "Opción no disponible", Toast.LENGTH_LONG).show();
//        }else{
//            if (position_Del_Menu_Seleccionada != 5) finish();
//        }
    }

    @Override
    public boolean onLongClickEvento_CustomA_List_De_Eventos(Event m_Event, View m_View_Del_Evento) {

        return true;

    }

    //***********************************************************************************
    //***********************************************************************************
    //     21 agosto 2015, Actualizacion
    //     Juan, 21 agosto 2015: Todo lo nuevo para que la clase sea funcional.
    //***********************************************************************************
    //***********************************************************************************
    //Metodos de la interface del dialogo de la clase ClaseDialogGenericaDosTresBotones
    @Override
    public void onDialogPositiveClick_ClaseDialogGenericaDosTresBotones(DialogFragment dialog) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
        {
            Log.e(xxx, xxx + ": En metodo onDialogPositiveClick_ClaseDialogGenericaDosTresBotones: ");
        }
        //Devuelve al usuario a los programas
        Intent intent;
        intent = new Intent(this, com.o3j.es.estamosenfiestas.display_frame_layout.
                Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onDialogNegativeClick_ClaseDialogGenericaDosTresBotones(DialogFragment dialog) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
        {
            Log.e(xxx, xxx + ": En metodo  onDialogNegativeClick_ClaseDialogGenericaDosTresBotones: ");
        }

        method_Fuera_Del_On_Create_Chequea_Acceso_A_Internet();
    }

    @Override
    public void onDialogNeutralClick_ClaseDialogGenericaDosTresBotones(DialogFragment dialog) {
        //Solo sale con el error general en method_Muestra_Dialog_Si_Hay_Programas_Que_Actualizar
        // y en method_Muestra_Dialog_No_Hay_Programas_Que_Actualizar para volver a programas
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
        {
            Log.e(xxx, xxx + ": En metodo  onDialogNeutralClick_ClaseDialogGenericaDosTresBotones: ");
        }
        Intent intent;
        intent = new Intent(this, com.o3j.es.estamosenfiestas.display_frame_layout.
                Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
        startActivity(intent);
        finish();
    }

    public void method_Fuera_Del_On_Create_Chequea_Acceso_A_Internet() {
        //*************************************************************************************************
        if(method_Dime_Si_Hay_Red()) {//Cuequeamos si hay red
            //Si hay red, sigue ejecutando
            method_Compartir_App();
            //Vuelvo a programas
            methodGraciasPorCompartirEstaApp();

        }else{//NO hay red
            // Create an instance of the dialog fragment and show it
            DialogFragment dialog =  ClaseDialogNoHayInternet.newInstance
                    (R.string.titulo_dialog_error_no_hay_internet_reconfigurar, R.string.message_dialog_error_no_hay_internet_reconfigurar, R.string.positiveButton_error_no_hay_internet_reconfigurar,
                            R.string.negativeButton_error_no_hay_internet_reconfigurar, 2, R.string.boton_neutral_evento_guardado);
            dialog.show(getSupportFragmentManager(), "ClaseDialogNoHayInternet");
        }
        //*************************************************************************************************
    }

    private void methodGraciasPorCompartirEstaApp() {

            //Utilizo el dialogo general con boton OK y vuelvo a programas
            DialogFragment dialog =  ClaseDialogGenericaDosTresBotones.newInstance
                    (R.string.titulo_dialog_compartir, R.string.message_dialog_gracias_por_compartir, R.string.positiveButton_dialog_actualizar,
                            R.string.negativeButton_dialog_actualizar, 1, R.string.neutralButton_dialog_actualizar);
            dialog.show(getSupportFragmentManager(), "ClaseDialogGenericaDosTresBotones");

    }//Fin de methodGraciasPorCompartirEstaApp

    private void method_Compartir_App() {

        String to = "";
        String subject = "Hola, quiero compartir esta aplicación contigo";

        StringBuilder strBuilder = new StringBuilder("Mira esta aplicación en la play store:");
        strBuilder.append("\n");
        strBuilder.append("\n");
        strBuilder.append("https://play.google.com/store/apps/details?id=com.o3j.es.myapplication");

        String message = strBuilder.toString();


        //10 julio 2015, primera version para usar facebook, twitter, etc
        Intent intent_Generico = new Intent(Intent.ACTION_SEND);
        intent_Generico.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent_Generico.putExtra(Intent.EXTRA_TEXT, message);
        //need this to prompts email client only
        intent_Generico.setType("text/plain");

        if (intent_Generico.resolveActivity(getPackageManager()) != null) {
            startActivity(Intent.createChooser(intent_Generico, getResources().getString(R.string.compartir)));

        }
        //**************************************************************************************

    }//Fin de method_Compartir_App



    public boolean method_Dime_Si_Hay_Red() {

        //antes de lanzar cualquier servicio web al server, verifico si hay acceso a internet
        ClaseChequeaAcceso_A_Internet claseChequeaAcceso_A_Internet = new ClaseChequeaAcceso_A_Internet(this);
        if(claseChequeaAcceso_A_Internet.isOnline()) {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(xxx, xxx +"En metodo a method_Dime_Si_Hay_Red: SI" );
            }
            return true;
        }else {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(xxx, xxx +"En metodo a method_Dime_Si_Hay_Red: NO" );
            }
            return false;
        }
    }


    //Interface de dialogo de evento No hay Internet
    @Override
    public void onDialogPositiveClick_InterfaceClaseDialogNoHayInternet(DialogFragment dialog) {
        //Reintento mostrar la info
        method_Fuera_Del_On_Create_Chequea_Acceso_A_Internet();

    }

    @Override
    public void onDialogNegativeClick_InterfaceClaseDialogNoHayInternet(DialogFragment dialog) {

        //Devuelve al usuario a los programas
        Intent intent;
        intent = new Intent(this, com.o3j.es.estamosenfiestas.display_frame_layout.
                Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onDialogNeutralClick_InterfaceClaseDialogNoHayInternet(DialogFragment dialog) {

        //No hago nada
    }

//

    private void method_Muestra_Dialog_Si_Hay_Programas_Que_Actualizar() {

        //Utilizo el dialogo general con boton OK
        // Juan, 21 agosto 2015: Muestra el dialogo de inicio de actualizar
        DialogFragment dialog =  ClaseDialogGenericaDosTresBotones.newInstance
                (R.string.titulo_dialog_actualizar, R.string.message_dialog_actualizar_si_habia_que_actualizar, R.string.positiveButton_dialog_actualizar,
                        R.string.negativeButton_dialog_actualizar, 1, R.string.neutralButton_dialog_actualizar);
        dialog.show(getSupportFragmentManager(), "ClaseDialogGenericaDosTresBotones");

    }//Fin de method_Muestra_Dialog_Si_Hay_Programas_Que_Actualizar

    private void method_Muestra_Dialog_No_Hay_Programas_Que_Actualizar() {

        //Utilizo el dialogo general con boton OK
        // Juan, 21 agosto 2015: Muestra el dialogo de inicio de actualizar
        DialogFragment dialog =  ClaseDialogGenericaDosTresBotones.newInstance
                (R.string.titulo_dialog_actualizar, R.string.message_dialog_actualizar_no_habia_que_actualizar, R.string.positiveButton_dialog_actualizar,
                        R.string.negativeButton_dialog_actualizar, 1, R.string.neutralButton_dialog_actualizar);
        dialog.show(getSupportFragmentManager(), "ClaseDialogGenericaDosTresBotones");


    }//Fin de method_Muestra_Dialog_No_Hay_Programas_Que_Actualizar

    private void method_Muestra_Dialog_Error_Durante_La_Actualizacion() {

        //Nuevo dialogo con reintentear, volver a programas, puedo re-usar el de internet
        // Create an instance of the dialog fragment and show it
        DialogFragment dialog =  ClaseDialogNoHayInternet.newInstance
                (R.string.titulo_dialog_actualizar, R.string.message_dialog_actualizar_error, R.string.positiveButton_error_no_hay_internet_reconfigurar,
                        R.string.negativeButton_error_no_hay_internet_reconfigurar, 2, R.string.boton_neutral_evento_guardado);
        dialog.show(getSupportFragmentManager(), "ClaseDialogNoHayInternet");

    }//Fin de method_Muestra_Dialog_Error_Durante_La_Actualizacion



}//Fin de la clase
