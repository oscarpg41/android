package com.o3j.es.estamosenfiestas.display_galeria;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.eef.data.dataelements.DayEvents;
import com.eef.data.dataelements.Event;
import com.eef.data.dataelements.Program;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper;

import java.util.ArrayList;

/**
 * Created by Juan on 22/06/2015.
 */


public class AdaptadorDeSwipeViewsGaleriaDetalle extends FragmentStatePagerAdapter {
    private static String xxx;

    int int_Number_Of_Fragments_To_Show;

    DayEvents day_Events;

    public AdaptadorDeSwipeViewsGaleriaDetalle(FragmentManager fm, ArrayList<Event> arrayListDeEventos, int int_Dia_Seleccionado,
                                               Context context) {
//        public AdaptadorDeSwipeViews_Eventos_Del_Dia(FragmentManager fm, DayEvents eventos_Del_Dia) {
        super(fm);
//        this.day_Events = eventos_Del_Dia;
        this.arrayListDeEventos = arrayListDeEventos;
        this.int_Dia_Seleccionado = int_Dia_Seleccionado; //No lo uso
        this.context = context;
        xxx = this.getClass().getSimpleName();

    }

    ArrayList<Program> arrayList_Programa = new ArrayList<Program>();
    int int_Dia_Seleccionado;
    Context context;
    ArrayList<Event> arrayListDeEventos = new ArrayList<Event>();

    @Override
    public Fragment getItem(int i) {

        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, " : estoy en getItem,  arrayListDeEventos.size= " + arrayListDeEventos.size());
            Log.e(xxx, " : estoy en getItem,  int i= " +i);
        }

        Fragment fragment = getNewFragment(arrayList_Programa, i);
        return fragment;
    }
    public Fragment getNewFragment(ArrayList<Program> arrayList_Programa, int i) {
        Fragment fragment = new FragmentMuestraImagenDetalleConRecycleView();
        Bundle args = new Bundle();

        int int_Nivel_De_Info_Fiesta_Eventos_Del_Dia = i;

        args.putString(FragmentMuestraGaleriaConRecycleView.ARG_OBJECT, "fragment#" +(int_Nivel_De_Info_Fiesta_Eventos_Del_Dia + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(FragmentMuestraGaleriaConRecycleView.ARG_PARAM2, String.valueOf(int_Nivel_De_Info_Fiesta_Eventos_Del_Dia + 1));

        //Le paso el tipo de lista a presentar, aunque en el fragment no hago nada con esta variable
        args.putInt(FragmentMuestraGaleriaConRecycleView.TIPO_DE_LISTA, int_Nivel_De_Info_Fiesta_Eventos_Del_Dia);





        //Guardo en arrayListDeEventoDetalle solo el evento i que lo paso al fragment
        ArrayList<Event> arrayListDeEventoDetalle = new ArrayList<Event>();
        arrayListDeEventoDetalle.add(arrayListDeEventos.get(i));


        //Paso el arrayListDeEventoDetalle
//        args.putSerializable("arrayListDeEventos", arrayListDeEventos);
        args.putSerializable("arrayListDeEventos", arrayListDeEventoDetalle);


        fragment.setArguments(args);

        return fragment;
    }//Fin de getNewFragment




    //GetCount retorna el numero de fragmentos que va a mostrar el swipeView
    @Override
    public int getCount() {

        return arrayListDeEventos.size();
    }


    @Override
    public CharSequence getPageTitle(int position) {

        return arrayListDeEventos.get(position).getSortText();

    }


}//Fin de la clase
