package com.o3j.es.estamosenfiestas.display_frame_layout;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eef.data.dataelements.DayEvents;
import com.eef.data.dataelements.Event;
import com.eef.data.dataelements.EventPlanner;
import com.eef.data.dataelements.Program;
import com.eef.data.eefExceptions.eefException;
import com.eef.data.eeftypes.TypeConfiguration;
import com.eef.data.eeftypes.TypeEefError;
import com.eef.data.eeftypes.TypeState;
import com.eef.data.managers.IConfigurationManager;
import com.eef.data.managers.impl.ProgamListManager;
import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.activity_actualizar_fiestas.ClaseActualizacionAutomatica;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.ClaseChequeaAcceso_A_Internet;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper;
import com.o3j.es.estamosenfiestas.application_configuration.ClaseConfigurationHelper;
import com.o3j.es.estamosenfiestas.dialogs.ClaseDialogEventoGuardado;
import com.o3j.es.estamosenfiestas.dialogs.ClaseDialogNoHayInternet;
import com.o3j.es.estamosenfiestas.display_list_of_favorites.ClaseAvisoAutomaticoDeEventosFavoritos;
import com.o3j.es.estamosenfiestas.display_lists_package.AdaptadorDeSwipeViews_2;
import com.o3j.es.estamosenfiestas.display_lists_package.DividerItemDecoration;
import com.o3j.es.estamosenfiestas.factory.impl.ConfigurationFactory;
import com.o3j.es.estamosenfiestas.nav_drawer_utilities.ClassMenuItemsNavigationDrawer;
import com.o3j.es.estamosenfiestas.nav_drawer_utilities.CustomAdapter_NavigationDrawer_2;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import managers.DatabaseServerManager;


// Juan 2 Junio 2015: Esta clase usa frame layout para ir poniendo y quitando los fragments con la info de los eventos
//usando listas con este orden y sin salirse de esta actividad:
// 1: Se muestra la lista con todas las fiestas validas de ese ayuntamiento.
// 2: Se muestra la lista de la fiesta seleccionada en 1 por dias.
// 3: Se muestra la lista de eventos del dia seleccionado en 2.
// 4: Se muestra el detalle del evento seleccionado en 3, por ahora sin swipe.
public class Activity_Fragment_FrameLayout_WithNavDrawer_2 extends ActionBarActivity
        implements CustomAdapter_RecyclerView_Fiestas.OnItemClickListenerFiestaSeleccionada,
        CustomAdapter_RecyclerView_Fiesta_List_Por_Dias.OnItemClickListenerDiaSeleccionado,
        CustomAdapter_RecyclerView_List_De_Eventos.OnItemClickListenerEventoSeleccionado,
        CustomAdapter_RecyclerView_Evento_Detalle.OnItemClickListenerEventoDetalle,
        CustomAdapter_NavigationDrawer_2.OnItemClickListenerElementoDeMenuSeccionado,
        MiFragment_Fiestas_Con_RecycleView.OnFragmentInteractionListener,
        MiFragment_Fiesta_Seleccionada_Con_RecycleView.OnFragmentInteractionListener,
        MiFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView.OnFragmentInteractionListener,
        MiFragment_Evento.OnFragmentInteractionListener,
        ClaseDialogEventoGuardado.InterfaceClaseDialogEventoGuardado,
        ClaseDialogNoHayInternet.InterfaceClaseDialogNoHayInternet,
        CustomAdapter_RecyclerView_Fiestas.OnItemClickListenerImagenDeLaFiesta{
    //Original con fragment sin loader
//    public class Activity_Fragment_Base_2 extends ActionBarActivity implements MiFragment_1.OnFragmentInteractionListener {
    AdaptadorDeSwipeViews_2 adaptadorDeSwipeViews;
    ViewPager mViewPager;


    Toolbar toolBar_Actionbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        xxx = this.getClass().getSimpleName();

//        Nota: Juan 2 junio: para que funcione el view pager y los tabs tengo que usar el layout activity_fragment_lists_with_nav_drawer_3
//        setContentView(R.layout.activity_fragment_lists_with_nav_drawer_3);


//        Este layout funciona pero al tener el drawer como raiz, tapa la toolbar
//        setContentView(R.layout.activity_fragment_frame_layout_with_nav_drawer);


//        4 junio 2015, Uso este layout para que el navigation drawer no tape la roolbar
        setContentView(R.layout.activity_fragment_frame_layout_with_nav_drawer_3);


//        Toolbar toolBar_Actionbar = (Toolbar) findViewById (R.id.activity_my_toolbar);
         toolBar_Actionbar = (Toolbar) findViewById(R.id.view);
        //Toolbar will now take on default Action Bar characteristics

//        Le cambio el titulo por el ayuntamiento en method_Data_To_Show_In_Fragments_2
//        Para cambiar el tama�o del titulo lo hice con styles como en
//        http://stackoverflow.com/questions/28487312/how-to-change-the-toolbar-text-size-in-android


//        Tengo que poner este titulo para que aparezca el ayuntamiento en method_Data_To_Show_In_Fragments_2
        toolBar_Actionbar.setTitle(getResources().getString(R.string.menu_1));
        toolBar_Actionbar.setTitleTextColor(getResources().getColor(R.color.blanco));
//        toolBar_Actionbar.setTitle("acénto");

//        No uso el subtitulo
//        toolBar_Actionbar.setSubtitle(getResources().getString(R.string.sub_titulo_4));


//        toolBar_Actionbar.getMenu().clear();


        setSupportActionBar(toolBar_Actionbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        //9 septiembre 2015: dejo comentada la bottom_toolbar,
//        final Toolbar bottom_toolbar = (Toolbar) findViewById(R.id.bottom_toolbar);
//        bottom_toolbar.setLogo(R.drawable.ic_launcher);
//        bottom_toolbar.setTitle(getResources().getString(R.string.texto_1));
//        bottom_toolbar.setSubtitle(getResources().getString(R.string.texto_2));
//        bottom_toolbar.setNavigationIcon(R.drawable.ic_launcher);
//        //Toolbar que pongo abajo
//
//        // Set an OnMenuItemClickListener to handle menu item clicks
//        bottom_toolbar.setOnMenuItemClickListener(
//                new Toolbar.OnMenuItemClickListener() {
//                    @Override
//                    public boolean onMenuItemClick(MenuItem item) {
//                        // Handle the menu item
//
//                        int id = item.getItemId();
//
//                        //noinspection SimplifiableIfStatement
//                        if (id == R.id.action_settings) {
//                            Toast.makeText(Activity_Fragment_FrameLayout_WithNavDrawer_2.this, "Bottom toolbar pressed: ", Toast.LENGTH_LONG).show();
//                            return true;
//                        }
//                        return true;
//                    }
//                });
//
//
//        bottom_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(Activity_Fragment_FrameLayout_WithNavDrawer_2.this, "Navigation Icon pressed: ", Toast.LENGTH_LONG).show();
//                // Para esta practica, al presionar el navigation icon lo quito.
//                bottom_toolbar.setVisibility(View.GONE);
//            }
//        });
//
//        // Inflate a menu to be displayed in the toolbar
//        bottom_toolbar.inflateMenu(R.menu.menu_main);
        //FIN 9 septiembre 2015: dejo comentada la bottom_toolbar,




        //Por ahora utilizo este metodo para jugar con el API estatico de Jose Luis, 21 Mayo 2015
//        method_Data_To_Show_In_Fragments();

        //4 Junio 2015, api bueno de JL con acceso a internet etc
        method_Data_To_Show_In_Fragments_2(toolBar_Actionbar);


        //****************************************************************
        //   Juan, 2 junio 2015: No uso los swipe views, quedan comentados
        //****************************************************************
        // ViewPager and its adapters use support library
        // fragments, so use getSupportFragmentManager.
//        adaptadorDeSwipeViews =
//                new AdaptadorDeSwipeViews_2(
//                        getSupportFragmentManager(), int_Number_Of_Fragments_To_Show, List_Programs);
//        mViewPager = (ViewPager) findViewById(R.id.viewpager_1);
//        mViewPager.setAdapter(adaptadorDeSwipeViews);
//
//        //Poner tabs del swipe view
//        metodoPonerTabs_1(mViewPager);
        //****************************************************************
        //   FIN de Juan, 2 junio 2015: No uso los swipe views, quedan comentados
        //****************************************************************



        //Inicializar el nav drawer, 21 Mayo 2015
        method_Init_NavigationDrawer();


        //OJO OJO OJO OJO OJO OJO OJO EL TEMA DE LAS IMAGENES
//        method_Init_Imagen_Del_Ayuntamiento();// Asi funciona OK el emulador, pero se cuelga en la tablet
        //Por falta de memoria

        //Asi, se queda colgado el emulador.
//        final ImageView imageView_en_nav_drawer_abajo = (ImageView) findViewById(R.id.imageView_en_nav_drawer_abajo);
////        imageView_en_nav_drawer_abajo.setImageResource(R.drawable.escudo_san_ildefonso);
//        imageView_en_nav_drawer_abajo.setVisibility(View.VISIBLE);
//
//        imageView_en_nav_drawer_abajo.post(new Runnable() {
//            public void run() {
//                imageView_en_nav_drawer_abajo.setImageResource(R.drawable.escudo_san_ildefonso);
//            }
//        });

        //OJO OJO OJO OJO OJO OJO OJO EL TEMA DE LAS IMAGENES
//        Para resolver estode la memoria, tengo que mirar:
//        http://developer.android.com/training/displaying-bitmaps/load-bitmap.html
//        http://developer.android.com/training/displaying-bitmaps/process-bitmap.html

//        Y tambien poner en google:   out of memory bitmapfactory.decodefile




//        Por ahora, cargo el icono que hice (lo del mipmap):
        //Juan 18 agosto 2015, lo dejo comentado, ahora lo hago con picassa
//        ImageView imageView_en_nav_drawer_abajo = (ImageView) findViewById(R.id.imageView_en_nav_drawer_abajo);
//        imageView_en_nav_drawer_abajo.setImageResource(R.drawable.ic_sanildefonso_logo_correcto_oscar);
//        imageView_en_nav_drawer_abajo.setVisibility(View.VISIBLE);


    }//Fin del onCreate

    public void method_Descarga_Imagen_De_Internet(ImageView imageView, EventPlanner eventPlanner) {
        //Juan 17 agosto 2015: presento la imagen como en:
        //http://themakeinfo.com/2015/04/android-retrofit-images-tutorial/
        //Con esta instruccion:
        //Picasso.with(context).load("http://i.imgur.com/DvpvklR.png").into(imageView);
        //O esta:
        //Picasso.with(getContext()).load(url+flower.getPhoto()).resize(100,100).into(img);

        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, xxx + " : estoy en method_Descarga_Imagen_De_Internet ");
        }

        String url = null;

        try {
            url = eventPlanner.getMultimediaElementList().get(0).getMultimediaElementLocation();
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(xxx,  " : En metodo method_Descarga_Imagen_De_Internet url: " +url);
            }
            if(!Class_ConstantsHelper.methodStaticChequeaURL(url)){//url invalida
                //Original, queda comentada
//                if(url == null || url.isEmpty()){
                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.e(xxx,  " : En metodo method_Descarga_Imagen_De_Internet: url es INVALIDA ");
                }
                imageView.setVisibility(View.GONE);
            }else{//url es valido, invoca la descarga de la imagen
//                Picasso.with(this).load(url).resize(75, 80).into(imageView);
                Picasso.with(this).load(url).fit().centerInside().into(imageView);
                imageView.setVisibility(View.VISIBLE);
                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.e(xxx, " : En metodo method_Descarga_Imagen_De_Internet: url es VALIDA ");
                }
            }
//            Picasso.with(activity_Display_Event_Planner_Information_WithNavDrawer_2).load(url).resize(400, 400).into(imageView);
        } catch (eefException eef) {
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(xxx, xxx + " : En metodo method_Descarga_Imagen_De_Internet en eefException: \" + eef.getLocalizedMessage()");
            }
            //Si hay algun problema, hago invisible la imagen
            imageView.setVisibility(View.GONE);

        }

    }//Fin de method_Descarga_Imagen_De_Internet


    public void method_Init_Imagen_Del_Ayuntamiento() {

       ImageView imageView_en_nav_drawer_abajo = (ImageView) findViewById(R.id.imageView_en_nav_drawer_abajo);
// Load a bitmap from the drawable folder
        Bitmap bMap = BitmapFactory.decodeResource(getResources(), R.drawable.escudo_san_ildefonso);
// Resize the bitmap to 150x100 (width x height)
        Bitmap bMapScaled = Bitmap.createScaledBitmap(bMap, 60, 55, true);
// Loads the resized Bitmap into an ImageView
        imageView_en_nav_drawer_abajo.setImageBitmap(bMapScaled);

        imageView_en_nav_drawer_abajo.setVisibility(View.VISIBLE);


    }//Fin de method_Init_Imagen_Del_Ayuntamiento

//    Juan 2 junio 2015, no uso el view pager ni los tabs, dejo el metodo metodoPonerTabs_1 comentado

//    private void metodoPonerTabs_1(ViewPager mViewPager) {
//
////        Hecho como en: http://guides.codepath.com/android/Google-Play-Style-Tabs-using-SlidingTabLayout
//// Give the SlidingTabLayout the ViewPager
//        SlidingTabLayout slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
//        // Center the tabs in the layout
//        slidingTabLayout.setDistributeEvenly(true);
//        slidingTabLayout.setViewPager(mViewPager);
//        //Colorear el tab
//        slidingTabLayout.setBackgroundColor(getResources().getColor(R.color.primary_color));
//        // Customize tab indicator color
//        slidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
//            @Override
//            public int getIndicatorColor(int position) {
//                return getResources().getColor(R.color.accent_color);
//            }
//        });
//    }


    //**************************************************************************
    //**************************************************************************
    //  24 junio 2015: Setting correcto para no mostrar menu en la toolbar
    //  24 junio 2015: Solo se muestra el Nav Drawer
    //**************************************************************************
    //**************************************************************************


//    Me ayude con:
//    https://github.com/codepath/android_guides/wiki/Fragment-Navigation-Drawer

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;

        // Uncomment to inflate menu items to Action Bar
        // inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = drawerLayout.isDrawerOpen(navigationDrawerRecyclerView);

        //No he inflado el menu, lo dejo comentado
//        menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }// Fin de onPrepareOptionsMenu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this. 21 Mayo 2015
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }//Codigo del nav drawer


        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        if (id == R.id.action_settings) {
//            Toast.makeText(Activity_Fragment_FrameLayout_WithNavDrawer_2.this, " toolbar pressed: ", Toast.LENGTH_LONG).show();
//
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }
    //**************************************************************************
    //**************************************************************************
    //  FIN DE 24 junio 2015: Setting correcto para no mostrar menu en la toolbar
    //  24 junio 2015: Solo se muestra el Nav Drawer
    //**************************************************************************
    //**************************************************************************

    //Implementa la interfaz de la clase MiFragment_Fiestas_Con_RecycleView
//    y la clase MiFragment_Fiesta_Seleccionada_Con_RecycleView

//    OJO: NO ESTOY HACIENDO USO DE ESTA INTERFAZ
    public void onFragmentInteraction(String string) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"Fragmento activo: " +string);

        }
    }



    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //  Inicio de Codigo relacionado con el navigation drawer, 21 Mayo 2015
    //****************************************************************
    //****************************************************************
    private void method_Init_NavigationDrawer() {

        //****************************************************************
        //   Codigo relacionado con recycler view del navigation drawer
        //****************************************************************


        //Inicializar los datos del array del menu a mostrar en el navigationDrawer
        init_arrayList_NavigationDrawer();

        navigationDrawerRecyclerView = (RecyclerView) findViewById(R.id.left_drawer_recycle_view);
//        navigationDrawerRecyclerView.setItemAnimator(new DefaultItemAnimator());

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        navigationDrawerRecyclerView.setHasFixedSize(true);


        //Agregar el separador de la lista en el recycle view
        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        navigationDrawerRecyclerView.addItemDecoration(itemDecoration);

        // END_INCLUDE(initializeRecyclerView)

        // use a linear layout manager
        navigationDrawerLayoutManager = new LinearLayoutManager(this);
        navigationDrawerRecyclerView.setLayoutManager(navigationDrawerLayoutManager);
//        navigationDrawerRecyclerView.setAdapter(new CustomAdapter_NavigationDrawer(

//        Adapter del navigation drawer con la cabecera
                navigationDrawerRecyclerView.setAdapter(new CustomAdapter_NavigationDrawer_2(
                arrayListItemsNavigationDrawer, "0",
                        (CustomAdapter_NavigationDrawer_2.OnItemClickListenerElementoDeMenuSeccionado) this));

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_1);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        //Este objeto permite que el navigation Drawer interactue correctamente con el action bar (la toolbar en mi caso)
        //Tengo que usar el actionBarDrawerToggle de V7 support por que el de V4 esta deprecated
        actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                drawerLayout,         /* DrawerLayout object */
                //Deprecated en V7, esto era valido en V4
//                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
//                getSupportActionBar().setTitle(getResources().getString(R.string.title_activity_main_activity_navigation_drawer));
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()

            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                getActionBar().setTitle(mDrawerTitle);
//                getSupportActionBar().setTitle(getTitle());
//                getSupportActionBar().setTitle(getResources().getString(R.string.titulo_navigation_drawer));
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()


            }
        };//Fin de actionBarDrawerToggle

        // Set the drawer toggle as the DrawerListener
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //****************************************************************
        //  FIN Codigo relacionado con recycler view del navigation drawer
        //****************************************************************


    }//Fin de method_Init_NavigationDrawer





    //Datos del menu del navigation drawer
    private void init_arrayList_NavigationDrawer() {//Relacionado con recycler view

        String[] array_menu = {getString(R.string.menu_1),getString(R.string.menu_2),getString(R.string.menu_3)
                        ,getString(R.string.menu_4),getString(R.string.menu_6),getString(R.string.menu_7), getString(R.string.menu_5)};

        arrayListItemsNavigationDrawer = new ArrayList<ClassMenuItemsNavigationDrawer>();
        for (int i = 0; i < DATASET_COUNT; i++) {
            arrayListItemsNavigationDrawer.add(
                    ClassMenuItemsNavigationDrawer.newInstance(array_menu[i],
                            " ",
                            R.drawable.ic_launcher));
        }
    }



    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    //Si estos metodos no se llaman, no aparece la hamburguesa
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    //Variables para el recycler view
    private DrawerLayout drawerLayout;
    private RecyclerView navigationDrawerRecyclerView;
    private RecyclerView.Adapter navigationDrawerAdapter;
    private RecyclerView.LayoutManager navigationDrawerLayoutManager;
    protected ArrayList<ClassMenuItemsNavigationDrawer> arrayListItemsNavigationDrawer = null;
    private static final int DATASET_COUNT = 7;
    //Este objeto permite que el navigation Drawer interactue correctamente con el action bar (la toolbar en mi caso)
    private ActionBarDrawerToggle actionBarDrawerToggle;
    //FIN Variables para el recycler view

    //****************************************************************
    //****************************************************************
    //     FIN del Codigo relacionado con el navigation drawer
    //****************************************************************
    //****************************************************************




    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //  Inicio de Codigo relacionado con el API de JL, 21 Mayo 2015
    //****************************************************************
    //****************************************************************
    private void method_Data_To_Show_In_Fragments() {
//        ESte metodo obtiene datos del api de JL, pero habra que cambiar cosas.

        ConfigurationFactory cmTFactory = new ConfigurationFactory();

        try {
            myCmT = cmTFactory.getConfigurationManager(TypeConfiguration.INSTALLATION_DEFAULT_WITH_AUTONOMOUS_COMMUNITY);

            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(xxx, xxx +" Esta bien configurado [" + myCmT.isRightConfigured() + "]");
                Log.d(xxx, xxx +" El cmt esta configurado[" + myCmT.isRightConfigured() + "] numero AU[" + myCmT.getAutonomousCommunityList().size() + "]");
                Log.d(xxx, xxx +" La comunidad Autonoma Configurada[" + myCmT.getAutonomousCommunityConfigured().getAutonomousCommunityName()
                        + "] y el pueblo configurado[" + myCmT.getEventPlannerConfigured().getName() + "]");
            }

            ProgamListManager pmT = new ProgamListManager();
            pmT.loadData(myCmT.getAutonomousCommunityConfigured(),myCmT.getEventPlannerConfigured());
            if(pmT.existCurrentProgram()){
                Program p = pmT.getCurrentProgram();

                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(xxx, xxx +"HAY PROGRAMA EN MARCHA ["+p.getProgramSortDescription()+"]");

                }
                //Obtener la lista de eventos del programa por dias
                List<DayEvents> ltd = p.getEventByDay();
                //Obtener la lista de eventos completa
                List<Event> ltf =p.getFullList();
            }



            if(pmT.existCurrentProgram() || pmT.existFuturetPrograms() || pmT.existPastPrograms()) {
                 List_Programs = pmT.getFullProgramList();


                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(xxx, "HAY  [" + List_Programs.size() + "]" +" PROGRAMAS EN MARCHA");
                    for (int i=0; i<List_Programs.size(); i++){
                        Log.d(xxx, xxx +" Codigo del programa numero  [" +i + ": "
                                +List_Programs.get(i).getState().getCode() + "]" +" PROGRAMAS EN MARCHA");

                        Log.d(xxx, xxx +" Codigo del programa numero  [" +i + ": "
                                +List_Programs.get(i).getProgramSortDescription() + "]" +" PROGRAMAS EN MARCHA");

                        if(List_Programs.get(i).getState().getCode().equals(TypeState.ENDED)){
                            int_Number_Of_Fragments_To_Show++;
                            List_Programs.remove(i);
                        }

                    }
                }



                for (int i=0; i<List_Programs.size(); i++){
                    arrayList_Lista_De_Fiesta_Serializable.add(List_Programs.get(i));
                }
                method_Muestra_Fragment_Con_La_Lista_De_Las_Fiestas(int_Nivel_De_Info_Fiestas);


            }



//            Juan, 10 junio 2015, VER ESTA FORMA DE PASAR PARAMETROS CON PUNTOS SUSPENSIVOS Y EL FOR CON LOS : PUNTOS

//            private static void toggleVisibility(View... views) {
//                for (View view : views) {
//                    boolean isVisible = view.getVisibility() == View.VISIBLE;
//                    view.setVisibility(isVisible ? View.INVISIBLE : View.VISIBLE);
//                }
//            }

        }
        catch(eefException eef)
        {
            Log.e("onCreate","Error inciando la conficuracion");
        }
        catch (Exception ex){
            Log.e("onCreate","Excepotion no eef inciando la configuracion");
        }


    }//Fin de method_Data_To_Show_In_Fragments


    IConfigurationManager myCmT=null;
    private static String xxx;
    List<Program> List_Programs;
    int int_Number_Of_Fragments_To_Show = 0;
    ArrayList<Program> arrayList_Lista_De_Fiesta_Serializable = new ArrayList<Program>();

    int int_Nivel_De_Info_Fiestas = 1;
    int int_Nivel_De_Info_Fiesta_Por_Dias = 2;
    int int_Nivel_De_Info_Fiesta_Eventos_Del_Dia = 3;
    int int_Nivel_De_Info_Fiesta_Evento_Detalle = 4;


    //****************************************************************
    //****************************************************************
    //     FIN del Codigo relacionado con el API de JL
    //****************************************************************
    //****************************************************************
    Fragment newFragment;
    public void method_Muestra_Fragment_Con_La_Lista_De_Las_Fiestas(int int_Nivel_De_Info) {
// Create new fragment and transaction


//        Fragment newFragment = getNewFragmnet(int_Nivel_De_Info);
         newFragment = getNewFragmnet(int_Nivel_De_Info);
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack

        if (int_Nivel_De_Info == int_Nivel_De_Info_Fiestas) {
            transaction.add(R.id.details, newFragment);
        }
        else{
            transaction.replace(R.id.details, newFragment);
            transaction.addToBackStack(null);
        }

// Commit the transaction
        transaction.commit();
    }// Fin de method_Muestra_Fragment_Con_La_Lista_De_Las_Fiestas

    public Fragment getNewFragmnet(int i) {
        Fragment fragment = new MiFragment_Fiestas_Con_RecycleView();
        Bundle args = new Bundle();
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
        args.putString(MiFragment_Fiestas_Con_RecycleView.ARG_OBJECT, "fragment#" +(i + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiestas_Con_RecycleView.ARG_PARAM2, String.valueOf(i + 1));

        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiestas_Con_RecycleView.TIPO_DE_LISTA, i);


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
        args.putSerializable("Lista de Programas Serializado", arrayList_Lista_De_Fiesta_Serializable);


        fragment.setArguments(args);

        return fragment;
    }

    //    Interfaz de la clase CustomAdapter_RecyclerView_Fiestas.
    @Override
    public void onClick(Program m_Programa_seleccionado) {

        //****************************************************************************************
//        //Lo dejo comentado para no mostrar la lista de dias, segunda pantalla
//        Fragment newFragment = getNewFragmentForFiesta(m_Programa_seleccionado);
////        FragmentTransaction transaction = getFragmentManager().beginTransaction();
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//
//// Replace whatever is in the fragment_container view with this fragment,
//// and add the transaction to the back stack
//
//        transaction.replace(R.id.details, newFragment);
//        transaction.addToBackStack(null);
//        // Commit the transaction
//        transaction.commit();
        //****************************************************************************************

        //11 julio 2015: Hago esta llamada para no mostrar la segunda pantalla segun acorde con Oscar.

        //Dejo comentado todo lo anterior arriba

        //La segunda pantalla es redundante con la vista deslizante de eventos por dia.
//        method_Envia_Intent_Al_SwipeView_De_Eventos_Del_Dia(int_Dia_Seleccionado);
        //El int_Dia_Seleccionado lo pongo a cero para que muestre el primer elemento.
        //Pero tengo que hacer el algoritmo para mostrar en la vista deslizante el dia mas
        //cercano al dia actual en la activity Activity_Fragment_SwipeView_EventosDelDia_BackToParent

        //11 julio 2015: field para pasar el programa seleccionado a la clase de swipe views para eventos del dia
        //m_Programa_seleccionado lo usa el intent del method method_Envia_Intent_Al_SwipeView_De_Eventos_Del_Dia
        this.m_Programa_seleccionado = m_Programa_seleccionado;
        Class_ConstantsHelper.titulo_De_La_Fiesta = m_Programa_seleccionado.getProgramSortDescription();
        method_Envia_Intent_Al_SwipeView_De_Eventos_Del_Dia(0);
    }//Fin de onClick


    public Fragment getNewFragmentForFiesta(Program m_Programa_seleccionado) {
        Fragment fragment = new MiFragment_Fiesta_Seleccionada_Con_RecycleView();
        Bundle args = new Bundle();
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "fragment#" +(int_Nivel_De_Info_Fiesta_Por_Dias + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, String.valueOf(int_Nivel_De_Info_Fiesta_Por_Dias + 1));

        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, int_Nivel_De_Info_Fiesta_Por_Dias);


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
        args.putSerializable("Programa Serializado", m_Programa_seleccionado);


        fragment.setArguments(args);

        //22 Junio 2015: field para pasar el programa seleccionado a la clase de swipe views para eventos del dia
        this.m_Programa_seleccionado = m_Programa_seleccionado;

        return fragment;
    }

    Program m_Programa_seleccionado;
//    Interfaz de la clase CustomAdapter_RecyclerView_Fiesta_List_Por_Dias.
    @Override
    public void onClickDia(DayEvents m_DayEvents) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"He llegado a onClickDia" );
        }
        //22 Junio 2015: No genero este fragment, salto a la actividad de swipe views eventos del dia
//        Fragment newFragment = getNewFragmentForDayEvents(m_DayEvents);
////        FragmentTransaction transaction = getFragmentManager().beginTransaction();
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//
//// Replace whatever is in the fragment_container view with this fragment,
//// and add the transaction to the back stack
//
//        transaction.replace(R.id.details, newFragment);
//        transaction.addToBackStack(null);
//        // Commit the transaction
//        transaction.commit();
        //FIN DE 22 Junio 2015: No genero este fragment, salto a la actividad de swipe views eventos del dia


//        Para encontrar el dia seleccionado, comparo los date de la lista de day evenets con el date de
//        m_DayEvents que es el dia clickado en la lista

        int int_Dia_Seleccionado = 0;

        for (int i = 1; i < m_Programa_seleccionado.getEventByDay().size(); i++) {
            if (m_Programa_seleccionado.getEventByDay().get(i).getProgramEventDate() ==
                    m_DayEvents.getProgramEventDate()) {

                int_Dia_Seleccionado = i;
            }
        }

        method_Envia_Intent_Al_SwipeView_De_Eventos_Del_Dia(int_Dia_Seleccionado);

    }

    private void method_Envia_Intent_Al_SwipeView_De_Eventos_Del_Dia(int int_Dia_Seleccionado) {
        //22 Junio 2015: salto a la actividad de swipe views eventos del dia
        Intent intent;

        intent = new Intent(this, com.o3j.es.estamosenfiestas.display_vista_deslizante_1.
                Activity_Fragment_SwipeView_EventosDelDia_BackToParent.class);

        //22 junio 2015, datos que se pasan en un bundle a la actividad con swipe views eventos del dia
        Bundle args_Datos_Para_SwipeView_De_Eventos_Del_Dia = new Bundle();

//        Hago esto por que la lista de eventos no es serializable, asi que lo pongo en array para
//                pasarlo al fragment con la lista de los eventos del dia seleccionado
        ArrayList<Program> arrayList_Programa = new ArrayList<Program>();
        arrayList_Programa.add(m_Programa_seleccionado);


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
        args_Datos_Para_SwipeView_De_Eventos_Del_Dia.putSerializable("Fiesta_Serializada", arrayList_Programa);

        args_Datos_Para_SwipeView_De_Eventos_Del_Dia.putInt("int_Dia_Seleccionado", int_Dia_Seleccionado);

        intent.putExtras(args_Datos_Para_SwipeView_De_Eventos_Del_Dia);

        startActivity(intent);


        //FIN DE 22 Junio 2015: salto a la actividad de swipe views eventos del dia
    }//FIN DE method_Envia_Intent_Al_SwipeView_De_Eventos_Del_Dia

    public Fragment getNewFragmentForDayEvents(DayEvents m_DayEvents) {
        Fragment fragment = new MiFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView();
        Bundle args = new Bundle();
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "fragment#" +(int_Nivel_De_Info_Fiesta_Eventos_Del_Dia + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, String.valueOf(int_Nivel_De_Info_Fiesta_Eventos_Del_Dia + 1));

        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, int_Nivel_De_Info_Fiesta_Eventos_Del_Dia);

//        Hago esto por que la lista de eventos no es serializable, asi que lo pongo en array para
//                pasarlo al fragment con la lista de los eventos del dia seleccionado
        ArrayList<Event> arrayList_Lista_De_Eventos_Del_Dia = new ArrayList<Event>();
        for (int i=0; i<m_DayEvents.getListaEventos().size(); i++){
            arrayList_Lista_De_Eventos_Del_Dia.add(m_DayEvents.getListaEventos().get(i));
        }


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
        args.putSerializable("Eventos del dia Serializado", arrayList_Lista_De_Eventos_Del_Dia);


        fragment.setArguments(args);

        return fragment;
    }

    //    Interfaz de la clase CustomAdapter_RecyclerView_List_De_Eventos.
    @Override
    public void onClickEvento(Event m_Event) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"He llegado a onClickEvento" );
        }
        Fragment newFragment = getNewFragmentToShowEvent(m_Event);
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack

        transaction.replace(R.id.details, newFragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }

    public Fragment getNewFragmentToShowEvent(Event m_Event) {
        Fragment fragment = new MiFragment_Evento();
        Bundle args = new Bundle();
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "fragment#" + (int_Nivel_De_Info_Fiesta_Evento_Detalle + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, String.valueOf(int_Nivel_De_Info_Fiesta_Evento_Detalle + 1));
        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, int_Nivel_De_Info_Fiesta_Evento_Detalle);

        args.putSerializable("Datos del Evento Serializado", m_Event);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onClickEventoDetalle(Event m_Event) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"He llegado a onClickEventoDetalle" );
        }
    }



    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //  4 Junio 2015 method_Data_To_Show_In_Fragments_2
    //****************************************************************
    //****************************************************************

    //Esta variable solo es para pintar el icono del event planner
    private ArrayList<EventPlanner> arrayList_Event_Planner_Information = new ArrayList<EventPlanner>();

    private void method_Data_To_Show_In_Fragments_2(Toolbar toolBar_Actionbar) {
//        ESte metodo obtiene datos del api de JL, pero habra que cambiar cosas.

        ConfigurationFactory cmTFactory = new ConfigurationFactory();
        cmTFactory.initFactory(this, "nada_1", "nada2" );


        try {
//            myCmT = cmTFactory.getConfigurationManager(TypeConfiguration.INSTALLATION_DEFAULT_WITH_AUTONOMOUS_COMMUNITY);

//            12 junio 2015: chequea si se ha cambiado la configuracion con el api viejo de JL, ya no lo uso
            if (Class_ConstantsHelper.boolean_Hay_Una_nueva_configuracion) {
                myCmT = Class_ConstantsHelper.m_IConfigurationManager;
            } else {
                //15 junio 2015: Con este no vuelve a mostrar san ildefonso despues de un cambio de pueblo
//                myCmT = cmTFactory.getConfigurationManager(TypeConfiguration.INSTALLATION_DEFAULT_WITH_AUTONOMOUS_COMMUNITY);

                //15 junio 2015: Con este no vuelve a mostrar san ildefonso despues de un cambio de pueblo
                myCmT = cmTFactory.getConfigurationManager(TypeConfiguration.USER_CONFIGURATION);

                //********************************************************************************
                //Juan 18 agosto 2015, para pintar el icono del ayuntamiento con picassa
                EventPlanner eventPlanner = myCmT.getEventPlannerConfigured();
                arrayList_Event_Planner_Information.add(myCmT.getEventPlannerConfigured());
                ImageView imageView_en_nav_drawer_abajo = (ImageView) findViewById(R.id.imageView_en_nav_drawer_abajo);
                //Juan 17 agosto 2015: presento la imagen como en:
                //http://themakeinfo.com/2015/04/android-retrofit-images-tutorial/
                //Con esta instruccion:
                //Picasso.with(context).load("http://i.imgur.com/DvpvklR.png").into(imageView);
                method_Descarga_Imagen_De_Internet(imageView_en_nav_drawer_abajo, eventPlanner);
                //Copio el eventPlanner en la variable estatica
                Class_ConstantsHelper.eventPlanner = eventPlanner;
                //*************************************************************************************

                //15 junio 2015: Con este no funciona al empezar la app
//                myCmT = cmTFactory.getConfigurationManager(TypeConfiguration.AUTONOMOUS_COMMUNITY_ENABLED);
            }







            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(xxx, xxx +" Esta bien configurado [" + myCmT.isRightConfigured() + "]");
                Log.e(xxx, xxx +" El cmt esta configurado[" + myCmT.isRightConfigured() + "] numero AU[" + myCmT.getAutonomousCommunityList().size() + "]");
                Log.e(xxx, xxx +" La comunidad Autonoma Configurada[" + myCmT.getAutonomousCommunityConfigured().getAutonomousCommunityName()
                        + "] y el pueblo configurado[" + myCmT.getEventPlannerConfigured().getName() + "]");
            }



            DatabaseServerManager databaseServerManager = new DatabaseServerManager();
            databaseServerManager.initManager(this);
            List_Programs = databaseServerManager.getFullProgramList();

            //Asi, aparece el nombre del ayuntamiento
//            toolBar_Actionbar.setTitle(myCmT.getEventPlannerConfigured().getName());
            //Asi aparece el pueblo
            toolBar_Actionbar.setTitle(myCmT.getEventPlannerConfigured().getSortDescription());

            //8 sept 2015: Ahora pongo el nombre del event planner en un text view para que
            //aparezca a la derecha del logo
//            <!-- Juan 8 sept 2015: Nuevo linear layout por que oscar queria que apareciera
//            el logo al lado del nav drawer y el nombre del eventplanner a la derecha del logo.
//            Como he puesto match parent en width, tapa cuando hago set title del toolbar arriba-->
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linear_eventplaner_logo_y_nombre);
            linearLayout.setVisibility(View.VISIBLE);

            TextView textview_nombre_del_event_planer = (TextView) findViewById(R.id.textview_nombre_del_event_planer);
            textview_nombre_del_event_planer.setText(myCmT.getEventPlannerConfigured().getSortDescription());

//            Class_ConstantsHelper.string_Nombre_Del_Pueblo_O_Ayuntamiento = myCmT.getEventPlannerConfigured().getName();
            Class_ConstantsHelper.string_Nombre_Del_Pueblo_O_Ayuntamiento = myCmT.getEventPlannerConfigured().getSortDescription();

            for (int i=0; i<List_Programs.size(); i++){
                arrayList_Lista_De_Fiesta_Serializable.add(List_Programs.get(i));
            }

            method_Muestra_Fragment_Con_La_Lista_De_Las_Fiestas(int_Nivel_De_Info_Fiestas);



            //4 septiembre 2015: puse aqui esta llamada SOLO PARA PRUEBAS. Este no es el sitio correcto de esta llamada
//            methodNoSePuedenMostrarProgramas();




            //Juan, 31 agosto 2015, prueba de actualizacion automatica
//            methodLanzarActualizacionAutomaticaFake();


            //10 septiembre 2015: Probando las notificaciones de eventos favoritos
            methodLanzarChequeoDeFavoritosParaNotificaciones();

            //Juan, 6 oct 2015: lanzo la actualizacion si esta checked por que
            //Creo que al actualizar la app, las alarmas repetitivas se paran todas!!
            //Asi que por si acaso, hago esto
            methodLanzarActualizacionAutomatica(this);


        }
        catch(eefException eef)//Error de myCmT = cmTFactory.getConfigurationManager(TypeConfiguration.USER_CONFIGURATION);
        {
            Log.e("onCreate" + xxx, "Error eef  inciando la conficuracion: " + eef.getLocalizedMessage());




            ClaseConfigurationHelper claseConfigurationHelper =
                    ClaseConfigurationHelper.newInstance(this);
            //4 septiembre 2015: chequeo si he intentado instalar, para que en caso de que haya fallado
            //la instalacion, no entre en un loop cada vez que vuelvo a programas si ha fallado la instalacion
            //por que no hay programas en el server en ese momento disponibles
            if (!claseConfigurationHelper.methodEstoyConfigurando()) {


                //17 agosto 2015: la app no esta configurada, va a la actividad de instalacion si hay conexion a INTERNET
                if (eef.getLocalizedMessage().equals(TypeEefError.EEF_NOT_CONFIGURED.getErrorText())) {
                    if (method_Dime_Si_Hay_Red()) {//Chuequeamos si hay red
                        //Indico que estoy instalando con esta variable:
                        Class_ConstantsHelper.boolean_estoy_instalando = true;
                        Intent intent = new Intent(this, com.o3j.es.estamosenfiestas.application_configuration.
                                Activity_Application_Configuration_WithNavDrawer_2.class);
                        startActivity(intent);
                        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                            Log.e(xxx, xxx + " : estoy en catch eefException)");
                        }


                        finish();
                    } else {//NO hay red
                        // Create an instance of the dialog fragment and show it
                        DialogFragment dialog = ClaseDialogNoHayInternet.newInstance
                                (R.string.titulo_dialog_error_no_hay_internet, R.string.message_dialog_error_no_hay_internet, R.string.positiveButton_error_no_hay_internet,
                                        R.string.negativeButton_error_no_hay_internet, 2, R.string.boton_neutral_evento_guardado);
                        dialog.show(getSupportFragmentManager(), "ClaseDialogNoHayInternet");
                    }
                } else {
                    //Caso que maneja cualquier condicion que impida mostrar los programas de fiestas
                    methodNoSePuedenMostrarProgramas();
                }

            } else {
                //Caso que maneja cualquier condicion que impida mostrar los programas de fiestas
                methodNoSePuedenMostrarProgramas();
            }


        }
        catch (Exception ex){//error de DatabaseServerManager databaseServerManager = new DatabaseServerManager();
            Log.e("onCreate" +xxx,"Excepotion no eef inciando la configuracion: " +ex.getLocalizedMessage());
            //Caso que maneja cualquier condicion que impida mostrar los programas de fiestas
            methodNoSePuedenMostrarProgramas();
        }
    }//Fin de method_Data_To_Show_In_Fragments_2

    public void methodLanzarActualizacionAutomatica(Context context) {

        ClaseActualizacionAutomatica claseActualizacionAutomatica =  ClaseActualizacionAutomatica.newInstance(context);
        //Averiguo  si hay que lanzar las actualizaciones automaticas.
        if(claseActualizacionAutomatica.methodGetBooleanCheckBoxChecked()){
            claseActualizacionAutomatica.methodIniciaActualizacionAutomatica("fake1", "fake2");
        }
    }

    public void methodLanzarChequeoDeFavoritosParaNotificaciones() {
        ClaseAvisoAutomaticoDeEventosFavoritos claseAvisoAutomaticoDeEventosFavoritos =
                ClaseAvisoAutomaticoDeEventosFavoritos.newInstance(this);
        claseAvisoAutomaticoDeEventosFavoritos.methodDetenerArrancarChequeoAutomaticodeFavoritos();
    }

    public void methodNoSePuedenMostrarProgramas() {

//        CardView cardView = (CardView) findViewById(R.id.card_View_lista_de_programas_vacia);
//        cardView.setVisibility(View.VISIBLE);
//
//        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.details);
//        frameLayout.setVisibility(View.GONE);

        arrayList_Lista_De_Fiesta_Serializable.clear();
        method_Muestra_Fragment_Con_La_Lista_De_Las_Fiestas(int_Nivel_De_Info_Fiestas);



    }//Fin de methodNoSePuedenMostrarProgramas

    public void methodLanzarActualizacionAutomaticaFake() {//Solo esta aqui para las pruebas que hice. Todo OK

        ClaseActualizacionAutomatica claseActualizacionAutomatica =  ClaseActualizacionAutomatica.newInstance(this);
        claseActualizacionAutomatica.methodIniciaActualizacionAutomatica("fake1", "fake2");

    }


    public boolean method_Dime_Si_Hay_Red() {

        //antes de lanzar cualquier servicio web al server, verifico si hay acceso a internet
        ClaseChequeaAcceso_A_Internet claseChequeaAcceso_A_Internet = new ClaseChequeaAcceso_A_Internet(this);
        if(claseChequeaAcceso_A_Internet.isOnline()) {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(xxx, xxx +"He llegado a method_Dime_Si_Hay_Red: SI" );
            }
            return true;
        }else {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(xxx, xxx +"He llegado a method_Dime_Si_Hay_Red: NO" );
            }
            return false;
        }
    }

    @Override
    public void onClickMenuItenDelNavDrawer(int position_Del_Menu_Seleccionada) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"He llegado a onClickMenuItenDelNavDrawer  con click: " +position_Del_Menu_Seleccionada );
        }

//        Intent intent = new Intent(this, com.o3j.es.estamosenfiestas.activity_actualizar_fiestas.
//                Activity_Fragment_Actualizar_Fiesta_WithNavDrawer_2.class);
//        startActivity(intent);


        Intent intent;


        switch (position_Del_Menu_Seleccionada)
        {
            case 0:
//                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_frame_layout.
//                        Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
//                startActivity(intent);

                drawerLayout.closeDrawer(Gravity.LEFT);

                break;
            case 1://Mis Eventos
                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_list_of_favorites.
                        Activity_Fragment_List_Of_Favorites_With_Nav_Drawer.class);
                startActivity(intent);
                break;
            case 2:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.activity_actualizar_fiestas.
                        Activity_Fragment_Actualizar_Fiesta_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 3:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.application_configuration.
                        Activity_Application_Configuration_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 4:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_event_planner_information.
                        Activity_Display_Event_Planner_Information_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 5:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.activity_compartir.
                        Activity_Fragment_Compartir_App_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 6:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_informacion.
                        Activity_Display_Developer_Information_WithNavDrawer_2.class);
                startActivity(intent);
                break;
        }

        if (position_Del_Menu_Seleccionada != 0) finish();




//        if(position_Del_Menu_Seleccionada == 2){
//            //Solo muestra el toast
//            Toast.makeText(this, "Opción no disponible", Toast.LENGTH_LONG).show();
//        }else{
//        if (position_Del_Menu_Seleccionada != 0) finish();
//        }
    }


    //****************************************************************
    //****************************************************************
    //     9 Junio 2015: Codigo relacionado con el contextual action mode para
    //    insetar un evento en la lista de favoritos.
    //    Este interfaz es de la clase: CustomAdapter_RecyclerView_List_De_Eventos
    //    Al hacer long click en un evento
    //****************************************************************
    //****************************************************************
    @Override
    public boolean onLongClickEvento_CustomA_List_De_Eventos(Event m_Event, View m_View_Del_Evento) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +" He llegado a onLongClickEvento_CustomA_List_De_Eventos  con click: " );
        }


        m_Event_Poner_en_favoritos = m_Event;

        if (mActionMode != null) {
            return false;
        }

        // Start the CAB using the ActionMode.Callback defined above
        mActionMode = this.startActionMode(mActionModeCallback);
        m_View_Del_Evento.setSelected(true);


        return true;
    }




    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_contextual_action_mode_evento_de_la_lista, menu);
            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_agregar_a_favoritos:
                    if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                        Log.d(xxx, xxx +" action_agregar_a_favorito " );
                    }

                    //19 agosto, este metodo ya no lo uso
//                    methodFake_Add_To_List_Of_Favorites();
                    
                    //19 agosto 2015: Uso la clase Clase_Add_To_List_Of_Favorites que tiene el metodo method_Add_To_List_Of_Favorites
                    Clase_Add_To_List_Of_Favorites clase_add_to_list_of_favorites =
                            Clase_Add_To_List_Of_Favorites.newInstance(m_Event_Poner_en_favoritos,
                                    Activity_Fragment_FrameLayout_WithNavDrawer_2.this);
                    clase_add_to_list_of_favorites.method_Add_To_List_Of_Favorites();
//                    shareCurrentItem();
                    mode.finish(); // Action picked, so close the CAB

                    return true;


                case R.id.action_enviar_mail:
                    if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                        Log.d(xxx, xxx +" action_enviar_mail " );
                    }

                    methodFake_Enviar_Mail();
//                    shareCurrentItem();
                    mode.finish(); // Action picked, so close the CAB

                    return true;



                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };


    ActionMode mActionMode;

//    12 junio 2015 metodo fake para agregar elementos a la lista de favoritos
//    Este metodo no lo uso cuando la api de JL este terminada

    Event m_Event_Poner_en_favoritos;
    private void methodFake_Add_To_List_Of_Favorites() {

        boolean boolean_evento_guardado = true;

        Class_Modelo_De_Datos_Lista_Favoritos m_Class_Modelo_De_Datos_Lista_Favoritos =
                new Class_Modelo_De_Datos_Lista_Favoritos();

        m_Class_Modelo_De_Datos_Lista_Favoritos.setStringTituloDeLaFiesta(Class_ConstantsHelper.titulo_De_La_Fiesta);
        m_Class_Modelo_De_Datos_Lista_Favoritos.setStringFechaDelEvento(Class_ConstantsHelper.fecha_Del_Dia);
        m_Class_Modelo_De_Datos_Lista_Favoritos.setEventEventoFavorito(m_Event_Poner_en_favoritos);
        Class_ConstantsHelper.arraylist_Datos_Lista_Favoritos.add(m_Class_Modelo_De_Datos_Lista_Favoritos);

        //*************************************************************************************************
//        Mostrar el dialogo de has guardado en favoritos

        if (boolean_evento_guardado) {

            // Create an instance of the dialog fragment and show it
            DialogFragment dialog =  ClaseDialogEventoGuardado.newInstance
                    (R.string.titulo_dialog_evento_guardado, R.string.message_dialog_evento_guardado, R.string.positiveButton,
                            R.string.negativeButton, 1, R.string.boton_neutral_evento_guardado);
            dialog.show(getSupportFragmentManager(), "ClaseDialogGenericaDosTresBotones");
        }else{
            DialogFragment dialog =  ClaseDialogEventoGuardado.newInstance
                    (R.string.titulo_dialog_evento_guardado, R.string.message_dialog_evento_guardado_Error, R.string.positiveButton,
                            R.string.negativeButton, 1, R.string.boton_neutral_evento_guardado);
            dialog.show(getSupportFragmentManager(), "ClaseDialogGenericaDosTresBotones");
        }

        //*************************************************************************************************

    }

    private void methodFake_Enviar_Mail() {

//        Class_Modelo_De_Datos_Lista_Favoritos m_Class_Modelo_De_Datos_Lista_Favoritos =
//                new Class_Modelo_De_Datos_Lista_Favoritos();
//
//        m_Class_Modelo_De_Datos_Lista_Favoritos.setStringTituloDeLaFiesta(Class_ConstantsHelper.titulo_De_La_Fiesta);
//        m_Class_Modelo_De_Datos_Lista_Favoritos.setStringFechaDelEvento(Class_ConstantsHelper.fecha_Del_Dia);
//        m_Class_Modelo_De_Datos_Lista_Favoritos.setEventEventoFavorito(m_Event_Poner_en_favoritos);
//        Class_ConstantsHelper.arraylist_Datos_Lista_Favoritos.add(m_Class_Modelo_De_Datos_Lista_Favoritos);




        //*************************************************************************
//        String to = textTo.getText().toString();
//        String subject = textSubject.getText().toString();
//        String message = textMessage.getText().toString();

        String to = "";
        String subject = Class_ConstantsHelper.titulo_De_La_Fiesta;
//        String message = m_Event_Poner_en_favoritos.getSortText();
//         message.app = m_Event_Poner_en_favoritos.getSortText();

        //Escribo la fecha de la fiesta
        SimpleDateFormat sfd = new SimpleDateFormat("EEEE dd LLLL yyyy");

        StringBuilder strBuilder = new StringBuilder(m_Event_Poner_en_favoritos.getSortText());
        strBuilder.append("\n");
//        strBuilder.append(Class_ConstantsHelper.fecha_Del_Dia);
        strBuilder.append(sfd.format(m_Event_Poner_en_favoritos.getStartDate()));
        strBuilder.append("\n");
        strBuilder.append("Hora del evento: ");

        DateFormat df2 = new SimpleDateFormat("HH:mm");
//        df2.format(m_Event_Poner_en_favoritos.getStartDate());
        strBuilder.append(df2.format(m_Event_Poner_en_favoritos.getStartDate()));


        strBuilder.append("\n");
        strBuilder.append(m_Event_Poner_en_favoritos.getLongText());
        String message = strBuilder.toString();

        //10 julio 2015, original que solo enviaba e-mail
//        Intent intent_email = new Intent(Intent.ACTION_SEND);
//        intent_email.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
//        intent_email.putExtra(Intent.EXTRA_SUBJECT, subject);
//        intent_email.putExtra(Intent.EXTRA_TEXT, message);
//        //need this to prompts email client only
//        intent_email.setType("message/rfc822");
//        if (intent_email.resolveActivity(getPackageManager()) != null) {
//            startActivity(Intent.createChooser(intent_email, "Choose an Email client :"));
//        }
        //**************************************************************************************

        //10 julio 2015, primera version para usar facebook, twitter, etc
        Intent intent_Generico = new Intent(Intent.ACTION_SEND);
        intent_Generico.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent_Generico.putExtra(Intent.EXTRA_TEXT, message);
        //need this to prompts email client only
        intent_Generico.setType("text/plain");

        if (intent_Generico.resolveActivity(getPackageManager()) != null) {
            startActivity(Intent.createChooser(intent_Generico, "Choose an application :"));
        }

    }
    //****************************************************************
    //****************************************************************
    //     FIN de 9 Junio 2015: Codigo relacionado con el contextual action mode para
    //    insetar un evento en la lista de favoritos
    //****************************************************************
    //****************************************************************

    //****************************************************************
    //****************************************************************
    //     11 junio 2015, Instalacion
    //     Inicio de todo lo relacionado con la instalacion
    //****************************************************************
    //****************************************************************
    IConfigurationManager myCmT_Instalacion =null;

    private void method_Gestion_De_Instalacion_de_Pueblos(){

        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"  En metodo method_Gestion_De_Instalacion_de_Pueblos");

        }

        ConfigurationFactory cmTFactory = new ConfigurationFactory();

        try {
            myCmT_Instalacion = cmTFactory.getConfigurationManager(TypeConfiguration.INSTALLATION_DEFAULT_WITH_AUTONOMOUS_COMMUNITY);

            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(xxx, xxx +" Esta bien configurado_2 [" + myCmT_Instalacion.isRightConfigured() + "]");
                Log.d(xxx, xxx +" El cmt esta configurado_2[" + myCmT_Instalacion.isRightConfigured() + "] numero AU["
                        + myCmT_Instalacion.getAutonomousCommunityList().size() + "]");

                for (int i = 0; i < myCmT_Instalacion.getAutonomousCommunityList().size(); i++) {
                    Log.d(xxx, xxx + " :Comunidad " + i + ": "
                            + myCmT_Instalacion.getAutonomousCommunityList().get(i).getAutonomousCommunityName()
                            + "\n");
                    for (int i2 = 0; i2 < myCmT_Instalacion.getAutonomousCommunityList().get(i).getEventPlannerList().size(); i2++) {
                        Log.d(xxx, xxx + " :Pueblo " + i2 + ": "
                                +myCmT_Instalacion.getAutonomousCommunityList().get(i).getEventPlannerList().size()
                                        + "\n"
                                        + myCmT_Instalacion.getAutonomousCommunityList().get(i).getEventPlannerList().get(i2).getName()
                                        + "\n"
                                        + myCmT_Instalacion.getAutonomousCommunityList().get(i).getEventPlannerList().get(i2).getSortDescription()

                        );
                    }

                }
            }




        }
        catch(eefException eef)
        {
            Log.e("onCreate","Error inciando la conficuracion: " +eef.getLocalizedMessage());
        }
        catch (Exception ex){
            Log.e("onCreate","Excepotion no eef inciando la configuracion");
        }

    }//Fin de method_Gestion_De_Instalacion_de_Pueblos


    //****************************************************************
    //****************************************************************
    //     FIN  de Inicio de todo lo relacionado con la instalacion
    //****************************************************************
    //****************************************************************

    //Interface de dialogo de evento guardado. No hago nada en estos metodos

    @Override
    public void onDialogPositiveClick_ClaseDialogEventoGuardado(DialogFragment dialog) {

    }

    @Override
    public void onDialogNegativeClick_ClaseDialogEventoGuardado(DialogFragment dialog) {

    }

    @Override
    public void onDialogNeutralClick_ClaseDialogEventoGuardado(DialogFragment dialog) {

    }

    //Interface de dialogo de evento No hay Internet


    @Override
    public void onDialogPositiveClick_InterfaceClaseDialogNoHayInternet(DialogFragment dialog) {
        //Reintento la instalacion
        method_Data_To_Show_In_Fragments_2(toolBar_Actionbar);

    }

    @Override
    public void onDialogNegativeClick_InterfaceClaseDialogNoHayInternet(DialogFragment dialog) {

        //Se cierra la aplicacion. La siguiente vez que se ejecute, se repetira la instalacion
//        finish();


        //4 septiembre 2015: estoy en el caso de instalar. Si
        methodNoSePuedenMostrarProgramas();

    }

    @Override
    public void onDialogNeutralClick_InterfaceClaseDialogNoHayInternet(DialogFragment dialog) {

        //No hago nada
    }

    //*************************************************************************************************
    //Juan, 25 agosto 2015: Manejo automatico de actualizaciones automaticas en el onResume
    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first

//        methodChequeaActualizacionAutomatica();

        if (Class_ConstantsHelper.boolean_Actualiza_Datos_Del_Programa_Con_Favoritos) {
            Class_ConstantsHelper.boolean_Actualiza_Datos_Del_Programa_Con_Favoritos = false;
            method_Actualiza_Datos_Del_Programa_Con_Favoritos();
        }
    }

    private void method_Actualiza_Datos_Del_Programa_Con_Favoritos() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.remove(newFragment);

// Commit the transaction
        transaction.commit();
        transaction = null;

        //Limpiar el array con los datos del programa
        arrayList_Lista_De_Fiesta_Serializable.clear();
        method_Data_To_Show_In_Fragments_2(toolBar_Actionbar);

    }//Fin de method_Actualiza_Datos_Del_Programa_Con_Favoritos

    private void methodChequeaActualizacionAutomatica() {
        //25 agosto, es solo para probar, el intent real hay que enviarlo a actualizar
        ClaseActualizacionAutomatica claseActualizacionAutomatica = ClaseActualizacionAutomatica.newInstance(this);
        if(claseActualizacionAutomatica.methodGetBooleanActualizarAhora(new Date())){
            Intent intent = new Intent(this, com.o3j.es.estamosenfiestas.application_configuration.
                    Activity_Application_Configuration_WithNavDrawer_2.class);
            startActivity(intent);
            Toast.makeText(this, "Actualizacion automatica en curso", Toast.LENGTH_LONG).show();

            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(xxx, " En metodo methodChequeaActualizacionAutomatica Si hay actualizacion automatica:  " );
            }
            finish();
        }else{
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(xxx, " En metodo methodChequeaActualizacionAutomatica NO hay actualizacion automatica:  ");
            }
        }


    }
    //*************************************************************************************************


    @Override
    public void onClickImagenDeLaFiesta(Program m_Programa_seleccionado) {

        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, xxx + "En metodo onClickImagenDeLaFiesta");
        }

//        this.m_Programa_seleccionado = m_Programa_seleccionado;
//        method_Envia_Intent_Muestra_Imagen(0);

        //Lo nuevo con chequeo de red
        if (method_Dime_Si_Hay_Red()) {//Chequeamos si hay red

            this.m_Programa_seleccionado = m_Programa_seleccionado;
            method_Envia_Intent_Muestra_Imagen(0);

        } else {//NO hay red

            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            "No hay acceso a Internet", Toast.LENGTH_SHORT);

            toast1.show();
        }
        //FIN de Lo nuevo con chequeo de red

    }

    private void method_Envia_Intent_Muestra_Imagen(int int_Dia_Seleccionado) {
        //22 Junio 2015: salto a la actividad de swipe views eventos del dia
        Intent intent;

        //Muestra solo el cartel de la fiesta:
//        intent = new Intent(this, com.o3j.es.estamosenfiestas.display_images.ActivityDisplayImages.class);

        //Muestra la galeria con todas las imagenes de la fiesta
        intent = new Intent(this, com.o3j.es.estamosenfiestas.display_galeria.ActivityDisplayGaleria.class);

        //22 junio 2015, datos que se pasan en un bundle a la actividad con swipe views eventos del dia
        Bundle args_Datos_Para_SwipeView_De_Eventos_Del_Dia = new Bundle();

//        Hago esto por que la lista de eventos no es serializable, asi que lo pongo en array para
//                pasarlo al fragment con la lista de los eventos del dia seleccionado
        ArrayList<Program> arrayList_Programa = new ArrayList<Program>();
        arrayList_Programa.add(m_Programa_seleccionado);


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
        args_Datos_Para_SwipeView_De_Eventos_Del_Dia.putSerializable("Fiesta_Serializada", arrayList_Programa);

        args_Datos_Para_SwipeView_De_Eventos_Del_Dia.putInt("int_Dia_Seleccionado", int_Dia_Seleccionado);

        intent.putExtras(args_Datos_Para_SwipeView_De_Eventos_Del_Dia);

        startActivity(intent);


        //FIN DE 22 Junio 2015: salto a la actividad de swipe views eventos del dia
    }//FIN DE method_Envia_Intent_Al_SwipeView_De_Eventos_Del_Dia


}//Fin de la clase
