package com.o3j.es.estamosenfiestas.activity_fragment_base_2;


import android.content.Context;
import android.util.Log;

import com.o3j.es.estamosenfiestas.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Juan on 04/03/2015.
 */


//Clase muy sencilla para bajar json de estamos en fiestas. La llamo desde ClaseLoaderDeListaDeFiesta_Loader_1
//He usado el android sample: NetworkConnect
public class DownloadStringGetWithJson {

    private static  String TAG;

    //Uso el patron builder para obtener una instance de esta clase
    public static com.o3j.es.estamosenfiestas.activity_fragment_base_2.DownloadStringGetWithJson newInstance(String param1, String param2) {
        com.o3j.es.estamosenfiestas.activity_fragment_base_2.DownloadStringGetWithJson downloadStringGetWithJson = new com.o3j.es.estamosenfiestas.activity_fragment_base_2.DownloadStringGetWithJson();
        return downloadStringGetWithJson;
    }//Fin de newInstance


    public ArrayList<String> fetchJson2(String  url, Context context) {

        //Este metodo es igual a fetchJson pero devuelve un arraylist de strings que incluye
        //como segundo parametro el response code de la operacion http y como tercer parametro
        //el response message

        //LLamo a este metodo para obtener los datos json en un string
        //Necesito el context para en caso de IOException copiar el mensaje de error del xml strings
        try {
            TAG = this.getClass().getSimpleName();
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(TAG, "fetchJson OK, url: " + url);
            }

            //Original
//            return loadFromNetwork(url);

            ArrayList<String> arrayListJsonStringAndResponse = new ArrayList<>();
            String string1 =  loadFromNetwork(url);
            arrayListJsonStringAndResponse.add(string1);
            arrayListJsonStringAndResponse.add(String.valueOf(conn2.getResponseCode()));
            arrayListJsonStringAndResponse.add(conn2.getResponseMessage());
            return arrayListJsonStringAndResponse;
        } catch (IOException e) {
            TAG = this.getClass().getSimpleName();
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(TAG, "fetchJson IOException: " +e.getMessage());
            }

            //Original
//            return context.getString(R.string.connection_error);

            //DEvuelve un IOException
            ArrayList<String> arrayListJsonStringAndResponse = new ArrayList<>();
            String string1 =  context.getString(R.string.connection_error);
            arrayListJsonStringAndResponse.add(string1);
            arrayListJsonStringAndResponse.add(e.getMessage());
            return arrayListJsonStringAndResponse;
        }
    }//Fin de fetchJson2

    public String fetchJson(String  url, Context context) {
        //LLamo a este metodo para obtener los datos json en un string
        //Necesito el context para en caso de IOException copiar el mensaje de error del xml strings
        try {
            TAG = this.getClass().getSimpleName();
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(TAG, "fetchJson OK, url: " + url);
            }

            return loadFromNetwork(url);

        } catch (IOException e) {
            TAG = this.getClass().getSimpleName();
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(TAG, "fetchJson IOException: " +e.getMessage());
            }

            return context.getString(R.string.connection_error);
        }
    }//Fin de fetchJson

    /** Initiates the fetch operation. */
    private String loadFromNetwork(String urlString) throws IOException {
        InputStream stream = null;
        String str ="";

        try {
            stream = downloadUrl(urlString);
            str = readIt(stream, 1000);
            TAG = this.getClass().getSimpleName();
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(TAG, "loadFromNetwork, json: " +str);
            }
        } finally {
            if (stream != null) {
                stream.close();
            }
        }
        return str;
    }//Fin de loadFromNetwork

    /**
     * Given a string representation of a URL, sets up a connection and gets
     * an input stream.
     * @param urlString A string representation of a URL.
     * @return An InputStream retrieved from a successful HttpURLConnection.
     * @throws IOException
     */
    private InputStream downloadUrl(String urlString) throws IOException {
        // BEGIN_INCLUDE(get_inputstream)
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(10000 /* milliseconds */);
        conn.setConnectTimeout(15000 /* milliseconds */);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        // Start the query
        conn.connect();
        InputStream stream = conn.getInputStream();

        //Muestra el codigo y el mensaje de respuesta
        TAG = this.getClass().getSimpleName();
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(TAG, "downloadUrl Response Code: " + conn.getResponseCode() + " " +conn.getResponseMessage());
        }

        string3 = String.valueOf(conn.getResponseCode());
        string4 = conn.getResponseMessage();

        conn2 = conn;

        return stream;
        // END_INCLUDE(get_inputstream)
    }//Fin de downloadUrl

    HttpURLConnection conn2;
    String string3;
    String string4;


    /** Reads an InputStream and converts it to a String.
     * @param stream InputStream containing HTML from targeted site.
     * @param len Length of string that this method returns.
     * @return String concatenated according to len parameter.
     * @throws IOException
     * @throws UnsupportedEncodingException
     */

    //Este metodo no vale por que lee un numero fijo de caracteres. Uso readIt de abajo
    private String readIt2(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }//Fin de readIt



    private String readIt(InputStream in, int len) {
        String returnString = null;
        //Este es el que vale, no el anterior readIt2 que leia un numero fijo dado por len
        //Original
//    private void readStream(InputStream in) {
        //Copiado de http://www.vogella.com/tutorials/AndroidNetworking/article.html
        //No uso len, lo dejo asi para no cambiar la llamada original
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                //Imprime en el logcat, lo dejo comentado
//                System.out.println(line);
                returnString = line;
            }
            //Si lo dejo aqui, siempre da nulo, pero no se porque
//            returnString = line;
        } catch (IOException e) {
            e.printStackTrace();
            return e.getMessage();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return returnString;

        }
//        return returnString;
    }
}//Fin de DownloadStringGetWithJson
