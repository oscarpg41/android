package com.o3j.es.estamosenfiestas.display_vista_deslizante_1;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.eef.data.dataelements.DayEvents;
import com.eef.data.dataelements.Event;
import com.eef.data.dataelements.Program;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Fiesta_Seleccionada_Con_RecycleView;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Juan on 22/06/2015.
 */


public class AdaptadorDeSwipeViews_Eventos_Del_Dia extends FragmentStatePagerAdapter {

    int int_Number_Of_Fragments_To_Show;

    DayEvents day_Events;

    public AdaptadorDeSwipeViews_Eventos_Del_Dia(FragmentManager fm, ArrayList<Program> arrayList_Programa, int int_Dia_Seleccionado) {
//        public AdaptadorDeSwipeViews_Eventos_Del_Dia(FragmentManager fm, DayEvents eventos_Del_Dia) {
        super(fm);
//        this.day_Events = eventos_Del_Dia;
        this.arrayList_Programa = arrayList_Programa;
        this.int_Dia_Seleccionado = int_Dia_Seleccionado;
    }

    ArrayList<Program> arrayList_Programa = new ArrayList<Program>();
    int int_Dia_Seleccionado;

    @Override
    public Fragment getItem(int i) {
//        Fragment fragment = new MiFragment_Con_Loader_Details();
//        Bundle args = new Bundle();
////        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
//        args.putString(MiFragment_Con_Loader_Details.ARG_OBJECT, "fragment#" +(i + 1));
//        //Paso el numero del fragment que estoy creando como un string, para usarlo en
//        //el fragment para crear el loader con un unico ID para cada fragment
//        args.putString(MiFragment_Con_Loader_Details.ARG_PARAM2, String.valueOf(i + 1));
//
////        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", day_Events);
//        fragment.setArguments(args);



//        Fragment fragment = getNewFragmentForDayEvents(m_DayEvents);
        Fragment fragment = getNewFragmentForDayEvents(arrayList_Programa.get(0).getEventByDay().get(i));



        return fragment;
    }


    //GetCount retorna el numero de fragmentos que va a mostrar el swipeView
    @Override
    public int getCount() {

        return arrayList_Programa.get(0).getEventByDay().size();
    }


    @Override
    public CharSequence getPageTitle(int position) {

//        return "Lista de Eventos  " + (position + 1);


        //Escribe la fecha de los dias con eventos en el tab que corresponde
//        return arrayList_Programa.get(0).getEventByDay().get(position).toString();


        //11 julio 2015, he cambiado el formato: dias con 3 letras(en spanish sale siempre en
        //minusculas la primera letra, y quita el cero delante de los dias
//        SimpleDateFormat sfd = new SimpleDateFormat("E dd LLLL yyyy"); //Con leading cero para el dia
        SimpleDateFormat sfd = new SimpleDateFormat("E d LLLL yyyy");
        return  sfd.format(arrayList_Programa.get(0).getEventByDay().get(position).getProgramEventDate());


    }

    public Fragment getNewFragmentForDayEvents(DayEvents m_DayEvents) {
        Fragment fragment = new MiFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView();
        Bundle args = new Bundle();
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);

        int int_Nivel_De_Info_Fiesta_Eventos_Del_Dia = 1;
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "fragment#" +(int_Nivel_De_Info_Fiesta_Eventos_Del_Dia + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, String.valueOf(int_Nivel_De_Info_Fiesta_Eventos_Del_Dia + 1));

        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, int_Nivel_De_Info_Fiesta_Eventos_Del_Dia);

//        Hago esto por que la lista de eventos no es serializable, asi que lo pongo en array para
//                pasarlo al fragment con la lista de los eventos del dia seleccionado
        ArrayList<Event> arrayList_Lista_De_Eventos_Del_Dia = new ArrayList<Event>();
        for (int i=0; i<m_DayEvents.getListaEventos().size(); i++){
            arrayList_Lista_De_Eventos_Del_Dia.add(m_DayEvents.getListaEventos().get(i));
        }


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
        args.putSerializable("Eventos del dia Serializado", arrayList_Lista_De_Eventos_Del_Dia);


        fragment.setArguments(args);

        return fragment;
    }//Fin de getNewFragmentForDayEvents
}//Fin de la clase
