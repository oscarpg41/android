package com.o3j.es.estamosenfiestas.servercall.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by jluis on 23/07/15.
 */
public class ProgramEventPojo  implements Parcelable{
    @Expose
    private String idEvent;
    @Expose
    private String idOrganizer;
    @Expose
    private String idProgram;
    @SerializedName("ff_event")
    @Expose
    private String ffEvent;
    @Expose
    private String hour;
    @Expose
    private String title;
    @Expose
    private String description;
    @Expose
    private String image;
    @Expose
    private String popular;

    /**
     *
     * @return
     * The idEvent
     */
    public String getIdEvent() {
        return idEvent;
    }

    /**
     *
     * @param idEvent
     * The idEvent
     */
    public void setIdEvent(String idEvent) {
        this.idEvent = idEvent;
    }

    /**
     *
     * @return
     * The idOrganizer
     */
    public String getIdOrganizer() {
        return idOrganizer;
    }

    /**
     *
     * @param idOrganizer
     * The idOrganizer
     */
    public void setIdOrganizer(String idOrganizer) {
        this.idOrganizer = idOrganizer;
    }

    /**
     *
     * @return
     * The idProgram
     */
    public String getIdProgram() {
        return idProgram;
    }

    /**
     *
     * @param idProgram
     * The idProgram
     */
    public void setIdProgram(String idProgram) {
        this.idProgram = idProgram;
    }

    /**
     *
     * @return
     * The ffEvent
     */
    public String getFfEvent() {
        return ffEvent;
    }

    /**
     *
     * @param ffEvent
     * The ff_event
     */
    public void setFfEvent(String ffEvent) {
        this.ffEvent = ffEvent;
    }

    /**
     *
     * @return
     * The hour
     */
    public String getHour() {
        return hour;
    }

    /**
     *
     * @param hour
     * The hour
     */
    public void setHour(String hour) {
        this.hour = hour;
    }

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The image
     */
    public String getImage() {
        return image;
    }

    /**
     *
     * @param image
     * The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     *
     * @return
     * The popular
     */
    public String getPopular() {
        return popular;
    }

    /**
     *
     * @param popular
     * The popular
     */
    public void setPopular(String popular) {
        this.popular = popular;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.idEvent);
        dest.writeString(this.idOrganizer);
        dest.writeString(this.idProgram);
        dest.writeString(this.ffEvent);
        dest.writeString(this.hour);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.image);
        dest.writeString(this.popular);
    }

    public ProgramEventPojo() {
    }

    protected ProgramEventPojo(Parcel in) {
        this.idEvent = in.readString();
        this.idOrganizer = in.readString();
        this.idProgram = in.readString();
        this.ffEvent = in.readString();
        this.hour = in.readString();
        this.title = in.readString();
        this.description = in.readString();
        this.image = in.readString();
        this.popular = in.readString();
    }

    public static final Creator<ProgramEventPojo> CREATOR = new Creator<ProgramEventPojo>() {
        public ProgramEventPojo createFromParcel(Parcel source) {
            return new ProgramEventPojo(source);
        }

        public ProgramEventPojo[] newArray(int size) {
            return new ProgramEventPojo[size];
        }
    };
}
