package com.o3j.es.estamosenfiestas.servercall.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by jluis on 23/07/15.
 */
public class OrganizerPojo implements Parcelable {

    @Expose
    private String idOrganizer;
    @Expose
    private String name;
    @Expose
    private String town;
    @Expose
    private String logo;
    @SerializedName("ff_paid")
    @Expose
    private String ffPaid;
    @Expose
    private String idccaa;
    @Expose
    private String info;
    @SerializedName("ff_validity")
    @Expose
    private String ffValidity;

    /**
     *
     * @return
     * The idOrganizer
     */
    public String getIdOrganizer() {
        return idOrganizer;
    }

    /**
     *
     * @param idOrganizer
     * The idOrganizer
     */
    public void setIdOrganizer(String idOrganizer) {
        this.idOrganizer = idOrganizer;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The town
     */
    public String getTown() {
        return town;
    }

    /**
     *
     * @param town
     * The town
     */
    public void setTown(String town) {
        this.town = town;
    }

    /**
     *
     * @return
     * The logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     *
     * @param logo
     * The logo
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

    /**
     *
     * @return
     * The ffPaid
     */
    public String getFfPaid() {
        return ffPaid;
    }

    /**
     *
     * @param ffPaid
     * The ff_paid
     */
    public void setFfPaid(String ffPaid) {
        this.ffPaid = ffPaid;
    }

    /**
     *
     * @return
     * The idccaa
     */
    public String getIdccaa() {
        return idccaa;
    }

    /**
     *
     * @param idccaa
     * The idccaa
     */
    public void setIdccaa(String idccaa) {
        this.idccaa = idccaa;
    }

    /**
     *
     * @return
     * The info
     */
    public String getInfo() {
        return info;
    }

    /**
     *
     * @param info
     * The info
     */
    public void setInfo(String info) {
        this.info = info;
    }

    /**
     *
     * @return
     * The ffValidity
     */
    public String getFfValidity() {
        return ffValidity;
    }

    /**
     *
     * @param ffValidity
     * The ff_validity
     */
    public void setFfValidity(String ffValidity) {
        this.ffValidity = ffValidity;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.idOrganizer);
        dest.writeString(this.name);
        dest.writeString(this.town);
        dest.writeString(this.logo);
        dest.writeString(this.ffPaid);
        dest.writeString(this.idccaa);
        dest.writeString(this.info);
        dest.writeString(this.ffValidity);
    }

    public OrganizerPojo() {
    }

    protected OrganizerPojo(Parcel in) {
        this.idOrganizer = in.readString();
        this.name = in.readString();
        this.town = in.readString();
        this.logo = in.readString();
        this.ffPaid = in.readString();
        this.idccaa = in.readString();
        this.info = in.readString();
        this.ffValidity = in.readString();
    }

    public static final Creator<OrganizerPojo> CREATOR = new Creator<OrganizerPojo>() {
        public OrganizerPojo createFromParcel(Parcel source) {
            return new OrganizerPojo(source);
        }

        public OrganizerPojo[] newArray(int size) {
            return new OrganizerPojo[size];
        }
    };
}
