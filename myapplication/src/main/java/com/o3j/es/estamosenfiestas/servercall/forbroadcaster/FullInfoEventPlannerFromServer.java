package com.o3j.es.estamosenfiestas.servercall.forbroadcaster;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.eef.data.dataelements.Event;
import com.eef.data.dataelements.EventPlanner;
import com.eef.data.dataelements.MultimediaElement;
import com.eef.data.dataelements.Program;
import com.eef.data.eefExceptions.eefException;
import com.eef.data.eeftypes.TypeEefError;
import com.eef.data.eeftypes.TypeMultimediaElement;
import com.eef.data.eeftypes.TypeState;
import com.o3j.es.estamosenfiestas.servercall.pojos.InfoOrganizerPojo;
import com.o3j.es.estamosenfiestas.servercall.pojos.ProgramEventPojo;
import com.o3j.es.estamosenfiestas.servercall.pojos.ProgramPojo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jluis on 28/07/15.
 */
public class FullInfoEventPlannerFromServer implements Parcelable{

    Boolean rightDownloaded;

    Boolean evenPLannerRightDownloaded;
    private static InfoOrganizerPojo eventPlannerInfo;
    private static EventPlanner eventPlanner;

    Boolean listaProgramasRightDowloaded;
    List<ProgramPojo> listaProgramas;
    List<Program> listaPrograms;

    EventPlanner planner;
    HashMap<ProgramPojo,List<ProgramEventPojo>> programasDescargados;
    HashMap<Program,List<Event>> programsDownladed;

    public FullInfoEventPlannerFromServer() {
        rightDownloaded= false;
        evenPLannerRightDownloaded = false;
        eventPlannerInfo=null;
        listaProgramas=null;
        listaPrograms=null;
        programasDescargados=null;
    }

    public Boolean getRightDownloaded() {
        return rightDownloaded;
    }


    public InfoOrganizerPojo getEventPlannerInfo() {
        return eventPlannerInfo;
    }

    public EventPlanner getEventPlanner() {
        return eventPlanner;
    }

    public HashMap<Program, List<Event>> getProgramsDownladed() {
        return programsDownladed;
    }

    public HashMap<ProgramPojo, List<ProgramEventPojo>> getProgramasDescargados() {
        return programasDescargados;
    }

    public void setEventPlannerInfo(InfoOrganizerPojo eventPlannerInfo) {
        this.eventPlannerInfo = eventPlannerInfo;
        this.evenPLannerRightDownloaded = true;
        eventPlannerPojoToBean();
    }

    public List<ProgramPojo> getListaProgramas() {
        return listaProgramas;
    }

    public List<Program> getListPrograms() throws eefException {
         return listaPrograms;
    }

    private List<MultimediaElement> getImageAsMultimediaList(String img) {
        MultimediaElement logo = new MultimediaElement(img, TypeMultimediaElement.IMAGE_URL);
        ArrayList<MultimediaElement> logoList = new ArrayList<MultimediaElement>();
        logoList.add(logo);
        return logoList;
    }

    private void generateProgramList() throws eefException {
        if(listaProgramas==null)
            return;
        listaPrograms = new ArrayList<Program>();
        if(listaProgramas.isEmpty())
            return;
        for(ProgramPojo programa: listaProgramas)
        {
            listaPrograms.add(programPojoToProgram(programa));
        }

    }
    public void setListaProgramas(List<ProgramPojo> listaProgramas) {
        listaProgramasRightDowloaded = true;
        this.listaProgramas = listaProgramas;
        produceListToBeDownloaded();
        try {
            generateProgramList();
        } catch (eefException e) {
            listaProgramasRightDowloaded = false;
        }
    }

    private void produceListToBeDownloaded(){
        programasDescargados = new HashMap<ProgramPojo,List<ProgramEventPojo>>();
        programsDownladed = new HashMap<Program,List<Event>>();
    }

    public  synchronized void addProgramEventDownload(ProgramPojo programa, List<ProgramEventPojo> eventosPrograma)
    {
        if(programasDescargados.containsKey(programa))
            return;
        programasDescargados.put(programa, eventosPrograma);
        try {
            Program program = programPojoToProgram(programa);
            List<Event> listEvent = programEventPojoListToEventList(eventosPrograma, programPojoToProgram(programa), eventPlannerInfo);
            programsDownladed.put(program,listEvent);
        } catch (eefException e) {
            Log.e("ERROR POR FECHAS","LAS FECHAS DEL PROGRAMA O DE LOS EVENTOS SON ERRONEAS");
        }
        //programaFull.
    }

    //CHEQUEA SI SE HA RECIBIDO LOS EVENTOS DE TODOS LOS PROGRAMAS A DESCARGA
    public Boolean allProgramsDownladed(){
        //Si no tengo el organizador o la lista de programas la descarga esta sin finalizar
        if(!evenPLannerRightDownloaded || !listaProgramasRightDowloaded)
            return false;
        //Si la lista de programas es null o vacia devuelvo false
        if(listaProgramas==null || listaProgramas.size()==0)
            return false;
        //Si todavía no he añadido los eventos de un progrma retorno null
        if(programasDescargados==null)
            return false;
        //recorro de la lista de programas y compruebo si ya tengo la descarga
        for(ProgramPojo programa : listaProgramas)
            if(!programasDescargados.containsKey(programa))
                return false;
        //Si para todos los programas ya tengo la lista de eventos marco la configuración como
        //bien configurada.
        rightDownloaded=true;
        return true;
    }




    protected void eventPlannerPojoToBean()
    {
        eventPlanner = new EventPlanner();
        eventPlanner.setAutonomousCommunityId(Integer.parseInt(eventPlannerInfo.getIdccaa()));
        eventPlanner.setEventPlannerId(Integer.parseInt(eventPlannerInfo.getIdOrganizer()));
        eventPlanner.setName(eventPlannerInfo.getName());
        eventPlanner.setSortDescription(eventPlannerInfo.getTown());
        eventPlanner.setLongDescription(eventPlannerInfo.getInfo());
        eventPlanner.setMultimediaElementList(getImageAsMultimediaList(eventPlannerInfo.getLogo()));
        eventPlanner.setDownloadedDate(new GregorianCalendar().getTime());
        eventPlanner.setUpdatedDate(eventPlanner.getDownloadedDate());
    }

    protected Program programPojoToProgram(ProgramPojo programa) throws eefException
    {
        final String LOG_SEED = "FULL-->programPojoToProgram-->";
        Program program = new Program();
        program.setAutonomousCommunityId(Integer.parseInt(eventPlannerInfo.getIdccaa()));
        program.setEventPlannerId(Integer.parseInt(eventPlannerInfo.getIdOrganizer()));
        program.setProgramId(Integer.parseInt(programa.getIdProgram()));
        program.setProgramLongDescription(programa.getName());
        program.setProgramSortDescription(programa.getName());
        program.setListMultimediaElement(getImageAsMultimediaList(programa.getImage()));
        Date fecha = new GregorianCalendar().getTime();
        program.setDownloadedDay(fecha);
        //Se convierten a fecha los campos de incio y fin
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatofechaActualizacion = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try
        {
            program.setProgramStartDate(formatoDelTexto.parse(programa.getFfIni()));
            program.setProgramEndDate(formatoDelTexto.parse(programa.getFfEnd()));
            program.setLastUpdateDay(formatofechaActualizacion.parse(programa.getFfModification()));
        } catch (ParseException e) {
            Log.e(LOG_SEED, "Error parser de las fechas de programa fecha_ini[" + programa.getFfIni() + "] o fecha fin[" + programa.getFfEnd() + "]");
            throw new eefException(TypeEefError.PROGRAMS_DATE_PARSE_FORMAT_EXCEPTION);
        }
        return program;
    }


    protected List<Event> programEventPojoListToEventList(List<ProgramEventPojo> eventosP, Program program, InfoOrganizerPojo organizer) throws eefException
    {
        final String LOG_SEED = "FULL-->programPojoToProgram-->";
        ArrayList<Event> eventos = new ArrayList<Event>();
        for(ProgramEventPojo evento : eventosP) {
            Event event = new Event();
            event.setProgramId(program.getProgramId());
            event.setAutonomousCommunityId(Integer.parseInt(organizer.getIdccaa()));
            event.setEventId(Integer.parseInt(evento.getIdEvent()));
            event.setSortText(evento.getTitle());
            event.setLongText(evento.getDescription());
            event.setMultimediaElementList(getImageAsMultimediaList(evento.getImage()));
            event.setState(TypeState.SCHEDULED);
            SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String fechaEvento = evento.getFfEvent() + " " + evento.getHour();
            try {

                event.setStartDate(formatoDelTexto.parse(fechaEvento));

            } catch (ParseException e) {
                Log.e(LOG_SEED, "Error parser de las fechas de eventp[" + fechaEvento +"]  no cadena [yyyy-MM-dd HH:mm:ss]");
                throw new eefException(TypeEefError.PROGRAMS_DATE_PARSE_FORMAT_EXCEPTION);
            }
            eventos.add(event);
        }
        return eventos;
    }




    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.rightDownloaded);
        dest.writeValue(this.evenPLannerRightDownloaded);
        dest.writeParcelable(this.eventPlannerInfo, 0);
        dest.writeValue(this.listaProgramasRightDowloaded);
        dest.writeTypedList(listaProgramas);
        dest.writeMap(this.programasDescargados);
        //dest.writeSerializable(this.programasDescargados);rit
    }

    protected FullInfoEventPlannerFromServer(Parcel in) {
        this.rightDownloaded = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.evenPLannerRightDownloaded = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.eventPlannerInfo = in.readParcelable(InfoOrganizerPojo.class.getClassLoader());
        this.listaProgramasRightDowloaded = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.listaProgramas = in.createTypedArrayList(ProgramPojo.CREATOR);
        //this.programasDescargados = (HashMap<ProgramPojo, List<ProgramEventPojo>>) in.readSerializable();
    }

    public static final Creator<FullInfoEventPlannerFromServer> CREATOR = new Creator<FullInfoEventPlannerFromServer>() {
        public FullInfoEventPlannerFromServer createFromParcel(Parcel source) {
            return new FullInfoEventPlannerFromServer(source);
        }

        public FullInfoEventPlannerFromServer[] newArray(int size) {
            return new FullInfoEventPlannerFromServer[size];
        }
    };
}
