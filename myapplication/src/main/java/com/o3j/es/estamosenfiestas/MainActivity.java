package com.o3j.es.estamosenfiestas;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.eef.data.dataelements.DayEvents;
import com.eef.data.dataelements.Event;
import com.eef.data.dataelements.Program;
import com.eef.data.eefExceptions.eefException;
import com.eef.data.eeftypes.TypeConfiguration;
import com.eef.data.managers.IConfigurationManager;
import com.eef.data.managers.impl.ProgamListManager;
import com.o3j.es.estamosenfiestas.factory.impl.ConfigurationFactory;

import java.util.List;


public class MainActivity extends ActionBarActivity {

    IConfigurationManager myCmT=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ConfigurationFactory cmTFactory = new ConfigurationFactory();

        try {
            myCmT = cmTFactory.getConfigurationManager(TypeConfiguration.INSTALLATION_DEFAULT_WITH_AUTONOMOUS_COMMUNITY);

            Log.i("onCreate", "Esta bien configurado [" + myCmT.isRightConfigured() + "]");
            Log.i("onCreate", "El cmt esta configurado[" + myCmT.isRightConfigured() + "] numero AU[" + myCmT.getAutonomousCommunityList().size() + "]");
            Log.i("OnCreate", "La comunidad Autonoma Configurada[" + myCmT.getAutonomousCommunityConfigured().getAutonomousCommunityName()
                    + "] y el pueblo configurado[" + myCmT.getEventPlannerConfigured().getName() + "]");

            ProgamListManager pmT = new ProgamListManager();
            pmT.loadData(myCmT.getAutonomousCommunityConfigured(),myCmT.getEventPlannerConfigured());
            if(pmT.existCurrentProgram()){
                Program p = pmT.getCurrentProgram();
                Log.i("onCreate", "HAY PROGRAMA EN MARCHA ["+p.getProgramSortDescription()+"]");

                //Obtener la lista de eventos del programa por dias
                List<DayEvents> ltd = p.getEventByDay();
                //Obtener la lista de eventos completa
                List<Event> ltf =p.getFullList();
            }
            //Obtener la lista de


        }
        catch(eefException eef)
        {
            Log.e("onCreate","Error inciando la conficuración");
        }
        catch (Exception ex){
            Log.e("onCreate","Excepotion no eef inciando la configuracion");
        }

        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void on_Click_1(View view) {
        // Do something in response to button
        Intent intent = new Intent(this, JoseTest_Activity.class);
        startActivity(intent);
    }


    public void on_Click_2(View view) {
        // Do something in response to button
        Intent intent = new Intent(this, JuanTest_Activity.class);
        startActivity(intent);
    }
}// ENd of MainActivity
