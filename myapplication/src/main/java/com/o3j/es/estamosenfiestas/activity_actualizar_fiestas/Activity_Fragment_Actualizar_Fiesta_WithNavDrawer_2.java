package com.o3j.es.estamosenfiestas.activity_actualizar_fiestas;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import com.eef.data.dataelements.DayEvents;
import com.eef.data.dataelements.Event;
import com.eef.data.dataelements.Program;
import com.eef.data.eefExceptions.eefException;
import com.eef.data.eeftypes.TypeConfiguration;
import com.eef.data.eeftypes.TypeState;
import com.eef.data.managers.IConfigurationManager;
import com.eef.data.managers.impl.ProgamListManager;
import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.ClaseChequeaAcceso_A_Internet;
import com.o3j.es.estamosenfiestas.dialogs.ClaseDialogGenericaDosTresBotones;
import com.o3j.es.estamosenfiestas.dialogs.ClaseDialogNoHayInternet;
import com.o3j.es.estamosenfiestas.display_frame_layout.CustomAdapter_RecyclerView_Evento_Detalle;
import com.o3j.es.estamosenfiestas.display_frame_layout.CustomAdapter_RecyclerView_Fiesta_List_Por_Dias;
import com.o3j.es.estamosenfiestas.display_frame_layout.CustomAdapter_RecyclerView_Fiestas;
import com.o3j.es.estamosenfiestas.display_frame_layout.CustomAdapter_RecyclerView_List_De_Eventos;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Evento;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Fiesta_Seleccionada_Con_RecycleView;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Fiestas_Con_RecycleView;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView;
import com.o3j.es.estamosenfiestas.display_lists_package.AdaptadorDeSwipeViews_2;
import com.o3j.es.estamosenfiestas.factory.impl.ConfigurationFactory;
import com.o3j.es.estamosenfiestas.nav_drawer_utilities.ClassMenuItemsNavigationDrawer;
import com.o3j.es.estamosenfiestas.nav_drawer_utilities.CustomAdapter_NavigationDrawer_2;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import managers.DownloadInfoFromServerForUpdateManager;


// Juan 6 Junio 2015: Esta clase mostrara Actualizar Fiestas. Ahora es generica para mostrar uso del nav drawer, la tengo que limpiar
//usando listas con este orden y sin salirse de esta actividad:
// 1: Se muestra la lista con todas las fiestas validas de ese ayuntamiento.
// 2: Se muestra la lista de la fiesta seleccionada en 1 por dias.
// 3: Se muestra la lista de eventos del dia seleccionado en 2.
// 4: Se muestra el detalle del evento seleccionado en 3, por ahora sin swipe.
public class Activity_Fragment_Actualizar_Fiesta_WithNavDrawer_2 extends ActionBarActivity
        implements CustomAdapter_RecyclerView_Fiestas.OnItemClickListenerFiestaSeleccionada,
        CustomAdapter_RecyclerView_Fiesta_List_Por_Dias.OnItemClickListenerDiaSeleccionado,
        CustomAdapter_RecyclerView_List_De_Eventos.OnItemClickListenerEventoSeleccionado,
        CustomAdapter_RecyclerView_Evento_Detalle.OnItemClickListenerEventoDetalle,
        CustomAdapter_NavigationDrawer_2.OnItemClickListenerElementoDeMenuSeccionado,
        MiFragment_Fiestas_Con_RecycleView.OnFragmentInteractionListener,
        MiFragment_Fiesta_Seleccionada_Con_RecycleView.OnFragmentInteractionListener,
        MiFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView.OnFragmentInteractionListener,
        MiFragment_Evento.OnFragmentInteractionListener,
        ClaseDialogGenericaDosTresBotones.InterfaceClaseDialogGenericaDosTresBotonesListener,
        ClaseDialogNoHayInternet.InterfaceClaseDialogNoHayInternet{
    //Original con fragment sin loader
//    public class Activity_Fragment_Base_2 extends ActionBarActivity implements MiFragment_1.OnFragmentInteractionListener {
    AdaptadorDeSwipeViews_2 adaptadorDeSwipeViews;
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        xxx = this.getClass().getSimpleName();

        //*************************************************************************************
        //Inicializa los broadcast receivers
        myBroadcastReceiver_progress_bar_actualizar = new MyBroadcastReceiver_Progress_Bar_Actualizar();
        intentFilter_MyBroadcastReceiver_Progress_Bar_Actualizar = new IntentFilter(broadcast_Progreso_Actualizar);
        myBroadcastReceiver_fin_actualizar = new MyBroadcastReceiver_Fin_Actualizar();
        intentFilter_MyBroadcastReceiver_Fin_Actualizar = new IntentFilter(broadcast_Fin_De_La_Auctualizacion);
        //***********************************************************************

//        Nota: Juan 2 junio: para que funcione el view pager y los tabs tengo que usar el layout activity_fragment_lists_with_nav_drawer_3
//        setContentView(R.layout.activity_fragment_lists_with_nav_drawer_3);


//        Este layout funciona pero al tener el drawer como raiz, tapa la toolbar
//        setContentView(R.layout.activity_fragment_frame_layout_with_nav_drawer);


//        4 junio 2015, Uso este layout para que el navigation drawer no tape la roolbar
//        setContentView(R.layout.activity_fragment_frame_layout_with_nav_drawer_3);
        setContentView(R.layout.activity_under_construction);


        //Juan 21 agosto, ya no uso este texto, ya hay funcionalidad real
//        TextView textView_Titulo = (TextView) findViewById(R.id.titulo_de_under_construction);
//
//        textView_Titulo.setVisibility(View.VISIBLE);
//
//        textView_Titulo.setText(R.string.text_under_construction);

        
//        Toolbar toolBar_Actionbar = (Toolbar) findViewById (R.id.activity_my_toolbar);
        Toolbar toolBar_Actionbar = (Toolbar) findViewById(R.id.view);
        //Toolbar will now take on default Action Bar characteristics

//        Le cambio el titulo por el ayuntamiento en method_Data_To_Show_In_Fragments_2
//        Para cambiar el tama�o del titulo lo hice con styles como en
//        http://stackoverflow.com/questions/28487312/how-to-change-the-toolbar-text-size-in-android


//        Tengo que poner este titulo para que aparezca el ayuntamiento en method_Data_To_Show_In_Fragments_2
//        toolBar_Actionbar.setTitle(getResources().getString(R.string.text_under_construction));
        toolBar_Actionbar.setTitle(getResources().getString(R.string.menu_3));

//        No uso el subtitulo
//        toolBar_Actionbar.setSubtitle(getResources().getString(R.string.sub_titulo_4));


        setSupportActionBar(toolBar_Actionbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);




        //9 septiembre 2015: dejo comentada la bottom_toolbar,
//        final Toolbar bottom_toolbar = (Toolbar) findViewById(R.id.bottom_toolbar);
//        bottom_toolbar.setLogo(R.drawable.ic_launcher);
//        bottom_toolbar.setTitle(getResources().getString(R.string.texto_1));
//        bottom_toolbar.setSubtitle(getResources().getString(R.string.texto_2));
//        bottom_toolbar.setNavigationIcon(R.drawable.ic_launcher);
//        //Toolbar que pongo abajo
//
//        // Set an OnMenuItemClickListener to handle menu item clicks
//        bottom_toolbar.setOnMenuItemClickListener(
//                new Toolbar.OnMenuItemClickListener() {
//                    @Override
//                    public boolean onMenuItemClick(MenuItem item) {
//                        // Handle the menu item
//
//                        int id = item.getItemId();
//
//                        //noinspection SimplifiableIfStatement
//                        if (id == R.id.action_settings) {
//                            Toast.makeText(Activity_Fragment_Actualizar_Fiesta_WithNavDrawer_2.this, "Bottom toolbar pressed: ", Toast.LENGTH_LONG).show();
//                            return true;
//                        }
//                        return true;
//                    }
//                });
//
//
//        bottom_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(Activity_Fragment_Actualizar_Fiesta_WithNavDrawer_2.this, "Navigation Icon pressed: ", Toast.LENGTH_LONG).show();
//                // Para esta practica, al presionar el navigation icon lo quito.
//                bottom_toolbar.setVisibility(View.GONE);
//            }
//        });
//
//        // Inflate a menu to be displayed in the toolbar
//        bottom_toolbar.inflateMenu(R.menu.menu_main);
        //FIN 9 septiembre 2015: dejo comentada la bottom_toolbar,




        //Por ahora utilizo este metodo para jugar con el API de Jose Luis, 21 Mayo 2015
//        method_Data_To_Show_In_Fragments();

        //6 Junio 2015,  Esto no lo uso, es parte de la limpieza
//        method_Data_To_Show_In_Fragments_2(toolBar_Actionbar);


        //****************************************************************
        //   Juan, 2 junio 2015: No uso los swipe views, quedan comentados
        //****************************************************************
        // ViewPager and its adapters use support library
        // fragments, so use getSupportFragmentManager.
//        adaptadorDeSwipeViews =
//                new AdaptadorDeSwipeViews_2(
//                        getSupportFragmentManager(), int_Number_Of_Fragments_To_Show, List_Programs);
//        mViewPager = (ViewPager) findViewById(R.id.viewpager_1);
//        mViewPager.setAdapter(adaptadorDeSwipeViews);
//
//        //Poner tabs del swipe view
//        metodoPonerTabs_1(mViewPager);
        //****************************************************************
        //   FIN de Juan, 2 junio 2015: No uso los swipe views, quedan comentados
        //****************************************************************



        //Inicializar el nav drawer, 21 Mayo 2015
        method_Init_NavigationDrawer();


        //Juan 25 agosto 2015: prueba llamando solo una vez al metodo para actualizar la fecha actual desde aqui
        //Asi me ahorro ponerlo en un monton de sitios y ponerlo aqui vale para si sale bien o sale mal
        //o me voy por el navigation drawer
//        methodActivarConfiguracionAutomatica_SOLO_PARA_PRUEBAS();



        // Juan, 21 agosto 2015: Muestra el dialogo de inicio de actualizar
        //9 septiembre: ya no se muestra este dialogo, lo comento. Se muestra la pantalla con el boton y el
        //checkbox de actualizacion automatica
//        DialogFragment dialog =  ClaseDialogGenericaDosTresBotones.newInstance
//                (R.string.titulo_dialog_actualizar, R.string.message_dialog_actualizar, R.string.positiveButton_dialog_actualizar,
//                        R.string.negativeButton_dialog_actualizar, 2, R.string.boton_neutral);
//        dialog.show(getSupportFragmentManager(), "ClaseDialogGenericaDosTresBotones");


        methodMostrarPantallaDeActualizar();


    }//Fin del onCreate

    CheckBox CheckBox_ActualizacionAutomatica;

    public void onClickAtualizacionAutomatica(View view) {
        // Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();

        switch (view.getId()) {
            case R.id.CheckBox_ActualizacionAutomatica:

                if (checked) {
                    claseActualizacionAutomatica.methodSetCheckBox(true);
                    methodLanzarActualizacionAutomaticaFake();
                } else {
                    claseActualizacionAutomatica.methodSetCheckBox(false);
                    methodDetenerActualizacionAutomaticaFake();
                }
                break;
        }
    }//fIN DE onClick

    ClaseActualizacionAutomatica claseActualizacionAutomatica;

    private void methodMostrarPantallaDeActualizar() {
        //9 septiembre 2015, pantalla que diseño oscar para la actualizacion
        Log.e(xxx, xxx + ":  En metodo  methodMostrarPantallaDeActualizar ");

        CheckBox_ActualizacionAutomatica = (CheckBox) findViewById(R.id.CheckBox_ActualizacionAutomatica);
        //9 sept 2015, inicializo actualizacion automatica
         claseActualizacionAutomatica =  ClaseActualizacionAutomatica.newInstance(this);

        //Averiguo si el checkbox hay que presentarlo marcado o no (estado inicial) para que el usuario sepa si
        //esta activada la actualizacion automatica o no
        if(claseActualizacionAutomatica.methodGetBooleanCheckBoxChecked()){
            CheckBox_ActualizacionAutomatica.setChecked(true);
        }else{
            CheckBox_ActualizacionAutomatica.setChecked(false);
        }

        //Iniializar boton de actualizar: actualizacion manual con el boton actualizar
        Button button_iniciar_actualizacion = (Button) findViewById(R.id.button_iniciar_actualizacion);
        button_iniciar_actualizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                method_Fuera_Del_On_Create_Chequea_Acceso_A_Internet();            }
        });

    }//Fin de methodMostrarPantallaDeActualizar

    public void methodLanzarActualizacionAutomaticaFake() {
        claseActualizacionAutomatica.methodIniciaActualizacionAutomatica("fake1", "fake2");
    }

    public void methodDetenerActualizacionAutomaticaFake() {
        claseActualizacionAutomatica.methodDetenerActualizacionAutomaticaFake("fake1", "fake2");
    }


    private void methodActivarConfiguracionAutomatica_SOLO_PARA_PRUEBAS() {
        Log.e(xxx, xxx + ":  En metodo  methodActivarConfiguracionAutomatica_SOLO_PARA_PRUEBAS ");
        //Esto es solo para probar, tambien hay que ponerla en actualizar
        claseActualizacionAutomatica.methodGuardaFechaInicial(new Date());
    }

//    Juan 2 junio 2015, no uso el view pager ni los tabs, dejo el metodo metodoPonerTabs_1 comentado

//    private void metodoPonerTabs_1(ViewPager mViewPager) {
//
////        Hecho como en: http://guides.codepath.com/android/Google-Play-Style-Tabs-using-SlidingTabLayout
//// Give the SlidingTabLayout the ViewPager
//        SlidingTabLayout slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
//        // Center the tabs in the layout
//        slidingTabLayout.setDistributeEvenly(true);
//        slidingTabLayout.setViewPager(mViewPager);
//        //Colorear el tab
//        slidingTabLayout.setBackgroundColor(getResources().getColor(R.color.primary_color));
//        // Customize tab indicator color
//        slidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
//            @Override
//            public int getIndicatorColor(int position) {
//                return getResources().getColor(R.color.accent_color);
//            }
//        });
//    }

    //**************************************************************************
    //**************************************************************************
    //  24 junio 2015: Setting correcto para no mostrar menu en la toolbar
    //  24 junio 2015: Solo se muestra el Nav Drawer
    //**************************************************************************
    //**************************************************************************


//    Me ayude con:
//    https://github.com/codepath/android_guides/wiki/Fragment-Navigation-Drawer

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;

        // Uncomment to inflate menu items to Action Bar
        // inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = drawerLayout.isDrawerOpen(navigationDrawerRecyclerView);

        //No he inflado el menu, lo dejo comentado
//        menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }// Fin de onPrepareOptionsMenu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this. 21 Mayo 2015
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }//Codigo del nav drawer


        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        if (id == R.id.action_settings) {
//            Toast.makeText(Activity_Fragment_FrameLayout_WithNavDrawer_2.this, " toolbar pressed: ", Toast.LENGTH_LONG).show();
//
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }
    //**************************************************************************
    //**************************************************************************
    //  FIN DE 24 junio 2015: Setting correcto para no mostrar menu en la toolbar
    //  24 junio 2015: Solo se muestra el Nav Drawer
    //**************************************************************************
    //**************************************************************************

    //Implementa la interfaz de la clase MiFragment_Fiestas_Con_RecycleView
//    y la clase MiFragment_Fiesta_Seleccionada_Con_RecycleView

//    OJO: NO ESTOY HACIENDO USO DE ESTA INTERFAZ
    public void onFragmentInteraction(String string) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"Fragmento activo: " +string);

        }
    }



    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //  Inicio de Codigo relacionado con el navigation drawer, 21 Mayo 2015
    //****************************************************************
    //****************************************************************
    private void method_Init_NavigationDrawer() {

        //****************************************************************
        //   Codigo relacionado con recycler view del navigation drawer
        //****************************************************************


        //Inicializar los datos del array del menu a mostrar en el navigationDrawer
        init_arrayList_NavigationDrawer();

        navigationDrawerRecyclerView = (RecyclerView) findViewById(R.id.left_drawer_recycle_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        navigationDrawerRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        navigationDrawerLayoutManager = new LinearLayoutManager(this);
        navigationDrawerRecyclerView.setLayoutManager(navigationDrawerLayoutManager);
//        navigationDrawerRecyclerView.setAdapter(new CustomAdapter_NavigationDrawer(

//        Adapter del navigation drawer con la cabecera
                navigationDrawerRecyclerView.setAdapter(new CustomAdapter_NavigationDrawer_2(
                arrayListItemsNavigationDrawer, "0",
                        (CustomAdapter_NavigationDrawer_2.OnItemClickListenerElementoDeMenuSeccionado) this));

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_1);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        //Este objeto permite que el navigation Drawer interactue correctamente con el action bar (la toolbar en mi caso)
        //Tengo que usar el actionBarDrawerToggle de V7 support por que el de V4 esta deprecated
        actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                drawerLayout,         /* DrawerLayout object */
                //Deprecated en V7, esto era valido en V4
//                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
//                getSupportActionBar().setTitle(getResources().getString(R.string.title_activity_main_activity_navigation_drawer));
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()

            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                getActionBar().setTitle(mDrawerTitle);
//                getSupportActionBar().setTitle(getTitle());
//                getSupportActionBar().setTitle(getResources().getString(R.string.titulo_navigation_drawer));
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()


            }
        };//Fin de actionBarDrawerToggle

        // Set the drawer toggle as the DrawerListener
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //****************************************************************
        //  FIN Codigo relacionado con recycler view del navigation drawer
        //****************************************************************
    }//Fin de method_Init_NavigationDrawer





    //Datos del menu del navigation drawer
    private void init_arrayList_NavigationDrawer() {//Relacionado con recycler view
        String[] array_menu = {getString(R.string.menu_1),getString(R.string.menu_2),getString(R.string.menu_3)
                ,getString(R.string.menu_4),getString(R.string.menu_6),getString(R.string.menu_7), getString(R.string.menu_5)};
        arrayListItemsNavigationDrawer = new ArrayList<ClassMenuItemsNavigationDrawer>();
        for (int i = 0; i < DATASET_COUNT; i++) {
            arrayListItemsNavigationDrawer.add(
                    ClassMenuItemsNavigationDrawer.newInstance(array_menu[i],
                            " ",
                            R.drawable.ic_launcher));
        }
    }


    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    //Si estos metodos no se llaman, no aparece la hamburguesa
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    //Variables para el recycler view
    private DrawerLayout drawerLayout;
    private RecyclerView navigationDrawerRecyclerView;
    private RecyclerView.Adapter navigationDrawerAdapter;
    private RecyclerView.LayoutManager navigationDrawerLayoutManager;
    protected ArrayList<ClassMenuItemsNavigationDrawer> arrayListItemsNavigationDrawer = null;
    private static final int DATASET_COUNT = 7;
    //Este objeto permite que el navigation Drawer interactue correctamente con el action bar (la toolbar en mi caso)
    private ActionBarDrawerToggle actionBarDrawerToggle;
    //FIN Variables para el recycler view

    //****************************************************************
    //****************************************************************
    //     FIN del Codigo relacionado con el navigation drawer
    //****************************************************************
    //****************************************************************




    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //  Inicio de Codigo relacionado con el API de JL, 21 Mayo 2015
    //****************************************************************
    //****************************************************************
    private void method_Data_To_Show_In_Fragments() {
//        ESte metodo obtiene datos del api de JL, pero habra que cambiar cosas.

        ConfigurationFactory cmTFactory = new ConfigurationFactory();

        try {
            myCmT = cmTFactory.getConfigurationManager(TypeConfiguration.INSTALLATION_DEFAULT_WITH_AUTONOMOUS_COMMUNITY);

            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(xxx, xxx +" Esta bien configurado [" + myCmT.isRightConfigured() + "]");
                Log.d(xxx, xxx +" El cmt esta configurado[" + myCmT.isRightConfigured() + "] numero AU[" + myCmT.getAutonomousCommunityList().size() + "]");
                Log.d(xxx, xxx +" La comunidad Autonoma Configurada[" + myCmT.getAutonomousCommunityConfigured().getAutonomousCommunityName()
                        + "] y el pueblo configurado[" + myCmT.getEventPlannerConfigured().getName() + "]");
            }

            ProgamListManager pmT = new ProgamListManager();
            pmT.loadData(myCmT.getAutonomousCommunityConfigured(),myCmT.getEventPlannerConfigured());
            if(pmT.existCurrentProgram()){
                Program p = pmT.getCurrentProgram();

                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(xxx, xxx +"HAY PROGRAMA EN MARCHA ["+p.getProgramSortDescription()+"]");

                }
                //Obtener la lista de eventos del programa por dias
                List<DayEvents> ltd = p.getEventByDay();
                //Obtener la lista de eventos completa
                List<Event> ltf =p.getFullList();
            }



            if(pmT.existCurrentProgram() || pmT.existFuturetPrograms() || pmT.existPastPrograms()) {
                 List_Programs = pmT.getFullProgramList();


                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(xxx, "HAY  [" + List_Programs.size() + "]" +" PROGRAMAS EN MARCHA");
                    for (int i=0; i<List_Programs.size(); i++){
                        Log.d(xxx, xxx +" Codigo del programa numero  [" +i + ": "
                                +List_Programs.get(i).getState().getCode() + "]" +" PROGRAMAS EN MARCHA");

                        Log.d(xxx, xxx +" Codigo del programa numero  [" +i + ": "
                                +List_Programs.get(i).getProgramSortDescription() + "]" +" PROGRAMAS EN MARCHA");

                        if(List_Programs.get(i).getState().getCode().equals(TypeState.ENDED)){
                            int_Number_Of_Fragments_To_Show++;
                            List_Programs.remove(i);
                        }

                    }
                }



                for (int i=0; i<List_Programs.size(); i++){
                    arrayList_Lista_De_Fiesta_Serializable.add(List_Programs.get(i));
                }
                method_Muestra_Fragment_Con_La_Lista_De_Las_Fiestas(int_Nivel_De_Info_Fiestas);


            }


        }
        catch(eefException eef)
        {
            Log.e("onCreate","Error inciando la conficuracion");
        }
        catch (Exception ex){
            Log.e("onCreate","Excepotion no eef inciando la configuracion");
        }


    }//Fin de method_Data_To_Show_In_Fragments


    IConfigurationManager myCmT=null;
    private static String xxx;
    List<Program> List_Programs;
    int int_Number_Of_Fragments_To_Show = 0;
    ArrayList<Program> arrayList_Lista_De_Fiesta_Serializable = new ArrayList<Program>();

    int int_Nivel_De_Info_Fiestas = 1;
    int int_Nivel_De_Info_Fiesta_Por_Dias = 2;
    int int_Nivel_De_Info_Fiesta_Eventos_Del_Dia = 3;
    int int_Nivel_De_Info_Fiesta_Evento_Detalle = 4;


    //****************************************************************
    //****************************************************************
    //     FIN del Codigo relacionado con el API de JL
    //****************************************************************
    //****************************************************************

    public void method_Muestra_Fragment_Con_La_Lista_De_Las_Fiestas(int int_Nivel_De_Info) {
// Create new fragment and transaction


        Fragment newFragment = getNewFragmnet(int_Nivel_De_Info);
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack

        if (int_Nivel_De_Info == int_Nivel_De_Info_Fiestas) {
            transaction.add(R.id.details, newFragment);
        }
        else{
            transaction.replace(R.id.details, newFragment);
            transaction.addToBackStack(null);
        }

// Commit the transaction
        transaction.commit();
    }// Fin de method_Muestra_Fragment_Con_La_Lista_De_Las_Fiestas

    public Fragment getNewFragmnet(int i) {
        Fragment fragment = new MiFragment_Fiestas_Con_RecycleView();
        Bundle args = new Bundle();
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
        args.putString(MiFragment_Fiestas_Con_RecycleView.ARG_OBJECT, "fragment#" +(i + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiestas_Con_RecycleView.ARG_PARAM2, String.valueOf(i + 1));

        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiestas_Con_RecycleView.TIPO_DE_LISTA, i);


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
        args.putSerializable("Lista de Programas Serializado", arrayList_Lista_De_Fiesta_Serializable);


        fragment.setArguments(args);

        return fragment;
    }

    //    Interfaz de la clase CustomAdapter_RecyclerView_Fiestas.
    @Override
    public void onClick(Program m_Programa_seleccionado) {
        Fragment newFragment = getNewFragmentForFiesta(m_Programa_seleccionado);
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack

        transaction.replace(R.id.details, newFragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();

    }

    public Fragment getNewFragmentForFiesta(Program m_Programa_seleccionado) {
        Fragment fragment = new MiFragment_Fiesta_Seleccionada_Con_RecycleView();
        Bundle args = new Bundle();
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "fragment#" +(int_Nivel_De_Info_Fiesta_Por_Dias + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, String.valueOf(int_Nivel_De_Info_Fiesta_Por_Dias + 1));

        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, int_Nivel_De_Info_Fiesta_Por_Dias);


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
        args.putSerializable("Programa Serializado", m_Programa_seleccionado);


        fragment.setArguments(args);

        return fragment;
    }


//    Interfaz de la clase CustomAdapter_RecyclerView_Fiesta_List_Por_Dias.
    @Override
    public void onClickDia(DayEvents m_DayEvents) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"He llegado a onClickDia" );
        }
        Fragment newFragment = getNewFragmentForDayEvents(m_DayEvents);
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack

        transaction.replace(R.id.details, newFragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }

    public Fragment getNewFragmentForDayEvents(DayEvents m_DayEvents) {
        Fragment fragment = new MiFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView();
        Bundle args = new Bundle();
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "fragment#" +(int_Nivel_De_Info_Fiesta_Eventos_Del_Dia + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, String.valueOf(int_Nivel_De_Info_Fiesta_Eventos_Del_Dia + 1));

        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, int_Nivel_De_Info_Fiesta_Eventos_Del_Dia);

//        Hago esto por que la lista de eventos no es serializable, asi que lo pongo en array para
//                pasarlo al fragment con la lista de los eventos del dia seleccionado
        ArrayList<Event> arrayList_Lista_De_Eventos_Del_Dia = new ArrayList<Event>();
        for (int i=0; i<m_DayEvents.getListaEventos().size(); i++){
            arrayList_Lista_De_Eventos_Del_Dia.add(m_DayEvents.getListaEventos().get(i));
        }


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
        args.putSerializable("Eventos del dia Serializado", arrayList_Lista_De_Eventos_Del_Dia);


        fragment.setArguments(args);

        return fragment;
    }

    //    Interfaz de la clase CustomAdapter_RecyclerView_List_De_Eventos.
    @Override
    public void onClickEvento(Event m_Event) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"He llegado a onClickEvento" );
        }
        Fragment newFragment = getNewFragmentToShowEvent(m_Event);
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack

        transaction.replace(R.id.details, newFragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }

    public Fragment getNewFragmentToShowEvent(Event m_Event) {
        Fragment fragment = new MiFragment_Evento();
        Bundle args = new Bundle();
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "fragment#" + (int_Nivel_De_Info_Fiesta_Evento_Detalle + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, String.valueOf(int_Nivel_De_Info_Fiesta_Evento_Detalle + 1));
        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, int_Nivel_De_Info_Fiesta_Evento_Detalle);

        args.putSerializable("Datos del Evento Serializado", m_Event);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onClickEventoDetalle(Event m_Event) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"He llegado a onClickEventoDetalle" );
        }
    }



    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //  4 Junio 2015 method_Data_To_Show_In_Fragments_2
    //****************************************************************
    //****************************************************************
    private void method_Data_To_Show_In_Fragments_2(Toolbar toolBar_Actionbar) {
//        ESte metodo obtiene datos del api de JL, pero habra que cambiar cosas.

        ConfigurationFactory cmTFactory = new ConfigurationFactory();

        try {
            myCmT = cmTFactory.getConfigurationManager(TypeConfiguration.INSTALLATION_DEFAULT_WITH_AUTONOMOUS_COMMUNITY);

            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(xxx, xxx +" Esta bien configurado [" + myCmT.isRightConfigured() + "]");
                Log.d(xxx, xxx +" El cmt esta configurado[" + myCmT.isRightConfigured() + "] numero AU[" + myCmT.getAutonomousCommunityList().size() + "]");
                Log.d(xxx, xxx +" La comunidad Autonoma Configurada[" + myCmT.getAutonomousCommunityConfigured().getAutonomousCommunityName()
                        + "] y el pueblo configurado[" + myCmT.getEventPlannerConfigured().getName() + "]");
            }

            ProgamListManager pmT = new ProgamListManager();
            pmT.loadData(myCmT.getAutonomousCommunityConfigured(),myCmT.getEventPlannerConfigured());
            if(pmT.existCurrentProgram()){
                Program p = pmT.getCurrentProgram();

                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(xxx, xxx +"HAY PROGRAMA EN MARCHA ["+p.getProgramSortDescription()+"]");

                }
                //Obtener la lista de eventos del programa por dias
                List<DayEvents> ltd = p.getEventByDay();
                //Obtener la lista de eventos completa
                List<Event> ltf =p.getFullList();
            }



            if(pmT.existCurrentProgram() || pmT.existFuturetPrograms() || pmT.existPastPrograms()) {
                List_Programs = pmT.getFullProgramList();


                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(xxx, "HAY  [" + List_Programs.size() + "]" +" PROGRAMAS EN MARCHA");
                    for (int i=0; i<List_Programs.size(); i++){
                        Log.d(xxx, xxx +" Codigo del programa numero  [" +i + ": "
                                +List_Programs.get(i).getState().getCode() + "]" +" PROGRAMAS EN MARCHA");

                        Log.d(xxx, xxx +" Codigo del programa numero  [" +i + ": "
                                +List_Programs.get(i).getProgramSortDescription() + "]" +" PROGRAMAS EN MARCHA");


//                        if(List_Programs.get(i).getState().getCode().equals(TypeState.ENDED)){
//                            int_Number_Of_Fragments_To_Show++;
//                            List_Programs.remove(i);
//                        }

                    }
                }


                toolBar_Actionbar.setTitle(myCmT.getEventPlannerConfigured().getName());


                for (int i=0; i<List_Programs.size(); i++){
                    arrayList_Lista_De_Fiesta_Serializable.add(List_Programs.get(i));
                }
                method_Muestra_Fragment_Con_La_Lista_De_Las_Fiestas(int_Nivel_De_Info_Fiestas);


            }


        }
        catch(eefException eef)
        {
            Log.e("onCreate","Error inciando la conficuracion");
        }
        catch (Exception ex){
            Log.e("onCreate","Excepotion no eef inciando la configuracion");
        }


    }//Fin de method_Data_To_Show_In_Fragments_2

    @Override
    public void onClickMenuItenDelNavDrawer(int position_Del_Menu_Seleccionada) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"He llegado a onClickMenuItenDelNavDrawer  con click: " +position_Del_Menu_Seleccionada );
        }

//        Intent intent = new Intent(this, Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
//
//        startActivity(intent);



        Intent intent;


        switch (position_Del_Menu_Seleccionada)
        {
            case 0:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_frame_layout.
                        Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 1://Mis Eventos
                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_list_of_favorites.
                        Activity_Fragment_List_Of_Favorites_With_Nav_Drawer.class);
                startActivity(intent);
                break;
            case 2:
//                intent = new Intent(this, com.o3j.es.estamosenfiestas.activity_actualizar_fiestas.
//                        Activity_Fragment_Actualizar_Fiesta_WithNavDrawer_2.class);
//                startActivity(intent);

                drawerLayout.closeDrawer(Gravity.LEFT);

                break;
            case 3:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.application_configuration.
                        Activity_Application_Configuration_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 4:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_event_planner_information.
                        Activity_Display_Event_Planner_Information_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 5:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.activity_compartir.
                        Activity_Fragment_Compartir_App_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 6:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_informacion.
                        Activity_Display_Developer_Information_WithNavDrawer_2.class);
                startActivity(intent);
                break;
        }
        if (position_Del_Menu_Seleccionada != 2) finish();
//        if(position_Del_Menu_Seleccionada == 3){
//            //Solo muestra el toast
//            Toast.makeText(this, "Opción no disponible", Toast.LENGTH_LONG).show();
//        }else{
//            if (position_Del_Menu_Seleccionada != 2) finish();
//        }
    }

    @Override
    public boolean onLongClickEvento_CustomA_List_De_Eventos(Event m_Event, View m_View_Del_Evento) {

        return true;

    }

    //***********************************************************************************
    //***********************************************************************************
    //     21 agosto 2015, Actualizacion
    //     Juan, 21 agosto 2015: Todo lo nuevo para que la clase sea funcional.
    //***********************************************************************************
    //***********************************************************************************
    //Metodos de la interface del dialogo de la clase ClaseDialogGenericaDosTresBotones
    @Override
    public void onDialogPositiveClick_ClaseDialogGenericaDosTresBotones(DialogFragment dialog) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
        {
            Log.e(xxx, xxx + ": En metodo onDialogPositiveClick_ClaseDialogGenericaDosTresBotones: ");
        }
        //Continuar con la actualizacion:
        method_Fuera_Del_On_Create_Chequea_Acceso_A_Internet();
    }

    @Override
    public void onDialogNegativeClick_ClaseDialogGenericaDosTresBotones(DialogFragment dialog) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
        {
            Log.e(xxx, xxx + ": En metodo  onDialogNegativeClick_ClaseDialogGenericaDosTresBotones: ");
        }
        //Devuelve al usuario a los programas
        Intent intent;
        intent = new Intent(this, com.o3j.es.estamosenfiestas.display_frame_layout.
                Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onDialogNeutralClick_ClaseDialogGenericaDosTresBotones(DialogFragment dialog) {
        //Solo sale con el error general en method_Muestra_Dialog_Si_Hay_Programas_Que_Actualizar
        // y en method_Muestra_Dialog_No_Hay_Programas_Que_Actualizar para volver a programas
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
        {
            Log.e(xxx, xxx + ": En metodo  onDialogNeutralClick_ClaseDialogGenericaDosTresBotones: ");
        }
        Intent intent;
        intent = new Intent(this, com.o3j.es.estamosenfiestas.display_frame_layout.
                Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
        startActivity(intent);
        finish();
    }

    public void method_Fuera_Del_On_Create_Chequea_Acceso_A_Internet() {
        //*************************************************************************************************
        if(method_Dime_Si_Hay_Red()) {//Chequeamos si hay red
            //Si hay red, sigue ejecutando
            //24 julio 2015, nuevo metodo, ahora usamos retrofit y receivers
            method_Actualiza();

        }else{//NO hay red
            // Create an instance of the dialog fragment and show it
            DialogFragment dialog =  ClaseDialogNoHayInternet.newInstance
                    (R.string.titulo_dialog_error_no_hay_internet_reconfigurar, R.string.message_dialog_error_no_hay_internet_reconfigurar, R.string.positiveButton_error_no_hay_internet_reconfigurar,
                            R.string.negativeButton_error_no_hay_internet_reconfigurar, 2, R.string.boton_neutral_evento_guardado);
            dialog.show(getSupportFragmentManager(), "ClaseDialogNoHayInternet");
        }
        //*************************************************************************************************
    }

    public boolean method_Dime_Si_Hay_Red() {

        //antes de lanzar cualquier servicio web al server, verifico si hay acceso a internet
        ClaseChequeaAcceso_A_Internet claseChequeaAcceso_A_Internet = new ClaseChequeaAcceso_A_Internet(this);
        if(claseChequeaAcceso_A_Internet.isOnline()) {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(xxx, xxx +"En metodo a method_Dime_Si_Hay_Red: SI" );
            }
            return true;
        }else {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(xxx, xxx +"En metodo a method_Dime_Si_Hay_Red: NO" );
            }
            return false;
        }
    }


    //Interface de dialogo de evento No hay Internet
    @Override
    public void onDialogPositiveClick_InterfaceClaseDialogNoHayInternet(DialogFragment dialog) {
        //Reintento mostrar la info
        method_Fuera_Del_On_Create_Chequea_Acceso_A_Internet();

    }

    @Override
    public void onDialogNegativeClick_InterfaceClaseDialogNoHayInternet(DialogFragment dialog) {

        //Devuelve al usuario a los programas
        Intent intent;
        intent = new Intent(this, com.o3j.es.estamosenfiestas.display_frame_layout.
                Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onDialogNeutralClick_InterfaceClaseDialogNoHayInternet(DialogFragment dialog) {

        //No hago nada
    }

    public void method_Actualiza() {
//        DownloadInfoFromServerManager downloadInfoFromServerManager = new DownloadInfoFromServerManager();
//        downloadInfoFromServerManager.initManager(this, broadcast_Progreso_Actualizar, broadcast_Fin_De_La_Auctualizacion);




        //**********************************************************************************
        DownloadInfoFromServerForUpdateManager downloadInfoFromServerForUpdateManager = new DownloadInfoFromServerForUpdateManager();
        downloadInfoFromServerForUpdateManager.initManager(this, broadcast_Progreso_Actualizar, broadcast_Fin_De_La_Auctualizacion);
        //**********************************************************************************

        try {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(xxx, xxx +"En metodo a method_Actualiza dentro del try" );
            }
            //Actualiza la info de las fiestas
            downloadInfoFromServerForUpdateManager.updateProgramsFromServer();

            //Inicio aqui el progress bar
            if (boolean_Init_Progress_Dialog_2) {
                boolean_Init_Progress_Dialog_2 = false;
                method_Iniciar_Progress_Bar_Actualizar_Fiestas();
            }
        } catch (eefException eefe) {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(xxx, xxx +"En metodo a method_Actualiza: " +"catch, ERROR ACTUALIZANDO FIESTAS: " + eefe.getLocalizedMessage());
            }
            //Para el progress bar
            progressDialog.dismiss();
            //Muestr dialogo de error
            method_Muestra_Dialog_Error_Durante_La_Actualizacion();

        }
    }//Fin de method_Actualiza

    private void method_Muestra_Dialog_Si_Hay_Programas_Que_Actualizar() {

        //Utilizo el dialogo general con boton OK
        // Juan, 21 agosto 2015: Muestra el dialogo de inicio de actualizar
        DialogFragment dialog =  ClaseDialogGenericaDosTresBotones.newInstance
                (R.string.titulo_dialog_actualizar, R.string.message_dialog_actualizar_si_habia_que_actualizar, R.string.positiveButton_dialog_actualizar,
                        R.string.negativeButton_dialog_actualizar, 1, R.string.neutralButton_dialog_actualizar);
        dialog.show(getSupportFragmentManager(), "ClaseDialogGenericaDosTresBotones");

    }//Fin de method_Muestra_Dialog_Si_Hay_Programas_Que_Actualizar

    private void method_Muestra_Dialog_No_Hay_Programas_Que_Actualizar() {

        //Utilizo el dialogo general con boton OK
        // Juan, 21 agosto 2015: Muestra el dialogo de inicio de actualizar
        DialogFragment dialog =  ClaseDialogGenericaDosTresBotones.newInstance
                (R.string.titulo_dialog_actualizar, R.string.message_dialog_actualizar_no_habia_que_actualizar, R.string.positiveButton_dialog_actualizar,
                        R.string.negativeButton_dialog_actualizar, 1, R.string.neutralButton_dialog_actualizar);
        dialog.show(getSupportFragmentManager(), "ClaseDialogGenericaDosTresBotones");


    }//Fin de method_Muestra_Dialog_No_Hay_Programas_Que_Actualizar

    private void method_Muestra_Dialog_Error_Durante_La_Actualizacion() {

        //Nuevo dialogo con reintentear, volver a programas, puedo re-usar el de internet
        // Create an instance of the dialog fragment and show it
        DialogFragment dialog =  ClaseDialogNoHayInternet.newInstance
                (R.string.titulo_dialog_actualizar, R.string.message_dialog_actualizar_error, R.string.positiveButton_error_no_hay_internet_reconfigurar,
                        R.string.negativeButton_error_no_hay_internet_reconfigurar, 2, R.string.boton_neutral_evento_guardado);
        dialog.show(getSupportFragmentManager(), "ClaseDialogNoHayInternet");

    }//Fin de method_Muestra_Dialog_Error_Durante_La_Actualizacion

    //***********************************************************************************
    //***********************************************************************************
    //     21 agosto 2015, Actualizacion
    //     Inicio de todo lo relacionado con los broadcast receivers y progress bar
    //***********************************************************************************
    //***********************************************************************************
    //ID DEL BROADCAST PARA INDICAR EL PROGRESO DE LA DESCARGA DE PROGRAMAS Y EVENTOS
    String broadcast_Progreso_Actualizar = "broadcast_Progreso_Actualizar";
    //ID DEL BROADCAST PARA INDICAR EL FIN DE LA DESCARGA DE PROGRAMAS Y EVENTOS
    String broadcast_Fin_De_La_Auctualizacion = "broadcast_Fin_De_La_Auctualizacion";

    MyBroadcastReceiver_Progress_Bar_Actualizar myBroadcastReceiver_progress_bar_actualizar;
    IntentFilter intentFilter_MyBroadcastReceiver_Progress_Bar_Actualizar;
    boolean boolean_Init_Progress_Dialog_2 = true;

    public class MyBroadcastReceiver_Progress_Bar_Actualizar extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
            {
                Log.e(xxx, xxx + " en metodo onReceive de MyBroadcastReceiver_Progress_Bar_Actualizar");
            }

            Bundle bundle = intent.getExtras();
            if (bundle != null) {

//                int NUMERO_PROGRAMAS_A_DESCARGA = bundle.getInt(DownloadInfoFromServerManager.NUMERO_PROGRAMAS_A_DESCARGA, 0);
//                String DESCARGADO_INFO_EVENT_PLANNER = bundle.getString(DownloadInfoFromServerManager.DESCARGADO_INFO_EVENT_PLANNER, "string por defecto");
//                String DESCARGADAS_LISTAS_DE_PROGRAMAS = bundle.getString(DownloadInfoFromServerManager.DESCARGADAS_LISTAS_DE_PROGRAMAS, "string por defecto");
//                String CONTINUAR_DESCARGA = bundle.getString(DownloadInfoFromServerManager.CONTINUAR_DESCARGA, "string por defecto");
//                String PROGRAMA_DESCARGADO = bundle.getString(DownloadInfoFromServerManager.PROGRAMA_DESCARGADO, "string por defecto");
//                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
//                {
//                    Log.e(xxx, xxx + " MyBroadcastReceiver_Progress_Bar_Programa_y_Eventos: " +"\n"
//                            +"NUMERO_PROGRAMAS_A_DESCARGA" +NUMERO_PROGRAMAS_A_DESCARGA +"\n"
//                            +"DESCARGADO_INFO_EVENT_PLANNER" +DESCARGADO_INFO_EVENT_PLANNER +"\n"
//                            +"DESCARGADAS_LISTAS_DE_PROGRAMAS" +DESCARGADAS_LISTAS_DE_PROGRAMAS +"\n"
//                            +"CONTINUAR_DESCARGA" +CONTINUAR_DESCARGA +"\n"
//                            +"PROGRAMA_DESCARGADO" +PROGRAMA_DESCARGADO);
//                }

                //Iniciado el progress bar en method_Actualiza tambien
                if (boolean_Init_Progress_Dialog_2) {
                    boolean_Init_Progress_Dialog_2 = false;
                    method_Iniciar_Progress_Bar_Actualizar_Fiestas();
                }

                //Por ahora no llamo a incrementar y uso el spinner
//                method_Actualiza_Progress_Dialog_Programas_Y_Eventos();


            }
        }
    }//Fin de MyBroadcastReceiver_Progress_Bar_Actualizar

    MyBroadcastReceiver_Fin_Actualizar myBroadcastReceiver_fin_actualizar;
    IntentFilter intentFilter_MyBroadcastReceiver_Fin_Actualizar;

    public class MyBroadcastReceiver_Fin_Actualizar extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            progressDialog.dismiss();

            //Vuelvo a poner true esta variable por si este pueblo no tiene nada y selecciono otro pueblo
            boolean_Init_Progress_Dialog_2 = true;

//            String UPDATE_HAY_PROGRAMAS_QUE_ACTUALIZAR = DownloadInfoFromServerForUpdateManager.UPDATE_HAY_PROGRAMAS_QUE_ACTUALIZAR;
//            String UPDATE_NUMERO_PROGRAMAS = DownloadInfoFromServerForUpdateManager.UPDATE_NUMERO_PROGRAMAS;
//            String UPDATE_NUMERO_PROGRRAMAS_ACTUALIZADOS = DownloadInfoFromServerForUpdateManager.UPDATE_NUMERO_PROGRRAMAS_ACTUALIZADOS;
//            String UPDATE_PROGRAMS_PROCESS_ERROR = DownloadInfoFromServerForUpdateManager.UPDATE_PROGRAMS_PROCESS_ERROR;

            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
            {
                Log.e(xxx, xxx + "  en metodo onReceive de MyBroadcastReceiver_Fin_Actualizar");
            }

            Bundle bundle = intent.getExtras();
            if (bundle != null) {

                Boolean Boolean_UPDATE_PROGRAMS_PROCESS_ERROR = bundle.getBoolean(DownloadInfoFromServerForUpdateManager.UPDATE_PROGRAMS_PROCESS_ERROR);
                Boolean UPDATE_HAY_PROGRAMAS_NUEVOS = bundle.getBoolean(DownloadInfoFromServerForUpdateManager.UPDATE_HAY_PROGRAMAS_NUEVOS);
                Boolean boolean_UPDATE_HAY_PROGRAMAS_QUE_ACTUALIZAR = bundle.getBoolean(DownloadInfoFromServerForUpdateManager.UPDATE_HAY_PROGRAMAS_QUE_ACTUALIZAR);

                //***********************************************************************************************
                //No se si los demas son integer o string, hago esto por si acaso
                int UPDATE_NUMERO_PROGRAMAS_ACTUALIZAR = bundle.getInt(DownloadInfoFromServerForUpdateManager.UPDATE_NUMERO_PROGRAMAS_ACTUALIZAR);
                int UPDATE_NUMERO_PROGRAMAS_NUEVOS = bundle.getInt(DownloadInfoFromServerForUpdateManager.UPDATE_NUMERO_PROGRAMAS_NUEVOS);

//                String string_UPDATE_NUMERO_PROGRAMAS = bundle.getString
//                        (DownloadInfoFromServerManager.UPDATE_NUMERO_PROGRAMAS, "numero_programas_es_integer");
//                String string_UPDATE_NUMERO_PROGRRAMAS_ACTUALIZADOS = bundle.getString
//                        (DownloadInfoFromServerManager.UPDATE_NUMERO_PROGRRAMAS_ACTUALIZADOS, "numero_programas_actualizado_es_integer");


//                broadcastIntent.putExtra(CONFIGURACION_PROGRAMAS_EN_DB, "true");


//            OK    broadcastIntent.putExtra(UPDATE_HAY_PROGRAMAS_QUE_ACTUALIZAR, "true"); OK
//            OK    broadcastIntent.putExtra(UPDATE_HAY_PROGRAMAS_NUEVOS,updatedProgramInfo.getListaProgramasNew().size() > 0?new Boolean(true): new Boolean(false))
//            OK    broadcastIntent.putExtra(UPDATE_NUMERO_PROGRAMAS_ACTUALIZAR, updatedProgramInfo.getListaProgramasToUpdate().size());
//            OK    broadcastIntent.putExtra(UPDATE_NUMERO_PROGRAMAS_NUEVOS,updatedProgramInfo.getListaProgramasNew().size());
//             OK   broadcastIntent.putExtra(UPDATE_PROGRAMS_PROCESS_ERROR, "false");


                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
                {
                    Log.e(xxx, xxx + "  en metodo onReceive de MyBroadcastReceiver_Fin_Actualizar" +"\n" +
                            "Boolean_UPDATE_PROGRAMS_PROCESS_ERROR: " +Boolean_UPDATE_PROGRAMS_PROCESS_ERROR +"\n" +
                            " UPDATE_HAY_PROGRAMAS_NUEVOS  " +UPDATE_HAY_PROGRAMAS_NUEVOS +"\n" +
                            "boolean_UPDATE_HAY_PROGRAMAS_QUE_ACTUALIZAR: " +boolean_UPDATE_HAY_PROGRAMAS_QUE_ACTUALIZAR +"\n" +
                            "UPDATE_NUMERO_PROGRAMAS_ACTUALIZAR: " +UPDATE_NUMERO_PROGRAMAS_ACTUALIZAR +"\n" +
                            "UPDATE_NUMERO_PROGRAMAS_NUEVOS: " +UPDATE_NUMERO_PROGRAMAS_NUEVOS);
                }
                //***********************************************************************************************


//
//                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
//                {
//                    Log.e(xxx, xxx + "  en metodo onReceive de MyBroadcastReceiver_Fin_Actualizar" +
//                    "string_UPDATE_PROGRAMS_PROCESS_ERROR: " +string_UPDATE_PROGRAMS_PROCESS_ERROR +
//                    "Hay boolean_UPDATE_HAY_PROGRAMAS_QUE_ACTUALIZAR que actualizar: " +boolean_UPDATE_HAY_PROGRAMAS_QUE_ACTUALIZAR +
//                    "integer_UPDATE_NUMERO_PROGRAMAS: " +integer_UPDATE_NUMERO_PROGRAMAS +
//                    "integer_UPDATE_NUMERO_PROGRRAMAS_ACTUALIZADOS: " +integer_UPDATE_NUMERO_PROGRRAMAS_ACTUALIZADOS +
//                    "string_UPDATE_NUMERO_PROGRAMAS: " +string_UPDATE_NUMERO_PROGRAMAS +
//                    "string_UPDATE_NUMERO_PROGRRAMAS_ACTUALIZADOS: " +string_UPDATE_NUMERO_PROGRRAMAS_ACTUALIZADOS);
//                }
//                //***********************************************************************************************

                if(!Boolean_UPDATE_PROGRAMS_PROCESS_ERROR) {
                    if(boolean_UPDATE_HAY_PROGRAMAS_QUE_ACTUALIZAR){
                        method_Muestra_Dialog_Si_Hay_Programas_Que_Actualizar();
                    }else{
                        if(UPDATE_HAY_PROGRAMAS_NUEVOS){
                            method_Muestra_Dialog_Hay_Programas_Nuevos();
                        }else{
                            method_Muestra_Dialog_No_Hay_Programas_Que_Actualizar();
                        }
                    }

                }else{
                    method_Muestra_Dialog_Error_Durante_La_Actualizacion();
                }
            }
        }
    }//FIN DE MyBroadcastReceiver_Fin_Actualizar


    private void method_Muestra_Dialog_Hay_Programas_Nuevos() {

        //Utilizo el dialogo general con boton OK
        // Juan, 21 agosto 2015: Muestra el dialogo de inicio de actualizar
        DialogFragment dialog =  ClaseDialogGenericaDosTresBotones.newInstance
                (R.string.titulo_dialog_actualizar, R.string.Message_Hay_un_nuevo_programa_de_fiestas, R.string.positiveButton_dialog_actualizar,
                        R.string.negativeButton_dialog_actualizar, 1, R.string.neutralButton_dialog_actualizar);
        dialog.show(getSupportFragmentManager(), "ClaseDialogGenericaDosTresBotones");

    }//Fin de method_Muestra_Dialog_Hay_Programas_Nuevos

    ProgressDialog progressDialog;
    private void method_Iniciar_Progress_Bar_Actualizar_Fiestas() {
        progressDialog = null;
//        Como en:
//        http://www.compiletimeerror.com/2013/09/android-progress-dialog-example.html#.VbO_KvntmM0
        progressDialog = new ProgressDialog(this);
        progressDialog.setMax(100);
//        progressDialog.setMessage("Cargando datos....");
//        progressDialog.setTitle("Configuración de la aplicación");
        progressDialog.setMessage(getResources().getString(R.string.progress_bar_mensaje_1));
        progressDialog.setTitle(getResources().getString(R.string.progress_bar_actualizando_la_aplicación));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        //Registra los receptores
        registerReceiver(myBroadcastReceiver_progress_bar_actualizar, intentFilter_MyBroadcastReceiver_Progress_Bar_Actualizar);
        registerReceiver(myBroadcastReceiver_fin_actualizar, intentFilter_MyBroadcastReceiver_Fin_Actualizar);
    }

    @Override
    public void onPause() {
        super.onPause();
        //Elimina el registro de los receptores
        unregisterReceiver(myBroadcastReceiver_progress_bar_actualizar);
        unregisterReceiver(myBroadcastReceiver_fin_actualizar);
    }
    //***********************************************************************************
    //***********************************************************************************
    //     21 agosto 2015, Actualizacion
    //     FIN de todo lo relacionado con los broadcast receivers y progress bar
    //***********************************************************************************
    //***********************************************************************************

}//Fin de la clase
