/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.o3j.es.estamosenfiestas.display_event_planner_information;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eef.data.dataelements.EventPlanner;
import com.eef.data.eefExceptions.eefException;
import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Provide views to RecyclerView with data from dataSet.
 */
//Juan, 4 Junio 2015. Uso este adpater para gestionar el evento como una lista con recycle view,
//    y mantener el mismo manejo que para la lista de fiestas, dias de la fiesta y lista de eventos
//    Se muestran los detalles del evento en la lista List_De_Event_Planner
public class CustomAdapter_RecyclerView_Event_Planner_Information extends RecyclerView.Adapter<CustomAdapter_RecyclerView_Event_Planner_Information.ViewHolder> {
//    private static final String TAG = "CustomAdapter";
    private static  String TAG;



//    private static FragmentActivity actividad;


    //Original
//    private String[] mDataSet;


    private List<EventPlanner> List_De_Event_Planner; //Solo tiene el evento seleccionado con sus detalles

//    Agrego la interface para recoger los clicks en la actividad
    private OnItemClickListenerEventPlannerInformation mListener;

    // BEGIN_INCLUDE(recyclerViewSampleViewHolder)
    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */


    public static class ViewHolder extends RecyclerView.ViewHolder {
        //Los nombres como en Class_Evento_1
        private final TextView textView_Titulo;
        private final TextView textView_Descripcion;
        private final TextView textView_Descripcion2;
        private final ImageView imageView;
        private final ImageView imageViewGrande;
        private final View m_View;



        public TextView getTextView_Titulo() {

            return textView_Titulo;
        }

        public TextView getTextView_Descripcion() {

            return textView_Descripcion;
        }

        public TextView getTextView_Descripcion2() {

            return textView_Descripcion2;
        }

        public ImageView getImageView() {
            return imageView;
        }

        public ImageView getImageView_Grande() {
            return imageViewGrande;
        }


        public View getView() {
            return m_View;
        }



        public ViewHolder(View v) {
            super(v);

            m_View = v;
            textView_Titulo = (TextView) v.findViewById(R.id.textView6);
            textView_Descripcion = (TextView) v.findViewById(R.id.textView7);
            textView_Descripcion2 = (TextView) v.findViewById(R.id.textView8);
            imageView = (ImageView) v.findViewById(R.id.imageView);
            imageViewGrande = (ImageView) v.findViewById(R.id.imageView_foto_Grande);



        }//Fin del constructor de view holder

    }//Fin de la clase ViewHolder
    // END_INCLUDE(recyclerViewSampleViewHolder)


    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used by RecyclerView.
     */
    //Original
//    public CustomAdapter_RecyclerView_3(String[] dataSet) {

    //21 Feb 2015 modificado para funcionar con el array list
//    public CustomAdapter_RecyclerView_3(ArrayList<Class_Evento_1> dataSet) {
    //Original
//    public CustomAdapter_RecyclerView_3(ArrayList<Class_Evento_1> dataSet, String intNumeroAdapter) {

    //Le paso el context para poder llamar a la actividad que muestra el detalle
    public CustomAdapter_RecyclerView_Event_Planner_Information(List<EventPlanner> dataSet,
                                                                String intNumeroAdapter,
                                                                OnItemClickListenerEventPlannerInformation mListener,
                                                                Activity_Display_Event_Planner_Information_WithNavDrawer_2
                                                                        activity_Display_Event_Planner_Information_WithNavDrawer_2) {
//        mDataSet = dataSet;
        List_De_Event_Planner = dataSet;
//        TAG = this.getClass().getName();
        //Con simple name por que con name el string es muy largo por que nombra el paquete tambien
        TAG = this.getClass().getSimpleName();
            this.intNumeroAdapter = intNumeroAdapter;
            this.stringNumeroAdapter = TAG +" " + this.intNumeroAdapter +", ";

        this.mListener = mListener;

        this.activity_Display_Event_Planner_Information_WithNavDrawer_2 = activity_Display_Event_Planner_Information_WithNavDrawer_2;
    }//Fin del constructor

    Activity_Display_Event_Planner_Information_WithNavDrawer_2
            activity_Display_Event_Planner_Information_WithNavDrawer_2;

    //Uso eete integer para saber que instancia estoy viendo en el log
    private String intNumeroAdapter;
    private String stringNumeroAdapter;

    // BEGIN_INCLUDE(recyclerViewOnCreateViewHolder)
    // Create new views (invoked by the layout manager)
    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
//                .inflate(R.layout.fila_info_eventos_1, viewGroup, false);
        .inflate(R.layout.fila_info_detail_card_view, viewGroup, false);

//        NOTA IMPORTANTE: ver selectableItemBackground en fila_info_eventos_1
//        To display visual responses like ripples on screen when a click event is detected add selectableItemBackground
//        resource in the layout (highlighted above).



//        return new ViewHolder(v);
        return new ViewHolder(v);

    }
    // END_INCLUDE(recyclerViewOnCreateViewHolder)

    // BEGIN_INCLUDE(recyclerViewOnBindViewHolder)
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
//            Log.d(TAG, stringNumeroAdapter +": En metodo onBindViewHolder " + position + " set.");
            Log.d(TAG,  ": En metodo onBindViewHolder " + position + " set.");
        }

        //Prueba de uso de loader
        //Hago un chequeo de null, y regreso sin imprimir nada
        if(List_De_Event_Planner == null) return;


//        Juan, 26 Mayo 2015: Relleno el ViewHolder con los datos de los dias:

        // Get element from your dataset at this position and replace the contents of the view
        // with that element
//        Por ahora, solo muestro el string con el dia

        viewHolder.getTextView_Titulo().
                setText(List_De_Event_Planner.get(position).getName());

        viewHolder.getTextView_Descripcion().
        setText(List_De_Event_Planner.get(position).getSortDescription());

        viewHolder.getTextView_Descripcion2().
                setText(List_De_Event_Planner.get(position).getLongDescription());


//        viewHolder.getImageView().
//                setImageResource(R.drawable.ic_launcher);

//        viewHolder.getImageView().
//                 setImageResource(R.drawable.bandera_ayto_san_ildefonso);
        viewHolder.getImageView().setVisibility(View.GONE);



        //Voy a usar el metodo method_Descarga_Imagen_De_Internet
//        viewHolder.getImageView_Grande().setImageResource(R.drawable.escudo_san_ildefonso);

        //************************************************************************************
        //Juan 17 agosto 2015: presento la imagen como en:
        //http://themakeinfo.com/2015/04/android-retrofit-images-tutorial/
        //Con esta instruccion:
        //Picasso.with(context).load("http://i.imgur.com/DvpvklR.png").into(imageView);
        method_Descarga_Imagen_De_Internet(viewHolder.getImageView_Grande(),
                List_De_Event_Planner.get(position));
        //*************************************************************************************

//        Juan, 3 Junio 2015, pongo los onclick aqui (como en el proyecto NavigationDrawer) y no en el constructor del viewHolder
        viewHolder.getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(TAG, "Element " + position + " clicked.");
                }


                //23 Feb 2015: Llamo a la actividad que muestra el detalle del evento
                // Do something in response to button

//                Esta llamada obtiene un objeto Events que tiene todos los datos de ese evento
                mListener.onClickEventPlannerInformation(List_De_Event_Planner.get(position));

            }
        });


        viewHolder.getView().setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(TAG, "Element " + position + " LONG clicked.");
                }
//                    OJO: Si retorno false, entonces se dispara el onClick!!
//                    return false;
//                    Por eso si solo quiero detectar el long click, retorno true
                return true;
            }
        });
    }
    // END_INCLUDE(recyclerViewOnBindViewHolder)

    public void method_Descarga_Imagen_De_Internet(ImageView imageView, EventPlanner eventPlanner) {
        //Juan 17 agosto 2015: presento la imagen como en:
        //http://themakeinfo.com/2015/04/android-retrofit-images-tutorial/
        //Con esta instruccion:
        //Picasso.with(context).load("http://i.imgur.com/DvpvklR.png").into(imageView);
        //O esta:
        //Picasso.with(getContext()).load(url+flower.getPhoto()).resize(100,100).into(img);

        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, ": En metodo method_Descarga_Imagen_De_Internet");
        }

        String url = null;


        try {
            url = eventPlanner.getMultimediaElementList().get(0).getMultimediaElementLocation();
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG, ": En metodo method_Descarga_Imagen_De_Internet, url: " +url);
            }
            if(!Class_ConstantsHelper.methodStaticChequeaURL(url)){//url invalida
//            if(url == null || url.isEmpty()){
                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.e(TAG, ": En metodo method_Descarga_Imagen_De_Internet: url es NULL o esta VACIO");
                }
                  imageView.setVisibility(View.GONE);
//                imageView.setVisibility(View.INVISIBLE);
            }else{//url es valido, invoca la descarga de la imagen
                //Asi se puede ver deformada
//                Picasso.with(activity_Display_Event_Planner_Information_WithNavDrawer_2).load(url).resize(400, 400).into(imageView);
                //Asi se ven bien, aunque algo recortadas, depende del tamaño
//                Picasso.with(activity_Display_Event_Planner_Information_WithNavDrawer_2).load(url).fit().centerCrop().into(imageView);
                Picasso.with(activity_Display_Event_Planner_Information_WithNavDrawer_2).load(url).fit().centerInside().into(imageView);
                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.e(TAG, ": En metodo method_Descarga_Imagen_De_Internet: hay una url y llamo a picasso");
                    //Se a el caso que una url "" no la trata como un empty y llama a picasso
                }
            }
//            Picasso.with(activity_Display_Event_Planner_Information_WithNavDrawer_2).load(url).resize(400, 400).into(imageView);
        } catch (eefException eef) {
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG, ": En metodo method_Descarga_Imagen_De_Internet en eefException: " + eef.getLocalizedMessage());
            }
            //Si hay algun problema, hago invisible la imagen
            imageView.setVisibility(View.INVISIBLE);

        }

    }//Fin de method_Descarga_Imagen_De_Internet

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
//        return mDataSet.length;

        if (List_De_Event_Planner == null) {
            return 0;
        }

        return List_De_Event_Planner.size();
    }

    /**
     * Interface para recibir en Activity_Fragment_FrameLayout_WithNavDrawer_2 el click de la fiesta seleccionada
     */
    public interface OnItemClickListenerEventPlannerInformation {
        public void onClickEventPlannerInformation(EventPlanner m_Event_Planner);
    }
}//Fin de la clase
