package com.o3j.es.estamosenfiestas.orm.pojosDB;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by jluis on 27/07/15.
 */
@DatabaseTable
public class Programa {
    public final static String ID ="_id";
    public final static String ORGANIZADOR="organizador";
    public final static String NAME="name";
    public final static String FECHA_INICIO="fecha_inicio";
    public final static String FECHA_FIN="fecha_fin";
    public final static String URL_LOGO="url_logo";
    public final static String LOCAL_LOGO="local_logo";
    public final static String FECHA_ULTIMA_DESCARGA="fecha_descarga";
    public final static String FECHA_ULTIMA_ACTUALIZACION="fecha_ultima_actualizacion";

    @DatabaseField(id=true, columnName = ID)
    private int id;
    @DatabaseField(foreign = true, columnName = ORGANIZADOR)
    private Organizador organizador;
    @DatabaseField(width = 255,columnName = NAME)
    private String name;
    @DatabaseField(columnName = FECHA_INICIO)
    private Date fecha_inicio;
    @DatabaseField(columnName = FECHA_FIN)
    private Date fecha_fin;
    @DatabaseField(width = 255,columnName = URL_LOGO)
    private String url_logo;
    @DatabaseField(width = 255,columnName = LOCAL_LOGO)
    private String local_logo;
    @DatabaseField(columnName = FECHA_ULTIMA_DESCARGA)
    private Date fecha_descarga;
    @DatabaseField(columnName = FECHA_ULTIMA_ACTUALIZACION)
    private Date fecha_ultima_actualizacion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Organizador getOrganizador() {
        return organizador;
    }

    public void setOrganizador(Organizador organizador) {
        this.organizador = organizador;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(Date fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public Date getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(Date fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public String getUrl_logo() {
        return url_logo;
    }

    public void setUrl_logo(String url_logo) {
        this.url_logo = url_logo;
    }

    public String getLocal_logo() {
        return local_logo;
    }

    public void setLocal_logo(String local_logo) {
        this.local_logo = local_logo;
    }

    public Date getFecha_descarga() {
        return fecha_descarga;
    }

    public void setFecha_descarga(Date fecha_descarga) {
        this.fecha_descarga = fecha_descarga;
    }

    public Date getFecha_ultima_actualizacion() {
        return fecha_ultima_actualizacion;
    }

    public void setFecha_ultima_actualizacion(Date fecha_ultima_actualizacion) {
        this.fecha_ultima_actualizacion = fecha_ultima_actualizacion;
    }
}
