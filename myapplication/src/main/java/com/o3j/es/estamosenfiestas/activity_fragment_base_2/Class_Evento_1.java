package com.o3j.es.estamosenfiestas.activity_fragment_base_2;

import java.io.Serializable;

/**
 * Created by Juan on 20/02/2015.
 */
//20 Feb 2015: Esta clase es un modelo de datos  para cada evento de una determinada fiesta
//Esta clase sigue el patron builder
//Esta clase se puede usar para listar los eventos en la lista de eventos y tambien para mostrar
//el detalle de ese evento con el xml fila_info_eventos_1
public class Class_Evento_1 implements Serializable{
    public Class_Evento_1() {
    }

    public static com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_Evento_1 newInstance(String param1, String param2, String param3, int param4) {
        com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_Evento_1 class_Evento_1 = new com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_Evento_1();
        class_Evento_1.setTitulo(param1);
        class_Evento_1.setDescripcion(param2);
        class_Evento_1.setDescripcion2(param3);
        class_Evento_1.setSrc_Del_Image_View(param4);
        return class_Evento_1;
    }

    private String titulo;
    private String descripcion;
    private String descripcion2;
    private int src_Del_Image_View;

    public int getSrc_Del_Image_View() {
        return src_Del_Image_View;
    }

    public void setSrc_Del_Image_View(int src_Del_Image_View) {
        this.src_Del_Image_View = src_Del_Image_View;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getDescripcion2() {
        return descripcion2;
    }

    public void setDescripcion2(String descripcion2) {

        this.descripcion2 = descripcion2;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
