package com.o3j.es.estamosenfiestas.display_lists_package;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.eef.data.dataelements.Program;

import java.util.List;

/**
 * Created by Juan on 14/02/2015.
 */


public class AdaptadorDeSwipeViews_2 extends FragmentStatePagerAdapter {

    int int_Number_Of_Fragments_To_Show;
    List<Program> List_Programs;

    public AdaptadorDeSwipeViews_2(FragmentManager fm,int int_Number_Of_Fragments_To_Show,
                                   List<Program> List_Programs) {
        super(fm);
        this.int_Number_Of_Fragments_To_Show = int_Number_Of_Fragments_To_Show;
        this.List_Programs = List_Programs;
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = new MiFragment_Con_Loader_3();
        Bundle args = new Bundle();
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
        args.putString(MiFragment_Con_Loader_2.ARG_OBJECT, "fragment#" +(i + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Con_Loader_2.ARG_PARAM2, String.valueOf(i + 1));

//        Estoy probando con la clase Progarm como serializable
        args.putSerializable("Programa Serializado", List_Programs.get(i));
        fragment.setArguments(args);






        return fragment;
    }


    //GetCount retorna el numero de fragmentos que va a mostrar el swipeView
    @Override
    public int getCount() {

//        No devuelvo un valor fijo
//        return 10;
        return List_Programs.size();
    }

    //Original
//    public int getCount() {
//        return 5;
//    }

    @Override
    public CharSequence getPageTitle(int position) {

        return "Programa  " + (position + 1);
    }
}
