package com.o3j.es.estamosenfiestas.servercall.pojos;

/**
 * Created by jluis on 17/08/15.
 */
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class ProgramModified  implements Parcelable{

    @Expose
    private String idProgram;
    @Expose
    private String modification;
    @SerializedName("ff_modification")
    @Expose
    private String ffModification;
    /**
     *
     * @return
     * The idProgram
     */
    public String getIdProgram() {
        return idProgram;
    }

    /**
     *
     * @param idProgram
     * The idProgram
     */
    public void setIdProgram(String idProgram) {
        this.idProgram = idProgram;
    }

    /**
     *
     * @return
     * The modification
     */
    public String getModification() {
        return modification;
    }

    /**
     *
     * @param modification
     * The modification
     */
    public void setModification(String modification) {
        this.modification = modification;
    }

    /**
     *
     * @return
     * The ffModification
     */
    public String getFfModification() {
        return ffModification;
    }

    /**
     *
     * @param ffModification
     * The ff_modification
     */
    public void setFfModification(String ffModification) {
        this.ffModification = ffModification;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.idProgram);
        dest.writeString(this.modification);
        dest.writeString(this.ffModification);
    }

    public ProgramModified() {
    }

    protected ProgramModified(Parcel in) {
        this.idProgram = in.readString();
        this.modification = in.readString();
        this.ffModification = in.readString();
    }

    public static final Creator<ProgramModified> CREATOR = new Creator<ProgramModified>() {
        public ProgramModified createFromParcel(Parcel source) {
            return new ProgramModified(source);
        }

        public ProgramModified[] newArray(int size) {
            return new ProgramModified[size];
        }
    };
}
