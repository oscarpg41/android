package com.o3j.es.estamosenfiestas.display_frame_layout;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.o3j.es.estamosenfiestas.activity_actualizar_fiestas.ClaseActualizacionAutomatica;
import com.o3j.es.estamosenfiestas.display_list_of_favorites.ClaseAvisoAutomaticoDeEventosFavoritos;

/**
 * Created by Juan on 31/08/2015.
 */
public class MyBroadcastReceiverEefBootUp extends BroadcastReceiver {
    private static String xxx;
    Context context;

    //No hago nada en este broadcast
    @Override
    public void onReceive(Context context, Intent intent) {

        xxx = this.getClass().getSimpleName();
        this.context = context;

        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
        {
            Log.e(xxx, " en metodo onReceive de MyBroadcastReceiverEefBootUp");
        }

        methodLanzarChequeoDeFavoritosParaNotificaciones(context);
        methodLanzarActualizacionAutomaticaAlEncenderElDispositivo(context);

    }//Fin de OnReceive

    public void methodLanzarChequeoDeFavoritosParaNotificaciones(Context context) {
        ClaseAvisoAutomaticoDeEventosFavoritos claseAvisoAutomaticoDeEventosFavoritos =
                ClaseAvisoAutomaticoDeEventosFavoritos.newInstance(context);
        claseAvisoAutomaticoDeEventosFavoritos.methodDetenerArrancarChequeoAutomaticodeFavoritos();
    }

    public void methodLanzarActualizacionAutomaticaAlEncenderElDispositivo(Context context) {

        ClaseActualizacionAutomatica claseActualizacionAutomatica =  ClaseActualizacionAutomatica.newInstance(context);
        //Averiguo  si hay que lanzar las actualizaciones automaticas.
        if(claseActualizacionAutomatica.methodGetBooleanCheckBoxChecked()){
            claseActualizacionAutomatica.methodIniciaActualizacionAutomatica("fake1", "fake2");
        }
    }

}
