package com.o3j.es.estamosenfiestas.orm.pojosDB;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by jluis on 26/07/15.
 */
@DatabaseTable
public class Configuracion {

    public static final String ID = "_id";
    public static final String COMUNNINITY_ID = "ccaa_id";
    public static final String EVENTPLANNER_ID = "eventplanner_id";
    public static final String FECHACONFIGURACION = "fecha_configuracion";
    public static final String DESCARGADO_EVENT_PLANNER = "descargado_organizador";
    public static final String FECHA_DESCARGADO_EVENT_PLANNER = "fecha_descarga_organizador";
    public static final String DESCARGADOS_PROGRAMAS = "descargados_programas";
    public static final String FECHA_DESCARGA_DE_PROGRAMAS = "FECHA_DESCARGA_PROGRAMAS";

    @DatabaseField(columnName = ID)
    private int id;
    @DatabaseField(columnName = COMUNNINITY_ID)
    private int ccaaId;
    @DatabaseField(columnName = EVENTPLANNER_ID)
    private int eventPlannerId;
    @DatabaseField(columnName = FECHACONFIGURACION)
    private Date fechaConfiguracion;
    @DatabaseField(columnName = DESCARGADO_EVENT_PLANNER)
    int descargadoEventPlanner;
    @DatabaseField(columnName = FECHA_DESCARGADO_EVENT_PLANNER)
        Date fechaDescargadoEventPlanner;
    @DatabaseField(columnName = DESCARGADOS_PROGRAMAS)
    int descargadosProgramas;
    @DatabaseField(columnName = FECHA_DESCARGA_DE_PROGRAMAS)
    Date fechaDescargadoProgramas;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCcaaId() {
        return ccaaId;
    }

    public void setCcaaId(int ccaaId) {
        this.ccaaId = ccaaId;
    }

    public int getEventPlannerId() {
        return eventPlannerId;
    }

    public void setEventPlannerId(int eventPlannerId) {
        this.eventPlannerId = eventPlannerId;
    }

    public Date getFechaConfiguracion() {
        return fechaConfiguracion;
    }

    public void setFechaConfiguracion(Date fechaConfiguracion) {
        this.fechaConfiguracion = fechaConfiguracion;
    }

    public int getDescargadoEventPlanner() {
        return descargadoEventPlanner;
    }

    public void setDescargadoEventPlanner(int descargadoEventPlanner) {
        this.descargadoEventPlanner = descargadoEventPlanner;
    }

    public Date getFechaDescargadoEventPlanner() {
        return fechaDescargadoEventPlanner;
    }

    public void setFechaDescargadoEventPlanner(Date fechaDescargadoEventPlanner) {
        this.fechaDescargadoEventPlanner = fechaDescargadoEventPlanner;
    }

    public int getDescargadosProgramas() {
        return descargadosProgramas;
    }

    public void setDescargadosProgramas(int descargadosProgramas) {
        this.descargadosProgramas = descargadosProgramas;
    }

    public Date getFechaDescargadoProgramas() {
        return fechaDescargadoProgramas;
    }

    public void setFechaDescargadoProgramas(Date fechaDescargadoProgramas) {
        this.fechaDescargadoProgramas = fechaDescargadoProgramas;
    }
}
