package com.o3j.es.estamosenfiestas.display_vista_deslizante_2;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.eef.data.dataelements.DayEvents;
import com.eef.data.dataelements.Event;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Evento;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Fiesta_Seleccionada_Con_RecycleView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Juan on 22/06/2015.
 */


public class AdaptadorDeSwipeViews_Evento_Detalle extends FragmentStatePagerAdapter {

    int int_Number_Of_Fragments_To_Show;

    DayEvents day_Events;

    public AdaptadorDeSwipeViews_Evento_Detalle(FragmentManager fm, List<Event> arrayList_Evento_Seleccionado,
                                                int int_Id_Evento_Seleccionado, Context context) {
//        public AdaptadorDeSwipeViews_Eventos_Del_Dia(FragmentManager fm, DayEvents eventos_Del_Dia) {
        super(fm);
//        this.day_Events = eventos_Del_Dia;
        this.arrayList_Evento_Seleccionado = arrayList_Evento_Seleccionado;
        this.int_Id_Evento_Seleccionado = int_Id_Evento_Seleccionado;
        this.context = context;
    }

    List<Event> arrayList_Evento_Seleccionado = new ArrayList<Event>();
    int int_Id_Evento_Seleccionado;
    Context context;

    @Override
    public Fragment getItem(int i) {

        Fragment fragment = getNewFragmentToShowEvent(arrayList_Evento_Seleccionado.get(i));

        return fragment;
    }


    //GetCount retorna el numero de fragmentos que va a mostrar el swipeView
    @Override
    public int getCount() {

        return arrayList_Evento_Seleccionado.size();
    }


    @Override
    public CharSequence getPageTitle(int position) {

//        return "Evento  " + (position + 1);


        //Escribe la fecha de los dias con eventos en el tab que corresponde
//        return arrayList_Programa.get(0).getEventByDay().get(position).toString();



        SimpleDateFormat df2 = new SimpleDateFormat("HH:mm");


        //11 julio 2015:No se muestra la palabra hora: antes de la hora en los tabs
//        String string_Hora_Del_Evento = context.getResources().getString(R.string.hora_del_evento_2);
//        return string_Hora_Del_Evento.concat(" " +df2.format(arrayList_Evento_Seleccionado.get(position).getStartDate()));
        String string_Hora_Del_Evento;
        return string_Hora_Del_Evento=(" " +df2.format(arrayList_Evento_Seleccionado.get(position).getStartDate()));
    }


    int int_Nivel_De_Info_Fiesta_Evento_Detalle = 1;

    public Fragment getNewFragmentToShowEvent(Event m_Event) {
        Fragment fragment = new MiFragment_Evento();
        Bundle args = new Bundle();
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "fragment#" + (int_Nivel_De_Info_Fiesta_Evento_Detalle + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, String.valueOf(int_Nivel_De_Info_Fiesta_Evento_Detalle + 1));
        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, int_Nivel_De_Info_Fiesta_Evento_Detalle);

        args.putSerializable("Datos del Evento Serializado", m_Event);

        fragment.setArguments(args);

        return fragment;
    }


}//Fin de la clase
