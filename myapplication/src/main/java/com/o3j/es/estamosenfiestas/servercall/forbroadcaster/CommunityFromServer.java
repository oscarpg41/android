package com.o3j.es.estamosenfiestas.servercall.forbroadcaster;

import android.os.Parcel;
import android.os.Parcelable;

import com.o3j.es.estamosenfiestas.servercall.pojos.OrganizerPojo;

import java.util.List;

/**
 * Created by jluis on 25/07/15.
 */
public class CommunityFromServer implements Parcelable{

    Integer cca_Id;
    List<OrganizerPojo> listEO;

    public Integer getCca_Id() {
        return cca_Id;
    }

    public void setCca_Id(Integer cca_Id) {
        this.cca_Id = cca_Id;
    }

    public List<OrganizerPojo> getListEO() {
        return listEO;
    }

    public void setListEO(List<OrganizerPojo> listEO) {
        this.listEO = listEO;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.cca_Id);
        dest.writeTypedList(listEO);
    }

    public CommunityFromServer() {
    }

    protected CommunityFromServer(Parcel in) {
        this.cca_Id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.listEO = in.createTypedArrayList(OrganizerPojo.CREATOR);
    }

    public static final Creator<CommunityFromServer> CREATOR = new Creator<CommunityFromServer>() {
        public CommunityFromServer createFromParcel(Parcel source) {
            return new CommunityFromServer(source);
        }

        public CommunityFromServer[] newArray(int size) {
            return new CommunityFromServer[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CommunityFromServer)) return false;

        CommunityFromServer that = (CommunityFromServer) o;

        return cca_Id.equals(that.cca_Id);

    }

    @Override
    public int hashCode() {
        return cca_Id.hashCode();
    }
}
