package com.o3j.es.estamosenfiestas.servercall.forbroadcaster;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by jluis on 25/07/15.
 */
public class ConfigurationFromServer implements Parcelable{

    Boolean configurationToShow;

    List<CommunityFromServer> listComWithActiveServers;

    public Boolean areConfigurationAvailable() {
        return configurationToShow;
    }

    public List<CommunityFromServer> getListComWithActiveServers() {
        return listComWithActiveServers;
    }

    public void setListComWithActiveServers(List<CommunityFromServer> listComWithActiveServers) {
        this.listComWithActiveServers = listComWithActiveServers;
        if(listComWithActiveServers == null || listComWithActiveServers.size()==0)
            configurationToShow = new Boolean(false);
        else
            configurationToShow = new Boolean(true);
    }

    public boolean contains(CommunityFromServer ccaa)
    {
        if(!areConfigurationAvailable())
            return areConfigurationAvailable();
        return listComWithActiveServers.contains(ccaa);
    }

    public CommunityFromServer getCcAa(CommunityFromServer ccaa)
    {
        if(!contains(ccaa))
            return null;
        return listComWithActiveServers.get(listComWithActiveServers.indexOf(ccaa));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.configurationToShow);
        dest.writeTypedList(listComWithActiveServers);
    }

    public ConfigurationFromServer() {
    }

    protected ConfigurationFromServer(Parcel in) {
        this.configurationToShow = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.listComWithActiveServers = in.createTypedArrayList(CommunityFromServer.CREATOR);
    }

    public static final Creator<ConfigurationFromServer> CREATOR = new Creator<ConfigurationFromServer>() {
        public ConfigurationFromServer createFromParcel(Parcel source) {
            return new ConfigurationFromServer(source);
        }

        public ConfigurationFromServer[] newArray(int size) {
            return new ConfigurationFromServer[size];
        }
    };
}
