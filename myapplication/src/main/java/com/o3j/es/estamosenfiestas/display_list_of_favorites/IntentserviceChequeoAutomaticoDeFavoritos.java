package com.o3j.es.estamosenfiestas.display_list_of_favorites;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.format.DateUtils;
import android.util.Log;

import com.eef.data.dataelements.Event;
import com.eef.data.dataelements.Program;
import com.eef.data.eefExceptions.eefException;
import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import managers.DatabaseServerManager;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * helper methods.
 */
public class IntentserviceChequeoAutomaticoDeFavoritos extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    public static final String ACTION_FOO = "com.o3j.es.estamosenfiestas.action.FOO";
    public static final String ACTION_BAZ = "com.o3j.es.estamosenfiestas.action.BAZ";

    // TODO: Rename parameters
    public static final String EXTRA_PARAM1 = "com.o3j.es.estamosenfiestas.extra.PARAM1";
    public static final String EXTRA_PARAM2 = "com.o3j.es.estamosenfiestas.extra.PARAM2";

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionFoo(Context context, String param1, String param2) {
        Intent intent = new Intent(context, IntentserviceChequeoAutomaticoDeFavoritos.class);
        intent.setAction(ACTION_FOO);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionBaz(Context context, String param1, String param2) {
        Intent intent = new Intent(context, IntentserviceChequeoAutomaticoDeFavoritos.class);
        intent.setAction(ACTION_BAZ);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    public IntentserviceChequeoAutomaticoDeFavoritos() {
        super("IntentserviceChequeoAutomaticoDeFavoritos");
        xxx = this.getClass().getSimpleName();

    }
    private static String xxx;




    @Override
    protected void onHandleIntent(Intent intent) {
        //**********************************************************************************
        // acquire the wake lock como lo explican en android developers
        //para el caso de que el movil este dormido
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "MyWakelockTag");
        wakeLock.acquire();
        //**********************************************************************************
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_FOO.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                //Comento handleActionFoo para que no casque debido a la exception "Not yet implemented"
//                handleActionFoo(param1, param2);

            method_Actualiza();


            } else if (ACTION_BAZ.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                //Comento handleActionFoo para que no casque debido a la exception "Not yet implemented"
//                handleActionBaz(param1, param2);
            }
        }

        //**********************************************************************************
        wakeLock.release();
        //**********************************************************************************
    }






    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFoo(String param1, String param2) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionBaz(String param1, String param2) {
        throw new UnsupportedOperationException("Not yet implemented");
    }



    public void method_Actualiza() {

        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, xxx +"En metodo  method_Actualiza" );
        }

        //Busco los eventos favoritos en la DB
        List<Program> List_Programs = methodGetEventosFavoritos();//Voy a la DB a buscar los favoritos


        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, xxx +"En metodo  method_Actualiza, List_Programs.isEmpty " +List_Programs.isEmpty() );
            Log.e(xxx, xxx +"En metodo  method_Actualiza, List_Programs.size " +List_Programs.size());
            Log.e(xxx, xxx +"En metodo  method_Actualiza, List_Programs.get(i).getFullList().size() "
                    +List_Programs.get(0).getFullList().size());
        }

        //parece que JL me devuelve la lista vacio, no nula
//        if (List_Programs != null || !List_Programs.isEmpty()) {
        if (List_Programs != null) {
            methodChequeaFechasDeLosEventosFavoritos(List_Programs);
        } else {
            //EnviaNotificacionFake  es solo para pruebas
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(xxx, "En metodo  method_Actualiza, List_Programs ES NULO,  " +
                        "no hay eventos en favoritos" );
            }
        }




    }//Fin de method_Actualiza

    public void methodChequeaFechasDeLosEventosFavoritos(List<Program> List_Programs) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx,  "En metodo  methodChequeaFechasDeLosEventosFavoritos ");
        }

        DateFormat df5 = new SimpleDateFormat("MMMM dd yyyy, HH:mm:ss");

        //Voy a trabajar con todas las fechas como objetos calendar
        Calendar calendarFechaActual = Calendar.getInstance();
        calendarFechaActual.setTimeInMillis(System.currentTimeMillis());

        //Estas 5 lineas son solo para pruebas.
        //OJO: month empieza en cero
//        calendarFechaActual.set(Calendar.MONTH, 7);
//        calendarFechaActual.set(Calendar.DAY_OF_MONTH, 21);
//        calendarFechaActual.set(Calendar.HOUR_OF_DAY, 23);
//        calendarFechaActual.set(Calendar.MINUTE, 00);
//        calendarFechaActual.set(Calendar.SECOND, 00);

        String string_calendar_calendarFechaActual = df5.format(calendarFechaActual.getTime());



        //Chequeo que eventos hay que notificar
        for (int i = 0; i < List_Programs.size(); i++) {

            Calendar calendarFechaInicioProgram = Calendar.getInstance();
            calendarFechaInicioProgram.setTimeInMillis(List_Programs.get(i).getProgramStartDate().getTime());
            String string_calendarFechaInicioProgram = df5.format(calendarFechaInicioProgram.getTime());

            Calendar calendarFechaFinProgram = Calendar.getInstance();
            calendarFechaFinProgram.setTimeInMillis(List_Programs.get(i).getProgramEndDate().getTime());
            String string_calendarFechaFinProgram = df5.format(calendarFechaFinProgram.getTime());


//            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
//                Log.e(xxx,  "En metodo  methodChequeaFechasDeLosEventosFavoritos Program: " +List_Programs.get(i).getProgramSortDescription());
//                Log.e(xxx,  "En metodo  methodChequeaFechasDeLosEventosFavoritos Fecha actual: " +string_calendar_calendarFechaActual);
//                Log.e(xxx,  "En metodo  methodChequeaFechasDeLosEventosFavoritos Fecha Inicio Programa: " +string_calendarFechaInicioProgram);
//                Log.e(xxx,  "En metodo  methodChequeaFechasDeLosEventosFavoritos Fecha Fin Programa: " +string_calendarFechaFinProgram);
//            }


            if (List_Programs.get(i).getFullList().isEmpty()) {
                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.e(xxx, "En metodo  methodChequeaFechasDeLosEventosFavoritos listas de eventos vacias");
                }

            } else {//Chequea los eventos contra la fecha actual

                for (int i2 = 0; i2 < List_Programs.get(i).getFullList().size(); i2++) {

                    //*************************************************************************************************
                    //*************************************************************************************************
                    //*************************************************************************************************
                    //Juan, 4 oct 2015:Antes que nada, chequeo si este evento es de madrugada para sumar uno al dia
                    if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                        Log.e(xxx, "En metodo  methodChequeaFechasDeLosEventosFavoritos FECHA EVENTO ANTES CHEQUEO MADRUGADA: "
                                + df5.format(List_Programs.get(i).getFullList().get(i2).getStartDate().getTime()));
                    }

                    method_Chequea_Si_Hay_Que_Sumar_Un_Dia(List_Programs.get(i).getFullList().get(i2));

                    if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                        Log.e(xxx, "En metodo  methodChequeaFechasDeLosEventosFavoritos FECHA EVENTO DESPUES CHEQUEO MADRUGADA: "
                                + df5.format(List_Programs.get(i).getFullList().get(i2).getStartDate().getTime()));
                    }
                    //Fin del chequeo de madrugada
                    //*************************************************************************************************
                    //*************************************************************************************************
                    //*************************************************************************************************

                    Calendar calendarFechaInicioEvento = Calendar.getInstance();
                    calendarFechaInicioEvento.setTimeInMillis(List_Programs.get(i).getFullList().get(i2).getStartDate().getTime());
                    String string_calendar_calendarFechaInicioEvento = df5.format(calendarFechaInicioEvento.getTime());

                    if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
//                        Log.e(xxx,  "En metodo  methodChequeaFechasDeLosEventosFavoritos Evento: " +List_Programs.get(i).getFullList().get(i2).getSortText());
                        Log.e(xxx, "En metodo  methodChequeaFechasDeLosEventosFavoritos Fecha actual: " + string_calendar_calendarFechaActual);
//                        Log.e(xxx,  "En metodo  methodChequeaFechasDeLosEventosFavoritos Fecha Inicio evento: " +string_calendar_calendarFechaInicioEvento);
                    }


                    //Compara si el la fecha del evento es anterior a la fecha actual
                    if (calendarFechaInicioEvento.after(calendarFechaActual)) {

                        long longDiferenciaEnMiliSegundos = methodCalculaDiff(calendarFechaInicioEvento.getTime(), calendarFechaActual.getTime());
                        //************************************************************************************************************************
                        long differenceInSeconds = longDiferenciaEnMiliSegundos / DateUtils.SECOND_IN_MILLIS;
                        // formatted will be HH:MM:SS or MM:SS
                        String formatted_DiferenciaEntreHoraActual_Y_Evento = DateUtils.formatElapsedTime(differenceInSeconds);
                        //*************************************************************************************************************************

                        String formatted_Rango_de_Fechas = DateUtils.formatDateRange(this, calendarFechaActual.getTime().getTime(),
                                calendarFechaInicioEvento.getTime().getTime(), DateUtils.FORMAT_ABBREV_ALL);

                        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                            Log.e(xxx, "En metodo  methodChequeaFechasDeLosEventosFavoritos formatted_DiferenciaEntreHoraActual_Y_Evento: "
                                    + formatted_DiferenciaEntreHoraActual_Y_Evento);
                            Log.e(xxx, "En metodo  methodChequeaFechasDeLosEventosFavoritos formatted_Rango_de_Fechas: "
                                    + formatted_Rango_de_Fechas);
                        }


                        if (methodFaltaEntre_1_Y_2_Horas_ParaInicioDelEvento(longDiferenciaEnMiliSegundos)) {
                            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                                Log.e(xxx, "En metodo  methodChequeaFechasDeLosEventosFavoritos: Evento dentro del rango, ENVIO LA NOTIFICACION ");
                            }
                            //Se envia la notificacion con los datos del evento
//                            methodEnviarNotificacion(List_Programs.get(i).getFullList().get(i2));
                            //Ahora chequeo si estoy antes de lollipop o con lollipop o superior

                            //*************************************************************************************************
                            //*************************************************************************************************
                            //*************************************************************************************************
                            //Juan, 5 oct 2015:Chequeo si la version de android es menor a lollipop

                            //Y ademas cambio el titulo de la notificacion por el nombre de la fiesta
                            String stringNombredeLaFiesta = List_Programs.get(i).getProgramSortDescription();



                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                                methodEnviarNotificacion_version_menor_que_lollipop(List_Programs.get(i).getFullList().get(i2),
                                               stringNombredeLaFiesta );
                            } else {//es lollipop o superior
                                methodEnviarNotificacion(List_Programs.get(i).getFullList().get(i2), stringNombredeLaFiesta);
                            }

                            //Fin del chequeo de madrugada
                            //*************************************************************************************************
                            //*************************************************************************************************
                            //*************************************************************************************************

                        } else {
                            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                                Log.e(xxx, "En metodo  methodChequeaFechasDeLosEventosFavoritos: Evento fuera del rango, " +
                                        "NO ENVIO LA NOTIFICACION TODAVIA");
                            }
                        }

                    } else {//calendarFechaInicioEvento.after(calendarFechaActual
                        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                            Log.e(xxx, "En metodo  methodChequeaFechasDeLosEventosFavoritos: Evento ya Terminado, No envio Notificacion ");
                        }
                    }

                }//Fin de for (int i2 = 0; i2 < List_Programs.get(i).getFullList().size(); i2++)
            }

        }//Fin de for (int i = 0; i < List_Programs.size(); i++)
    }//Fin de methodChequeaFechasDeLosEventosFavoritos

    public boolean methodFaltaEntre_1_Y_2_Horas_ParaInicioDelEvento(long longDiferenciaEnMiliSegundos) {
        //Nota: intMinutosElapse siempre estara en minutos
        int intMinutosElapse = 120;
        if (longDiferenciaEnMiliSegundos <= (1000 * intMinutosElapse * 60)) {
            return true;
        } else {
            return false;
        }
    }//FIN de methodCalculaDiff

    public long methodCalculaDiff(Date dateFinal, Date dateInicial) {
        long longDiferenciaEnMiliSegundos = dateFinal.getTime() - dateInicial.getTime();
        return longDiferenciaEnMiliSegundos;
    }//FIN de methodCalculaDiff

    public void methodEnviarNotificacion(Event event, String stringNombredeLaFiesta) {//>= Lollipop

        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, xxx + "En metodo  methodEnviarNotificacion");
        }

//        DateFormat df5 = new SimpleDateFormat("MMMM dd yyyy, HH:mm");
        DateFormat df5 = new SimpleDateFormat("HH:mm");

        Bitmap notificationLargeIconBitmap = BitmapFactory.decodeResource(
                this.getResources(),
                R.drawable.logo01);


        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
//                        .setSmallIcon(R.drawable.logo01)
                        .setSmallIcon(R.drawable.logo_01_alpha_2_gimp)
//                        .setContentTitle(event.getSortText())
                        .setContentTitle(stringNombredeLaFiesta)
//                        .setContentText("Próximo evento: " + df5.format(event.getStartDate()))
                        .setContentText(df5.format(event.getStartDate()) + " " + event.getSortText())
                        .setAutoCancel(true)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
//                        .setDefaults(NotificationCompat.DEFAULT_SOUND | NotificationCompat.DEFAULT_VIBRATE
//                                | NotificationCompat.DEFAULT_LIGHTS)
                        .setDefaults(NotificationCompat.DEFAULT_SOUND)
//                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setLargeIcon(notificationLargeIconBitmap);
        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, Activity_Fragment_List_Of_Favorites_With_Nav_Drawer.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(Activity_Fragment_List_Of_Favorites_With_Nav_Drawer.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.

//        int intIdNotification = 43256788;
        //El identificador de la notificacion es el eventid que es unico
        int intIdNotification = event.getEventId();

        mNotificationManager.notify(intIdNotification, mBuilder.build());

    }//Fin de methodEnviarNotificacion

    public List<Program> methodGetEventosFavoritos() {

        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, xxx +"En metodo  methodGetEventosFavoritos" );
        }

        //Hago lo mismo que en la clase Activity_Fragment_List_Of_Favorites_With_Nav_Drawer
        List<Program> List_Programs = null;
        //10 sept 2015: recupero los datos de favoritos de sqlite
        DatabaseServerManager databaseServerManager = new DatabaseServerManager();
        databaseServerManager.initManager(this);

        try {
            List_Programs = databaseServerManager.getFullProgramListOnlyFavoritos();
        } catch (eefException eef) {
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(xxx, xxx +" en metodo methodGetEventosFavoritos Exception: " +eef.getLocalizedMessage());
            }
            //NO HAGO NADA
        }

        return List_Programs;
    }//Fin de methodGetEventosFavoritos

    public void methodEnviaNotificacionFake_2_Evento_Mas_Adelante_En_El_Futuro(Event event) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, xxx + "En metodo  methodEnviaNotificacionFake_2_Evento_Mas_Adelante_En_El_Futuro");
        }

        DateFormat df5 = new SimpleDateFormat("MMMM dd yyyy, HH:mm");

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.logo01)
                        .setContentTitle(event.getSortText())
                        .setContentText("Evento en el futuro: " +  df5.format(event.getStartDate()))
                        .setAutoCancel(true)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                        .setDefaults(NotificationCompat.DEFAULT_SOUND | NotificationCompat.DEFAULT_VIBRATE
                                | NotificationCompat.DEFAULT_LIGHTS)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, Activity_Fragment_List_Of_Favorites_With_Nav_Drawer.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(Activity_Fragment_List_Of_Favorites_With_Nav_Drawer.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.

        int intIdNotification = 43256709;

        mNotificationManager.notify(intIdNotification, mBuilder.build());
    }//Fin de methodEnviaNotificacionFake_2_Evento_Mas_Adelante_En_El_Futuro

    public void methodEnviaNotificacionFake_3_Evento_Finalizado(Event event) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, xxx + "En metodo  methodEnviaNotificacionFake_3_Evento_Finalizado");
        }

        DateFormat df5 = new SimpleDateFormat("MMMM dd yyyy, HH:mm");

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.logo01)
                        .setContentTitle(event.getSortText())
                        .setContentText("Evento finalizado: " +  df5.format(event.getStartDate()))
                        .setAutoCancel(true)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                        .setDefaults(NotificationCompat.DEFAULT_SOUND | NotificationCompat.DEFAULT_VIBRATE
                                | NotificationCompat.DEFAULT_LIGHTS)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, Activity_Fragment_List_Of_Favorites_With_Nav_Drawer.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(Activity_Fragment_List_Of_Favorites_With_Nav_Drawer.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.

        int intIdNotification = 43256555;

        mNotificationManager.notify(intIdNotification, mBuilder.build());
    }//FIN de methodEnviaNotificacionFake_3_Evento_Finalizado

    public void methodEnviaNotificacionFake_4_NoHayProgramas_en_curso(Program program) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, xxx + "En metodo  methodEnviaNotificacionFake_4_NoHayProgramas_en_curso");
        }

        DateFormat df5 = new SimpleDateFormat("MMMM dd yyyy, HH:mm");

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.logo01)
                        .setContentTitle(program.getProgramSortDescription())
                        .setContentText("No hay programa en curso: " +  df5.format(program.getProgramStartDate())
                                +  df5.format(program.getProgramEndDate()))
                        .setAutoCancel(true)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                        .setDefaults(NotificationCompat.DEFAULT_SOUND | NotificationCompat.DEFAULT_VIBRATE
                                | NotificationCompat.DEFAULT_LIGHTS)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, Activity_Fragment_List_Of_Favorites_With_Nav_Drawer.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(Activity_Fragment_List_Of_Favorites_With_Nav_Drawer.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.

        int intIdNotification = 43250008;

        mNotificationManager.notify(intIdNotification, mBuilder.build());
    }//FIN de methodEnviaNotificacionFake_4_NoHayProgramas_en_curso





    //Juan, 4 oct 2015:Antes que nada, chequeo si este evento es de madrugada para sumar uno al dia
    private void method_Chequea_Si_Hay_Que_Sumar_Un_Dia(Event m_Event) {
        Calendar calendarFechaInicioEvento = Calendar.getInstance();
        calendarFechaInicioEvento.setTimeInMillis(m_Event.getStartDate().getTime());

        //Ojo, los eventos no tienen fecha de final
        //Cuequeo si el evento ocurre entre las 00 y las 05 de la madrugada.
        //Si se cumple la condicion, le sumo 1 a DAY_OF_MONTH
        if (calendarFechaInicioEvento.get(Calendar.HOUR_OF_DAY) == 00) {
            calendarFechaInicioEvento.add(Calendar.DAY_OF_MONTH, 1);
            m_Event.setStartDate(calendarFechaInicioEvento.getTime());
        }

        if (calendarFechaInicioEvento.get(Calendar.HOUR_OF_DAY) == 01) {
            calendarFechaInicioEvento.add(Calendar.DAY_OF_MONTH, 1);
            m_Event.setStartDate(calendarFechaInicioEvento.getTime());

        }

        if (calendarFechaInicioEvento.get(Calendar.HOUR_OF_DAY) == 02) {
            calendarFechaInicioEvento.add(Calendar.DAY_OF_MONTH, 1);
            m_Event.setStartDate(calendarFechaInicioEvento.getTime());

        }

        if (calendarFechaInicioEvento.get(Calendar.HOUR_OF_DAY) == 03) {
            calendarFechaInicioEvento.add(Calendar.DAY_OF_MONTH, 1);
            m_Event.setStartDate(calendarFechaInicioEvento.getTime());

        }

        if (calendarFechaInicioEvento.get(Calendar.HOUR_OF_DAY) == 04) {
            calendarFechaInicioEvento.add(Calendar.DAY_OF_MONTH, 1);
            m_Event.setStartDate(calendarFechaInicioEvento.getTime());

        }

        if (calendarFechaInicioEvento.get(Calendar.HOUR_OF_DAY) == 05) {
            calendarFechaInicioEvento.add(Calendar.DAY_OF_MONTH, 1);
            m_Event.setStartDate(calendarFechaInicioEvento.getTime());

        }

    }



    public void methodEnviarNotificacion_version_menor_que_lollipop(Event event, String stringNombredeLaFiesta) {

        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {//< Lollipop
            Log.e(xxx, xxx + "En metodo  methodEnviarNotificacion");
        }

//        DateFormat df5 = new SimpleDateFormat("MMMM dd yyyy, HH:mm");
        DateFormat df5 = new SimpleDateFormat("HH:mm");
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        //Con logo01 se ve mal en la tablet de laura
//                        .setSmallIcon(R.drawable.logo01)
                        .setSmallIcon(R.drawable.logo_01_72x72)
//                        .setContentTitle(event.getSortText())
                        .setContentTitle(stringNombredeLaFiesta)
//                        .setContentText("Próximo evento: " + df5.format(event.getStartDate()))
                        .setContentText(df5.format(event.getStartDate()) + " " + event.getSortText())
                        .setAutoCancel(true)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
//                        .setDefaults(NotificationCompat.DEFAULT_SOUND | NotificationCompat.DEFAULT_VIBRATE
//                                | NotificationCompat.DEFAULT_LIGHTS)
                        .setDefaults(NotificationCompat.DEFAULT_SOUND)
//                        .setPriority(NotificationCompat.PRIORITY_MAX);
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, Activity_Fragment_List_Of_Favorites_With_Nav_Drawer.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(Activity_Fragment_List_Of_Favorites_With_Nav_Drawer.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.

//        int intIdNotification = 43256788;
        //El identificador de la notificacion es el eventid que es unico
        int intIdNotification = event.getEventId();

        mNotificationManager.notify(intIdNotification, mBuilder.build());

    }//Fin de methodEnviarNotificacion_version_menor_que_lollipop
}//Fin de la clase
