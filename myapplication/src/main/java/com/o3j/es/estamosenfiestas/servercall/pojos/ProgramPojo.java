package com.o3j.es.estamosenfiestas.servercall.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by jluis on 26/07/15.
 */
public class ProgramPojo implements Parcelable {

        @Expose
        private String idProgram;
        @Expose
        private String name;
        @SerializedName("ff_modification")
        @Expose
        private String ffModification;
        @SerializedName("ff_ini")
        @Expose
        private String ffIni;
        @SerializedName("ff_end")
        @Expose
        private String ffEnd;
        @Expose
        private String image;

        /**
         *
         * @return
         * The idProgram
         */
        public String getIdProgram() {
            return idProgram;
        }

        /**
         *
         * @param idProgram
         * The idProgram
         */
        public void setIdProgram(String idProgram) {
            this.idProgram = idProgram;
        }

        /**
         *
         * @return
         * The name
         */
        public String getName() {
            return name;
        }

        /**
         *
         * @param name
         * The name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
        *
        * @return
        * The ffModification
        */
        public String getFfModification() {
        return ffModification;
    }

        /**
        *
        * @param ffModification
        * The ffModification
        */
            public void setFfModification(String ffModification) {
            this.ffModification = ffModification;
        }

        /**
         *
         * @return
         * The ffIni
         */
        public String getFfIni() {
            return ffIni;
        }

        /**
         *
         * @param ffIni
         * The ff_ini
         */
        public void setFfIni(String ffIni) {
            this.ffIni = ffIni;
        }

        /**
         *
         * @return
         * The ffEnd
         */
        public String getFfEnd() {
            return ffEnd;
        }

        /**
         *
         * @param ffEnd
         * The ff_end
         */
        public void setFfEnd(String ffEnd) {
            this.ffEnd = ffEnd;
        }

        /**
         *
         * @return
         * The image
         */
        public String getImage() {
            return image;
        }

        /**
         *
         * @param image
         * The image
         */
        public void setImage(String image) {
            this.image = image;
        }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.idProgram);
        dest.writeString(this.name);
        dest.writeString(this.ffIni);
        dest.writeString(this.ffEnd);
        dest.writeString(this.image);
    }

    public ProgramPojo() {
    }

    protected ProgramPojo(Parcel in) {
        this.idProgram = in.readString();
        this.name = in.readString();
        this.ffIni = in.readString();
        this.ffEnd = in.readString();
        this.image = in.readString();
    }

    public static final Creator<ProgramPojo> CREATOR = new Creator<ProgramPojo>() {
        public ProgramPojo createFromParcel(Parcel source) {
            return new ProgramPojo(source);
        }

        public ProgramPojo[] newArray(int size) {
            return new ProgramPojo[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProgramPojo)) return false;

        ProgramPojo that = (ProgramPojo) o;

        return getIdProgram().equals(that.getIdProgram());

    }

    @Override
    public int hashCode() {
        return getIdProgram().hashCode();
    }
}
