package com.o3j.es.estamosenfiestas.display_lists_package;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.eef.data.dataelements.Program;
import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.CustomAdapter_RecyclerView_3;

import java.util.ArrayList;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MiFragment_Con_Loader_2.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MiFragment_Con_Loader_2#newInstance} factory method to
 * create an instance of this fragment.
 */



//Juan, 26 mayo 2015: Ya no uso esta clase ni su layout.
//AHORA ESTOY USANDO MiFragment_Con_Loader_3
public class MiFragment_Con_Loader_2 extends android.support.v4.app.Fragment
//        implements LoaderManager.LoaderCallbacks<ArrayList<Class_Evento_1>>{
        implements android.support.v4.app.LoaderManager.LoaderCallbacks<ArrayList<com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_Evento_1>> {
    //        implements android.support.v4.app.LoaderManager.LoaderCallbacks<ArrayList<Class_Evento_1>>{
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static String xxx;

    //    public static final String ARG_OBJECT = "object";
//    public static String ARG_OBJECT = "object";
    public static String ARG_OBJECT = "lista ";

    //ARG_OBJECT no puede ser static por que entonces coje el valor del ultimo objeto instanciado
//    public  String ARG_OBJECT = "object";


    private static final String ARG_PARAM1 = "param1";
    public static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private Program mPprogram;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MiFragment_1.
     */

    //Este builder no lo uso por ahora
    public static MiFragment_Con_Loader_2 newInstance(String param1, String param2) {
        MiFragment_Con_Loader_2 fragment = new MiFragment_Con_Loader_2();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public MiFragment_Con_Loader_2() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

            mParam1 = getArguments().getString(ARG_OBJECT);
            mPprogram = (Program )getArguments().getSerializable("Programa Serializado");

        }

        xxx = this.getClass().getSimpleName();

        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx + " " + mParam1 + " onCreate");
            Log.d(xxx, xxx + " " + mParam1 + " onCreate: " +mPprogram.getProgramSortDescription());
            Log.d(xxx, xxx + " " + mParam1 + " onCreate: " +mPprogram.getProgramLongDescription());
        }

        //****************************************************************
        //****************************************************************
        //             Codigo relacionado con recycler view
        //****************************************************************
        //****************************************************************
        // Initialize dataset, this data would usually come from a local content provider or
        // remote server.
//        initDataset();

        //Iniciar el array con los eventos de la fiesta

        //Lo comento por que ahora se hace con el loader
//        init_arrayList_De_Eventos_De_la_fiesta();
        //****************************************************************
        //****************************************************************
        //             FIN Codigo relacionado con recycler view
        //****************************************************************
        //****************************************************************
    }//Fin del onCreate

    /**
     * Generates Strings for RecyclerView's adapter. This data would usually come
     * from a local content provider or remote server.
     */
    private void initDataset() {//Relacionado con recycler view
        mDataset = new String[DATASET_COUNT];
        for (int i = 0; i < DATASET_COUNT; i++) {
            mDataset[i] = "Loader: This is element #" + i;
        }
    }

    /**
     * Generates Strings for RecyclerView's adapter. This data would usually come
     * from a local content provider or remote server.
     */
    private void init_arrayList_De_Eventos_De_la_fiesta() {//Relacionado con recycler view
        arrayList_De_Eventos_De_la_fiesta = new ArrayList<com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_Evento_1>();
        for (int i = 0; i < DATASET_COUNT; i++) {
            arrayList_De_Eventos_De_la_fiesta.add(
                    com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_Evento_1.newInstance("Titulo # " + i,
                            "Descripcion #" + i,
                            "Descripcion2 #" + i,
                            R.drawable.ic_launcher));
        }
    }

    protected String[] mDataset;
    //21 feb 2015: nuevo data set: array con objetos Class_Evento_
    protected ArrayList<com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_Evento_1> arrayList_De_Eventos_De_la_fiesta = null;
    private static final int DATASET_COUNT = 60;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_mi_fragment_1, container, false);
        //****************************************************************
        //****************************************************************
        //             Codigo relacionado con recycler view
        //****************************************************************
        //****************************************************************
        View rootView = inflater.inflate(R.layout.fragment_mi_fragment_1, container, false);
        rootView.setTag(TAG);
        // BEGIN_INCLUDE(initializeRecyclerView)
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view_1);

        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        mLayoutManager = new LinearLayoutManager(getActivity());

        mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;

        if (savedInstanceState != null) {
            // Restore saved layout manager type.
            mCurrentLayoutManagerType = (LayoutManagerType) savedInstanceState
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);

        //21 feb 2015, le paso al adater

//        mAdapter = new CustomAdapter_RecyclerView_2(mDataset);

//       ***************************************************************************
        //Esta dos lineas las paso al callback onLoadFinished para asegurar
//        que los datos ya estan cargados por el loader ClaseLoaderDeListaDeFiesta_Loader_1
        mAdapter = new com.o3j.es.estamosenfiestas.activity_fragment_base_2.CustomAdapter_RecyclerView_3(arrayList_De_Eventos_De_la_fiesta, mParam1,
                                    getActivity());

//        mAdapter = new CustomAdapter_RecyclerView_4(arrayList_De_Eventos_De_la_fiesta, mParam1,
//                getActivity());
        // Set CustomAdapter as the adapter for RecyclerView.
        mRecyclerView.setAdapter(mAdapter);
        // END_INCLUDE(initializeRecyclerView)
//        ****************************************************************************

        //Manejo de los radio buttons
        mLinearLayoutRadioButton = (RadioButton) rootView.findViewById(R.id.linear_layout_rb);
        mLinearLayoutRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRecyclerViewLayoutManager(LayoutManagerType.LINEAR_LAYOUT_MANAGER);
            }
        });

        mGridLayoutRadioButton = (RadioButton) rootView.findViewById(R.id.grid_layout_rb);
        mGridLayoutRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRecyclerViewLayoutManager(LayoutManagerType.GRID_LAYOUT_MANAGER);
            }
        });
        //****************************************************************
        //****************************************************************
        //             FIN Codigo relacionado con recycler view
        //****************************************************************
        //****************************************************************
        return rootView;
    }//Fin de onCreateView

    //Variables para que funcione el recycle view
    private static final String TAG = "RecyclerViewFragment";
    protected RecyclerView mRecyclerView;
    protected LayoutManagerType mCurrentLayoutManagerType;//Tipo enum


    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }

    protected RecyclerView.LayoutManager mLayoutManager;
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    //Original
//    protected CustomAdapter_RecyclerView_2 mAdapter;
    //Nuevo adapter para el array list
//    protected com.o3j.es.estamosenfiestas.activity_fragment_base_2.CustomAdapter_RecyclerView_3 mAdapter;
    protected CustomAdapter_RecyclerView_3 mAdapter;
    protected RadioButton mLinearLayoutRadioButton;
    protected RadioButton mGridLayoutRadioButton;

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save currently selected layout manager.
        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, mCurrentLayoutManagerType);
        super.onSaveInstanceState(savedInstanceState);
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction("ahora no lo uso");
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(String string);
    }

    /**
     * Set RecyclerView's LayoutManager to the one given.
     *
     * @param layoutManagerType Type of layout manager to switch to.
     */
    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                mLayoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
                mCurrentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }

    private static final int SPAN_COUNT = 2;

    //****************************************************************
    //****************************************************************
    //             Codigo relacionado con el loader
    //****************************************************************
    //****************************************************************

//    @Override
//    public Loader<ArrayList<Class_Evento_1>> onCreateLoader(int id, Bundle args) {
//        ClaseLoaderDeListaDeFiesta_Loader_1 claseLoaderDeListaDeFiesta_Loader_1 =
//                new ClaseLoaderDeListaDeFiesta_Loader_1(getActivity());
//        return claseLoaderDeListaDeFiesta_Loader_1;
//    }

    @Override
    public android.support.v4.content.Loader<ArrayList<com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_Evento_1>> onCreateLoader(int id, Bundle args) {
        com.o3j.es.estamosenfiestas.activity_fragment_base_2.ClaseLoaderDeListaDeFiesta_Loader_1 claseLoaderDeListaDeFiesta_Loader_1 =
                new com.o3j.es.estamosenfiestas.activity_fragment_base_2.ClaseLoaderDeListaDeFiesta_Loader_1(getActivity());
        return claseLoaderDeListaDeFiesta_Loader_1;

        //Nota, 23 feb 2015: funciona ok con support.v4.content.Loader, con standard funciona mal
        //y no pinta las listas en vertical cuando giro la tablet.
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<ArrayList<com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_Evento_1>> loader, ArrayList<com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_Evento_1> data) {
        arrayList_De_Eventos_De_la_fiesta = data;


////        Por ahora lo hago asi a ver que pasa
        mAdapter = new com.o3j.es.estamosenfiestas.activity_fragment_base_2.CustomAdapter_RecyclerView_3(arrayList_De_Eventos_De_la_fiesta, mParam1,
                                getActivity());

//        mAdapter = new CustomAdapter_RecyclerView_4(arrayList_De_Eventos_De_la_fiesta, mParam1,
//                getActivity());
//        // Set CustomAdapter as the adapter for RecyclerView.
        mRecyclerView.setAdapter(mAdapter);

        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx + " " + mParam1 + " onloadfinished");
        }
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<ArrayList<com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_Evento_1>> loader) {
        arrayList_De_Eventos_De_la_fiesta = null;

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Prepare the loader.  Either re-connect with an existing one,
        // or start a new one.
        //Para pasar el this de la actividad, hay que usar el get activity por que este fragment
        //no esta embebido en la actividad.
        intIdentificadorDelLoaderDelFragment = Integer.parseInt(mParam2);

        //Original
//        getActivity().getLoaderManager().initLoader(0, null, this);

        //En cada fragment, asigno un unico loader, con un ID unico
        //No funciona cuando cambio la orientacion
//        getActivity().getSupportLoaderManager().initLoader(intIdentificadorDelLoaderDelFragment, null, this);

        //Usando
        getActivity().getSupportLoaderManager().
                initLoader(intIdentificadorDelLoaderDelFragment, null, this);
    }
//****************************************************************
    //****************************************************************
    //             FIN del Codigo relacionado con el loader
    //****************************************************************
    //****************************************************************

    private int intIdentificadorDelLoaderDelFragment;


}
