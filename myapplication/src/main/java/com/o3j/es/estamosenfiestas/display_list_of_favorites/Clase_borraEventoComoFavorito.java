package com.o3j.es.estamosenfiestas.display_list_of_favorites;

import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

import com.eef.data.dataelements.Event;
import com.eef.data.eefExceptions.eefException;
import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.dialogs.ClaseDialogEventoGuardado;

import managers.DatabaseServerManager;


//Juan, 19 agosto 2015, clase para guardar favoritos. es usada por:
//CustomAdapter_RecyclerView_Evento_Detalle
//
public class Clase_borraEventoComoFavorito extends DialogFragment {

    private static  String TAG;
    Event m_Event_Borrar_de_favoritos;
    ActionBarActivity actionBarActivity;

    int m_position_favorito_a_borrar;
    MiFragment_Lista_De_Favoritos_Con_RecycleView m_MiFragment_Lista_De_Favoritos_Con_RecycleView;


    public static Clase_borraEventoComoFavorito newInstance(Event m_Event_Borrar_de_favoritos,
                                                             ActionBarActivity actionBarActivity,
                                                            MiFragment_Lista_De_Favoritos_Con_RecycleView m_MiFragment_Lista_De_Favoritos_Con_RecycleView,
                                                            int m_position_favorito_a_borrar) {



        Clase_borraEventoComoFavorito frag = new Clase_borraEventoComoFavorito();

        frag.m_Event_Borrar_de_favoritos = m_Event_Borrar_de_favoritos;
        frag.actionBarActivity = actionBarActivity;
        frag.m_MiFragment_Lista_De_Favoritos_Con_RecycleView = m_MiFragment_Lista_De_Favoritos_Con_RecycleView;
        frag.m_position_favorito_a_borrar = m_position_favorito_a_borrar;
        TAG = frag.getClass().getSimpleName();

        return frag;
    }//Fin de newInstance




    //******************************************************************************************************
    //19 agosto 2015: metodo definitivo para guardar evento en favoritos
    public void method_borraEventoComoFavorito() {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, " En metodo method_borraEventoComoFavorito:  ");
        }

        boolean boolean_evento_borrado = true;


        DatabaseServerManager databaseServerManager = new DatabaseServerManager();
        databaseServerManager.initManager(actionBarActivity);
        try {
            databaseServerManager.borraEventoComoFavorito(m_Event_Borrar_de_favoritos);
        } catch (eefException eef) {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG,  " En metodo method_borraEventoComoFavorito Exception:  " +eef.getLocalizedMessage());
            }
            boolean_evento_borrado = false;
        }


        //*************************************************************************************************
//        Mostrar el dialogo de has guardado en favoritos
        if (boolean_evento_borrado) {

            //Borrar de la pantalla de lista de favoritos, por que se ha borrado correctamente de la base de datos
            m_MiFragment_Lista_De_Favoritos_Con_RecycleView.pruebaDeBorrar(m_position_favorito_a_borrar);
            // Create an instance of the dialog fragment and show it
            DialogFragment dialog =  ClaseDialogEventoGuardado.newInstance
                    (R.string.titulo_dialog_evento_guardado, R.string.message_dialog_evento_borrado, R.string.positiveButton,
                            R.string.negativeButton, 1, R.string.boton_neutral_evento_guardado);
            dialog.show(actionBarActivity.getSupportFragmentManager(), "ClaseDialogGenericaDosTresBotones");
//            dialog.show(getSupportFragmentManager(), "ClaseDialogGenericaDosTresBotones");
        }else{
            DialogFragment dialog =  ClaseDialogEventoGuardado.newInstance
                    (R.string.titulo_dialog_evento_guardado, R.string.message_dialog_evento_borrado_Error, R.string.positiveButton,
                            R.string.negativeButton, 1, R.string.boton_neutral_evento_guardado);
            dialog.show(actionBarActivity.getSupportFragmentManager(), "ClaseDialogGenericaDosTresBotones");
        }

        //*************************************************************************************************

    }//method_borraEventoComoFavorito
}
