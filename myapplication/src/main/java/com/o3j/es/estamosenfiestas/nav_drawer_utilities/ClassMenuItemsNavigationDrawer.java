package com.o3j.es.estamosenfiestas.nav_drawer_utilities;

import java.io.Serializable;

/**
 * Created by Juan on 20/02/2015.
 */
//20 Feb 2015: Esta clase es un modelo de datos  para cada evento de una determinada fiesta
//Esta clase sigue el patron builder
//Esta clase se puede usar para listar los eventos en la lista de eventos y tambien para mostrar
//el detalle de ese evento con el xml fila_info_eventos_1
public class ClassMenuItemsNavigationDrawer implements Serializable{
    public ClassMenuItemsNavigationDrawer() {
    }

    public static ClassMenuItemsNavigationDrawer newInstance(String param1, String param2, int param4) {
        ClassMenuItemsNavigationDrawer class_Evento_1 = new ClassMenuItemsNavigationDrawer();
        class_Evento_1.setMenuItemTitle(param1);
        class_Evento_1.setItemTitleDescrption(param2);
        class_Evento_1.setSrc_Del_Image_View(param4);
        return class_Evento_1;
    }

    private String menuItemTitle;
    private String itemTitleDescrption;
    private int src_Del_Image_View;

    public int getSrc_Del_Image_View() {
        return src_Del_Image_View;
    }

    public void setSrc_Del_Image_View(int src_Del_Image_View) {
        this.src_Del_Image_View = src_Del_Image_View;
    }

    public String getMenuItemTitle() {
        return menuItemTitle;
    }

    public String getItemTitleDescrption() {
        return itemTitleDescrption;
    }

    public void setMenuItemTitle(String menuItemTitle) {
        this.menuItemTitle = menuItemTitle;
    }

    public void setItemTitleDescrption(String itemTitleDescrption) {
        this.itemTitleDescrption = itemTitleDescrption;
    }
}
