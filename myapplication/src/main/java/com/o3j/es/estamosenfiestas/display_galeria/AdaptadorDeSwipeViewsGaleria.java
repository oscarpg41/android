package com.o3j.es.estamosenfiestas.display_galeria;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.eef.data.dataelements.DayEvents;
import com.eef.data.dataelements.Event;
import com.eef.data.dataelements.Program;

import java.util.ArrayList;

/**
 * Created by Juan on 22/06/2015.
 */


public class AdaptadorDeSwipeViewsGaleria extends FragmentStatePagerAdapter {

    int int_Number_Of_Fragments_To_Show;

    DayEvents day_Events;

    public AdaptadorDeSwipeViewsGaleria(FragmentManager fm, ArrayList<Program> arrayList_Programa, int int_Dia_Seleccionado,
                                        Context context) {
//        public AdaptadorDeSwipeViews_Eventos_Del_Dia(FragmentManager fm, DayEvents eventos_Del_Dia) {
        super(fm);
//        this.day_Events = eventos_Del_Dia;
        this.arrayList_Programa = arrayList_Programa;
        this.int_Dia_Seleccionado = int_Dia_Seleccionado; //No lo uso
        this.context = context;
    }

    ArrayList<Program> arrayList_Programa = new ArrayList<Program>();
    int int_Dia_Seleccionado;
    Context context;
    ArrayList<Event> arrayListDeEventos = new ArrayList<Event>();

    @Override
    public Fragment getItem(int i) {

        Fragment fragment = getNewFragment(arrayList_Programa, i);
        return fragment;
    }
    public Fragment getNewFragment(ArrayList<Program> arrayList_Programa, int i) {
        Fragment fragment = new FragmentMuestraGaleriaConRecycleView();
        Bundle args = new Bundle();

        int int_Nivel_De_Info_Fiesta_Eventos_Del_Dia = 1;

        args.putString(FragmentMuestraGaleriaConRecycleView.ARG_OBJECT, "fragment#" +(int_Nivel_De_Info_Fiesta_Eventos_Del_Dia + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(FragmentMuestraGaleriaConRecycleView.ARG_PARAM2, String.valueOf(int_Nivel_De_Info_Fiesta_Eventos_Del_Dia + 1));

        //Le paso el tipo de lista a presentar, aunque en el fragment no hago nada con esta variable
        args.putInt(FragmentMuestraGaleriaConRecycleView.TIPO_DE_LISTA, int_Nivel_De_Info_Fiesta_Eventos_Del_Dia);


        ClaseCrearListasDeImagenesDeLasFiestas claseCrearListasDeImagenesDeLasFiestas =
                ClaseCrearListasDeImagenesDeLasFiestas.newInstance(context);

        //Le paso arrayList_Programa y obtengo un array list de eventos que tienen urls validas
        //Ojo: El primer elemento siempre tendra el cartel y el segundo siempre el logo
        //del ayuntamiento dado que estas imagenes tengan una url valida
        arrayListDeEventos = claseCrearListasDeImagenesDeLasFiestas.devuelveArrayDeEventos(arrayList_Programa.get(i));


        args.putSerializable("arrayListDeEventos", arrayListDeEventos);


        fragment.setArguments(args);

        return fragment;
    }//Fin de getNewFragment




    //GetCount retorna el numero de fragmentos que va a mostrar el swipeView
    @Override
    public int getCount() {

        return arrayList_Programa.size();
    }


    @Override
    public CharSequence getPageTitle(int position) {

        return arrayList_Programa.get(position).getProgramSortDescription();

    }


}//Fin de la clase
