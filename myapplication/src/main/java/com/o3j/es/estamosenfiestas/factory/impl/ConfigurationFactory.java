package com.o3j.es.estamosenfiestas.factory.impl;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.eef.data.dataelements.AutonomousCommunity;
import com.eef.data.dataelements.EventPlanner;
import com.eef.data.dataelements.MultimediaElement;
import com.eef.data.dataelements.specificCountry.SpainAutonomousCommunity;
import com.eef.data.eefExceptions.eefException;
import com.eef.data.eeftypes.TypeConfiguration;
import com.eef.data.eeftypes.TypeEefError;
import com.eef.data.eeftypes.TypeMultimediaElement;
import com.eef.data.managers.IConfigurationManager;
import com.eef.data.managers.impl.ConfigurationManager;
import com.o3j.es.estamosenfiestas.factory.IConfigurationFactory;
import com.o3j.es.estamosenfiestas.orm.pojosDB.Configuracion;
import com.o3j.es.estamosenfiestas.servercall.eefJsonConnection;
import com.o3j.es.estamosenfiestas.servercall.forbroadcaster.CommunityFromServer;
import com.o3j.es.estamosenfiestas.servercall.forbroadcaster.ConfigurationFromServer;
import com.o3j.es.estamosenfiestas.servercall.netInterface.IeefRetrofit;
import com.o3j.es.estamosenfiestas.servercall.pojos.OrganizerPojo;

import java.util.ArrayList;
import java.util.List;

import managers.DatabaseServerManager;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by jluis on 20/02/15.
 */

public class ConfigurationFactory implements IConfigurationFactory {

    //URLS DE COMUNICACION
/*
    public static String URL_TOWNS="http://www.estamosenfiestas.es/api/index.php/towns/";
    public static String URL_CCAA="http://www.estamosenfiestas.es/api/index.php/ccaa/";
*/

/*
    public static String URL_TOWNS="http://www.estamosenfiestas.es/api/index.php/towns2/";
    public static String URL_CCAA="http://www.estamosenfiestas.es/api/index.php/ccaa2/";
  */

    public static String URL_TOWNS="https://www.estamosenfiestas.es/api/index.php/towns/";
    public static String URL_CCAA="https://www.estamosenfiestas.es/api/index.php/ccaa/";



//    public static String URL_TOWNS="https://www.estamosenfiestas.es/api/index.php/towns2/";
//    public static String URL_CCAA="https://www.estamosenfiestas.es/api/index.php/ccaa2/";

    public static String URL_EVENTSPROGRAM="http://www.estamosenfiestas.es/api/index.php/eventsprogram/";

    //NUMERO DE COMUNIDADES
    private final static Integer NUMERO_COMUNIDADES = 19;


    static Context myContext;

    Integer comunidadesFinalizadas;

    //ID DEL BROADCAST PARA INDICAR EL FIN DE LA DESCARGA DE UNA COMUNIDAD
    String progressBarBR;
    //ID DEL BROADCAST PARA INDICAR QUE LA CONFIGURACION ESTA PREPARADA.
    String showconfigurationBR;

    ConfigurationFromServer confDescargada;

    ArrayList<CommunityFromServer> listaCD;

    private DatabaseServerManager dbm;


    @Override
    public void initFactory(Context contexto, String progressBarBR, String showconfigurationBR)
    {
        this.myContext=contexto;
        this.progressBarBR=progressBarBR;
        this.showconfigurationBR=showconfigurationBR;
        dbm = new DatabaseServerManager();
        dbm.initManager(myContext);
    }

    @Override
    public IConfigurationManager loadConfigrationManagerFromConfigurationFromServer(ConfigurationFromServer configuration,TypeConfiguration typeConfiguration) throws eefException {
        if(!configuration.areConfigurationAvailable())
            throw new eefException(TypeEefError.CONFIGURATION_ERROR);
        ConfigurationManager cmT = new ConfigurationManager(typeConfiguration);
        List<AutonomousCommunity> listaCompletaCcAa =SpainAutonomousCommunity.getAutonomousCommunityList();
        ArrayList<AutonomousCommunity> listaCcaForConfiguration = new ArrayList<AutonomousCommunity>();
        for(AutonomousCommunity ccaa:listaCompletaCcAa)
        {
            CommunityFromServer caTemp = new CommunityFromServer();
            caTemp.setCca_Id(ccaa.getAutonomousCommunityId());
            if(!configuration.contains(caTemp))
                continue;
            caTemp.setListEO(configuration.getCcAa(caTemp).getListEO());
            AutonomousCommunity auc = CommunityFromServerToAutonomusCommunity(caTemp,ccaa.getAutonomousCommunityName());
            listaCcaForConfiguration.add(auc);
        }
        cmT.setAutonomousCommunitiesList(listaCcaForConfiguration);
        return cmT;
    }

    private AutonomousCommunity CommunityFromServerToAutonomusCommunity(CommunityFromServer ccaa_fs, String ccaa_name)
    {
        AutonomousCommunity ac = new AutonomousCommunity();
        ac.setAutonomousCommunityId(ccaa_fs.getCca_Id());
        ac.setAutonomousCommunityName(ccaa_name);
        ac.setIsActiveInConfiguration(false);
        ArrayList<EventPlanner> listEventPlanner = new ArrayList<EventPlanner>();
        for(OrganizerPojo op : ccaa_fs.getListEO())
        {
            listEventPlanner.add(organizerPojoToEventPlanner(op));
        }
        ac.setEventPlannerList(listEventPlanner);
        return ac;
    }

    private EventPlanner organizerPojoToEventPlanner(OrganizerPojo op)
    {
        EventPlanner eventPlanner = new EventPlanner();
        eventPlanner.setAutonomousCommunityId(Integer.parseInt(op.getIdccaa()));
        eventPlanner.setEventPlannerId(Integer.parseInt(op.getIdOrganizer()));
        eventPlanner.setAutonomousCommunityActive(true);
        eventPlanner.setName(op.getTown());
        eventPlanner.setSortDescription(op.getName());
        eventPlanner.setLongDescription(op.getInfo());
        if(op.getLogo()==null || op.getLogo().isEmpty())
            return eventPlanner;

        MultimediaElement mEl = new MultimediaElement(op.getLogo(),TypeMultimediaElement.IMAGE_URL);
        List<MultimediaElement> listMEL = new ArrayList<MultimediaElement>();
        listMEL.add(mEl);
        eventPlanner.setMultimediaElementList(listMEL);
        return eventPlanner;
    }


    @Override
    public void getConfigurationManagerFromServer(TypeConfiguration configurationType) throws eefException{
        eefJsonConnection.checkConnection(myContext);
        if(myContext==null)
            throw new eefException(TypeEefError.CONFIGURATION_ERROR);

        this.confDescargada = new ConfigurationFromServer();
        this.listaCD = new ArrayList<CommunityFromServer>();

        //Indico que el número de comunidaades descargado es 0
        setComunidadesFinalizadas(0);
        for(int cca_id = 1; cca_id<=NUMERO_COMUNIDADES;cca_id++)
        {
            descargaOrganizadoresPorComunidad(cca_id);
        }
        return;
    }

    private void sendProgressBarBroadcast(Integer ccaa_id)
    {

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(progressBarBR);
        broadcastIntent.putExtra("comunidades_acabadas", getComunidadesFinalizadas());
        broadcastIntent.putExtra("comunidades_descargada", ccaa_id);
        myContext.sendBroadcast(broadcastIntent);
    }

    private void sendShowConfigurationBRBroadcast(Integer ccaa_id)
    {
        //Inicializo la configurcion descargada
        confDescargada.setListComWithActiveServers(listaCD);
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(showconfigurationBR);
        broadcastIntent.putExtra("comunidades_acabadas", getComunidadesFinalizadas());
        broadcastIntent.putExtra("comunidades_descargada", ccaa_id);
        broadcastIntent.putExtra("configuration",confDescargada);
        myContext.sendBroadcast(broadcastIntent);
    }

    private void sendBrodcastByEstadoDescarga(Integer ccaa_id)
    {
        if(esUltimaComunidad())
            sendShowConfigurationBRBroadcast(ccaa_id);
        else
            sendProgressBarBroadcast(ccaa_id);
    }

    private void descargaOrganizadoresPorComunidad(final Integer ccaa_id)
    {
        String SEED_LOG = "DOPC["+ccaa_id+"]->";
        RestAdapter myRest =  new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(URL_TOWNS).build();
        IeefRetrofit mySer = myRest.create(IeefRetrofit.class);


        try {

            Log.d(SEED_LOG,"INICIO DESCARGA ORGANIZADORES ["+ccaa_id +"]");

            mySer.getOrganizerByCcAa(ccaa_id, new Callback<List<OrganizerPojo>>() {
                final String LOG_HEAD_SUCCESS = "getOrganizerByCcA.Su-->";
                final String LOG_HEAD_FAILURE = "getOrganizerByCcA.Fa-->";

                @Override
                public void success(List<OrganizerPojo> organizerPojos, Response response) {
                    final ArrayList<OrganizerPojo> myArray = new ArrayList<OrganizerPojo>();

                    if (organizerPojos == null) {
                        Log.i(LOG_HEAD_SUCCESS, "URL[" + response.getUrl() + "] LA LISA DE POJOS ES NULL");
                        comunidadTerminada(ccaa_id);
                        sendBrodcastByEstadoDescarga(ccaa_id);
                        return;
                    }
                    if (organizerPojos.size() == 0) {
                        Log.e(LOG_HEAD_SUCCESS, "El size de la lista de Pojos es 0 [" + organizerPojos.size() + "]");
                        comunidadTerminada(ccaa_id);
                        sendBrodcastByEstadoDescarga(ccaa_id);
                        return;
                    }
                    Log.i(LOG_HEAD_SUCCESS, "ccaa_id[" + ccaa_id + "URL [" + response.getUrl() + "] REASON[" + response.getReason() + "]   ");
                    CommunityFromServer com = new CommunityFromServer();
                    com.setCca_Id(ccaa_id);
                    com.setListEO(organizerPojos);
                    listaCD.add(com);
                    comunidadTerminada(ccaa_id);
                    sendBrodcastByEstadoDescarga(ccaa_id);
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e(LOG_HEAD_FAILURE, "URL[" + error.getUrl()+"] type["+error+"]");
                    comunidadTerminada(ccaa_id);
                    sendBrodcastByEstadoDescarga(ccaa_id);
                }
            });
            Log.d(SEED_LOG,"FIN DESCARGA ORGANIZADORES ["+ccaa_id +"]");
        } catch (Exception ex) {
            Log.e("ERROR","Exception[" + ex.getMessage() +"]");
            ex.printStackTrace();
        }
    }

    public Integer getComunidadesFinalizadas() {
        return comunidadesFinalizadas;
    }


    public void setComunidadesFinalizadas(Integer comunidadesFinalizadas) {
        this.comunidadesFinalizadas = comunidadesFinalizadas;
    }

    private synchronized void  comunidadTerminada(Integer ccaa_id)
    {
        Log.i("comunidadTerminada","SE HA TERMINADO COMUNIDAD["+ccaa_id+"]");
        comunidadesFinalizadas = comunidadesFinalizadas + 1;
    }

    private Boolean esUltimaComunidad()
    {
        if(getComunidadesFinalizadas()>=NUMERO_COMUNIDADES)
            return true;
        return false;
    }

    public static Context getMyContext() {
        return myContext;
    }

    public static void setMyContext(Context myContext) {
        ConfigurationFactory.myContext = myContext;
    }

    public String getProgressBarBR() {
        return progressBarBR;
    }

    public void setProgressBarBR(String progressBarBR) {
        this.progressBarBR = progressBarBR;
    }

    public String getShowconfigurationBR() {
        return showconfigurationBR;
    }

    public void setShowconfigurationBR(String showconfigurationBR) {
        this.showconfigurationBR = showconfigurationBR;
    }


    @Override
     public  IConfigurationManager getConfigurationManager(TypeConfiguration configurationType) throws eefException {

        if(configurationType.getConfigurationId()==TypeConfiguration.INSTALLATION_DEFAULT_WITHOUT_AUTONOMOUS_COMMUNITY.getConfigurationId()) {
            //TODO RETURN A CONFIGURATION MANAGER THAT NOT USE AUTONOMOUS COMMUNITY.
            //CHECK HOw TO IMPLEMENT IT USING GRADLE OR A POSSIBLE SERVICE.
            //The method should include a http call in order to maintain a possible installation from a rest service
            //configuration.setEventPlannerId(1);
            return (IConfigurationManager) new ConfigurationManager(TypeConfiguration.AUTONOMOUS_COMMUNITY_DISABLED);

        }
        if(configurationType.getConfigurationId()==TypeConfiguration.INSTALLATION_DEFAULT_WITH_AUTONOMOUS_COMMUNITY.getConfigurationId()) {
            //TODO RETURN A CONFIGURATION MANAGER THAT NOT USE AUTONOMOUS COMMUNITY.
            //CHECK HOw TO IMPLEMENT IT USING GRADLE OR A POSSIBLE SERVICE.
            //The method should include a http call in order to maintain a possible installation from a rest service
            ConfigurationManager cmT = new ConfigurationManager(TypeConfiguration.AUTONOMOUS_COMMUNITY_ENABLED);
            cmT = getTestCommunity2(cmT);
            return (IConfigurationManager) cmT;
        }


        //The user has saved the configuration and it should be used to generate the configurationManager
        if(configurationType.getConfigurationId()==TypeConfiguration.USER_CONFIGURATION.getConfigurationId()) {
            try {
                Configuracion configuracion = dbm.getConfiguracion();

                //Obtenemos el EventPLanner
                EventPlanner eventPLanerConfigured = dbm.getEventPLanner(configuracion);
                ArrayList<EventPlanner> listEvent = new ArrayList<EventPlanner>();
                listEvent.add(eventPLanerConfigured);


                //Construyo la Autonomous Comunity desde
                AutonomousCommunity au = new AutonomousCommunity();
                ArrayList<AutonomousCommunity> aulist = new ArrayList<AutonomousCommunity>();
                au.setIsActiveInConfiguration(true);
                au.setAutonomousCommunityId(configuracion.getCcaaId());
                au.setAutonomousCommunityName(SpainAutonomousCommunity.getAutonomousCommunityList().get(configuracion.getCcaaId()).getAutonomousCommunityName());
                au.setEventPlannerList(listEvent);
                aulist.add(au);
                ConfigurationManager cmT = new ConfigurationManager(TypeConfiguration.AUTONOMOUS_COMMUNITY_ENABLED);
                cmT.setConfiguration(au, eventPLanerConfigured);
                cmT.setAutonomousCommunitiesList(aulist);

                return (IConfigurationManager)cmT;
            } catch (eefException e) {
                if(e.getErrorT() == TypeEefError.EEF_NOT_CONFIGURED)
                    Log.e("CF-->isconfigured","EEF NO ESTA CONFIGURADO");
                else
                    Log.e("CF-->isconfigured","ERROR ACCEDIENDO A LA CONFIGURACION");
                throw e;
            } catch (Exception ex){
                Log.e("CF-->isconfigured","EXCEPCION NO CONTROLADA ACCEDIENDO A LA CONFIGURACION");
                throw new eefException(TypeEefError.CONFIGURATION_ERROR);
            }

        }
        //If there is not defined a Configuration Manager for the Configuration Type throws an Exception
        throw new eefException(TypeEefError.NO_CONFIGURATION_FOR_TYPE);
    }

    public boolean isconfigured()
    {
        try {
            Configuracion configuracion = dbm.getConfiguracion();
            return true;
        } catch (eefException e) {
            if(e.getErrorT() == TypeEefError.EEF_NOT_CONFIGURED)
                Log.e("CF-->isconfigured","EEF NO ESTA CONFIGURADO");
            else
                Log.e("CF-->isconfigured","ERROR ACCEDIENDO A LA CONFIGURACION");
            return false;
        } catch (Exception ex){
            Log.e("CF-->isconfigured","EXCEPCION NO CONTROLADA ACCEDIENDO A LA CONFIGURACION");
            return false;
        }

    }


    @Override
    public ConfigurationManager saveConfiguration(ConfigurationManager configuration) throws eefException {
        return configuration;
    }


    private ConfigurationManager getTestCommunity2(ConfigurationManager cmn)
    {
        //Castilla la Mancha
        //Creo pueblos de CAstilla la mancha
        EventPlanner Siguenza = new EventPlanner();
        Siguenza.setAutonomousCommunityId(7);
        Siguenza.setEventPlannerId(10);
        Siguenza.setAutonomousCommunityActive(true);
        Siguenza.setName("Ayuntamiento de Sigüenza");
        Siguenza.setSortDescription("Ciudad Mediaval de Sigüenza");
        Siguenza.setLongDescription("Ciudad mediaval de Sigüenza famosa por su catedral y por la conocida obra del doncel de Sigüenza. Tendrá tres programas uno que ya está acabado, uno en marcha y otro para el futuro");

        //Creo pueblos de CAstilla la mancha
        EventPlanner Toledo = new EventPlanner();
        Toledo.setAutonomousCommunityId(7);
        Toledo.setEventPlannerId(11);
        Toledo.setAutonomousCommunityActive(true);
        Toledo.setName("Ayuntamiento de Toledo");
        Toledo.setSortDescription("Toledo");
        Toledo.setLongDescription("Capital de Castilla la mancha preparada para tener solo un progrma que ya está pasado");

        List<EventPlanner> cmel = new ArrayList<EventPlanner>();
        cmel.add(Siguenza);
        cmel.add(Toledo);
        //Creo Castilla la mancha
        AutonomousCommunity cm = new AutonomousCommunity();
        cm.setAutonomousCommunityId(7);
        cm.setAutonomousCommunityName("Castilla la Mancha");
        cm.setEventPlannerList(cmel);

        // Pueablos de castilla Leon
        EventPlanner Avila = new EventPlanner();
        Avila.setAutonomousCommunityId(8);
        Avila.setEventPlannerId(12);
        Avila.setAutonomousCommunityActive(true);
        Avila.setName("Ayuntamiento de Avila");
        Avila.setSortDescription("Avila");
        Avila.setLongDescription("Unica ciudad cargada en el juego de datos de Avila que tendrá uno en marcha u uno en el futuro");


        EventPlanner LaGranja = new EventPlanner();
        LaGranja.setAutonomousCommunityId(8);
        LaGranja.setEventPlannerId(1);
        LaGranja.setAutonomousCommunityActive(true);
        LaGranja.setName("Ayuntamiento de San Ildefonso");
        LaGranja.setSortDescription("San Ildefonso");
        LaGranja.setLongDescription("Sitio donde se celebra uno de los mejores mercados barrocos de España");


        List<EventPlanner> clel = new ArrayList<EventPlanner>();
        clel.add(Avila);
        clel.add(LaGranja);

        //Creo Castilla Leon
        AutonomousCommunity cl = new AutonomousCommunity();
        cl.setAutonomousCommunityId(8);
        cl.setAutonomousCommunityName("Castilla Leon");
        cl.setEventPlannerList(clel);


        //Creo pueblos de Madrid
        EventPlanner Alcala = new EventPlanner();
        Alcala.setAutonomousCommunityId(14);
        Alcala.setEventPlannerId(13);
        Alcala.setAutonomousCommunityActive(true);
        Alcala.setName("Ayuntamiento de Alcala de Henares");
        Alcala.setSortDescription("Ayuntamiento de Alcala de Henares");
        Alcala.setLongDescription("Ciudad Universitaria que tendrá una fiesta en el futuro");


        EventPlanner Navacerrada = new EventPlanner();
        Navacerrada.setAutonomousCommunityId(14);
        Navacerrada.setEventPlannerId(14);
        Navacerrada.setAutonomousCommunityActive(true);
        Navacerrada.setName("Ayuntamiento de Navacerrada");
        Navacerrada.setSortDescription("Navacerrada");
        Navacerrada.setLongDescription("Navacerrada tiene que tener solo un evento activo");

        List<EventPlanner> mel = new ArrayList<EventPlanner>();
        mel.add(Alcala);
        mel.add(Navacerrada);

        //Creo Madrid
        AutonomousCommunity ma = new AutonomousCommunity();
        ma.setAutonomousCommunityId(14);
        ma.setAutonomousCommunityName("Madrid");
        ma.setEventPlannerList(mel);

        List<AutonomousCommunity> aulist = new ArrayList<AutonomousCommunity>();
        aulist.add(cm);
        aulist.add(cl);
        aulist.add(ma);
        cmn.setAutonomousCommunitiesList(aulist);
        //ACTIVA LA DE SIGUENZA
        //cmn.setConfiguration(cm, Siguenza);
        //ACTIVA LA DE LA GRANJA
        cmn.setConfiguration(cl, LaGranja);
        return cmn;

    }
}
