package com.o3j.es.estamosenfiestas.activity_fragment_base_2;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


//Version 56, 19 dic 2014: antes de lanzar cualquier servicio web al server, verifico si hay acceso a internet
//La verificacion la hago en el metodo doInBackground de los asynctask de conneccion con el server
public class ClaseChequeaAcceso_A_Internet {
	
	Context context;
	public ClaseChequeaAcceso_A_Internet(Context context) {
		this.context = context;
	}
	
	
	public boolean isOnline() {
	    ConnectivityManager connMgr = (ConnectivityManager)
	    		context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
	    return (networkInfo != null && networkInfo.isConnected());
	} 
	
}
