package com.o3j.es.estamosenfiestas.display_frame_layout;

import com.eef.data.dataelements.Event;

import java.io.Serializable;

/**
 * Created by Juan on 12/06/2015.
 */
public class Class_Modelo_De_Datos_Lista_Favoritos implements Serializable {
    private static final long serialVersionUID = 1L;


    private String stringTituloDeLaFiesta;
    private String stringFechaDelEvento;
    private Event eventEventoFavorito;

    public String getStringTituloDeLaFiesta() {
        return stringTituloDeLaFiesta;
    }

    public void setStringTituloDeLaFiesta(String stringTituloDeLaFiesta) {
        this.stringTituloDeLaFiesta = stringTituloDeLaFiesta;
    }

    public String getStringFechaDelEvento() {
        return stringFechaDelEvento;
    }

    public void setStringFechaDelEvento(String stringFechaDelEvento) {
        this.stringFechaDelEvento = stringFechaDelEvento;
    }

    public Event getEventEventoFavorito() {
        return eventEventoFavorito;
    }

    public void setEventEventoFavorito(Event eventEventoFavorito) {
        this.eventEventoFavorito = eventEventoFavorito;
    }
}
