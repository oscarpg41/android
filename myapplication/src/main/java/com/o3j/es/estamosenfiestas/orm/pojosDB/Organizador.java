package com.o3j.es.estamosenfiestas.orm.pojosDB;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by jluis on 26/07/15.
 */
@DatabaseTable
public class Organizador {

    public final static String ID ="_id";
    public final static String NAME= "name";
    public final static String TOWN="town";
    public final static String URL_LOGO="url_logo";
    public final static String LOCAL_LOGO="local_logo";
    public final static String FECHA_ULTIMA_DESCARGA ="fecha_descarga";
    public final static String CCAA_ID="ccaa_id";
    public final static String INFO="info";
    public final static String NUMERO_PROGRAMAS="numero_programas";
    public final static String FECHA_ULTIMA_ACTUALIZACION="fecha_ultima_actualizacion";

    @DatabaseField(id=true,  columnName = ID)
    private int id;
    @DatabaseField(width = 255,columnName = NAME)
    private String name;
    @DatabaseField(width = 255,columnName = TOWN)
    private String town;
    @DatabaseField(width = 255,columnName = URL_LOGO)
    private String url_logo;
    @DatabaseField(width = 255,columnName = LOCAL_LOGO)
    private String local_logo;
    @DatabaseField(columnName = FECHA_ULTIMA_DESCARGA)
    private Date fecha_descarga;
    @DatabaseField(columnName = CCAA_ID)
    private int ccaa_id;
    @DatabaseField(columnName = INFO, width = 1000)
    private String info;
    @DatabaseField(columnName = NUMERO_PROGRAMAS)
    private int num_programas;
    @DatabaseField(columnName = FECHA_ULTIMA_ACTUALIZACION)
    private Date fecha_ultima_actualizacion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getUrl_logo() {
        return url_logo;
    }

    public void setUrl_logo(String url_logo) {
        this.url_logo = url_logo;
    }

    public String getLocal_logo() {
        return local_logo;
    }

    public void setLocal_logo(String local_logo) {
        this.local_logo = local_logo;
    }

    public Date getFecha_descarga() {
        return fecha_descarga;
    }

    public void setFecha_descarga(Date fecha_descarga) {
        this.fecha_descarga = fecha_descarga;
    }

    public int getCcaa_id() {
        return ccaa_id;
    }

    public void setCcaa_id(int ccaa_id) {
        this.ccaa_id = ccaa_id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getNum_programas() {
        return num_programas;
    }

    public void setNum_programas(int num_programas) {
        this.num_programas = num_programas;
    }

    public Date getFecha_ultima_actualizacion() {
        return fecha_ultima_actualizacion;
    }

    public void setFecha_ultima_actualizacion(Date fecha_ultima_actualizacion) {
        this.fecha_ultima_actualizacion = fecha_ultima_actualizacion;
    }
}
