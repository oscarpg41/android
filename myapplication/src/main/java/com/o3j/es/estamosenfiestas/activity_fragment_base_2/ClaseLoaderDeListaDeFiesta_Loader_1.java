package com.o3j.es.estamosenfiestas.activity_fragment_base_2;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.o3j.es.estamosenfiestas.R;

import java.util.ArrayList;

/**
 * Created by Juan on 21/02/2015.
 */

//Me he ayudado con:
//http://www.androiddesignpatterns.com/2012/08/implementing-loaders.html

//Subclasses de AsyncTaskLoader must implement loadInBackground() and should override onStartLoading(),
//        onStopLoading(), onReset(), onCanceled(), and deliverResult(D results) to achieve a fully
//        functioning Loader. Overriding these methods is very important as the LoaderManager will
//        call them regularly depending on the state of the Activity/Fragment lifecycle.
//        For example, when an Activity is first started, the Activity instructs the LoaderManager
//        to start each of its Loaders in Activity#onStart(). If a Loader is not already started,
//        the LoaderManager calls startLoading(), which puts the Loader in a started state and
//        immediately calls the Loader’s onStartLoading() method. In other words, a lot of work
//        that the LoaderManager does behind the scenes relies on the Loader being correctly
//        implemented, so don’t take the task of implementing these methods lightly!


public class ClaseLoaderDeListaDeFiesta_Loader_1 extends
//             android.content.AsyncTaskLoader <ArrayList<Class_Evento_1>> {
                AsyncTaskLoader<ArrayList<Class_Evento_1>> {

    private ArrayList<Class_Evento_1> arrayList_DataSource = null;

    public ClaseLoaderDeListaDeFiesta_Loader_1(Context context) {
        // Loaders may be used across multiple Activitys (assuming they aren't
        // bound to the LoaderManager), so NEVER hold a reference to the context
        // directly. Doing so will cause you to leak an entire Activity's context.
        // The superclass constructor will store a reference to the Application
        // Context instead, and can be retrieved with a call to getContext().
        super(context);
    }//Fin del Constructor


    @Override
    public ArrayList<Class_Evento_1> loadInBackground() {
        //Aqui se hace la carga asincrona de datos desde la fuente que sea: sqlite, web, etc

        //Iniciar el array con los eventos de la fiesta
        init_arrayList_De_Eventos_De_la_fiesta();


        //antes de lanzar cualquier servicio web al server, verifico si hay acceso a internet
        ClaseChequeaAcceso_A_Internet claseChequeaAcceso_A_Internet = new ClaseChequeaAcceso_A_Internet(this.getContext());
        if(claseChequeaAcceso_A_Internet.isOnline()){
            //Obtener datos json, agregado el 4 marzo 2015
            metodo_Obtener_String_Json();
        }else
        {
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                String string_Sin_Coneccion_A_Internet = this.getContext().getString(R.string.sin_coneccion_internet);
                Log.d("ClaseLoaderDeListaDeFiesta_Loader_1", "loadInBackground: " +string_Sin_Coneccion_A_Internet);

            }
        }

        return arrayList_DataSource;
    }//Fin de loadInBackground

   private void  metodo_Obtener_String_Json() {
       //Este metodo es usado para buscar string Json en la url correspondiente
       //Usa la clase DownloadStringGetWithJson

       //Obtengo una instancia de DownloadStringGetWithJson
       downloadStringGetWithJson = com.o3j.es.estamosenfiestas.activity_fragment_base_2.DownloadStringGetWithJson.newInstance(null, null);
       String stringConJson =  downloadStringGetWithJson.fetchJson("http://www.comomolo.com/eef/api/index.php/eventsprogram/1",
               this.getContext());
       if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
           Log.d("ClaseLoaderDeListaDeFiesta_Loader_1", "downloadStringGetWithJson: " +stringConJson);
       }
   }

    com.o3j.es.estamosenfiestas.activity_fragment_base_2.DownloadStringGetWithJson downloadStringGetWithJson;

    private void init_arrayList_De_Eventos_De_la_fiesta() {//Relacionado con recycler view
         final int DATASET_COUNT = 60;
        arrayList_DataSource = new ArrayList<Class_Evento_1>();
        for (int i = 0; i < DATASET_COUNT; i++) {
            arrayList_DataSource.add(
                    Class_Evento_1.newInstance("Titulo # " + i,
                            "Descripcion #" + i,
                            "Descripcion2 #" + i,
                            R.drawable.ic_launcher));
        }
    }//Fin de init_arrayList_De_Eventos_De_la_fiesta

    /********************************************************/
    /**  (2) Deliver the results to the registered listener **/
    /********************************************************/

    @Override
    public void deliverResult(ArrayList<Class_Evento_1> data) {
        if (isReset()) {
            // The Loader has been reset; ignore the result and invalidate the data.
            releaseResources(data);
            return;
        }

        // Hold a reference to the old data so it doesn't get garbage collected.
        // We must protect it until the new data has been delivered.
        ArrayList<Class_Evento_1> oldData = arrayList_DataSource;
        arrayList_DataSource = data;

        if (isStarted()) {
            // If the Loader is in a started state, deliver the results to the
            // client. The superclass method does this for us.
            super.deliverResult(data);
        }

        // Invalidate the old data as we don't need it any more.
        if (oldData != null && oldData != data) {
            releaseResources(oldData);
        }
    }//Fin de deliverResult

    /*********************************************************/
    /** (3) Implement the Loader’s state-dependent behavior **/
    /*********************************************************/

    @Override
    protected void onStartLoading() {
        if (arrayList_DataSource != null) {
            // Deliver any previously loaded data immediately.
            deliverResult(arrayList_DataSource);
        }

        // Begin monitoring the underlying data source.
        //Por ahora no uso un observer, ver al final de la clase
//        if (mObserver == null) {
//            mObserver = new SampleObserver();
//            // TODO: register the observer
//        }

        if (takeContentChanged() || arrayList_DataSource == null) {
            // When the observer detects a change, it should call onContentChanged()
            // on the Loader, which will cause the next call to takeContentChanged()
            // to return true. If this is ever the case (or if the current data is
            // null), we force a new load.
            forceLoad();
        }
    }//Fin de onStartLoading

    @Override
    protected void onStopLoading() {
        // The Loader is in a stopped state, so we should attempt to cancel the
        // current load (if there is one).
        cancelLoad();

        // Note that we leave the observer as is. Loaders in a stopped state
        // should still monitor the data source for changes so that the Loader
        // will know to force a new load if it is ever started again.
    }//Fin de onStopLoading

    @Override
    protected void onReset() {
        // Ensure the loader has been stopped.
        onStopLoading();

        // At this point we can release the resources associated with 'mData'.
        if (arrayList_DataSource != null) {
            releaseResources(arrayList_DataSource);
            arrayList_DataSource = null;
        }

        // The Loader is being reset, so we should stop monitoring for changes.

        //Por ahora no uso un observer, ver al final de la clase

//        if (mObserver != null) {
//            // TODO: unregister the observer
//            mObserver = null;
//        }

    }//Fin de onReset

    @Override
    public void onCanceled(ArrayList<Class_Evento_1> data) {
        // Attempt to cancel the current asynchronous load.
        super.onCanceled(data);

        // The load has been canceled, so we should release the resources
        // associated with 'data'.
        releaseResources(data);
    }//Fin de onCanceled

    private void releaseResources(ArrayList<Class_Evento_1> data) {
        // For a simple List, there is nothing to do. For something like a Cursor, we
        // would close it in this method. All resources associated with the Loader
        // should be released here.
        data = null;
    }

    /*********************************************************************/
    /** (4) Observer which receives notifications when the data changes **/
    /*********************************************************************/

    // NOTE: Implementing an observer is outside the scope of this post (this example
    // uses a made-up "SampleObserver" to illustrate when/where the observer should
    // be initialized).

    // The observer could be anything so long as it is able to detect content changes
    // and report them to the loader with a call to onContentChanged(). For example,
    // if you were writing a Loader which loads a list of all installed applications
    // on the device, the observer could be a BroadcastReceiver that listens for the
    // ACTION_PACKAGE_ADDED intent, and calls onContentChanged() on the particular
    // Loader whenever the receiver detects that a new application has been installed.
    // Please don’t hesitate to leave a comment if you still find this confusing! :)

//    private SampleObserver mObserver;

}//Fin de ClaseLoaderDeListaDeFiesta_Loader_1
