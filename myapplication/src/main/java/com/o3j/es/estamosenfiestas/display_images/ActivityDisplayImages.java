package com.o3j.es.estamosenfiestas.display_images;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.eef.data.dataelements.DayEvents;
import com.eef.data.dataelements.Event;
import com.eef.data.dataelements.EventPlanner;
import com.eef.data.dataelements.Program;
import com.eef.data.eefExceptions.eefException;
import com.eef.data.eeftypes.TypeConfiguration;
import com.eef.data.eeftypes.TypeState;
import com.eef.data.managers.IConfigurationManager;
import com.eef.data.managers.impl.ProgamListManager;
import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper;
import com.o3j.es.estamosenfiestas.dialogs.ClaseDialogEventoGuardado;
import com.o3j.es.estamosenfiestas.display_frame_layout.Clase_Add_To_List_Of_Favorites;
import com.o3j.es.estamosenfiestas.display_frame_layout.Class_Modelo_De_Datos_Lista_Favoritos;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Evento;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Fiesta_Seleccionada_Con_RecycleView;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Fiestas_Con_RecycleView;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView;
import com.o3j.es.estamosenfiestas.display_lists_package.AdaptadorDeSwipeViews_2;
import com.o3j.es.estamosenfiestas.display_vista_deslizante_1.AdaptadorDeSwipeViews_Eventos_Del_Dia;
import com.o3j.es.estamosenfiestas.factory.impl.ConfigurationFactory;
import com.o3j.es.estamosenfiestas.nav_drawer_utilities.ClassMenuItemsNavigationDrawer;
import com.o3j.es.estamosenfiestas.nav_drawer_utilities.CustomAdapter_NavigationDrawer_2;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


// Juan 2 Junio 2015: Esta clase usa frame layout para ir poniendo y quitando los fragments con la info de los eventos
//usando listas con este orden y sin salirse de esta actividad:
// 1: Se muestra la lista con todas las fiestas validas de ese ayuntamiento.
// 2: Se muestra la lista de la fiesta seleccionada en 1 por dias.
// 3: Se muestra la lista de eventos del dia seleccionado en 2.
// 4: Se muestra el detalle del evento seleccionado en 3, por ahora sin swipe.
public class ActivityDisplayImages extends ActionBarActivity
        implements CustomAdapter_NavigationDrawer_2.OnItemClickListenerElementoDeMenuSeccionado
{
    //Original con fragment sin loader
//    public class Activity_Fragment_Base_2 extends ActionBarActivity implements MiFragment_1.OnFragmentInteractionListener {
    AdaptadorDeSwipeViews_2 adaptadorDeSwipeViews;
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        xxx = this.getClass().getSimpleName();

//        Nota: Juan 2 junio: para que funcione el view pager y los tabs tengo que usar el layout activity_fragment_lists_with_nav_drawer_3
//        setContentView(R.layout.activity_fragment_lists_with_nav_drawer_3);


//        Este layout funciona pero al tener el drawer como raiz, tapa la toolbar
//        setContentView(R.layout.activity_fragment_frame_layout_with_nav_drawer);


//        4 junio 2015, Uso este layout para que el navigation drawer no tape la roolbar
//        setContentView(R.layout.activity_fragment_frame_layout_with_nav_drawer_3);

//        22 junio 2015, Uso este layout para mostrar swipe views sin nav drawer por que tengo navigation back to parent
//        setContentView(R.layout.activity_fragment_list_of_favorites);
        setContentView(R.layout.activity_display_images);


//        Toolbar toolBar_Actionbar = (Toolbar) findViewById (R.id.activity_my_toolbar);
        Toolbar toolBar_Actionbar = (Toolbar) findViewById(R.id.view);
        //Toolbar will now take on default Action Bar characteristics

//        Le cambio el titulo por el ayuntamiento en method_Data_To_Show_In_Fragments_2
//        Para cambiar el tama�o del titulo lo hice con styles como en
//        http://stackoverflow.com/questions/28487312/how-to-change-the-toolbar-text-size-in-android


//        Tengo que poner este titulo para que aparezca el ayuntamiento en method_Data_To_Show_In_Fragments_2
//        toolBar_Actionbar.setTitle(getResources().getString(R.string.title_main));
        toolBar_Actionbar.setTitle(Class_ConstantsHelper.string_Nombre_Del_Pueblo_O_Ayuntamiento);

        //8 sept 2015: Ahora pongo el nombre del event planner en un text view para que
        //aparezca a la derecha del logo
//            <!-- Juan 8 sept 2015: Nuevo linear layout por que oscar queria que apareciera
//            el logo al lado del nav drawer y el nombre del eventplanner a la derecha del logo.
//            Como he puesto match parent en width, tapa cuando hago set title del toolbar arriba-->
        TextView textview_nombre_del_event_planer = (TextView) findViewById(R.id.textview_nombre_del_event_planer);
        textview_nombre_del_event_planer.setText(Class_ConstantsHelper.string_Nombre_Del_Pueblo_O_Ayuntamiento);


//        No uso el subtitulo
//        toolBar_Actionbar.setSubtitle(getResources().getString(R.string.sub_titulo_4));


//        toolBar_Actionbar.getMenu().clear();


        setSupportActionBar(toolBar_Actionbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        //9 septiembre 2015: dejo comentada la bottom_toolbar,
//        final Toolbar bottom_toolbar = (Toolbar) findViewById(R.id.bottom_toolbar);
//        bottom_toolbar.setLogo(R.drawable.ic_launcher);
//        bottom_toolbar.setTitle(getResources().getString(R.string.texto_1));
//        bottom_toolbar.setSubtitle(getResources().getString(R.string.texto_2));
//        bottom_toolbar.setNavigationIcon(R.drawable.ic_launcher);
//        //Toolbar que pongo abajo
//
//        // Set an OnMenuItemClickListener to handle menu item clicks
//        bottom_toolbar.setOnMenuItemClickListener(
//                new Toolbar.OnMenuItemClickListener() {
//                    @Override
//                    public boolean onMenuItemClick(MenuItem item) {
//                        // Handle the menu item
//
//                        int id = item.getItemId();
//
//                        //noinspection SimplifiableIfStatement
//                        if (id == R.id.action_settings) {
//                            Toast.makeText(Activity_Fragment_SwipeView_EventosDelDia_BackToParent.this, "Bottom toolbar pressed: ", Toast.LENGTH_LONG).show();
//                            return true;
//                        }
//                        return true;
//                    }
//                });
//
//
//        bottom_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(Activity_Fragment_SwipeView_EventosDelDia_BackToParent.this, "Navigation Icon pressed: ", Toast.LENGTH_LONG).show();
//                // Para esta practica, al presionar el navigation icon lo quito.
//                bottom_toolbar.setVisibility(View.GONE);
//            }
//        });
//
//        // Inflate a menu to be displayed in the toolbar
//        bottom_toolbar.inflateMenu(R.menu.menu_main);
        //FIN 9 septiembre 2015: dejo comentada la bottom_toolbar,




        //Por ahora utilizo este metodo para jugar con el API de Jose Luis, 21 Mayo 2015
//        method_Data_To_Show_In_Fragments();

        //4 Junio 2015,
//        method_Data_To_Show_In_Fragments_2(toolBar_Actionbar);

//        method_Gestion_De_Instalacion_de_Pueblos();

        //****************************************************************
        //   Juan, 2 junio 2015: No uso los swipe views, quedan comentados
        //****************************************************************
        // ViewPager and its adapters use support library
        // fragments, so use getSupportFragmentManager.
//        adaptadorDeSwipeViews =
//                new AdaptadorDeSwipeViews_2(
//                        getSupportFragmentManager(), int_Number_Of_Fragments_To_Show, List_Programs);
//        mViewPager = (ViewPager) findViewById(R.id.viewpager_1);
//        mViewPager.setAdapter(adaptadorDeSwipeViews);
//
//        //Poner tabs del swipe view
//        metodoPonerTabs_1(mViewPager);
        //****************************************************************
        //   FIN de Juan, 2 junio 2015: No uso los swipe views, quedan comentados
        //****************************************************************



        //Inicializar el nav drawer, 21 Mayo 2015
//        method_Init_NavigationDrawer();


        //22 Junio 2015: Recupero el intent con los datos para las swipe views de eventos del dia
        method_Recuperar_Intent();
//        method_Generar_SwipeViews_Eventos_Del_Dia();

        //24 junio 2015: Se muestran el titulo de la fiesta encima del tabview
        //como me pidio Oscar
//        Texto para mostrar el titulo de la fiesta
        TextView textView_Titulo = (TextView) findViewById(R.id.titulo_del_fragment);

//        textView_Titulo.setVisibility(View.VISIBLE);

        textView_Titulo.setText(Class_ConstantsHelper.titulo_De_La_Fiesta);


        //        Por ahora, cargo el icono que hice (lo del mipmap):
//        ImageView imageView_en_nav_drawer_abajo = (ImageView) findViewById(R.id.imageView_en_nav_drawer_abajo);
//        imageView_en_nav_drawer_abajo.setImageResource(R.drawable.ic_sanildefonso_logo_correcto_oscar);
//        imageView_en_nav_drawer_abajo.setVisibility(View.VISIBLE);

        //Juan 18 agosto 2015, cargo el icono que guarde del event planner en frame layout

            ImageView imageView_en_nav_drawer_abajo = (ImageView) findViewById(R.id.imageView_en_nav_drawer_abajo);
            method_Descarga_Imagen_De_Internet(imageView_en_nav_drawer_abajo, Class_ConstantsHelper.eventPlanner);

        method_Descarga_Cartel_De_La_Fiesta_Desde_Internet
                ((ImageView) findViewById(R.id.imageView_cartel_fiesta), arrayList_Programa.get(0));

    }//Fin del onCreate
    public void method_Descarga_Imagen_De_Internet(ImageView imageView, EventPlanner eventPlanner) {
        //Juan 17 agosto 2015: presento la imagen como en:
        //http://themakeinfo.com/2015/04/android-retrofit-images-tutorial/
        //Con esta instruccion:
        //Picasso.with(context).load("http://i.imgur.com/DvpvklR.png").into(imageView);
        //O esta:
        //Picasso.with(getContext()).load(url+flower.getPhoto()).resize(100,100).into(img);

        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, xxx + " : estoy en method_Descarga_Imagen_De_Internet ");
        }

        String url = null;

        try {
            url = eventPlanner.getMultimediaElementList().get(0).getMultimediaElementLocation();
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(xxx,  " : En metodo method_Descarga_Imagen_De_Internet url: " +url);
            }
            if(!Class_ConstantsHelper.methodStaticChequeaURL(url)){//url invalida
                //original, queda comentada
//                if(url == null || url.isEmpty()){
                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.e(xxx,  " : En metodo method_Descarga_Imagen_De_Internet: url es INVALIDA ");
                }
//            imageView.setVisibility(View.GONE);
                imageView.setVisibility(View.GONE);
            }else{//url es valido, invoca la descarga de la imagen
//                Picasso.with(this).load(url).resize(75, 80).into(imageView);
                Picasso.with(this).load(url).fit().centerInside().into(imageView);
                imageView.setVisibility(View.VISIBLE);
            }
//            Picasso.with(activity_Display_Event_Planner_Information_WithNavDrawer_2).load(url).resize(400, 400).into(imageView);
        } catch (eefException eef) {
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(xxx, xxx + " : En metodo method_Descarga_Imagen_De_Internet en eefException: \" + eef.getLocalizedMessage()");
            }
            //Si hay algun problema, hago invisible la imagen
            imageView.setVisibility(View.GONE);

        }

    }//Fin de method_Descarga_Imagen_De_Internet


    public void method_Descarga_Cartel_De_La_Fiesta_Desde_Internet(ImageView imageView, Program program) {
        //Juan 17 agosto 2015: presento la imagen como en:
        //http://themakeinfo.com/2015/04/android-retrofit-images-tutorial/
        //Con esta instruccion:
        //Picasso.with(context).load("http://i.imgur.com/DvpvklR.png").into(imageView);
        //O esta:
        //Picasso.with(getContext()).load(url+flower.getPhoto()).resize(100,100).into(img);

        String url = null;

        url = program.getListMultimediaElement().get(0).getMultimediaElementLocation();

        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, ": En metodo method_Descarga_Imagen_De_Internet, url: " +url);
        }
        if(!Class_ConstantsHelper.methodStaticChequeaURL(url)){//url invalida
//            if(url == null || url.isEmpty()){
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(xxx, ": En metodo method_Descarga_Imagen_De_Internet: url es NULL o esta VACIO");
            }
            imageView.setVisibility(View.GONE);
        }else {//url es valido, invoca la descarga de la imagen

            //Tengo que hacer esto en un asyntask en segundo plano
            //por si no la app se para, vamos, ni raranca
            //java.lang.IllegalStateException: Method call should not happen from the main thread.
//            method_Tamaño_De_La_Imagen_Descargada(url);

            //SE puede ver deformada
//            Picasso.with(activity_fragment_frameLayout_withNavDrawer_2).load(url).resize(180, 180).into(imageView);
            //Asi se ven bien, aunque algo recortadas, depende del tamaño
            //todas las imagene se ven del mismo tamaño, pero recortadas
            //Entonces un cartel que tenga texto, se puede ver fatal
//            Picasso.with(activity_fragment_frameLayout_withNavDrawer_2).load(url).fit().centerCrop().into(imageView);

            //asi se ve bien, la imagen sale entera pero dependiendo de la imagen, saldra mas vertical,
            //O mas horizontal, y cada imagen, de diferente tamaño.
            //O sea, que muestra toda la imagen respetando sus proporciones
            Picasso.with(this).load(url).fit().centerInside().into(imageView);
            //Cambio la visibility a Visible
            imageView.setVisibility(View.VISIBLE);


            //ESto casca, me da puntero nulo, LOGICO
//             int origW = 0;
//             int origH = 0;
//            origW = imageView.getDrawable().getIntrinsicHeight();
//            origH = imageView.getDrawable().getIntrinsicWidth();
//            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
//                Log.e(TAG, ": En metodo method_Descarga_Imagen_De_Internet: ANCHO: " +origW);
//                Log.e(TAG, ": En metodo method_Descarga_Imagen_De_Internet: ALTO: " +origH);
//            }

        }
    }

//    Juan 2 junio 2015, no uso el view pager ni los tabs, dejo el metodo metodoPonerTabs_1 comentado

//    private void metodoPonerTabs_1(ViewPager mViewPager) {
//
////        Hecho como en: http://guides.codepath.com/android/Google-Play-Style-Tabs-using-SlidingTabLayout
//// Give the SlidingTabLayout the ViewPager
//        SlidingTabLayout slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
//        // Center the tabs in the layout
//        slidingTabLayout.setDistributeEvenly(true);
//        slidingTabLayout.setViewPager(mViewPager);
//        //Colorear el tab
//        slidingTabLayout.setBackgroundColor(getResources().getColor(R.color.primary_color));
//        // Customize tab indicator color
//        slidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
//            @Override
//            public int getIndicatorColor(int position) {
//                return getResources().getColor(R.color.accent_color);
//            }
//        });
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;

        // Uncomment to inflate menu items to Action Bar
        // inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this. 21 Mayo 2015


        //22 junio 2015: en esta clase no uso nav drawer, uso nav back to parent. Lo dejo comentado
//        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
//            return true;
//        }//Codigo del nav drawer
        //FIN DE 22 junio 2015: en esta clase no uso nav drawer, uso nav back to parent. Lo dejo comentado


        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//
//
////Para que no salga el menu de overflow, lo he hecho como en: (pero se cuelga)
////        http://stackoverflow.com/questions/28396775/new-android-toolbar-how-to-hide-the-overflow-menu-icon
//        if (id == R.id.action_settings) {
//            Toast.makeText(Activity_Fragment_SwipeView_EventosDelDia_BackToParent.this, " toolbar pressed: ", Toast.LENGTH_LONG).show();
//
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    //Implementa la interfaz de la clase MiFragment_Fiestas_Con_RecycleView
//    y la clase MiFragment_Fiesta_Seleccionada_Con_RecycleView

//    OJO: NO ESTOY HACIENDO USO DE ESTA INTERFAZ
    public void onFragmentInteraction(String string) {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"Fragmento activo: " +string);

        }
    }



    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //  Inicio de Codigo relacionado con el navigation drawer, 21 Mayo 2015
    //****************************************************************
    //****************************************************************
    private void method_Init_NavigationDrawer() {

        //****************************************************************
        //   Codigo relacionado con recycler view del navigation drawer
        //****************************************************************


        //Inicializar los datos del array del menu a mostrar en el navigationDrawer
        init_arrayList_NavigationDrawer();

        navigationDrawerRecyclerView = (RecyclerView) findViewById(R.id.left_drawer_recycle_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        navigationDrawerRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        navigationDrawerLayoutManager = new LinearLayoutManager(this);
        navigationDrawerRecyclerView.setLayoutManager(navigationDrawerLayoutManager);
//        navigationDrawerRecyclerView.setAdapter(new CustomAdapter_NavigationDrawer(

//        Adapter del navigation drawer con la cabecera
                navigationDrawerRecyclerView.setAdapter(new CustomAdapter_NavigationDrawer_2(
                arrayListItemsNavigationDrawer, "0",
                        (CustomAdapter_NavigationDrawer_2.OnItemClickListenerElementoDeMenuSeccionado) this));

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_1);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        //Este objeto permite que el navigation Drawer interactue correctamente con el action bar (la toolbar en mi caso)
        //Tengo que usar el actionBarDrawerToggle de V7 support por que el de V4 esta deprecated
        actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                drawerLayout,         /* DrawerLayout object */
                //Deprecated en V7, esto era valido en V4
//                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
//                getSupportActionBar().setTitle(getResources().getString(R.string.title_activity_main_activity_navigation_drawer));
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()

            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                getActionBar().setTitle(mDrawerTitle);
//                getSupportActionBar().setTitle(getTitle());
//                getSupportActionBar().setTitle(getResources().getString(R.string.titulo_navigation_drawer));
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()


            }
        };//Fin de actionBarDrawerToggle

        // Set the drawer toggle as the DrawerListener
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //****************************************************************
        //  FIN Codigo relacionado con recycler view del navigation drawer
        //****************************************************************
    }//Fin de method_Init_NavigationDrawer


    /* Called whenever we call invalidateOptionsMenu() */
//    @Override
//    public boolean onPrepareOptionsMenu(Menu menu) {
//        // If the nav drawer is open, hide action items related to the content view
//        boolean drawerOpen = drawerLayout.isDrawerOpen(navigationDrawerRecyclerView);
//        menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
//        return super.onPrepareOptionsMenu(menu);
//    }// Fin de onPrepareOptionsMenu


    //Datos del menu del navigation drawer
    private void init_arrayList_NavigationDrawer() {//Relacionado con recycler view
//        String[] array_menu = {"Actualizar fiestas", "Reconfigurar", "Información", "Organizador"};
//        String[] array_menu = {"Estamos en fiestas","Mis eventos","Actualizar fiestas", "Reconfigurar", "Información", "Organizador"};
        String[] array_menu = {getString(R.string.menu_1),getString(R.string.menu_2),getString(R.string.menu_3)
                        ,getString(R.string.menu_4),getString(R.string.menu_5),getString(R.string.menu_6)};

        arrayListItemsNavigationDrawer = new ArrayList<ClassMenuItemsNavigationDrawer>();
        for (int i = 0; i < DATASET_COUNT; i++) {
            arrayListItemsNavigationDrawer.add(
                    ClassMenuItemsNavigationDrawer.newInstance(array_menu[i],
                            " ",
                            R.drawable.ic_launcher));
        }
    }


    //Datos del menu del navigation drawer original, ya no lo uso, 6 junio 2015
//    private void init_arrayList_NavigationDrawer_Original() {//Relacionado con recycler view
//        arrayListItemsNavigationDrawer = new ArrayList<ClassMenuItemsNavigationDrawer>();
//        for (int i = 0; i < DATASET_COUNT; i++) {
//            arrayListItemsNavigationDrawer.add(
//                    ClassMenuItemsNavigationDrawer.newInstance("Titulo # " + i,
//                            "Descripcion #" + i,
//                            R.drawable.ic_launcher));
//        }
//    }
    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    //Si estos metodos no se llaman, no aparece la hamburguesa
//    @Override
//    protected void onPostCreate(Bundle savedInstanceState) {
//        super.onPostCreate(savedInstanceState);
//        // Sync the toggle state after onRestoreInstanceState has occurred.
//        actionBarDrawerToggle.syncState();
//    }

//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//        // Pass any configuration change to the drawer toggls
//        actionBarDrawerToggle.onConfigurationChanged(newConfig);
//    }

    //Variables para el recycler view
    private DrawerLayout drawerLayout;
    private RecyclerView navigationDrawerRecyclerView;
    private RecyclerView.Adapter navigationDrawerAdapter;
    private RecyclerView.LayoutManager navigationDrawerLayoutManager;
    protected ArrayList<ClassMenuItemsNavigationDrawer> arrayListItemsNavigationDrawer = null;
    private static final int DATASET_COUNT = 6;
    //Este objeto permite que el navigation Drawer interactue correctamente con el action bar (la toolbar en mi caso)
    private ActionBarDrawerToggle actionBarDrawerToggle;
    //FIN Variables para el recycler view

    //****************************************************************
    //****************************************************************
    //     FIN del Codigo relacionado con el navigation drawer
    //****************************************************************
    //****************************************************************




    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //  Inicio de Codigo relacionado con el API de JL, 21 Mayo 2015
    //****************************************************************
    //****************************************************************
    private void method_Data_To_Show_In_Fragments() {
//        ESte metodo obtiene datos del api de JL, pero habra que cambiar cosas.

        ConfigurationFactory cmTFactory = new ConfigurationFactory();

        try {
            myCmT = cmTFactory.getConfigurationManager(TypeConfiguration.INSTALLATION_DEFAULT_WITH_AUTONOMOUS_COMMUNITY);

            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(xxx, xxx +" Esta bien configurado [" + myCmT.isRightConfigured() + "]");
                Log.d(xxx, xxx +" El cmt esta configurado[" + myCmT.isRightConfigured() + "] numero AU[" + myCmT.getAutonomousCommunityList().size() + "]");
                Log.d(xxx, xxx +" La comunidad Autonoma Configurada[" + myCmT.getAutonomousCommunityConfigured().getAutonomousCommunityName()
                        + "] y el pueblo configurado[" + myCmT.getEventPlannerConfigured().getName() + "]");
            }

            ProgamListManager pmT = new ProgamListManager();
            pmT.loadData(myCmT.getAutonomousCommunityConfigured(),myCmT.getEventPlannerConfigured());
            if(pmT.existCurrentProgram()){
                Program p = pmT.getCurrentProgram();

                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(xxx, xxx +"HAY PROGRAMA EN MARCHA ["+p.getProgramSortDescription()+"]");

                }
                //Obtener la lista de eventos del programa por dias
                List<DayEvents> ltd = p.getEventByDay();
                //Obtener la lista de eventos completa
                List<Event> ltf =p.getFullList();
            }



            if(pmT.existCurrentProgram() || pmT.existFuturetPrograms() || pmT.existPastPrograms()) {
                 List_Programs = pmT.getFullProgramList();


                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(xxx, "HAY  [" + List_Programs.size() + "]" +" PROGRAMAS EN MARCHA");
                    for (int i=0; i<List_Programs.size(); i++){
                        Log.d(xxx, xxx +" Codigo del programa numero  [" +i + ": "
                                +List_Programs.get(i).getState().getCode() + "]" +" PROGRAMAS EN MARCHA");

                        Log.d(xxx, xxx +" Codigo del programa numero  [" +i + ": "
                                +List_Programs.get(i).getProgramSortDescription() + "]" +" PROGRAMAS EN MARCHA");

                        if(List_Programs.get(i).getState().getCode().equals(TypeState.ENDED)){
                            int_Number_Of_Fragments_To_Show++;
                            List_Programs.remove(i);
                        }

                    }
                }



                for (int i=0; i<List_Programs.size(); i++){
                    arrayList_Lista_De_Fiesta_Serializable.add(List_Programs.get(i));
                }
                method_Muestra_Fragment_Con_La_Lista_De_Las_Fiestas(int_Nivel_De_Info_Fiestas);


            }



//            Juan, 10 junio 2015, VER ESTA FORMA DE PASAR PARAMETROS CON PUNTOS SUSPENSIVOS Y EL FOR CON LOS : PUNTOS

//            private static void toggleVisibility(View... views) {
//                for (View view : views) {
//                    boolean isVisible = view.getVisibility() == View.VISIBLE;
//                    view.setVisibility(isVisible ? View.INVISIBLE : View.VISIBLE);
//                }
//            }

        }
        catch(eefException eef)
        {
            Log.e("onCreate","Error inciando la conficuracion");
        }
        catch (Exception ex){
            Log.e("onCreate","Excepotion no eef inciando la configuracion");
        }


    }//Fin de method_Data_To_Show_In_Fragments


    IConfigurationManager myCmT=null;
    private static String xxx;
    List<Program> List_Programs;
    int int_Number_Of_Fragments_To_Show = 0;
    ArrayList<Program> arrayList_Lista_De_Fiesta_Serializable = new ArrayList<Program>();

    int int_Nivel_De_Info_Fiestas = 1;
    int int_Nivel_De_Info_Fiesta_Por_Dias = 2;
    int int_Nivel_De_Info_Fiesta_Eventos_Del_Dia = 3;
    int int_Nivel_De_Info_Fiesta_Evento_Detalle = 4;


    //****************************************************************
    //****************************************************************
    //     FIN del Codigo relacionado con el API de JL
    //****************************************************************
    //****************************************************************

    public void method_Muestra_Fragment_Con_La_Lista_De_Las_Fiestas(int int_Nivel_De_Info) {
// Create new fragment and transaction


        Fragment newFragment = getNewFragmnet(int_Nivel_De_Info);
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack

        if (int_Nivel_De_Info == int_Nivel_De_Info_Fiestas) {
            transaction.add(R.id.details, newFragment);
        }
        else{
            transaction.replace(R.id.details, newFragment);
            transaction.addToBackStack(null);
        }

// Commit the transaction
        transaction.commit();
    }// Fin de method_Muestra_Fragment_Con_La_Lista_De_Las_Fiestas

    public Fragment getNewFragmnet(int i) {
        Fragment fragment = new MiFragment_Fiestas_Con_RecycleView();
        Bundle args = new Bundle();
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
        args.putString(MiFragment_Fiestas_Con_RecycleView.ARG_OBJECT, "fragment#" +(i + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiestas_Con_RecycleView.ARG_PARAM2, String.valueOf(i + 1));

        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiestas_Con_RecycleView.TIPO_DE_LISTA, i);


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
        args.putSerializable("Lista de Programas Serializado", arrayList_Lista_De_Fiesta_Serializable);


        fragment.setArguments(args);

        return fragment;
    }



    public Fragment getNewFragmentForFiesta(Program m_Programa_seleccionado) {
        Fragment fragment = new MiFragment_Fiesta_Seleccionada_Con_RecycleView();
        Bundle args = new Bundle();
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "fragment#" +(int_Nivel_De_Info_Fiesta_Por_Dias + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, String.valueOf(int_Nivel_De_Info_Fiesta_Por_Dias + 1));

        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, int_Nivel_De_Info_Fiesta_Por_Dias);


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
        args.putSerializable("Programa Serializado", m_Programa_seleccionado);


        fragment.setArguments(args);

        return fragment;
    }




    public Fragment getNewFragmentForDayEvents(DayEvents m_DayEvents) {
        Fragment fragment = new MiFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView();
        Bundle args = new Bundle();
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "fragment#" +(int_Nivel_De_Info_Fiesta_Eventos_Del_Dia + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, String.valueOf(int_Nivel_De_Info_Fiesta_Eventos_Del_Dia + 1));

        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, int_Nivel_De_Info_Fiesta_Eventos_Del_Dia);

//        Hago esto por que la lista de eventos no es serializable, asi que lo pongo en array para
//                pasarlo al fragment con la lista de los eventos del dia seleccionado
        ArrayList<Event> arrayList_Lista_De_Eventos_Del_Dia = new ArrayList<Event>();
        for (int i=0; i<m_DayEvents.getListaEventos().size(); i++){
            arrayList_Lista_De_Eventos_Del_Dia.add(m_DayEvents.getListaEventos().get(i));
        }


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
        args.putSerializable("Eventos del dia Serializado", arrayList_Lista_De_Eventos_Del_Dia);


        fragment.setArguments(args);

        return fragment;
    }



    private void method_Envia_Intent_Al_SwipeView_De_Evento_Detalle(Event m_Event) {
        //22 Junio 2015: salto a la actividad de swipe views eventos del dia
        Intent intent;

        intent = new Intent(this, com.o3j.es.estamosenfiestas.display_vista_deslizante_2.
                Activity_Fragment_SwipeView_EventoDetalle_BackToParent.class);

        //22 junio 2015, datos que se pasan en un bundle a la actividad con swipe views eventos del dia
        Bundle args_Datos_Para_SwipeView_De_Evento_Detalle = new Bundle();

//        Hago esto por que la lista de eventos no es serializable, asi que lo pongo en array para
//                pasarlo al fragment con la lista de los eventos del dia seleccionado
//        ArrayList<Program> arrayList_Programa = new ArrayList<Program>();
//        arrayList_Programa.add(m_Programa_seleccionado);


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);

//        Paso todos los datos de la fiesta
        args_Datos_Para_SwipeView_De_Evento_Detalle.putSerializable("Fiesta_Serializada", arrayList_Programa);



//        Paso el evento seleccionado para poder averiguar cual es el dia de los eventos a mostrar y el evento inicial
        ArrayList<Event> arrayList_Evento_Seleccionado = new ArrayList<Event>();
        arrayList_Evento_Seleccionado.add(m_Event);

        args_Datos_Para_SwipeView_De_Evento_Detalle.putSerializable("arrayList_Evento_Seleccionado", arrayList_Evento_Seleccionado);




        intent.putExtras(args_Datos_Para_SwipeView_De_Evento_Detalle);

        startActivity(intent);
        //FIN DE 22 Junio 2015: salto a la actividad de swipe views eventos del dia
    }//FIN DE method_Envia_Intent_Al_SwipeView_De_Eventos_Del_Dia


    public Fragment getNewFragmentToShowEvent(Event m_Event) {
        Fragment fragment = new MiFragment_Evento();
        Bundle args = new Bundle();
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "fragment#" + (int_Nivel_De_Info_Fiesta_Evento_Detalle + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, String.valueOf(int_Nivel_De_Info_Fiesta_Evento_Detalle + 1));
        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, int_Nivel_De_Info_Fiesta_Evento_Detalle);

        args.putSerializable("Datos del Evento Serializado", m_Event);

        fragment.setArguments(args);

        return fragment;
    }





    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //  4 Junio 2015 method_Data_To_Show_In_Fragments_2
    //****************************************************************
    //****************************************************************
    private void method_Data_To_Show_In_Fragments_2(Toolbar toolBar_Actionbar) {
//        ESte metodo obtiene datos del api de JL, pero habra que cambiar cosas.

        ConfigurationFactory cmTFactory = new ConfigurationFactory();

        try {
//            myCmT = cmTFactory.getConfigurationManager(TypeConfiguration.INSTALLATION_DEFAULT_WITH_AUTONOMOUS_COMMUNITY);

//            12 junio 2015: chequea si se ha cambiado la configuracion
            if (Class_ConstantsHelper.boolean_Hay_Una_nueva_configuracion) {
                myCmT = Class_ConstantsHelper.m_IConfigurationManager;
            } else {
                //15 junio 2015: Con este no vuelve a mostrar san ildefonso despues de un cambio de pueblo
//                myCmT = cmTFactory.getConfigurationManager(TypeConfiguration.INSTALLATION_DEFAULT_WITH_AUTONOMOUS_COMMUNITY);

                //15 junio 2015: Con este no vuelve a mostrar san ildefonso despues de un cambio de pueblo
                myCmT = cmTFactory.getConfigurationManager(TypeConfiguration.USER_CONFIGURATION);

                //15 junio 2015: Con este no funciona al empezar la app
//                myCmT = cmTFactory.getConfigurationManager(TypeConfiguration.AUTONOMOUS_COMMUNITY_ENABLED);
            }







            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(xxx, xxx +" Esta bien configurado [" + myCmT.isRightConfigured() + "]");
                Log.d(xxx, xxx +" El cmt esta configurado[" + myCmT.isRightConfigured() + "] numero AU[" + myCmT.getAutonomousCommunityList().size() + "]");
                Log.d(xxx, xxx +" La comunidad Autonoma Configurada[" + myCmT.getAutonomousCommunityConfigured().getAutonomousCommunityName()
                        + "] y el pueblo configurado[" + myCmT.getEventPlannerConfigured().getName() + "]");
            }

            ProgamListManager pmT = new ProgamListManager();
            pmT.loadData(myCmT.getAutonomousCommunityConfigured(),myCmT.getEventPlannerConfigured());
            if(pmT.existCurrentProgram()){
                Program p = pmT.getCurrentProgram();

                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(xxx, xxx +" HAY PROGRAMA EN MARCHA ["+p.getProgramSortDescription()+"]");
                    Log.d(xxx, xxx +" EVENT PLANNER NOMBRE:  ["+myCmT.getEventPlannerConfigured().getName()+"]");
                    Log.d(xxx, xxx +" EVENT PLANNER SHORT DESCRIPTION:  ["+myCmT.getEventPlannerConfigured().getSortDescription()+"]");
                    Log.d(xxx, xxx +" EVENT PLANNER LONG DESCRIPTION:  ["+myCmT.getEventPlannerConfigured().getLongDescription()+"]");

                }
                //Obtener la lista de eventos del programa por dias
                List<DayEvents> ltd = p.getEventByDay();
                //Obtener la lista de eventos completa
                List<Event> ltf =p.getFullList();
            }



            if(pmT.existCurrentProgram() || pmT.existFuturetPrograms() || pmT.existPastPrograms()) {
                List_Programs = pmT.getFullProgramList();


                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(xxx, "HAY  [" + List_Programs.size() + "]" +" PROGRAMAS EN MARCHA");
                    for (int i=0; i<List_Programs.size(); i++){
                        Log.d(xxx, xxx +" Codigo del programa numero  [" +i + ": "
                                +List_Programs.get(i).getState().getCode() + "]" +" PROGRAMAS EN MARCHA");

                        Log.d(xxx, xxx +" Codigo del programa numero  [" +i + ": "
                                +List_Programs.get(i).getProgramSortDescription() + "]" +" PROGRAMAS EN MARCHA");


//                        if(List_Programs.get(i).getState().getCode().equals(TypeState.ENDED)){
//                            int_Number_Of_Fragments_To_Show++;
//                            List_Programs.remove(i);
//                        }

                    }
                }


                toolBar_Actionbar.setTitle(myCmT.getEventPlannerConfigured().getName());


                for (int i=0; i<List_Programs.size(); i++){
                    arrayList_Lista_De_Fiesta_Serializable.add(List_Programs.get(i));
                }
                method_Muestra_Fragment_Con_La_Lista_De_Las_Fiestas(int_Nivel_De_Info_Fiestas);


            }


        }
        catch(eefException eef)
        {
            Log.e("onCreate" +xxx,"Error inciando la conficuracion: " +eef.getLocalizedMessage());
        }
        catch (Exception ex){
            Log.e("onCreate" +xxx,"Excepotion no eef inciando la configuracion: " +ex.getLocalizedMessage());
        }


    }//Fin de method_Data_To_Show_In_Fragments_2

    @Override
    public void onClickMenuItenDelNavDrawer(int position_Del_Menu_Seleccionada) {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"He llegado a onClickMenuItenDelNavDrawer  con click: " +position_Del_Menu_Seleccionada );
        }

//        Intent intent = new Intent(this, com.o3j.es.estamosenfiestas.activity_actualizar_fiestas.
//                Activity_Fragment_Actualizar_Fiesta_WithNavDrawer_2.class);
//        startActivity(intent);


        Intent intent;


        switch (position_Del_Menu_Seleccionada)
        {
            case 0:
//                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_frame_layout.
//                        Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
//                startActivity(intent);
                break;
            case 1://Mis Eventos
                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_list_of_favorites.
                        Activity_Fragment_List_Of_Favorites_With_Nav_Drawer.class);
                startActivity(intent);
                break;
            case 2:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.activity_actualizar_fiestas.
                        Activity_Fragment_Actualizar_Fiesta_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 3:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.application_configuration.
                        Activity_Application_Configuration_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 4:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_informacion.
                        Activity_Display_Developer_Information_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 5:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_event_planner_information.
                        Activity_Display_Event_Planner_Information_WithNavDrawer_2.class);
                startActivity(intent);
                break;
        }

        if (position_Del_Menu_Seleccionada != 0) finish();
    }


    //****************************************************************
    //****************************************************************
    //     9 Junio 2015: Codigo relacionado con el contextual action mode para
    //    insetar un evento en la lista de favoritos.
    //    Este interfaz es de la clase: CustomAdapter_RecyclerView_List_De_Eventos
    //    Al hacer long click en un evento
    //****************************************************************
    //****************************************************************
//    @Override
//    public boolean onLongClickEvento_CustomA_List_De_Eventos(Event m_Event, View m_View_Del_Evento) {
//        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
//            Log.d(xxx, xxx +" He llegado a onLongClickEvento_CustomA_List_De_Eventos  con click: " );
//        }
//
//        //Cuando aparece el contextual action bar, oculto la tool bar con Invisible para que haga el efecto de movimiento
//        Toolbar toolBar_Actionbar = (Toolbar) findViewById(R.id.view);
//        toolBar_Actionbar.setVisibility(View.GONE);
//
//        m_Event_Poner_en_favoritos = m_Event;
//
//        if (mActionMode != null) {
//            return false;
//        }
//
//        // Start the CAB using the ActionMode.Callback defined above
//        mActionMode = this.startActionMode(mActionModeCallback);
//        m_View_Del_Evento.setSelected(true);
//
//
//        return true;
//    }



    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_contextual_action_mode_evento_de_la_lista, menu);
            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_agregar_a_favoritos:
                    if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                        Log.d(xxx, xxx +" action_agregar_a_favorito " );
                    }



                    //19 agosto 2015, ya no uso el fake, uso Clase_Add_To_List_Of_Favorites
//                    methodFake_Add_To_List_Of_Favorites();

                    //19 agosto 2015: Uso la clase Clase_Add_To_List_Of_Favorites que tiene el metodo method_Add_To_List_Of_Favorites
                    Clase_Add_To_List_Of_Favorites clase_add_to_list_of_favorites =
                            Clase_Add_To_List_Of_Favorites.newInstance(m_Event_Poner_en_favoritos,
                                    ActivityDisplayImages.this);
                    clase_add_to_list_of_favorites.method_Add_To_List_Of_Favorites();


//                    shareCurrentItem();
                    mode.finish(); // Action picked, so close the CAB

                    //Cuando se ejecuta el contextual action bar, se hace visible la toolbar
                    Toolbar toolBar_Actionbar = (Toolbar) findViewById(R.id.view);
                    toolBar_Actionbar.setVisibility(View.VISIBLE);


                    //3 oct 2015: indico que se resalte el evento agregado a favoritos
                    miFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView.methodResaltarEventoFavorito(m_Event_Poner_en_favoritos);

                    //boolean para indicar a la act program que vuelva a obtener datos de la DB
                    Class_ConstantsHelper.boolean_Actualiza_Datos_Del_Programa_Con_Favoritos = true;

                    return true;


                case R.id.action_enviar_mail:
                    if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                        Log.d(xxx, xxx +" action_enviar_mail " );
                    }

                    methodFake_Enviar_Mail();
//                    shareCurrentItem();
                    mode.finish(); // Action picked, so close the CAB

                    //Cuando se ejecuta el contextual action bar, se hace visible la toolbar
                    Toolbar toolBar_Actionbar_2 = (Toolbar) findViewById(R.id.view);
                    toolBar_Actionbar_2.setVisibility(View.VISIBLE);

                    return true;



                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
            //Cuando se ejecuta el contextual action bar, se hace visible la toolbar
            Toolbar toolBar_Actionbar_2 = (Toolbar) findViewById(R.id.view);
            toolBar_Actionbar_2.setVisibility(View.VISIBLE);
        }
    };


    ActionMode mActionMode;

//    12 junio 2015 metodo fake para agregar elementos a la lista de favoritos
//    Este metodo no lo uso cuando la api de JL este terminada

    Event m_Event_Poner_en_favoritos;
    private void methodFake_Add_To_List_Of_Favorites() {

        Class_Modelo_De_Datos_Lista_Favoritos m_Class_Modelo_De_Datos_Lista_Favoritos =
                new Class_Modelo_De_Datos_Lista_Favoritos();

        m_Class_Modelo_De_Datos_Lista_Favoritos.setStringTituloDeLaFiesta(Class_ConstantsHelper.titulo_De_La_Fiesta);
        m_Class_Modelo_De_Datos_Lista_Favoritos.setStringFechaDelEvento(Class_ConstantsHelper.fecha_Del_Dia);
        m_Class_Modelo_De_Datos_Lista_Favoritos.setEventEventoFavorito(m_Event_Poner_en_favoritos);
        Class_ConstantsHelper.arraylist_Datos_Lista_Favoritos.add(m_Class_Modelo_De_Datos_Lista_Favoritos);
        //*************************************************************************************************
//        Mostrar el dialogo de has guardado en favoritos
        boolean boolean_evento_guardado = true;

        if (boolean_evento_guardado) {

            // Create an instance of the dialog fragment and show it
            DialogFragment dialog =  ClaseDialogEventoGuardado.newInstance
                    (R.string.titulo_dialog_evento_guardado, R.string.message_dialog_evento_guardado, R.string.positiveButton,
                            R.string.negativeButton, 1, R.string.boton_neutral_evento_guardado);
            dialog.show(getSupportFragmentManager(), "ClaseDialogGenericaDosTresBotones");
        }else{
            DialogFragment dialog =  ClaseDialogEventoGuardado.newInstance
                    (R.string.titulo_dialog_evento_guardado, R.string.message_dialog_evento_guardado_Error, R.string.positiveButton,
                            R.string.negativeButton, 1, R.string.boton_neutral_evento_guardado);
            dialog.show(getSupportFragmentManager(), "ClaseDialogGenericaDosTresBotones");
        }

        //*************************************************************************************************

    }

    private void methodFake_Enviar_Mail() {

//        Class_Modelo_De_Datos_Lista_Favoritos m_Class_Modelo_De_Datos_Lista_Favoritos =
//                new Class_Modelo_De_Datos_Lista_Favoritos();
//
//        m_Class_Modelo_De_Datos_Lista_Favoritos.setStringTituloDeLaFiesta(Class_ConstantsHelper.titulo_De_La_Fiesta);
//        m_Class_Modelo_De_Datos_Lista_Favoritos.setStringFechaDelEvento(Class_ConstantsHelper.fecha_Del_Dia);
//        m_Class_Modelo_De_Datos_Lista_Favoritos.setEventEventoFavorito(m_Event_Poner_en_favoritos);
//        Class_ConstantsHelper.arraylist_Datos_Lista_Favoritos.add(m_Class_Modelo_De_Datos_Lista_Favoritos);




        //*************************************************************************
//        String to = textTo.getText().toString();
//        String subject = textSubject.getText().toString();
//        String message = textMessage.getText().toString();

        String to = "";
        String subject = Class_ConstantsHelper.titulo_De_La_Fiesta;
//        String message = m_Event_Poner_en_favoritos.getSortText();
//         message.app = m_Event_Poner_en_favoritos.getSortText();



        StringBuilder strBuilder = new StringBuilder(m_Event_Poner_en_favoritos.getSortText());
        strBuilder.append("\n");
        strBuilder.append(Class_ConstantsHelper.fecha_Del_Dia);
        strBuilder.append("\n");
        strBuilder.append("Hora del evento: ");

        DateFormat df2 = new SimpleDateFormat("HH:mm");
//        df2.format(m_Event_Poner_en_favoritos.getStartDate());
        strBuilder.append(df2.format(m_Event_Poner_en_favoritos.getStartDate()));


        strBuilder.append("\n");
        strBuilder.append(m_Event_Poner_en_favoritos.getLongText());
        String message = strBuilder.toString();

        //10 julio 2015, original que solo enviaba e-mail
//        Intent intent_email = new Intent(Intent.ACTION_SEND);
//        intent_email.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
//        intent_email.putExtra(Intent.EXTRA_SUBJECT, subject);
//        intent_email.putExtra(Intent.EXTRA_TEXT, message);
//        //need this to prompts email client only
//        intent_email.setType("message/rfc822");
//
//        if (intent_email.resolveActivity(getPackageManager()) != null) {
//            startActivity(Intent.createChooser(intent_email, "Choose an Email client :"));
//        }
        //**************************************************************************************

        //10 julio 2015, primera version para usar facebook, twitter, etc
        Intent intent_Generico = new Intent(Intent.ACTION_SEND);
        intent_Generico.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent_Generico.putExtra(Intent.EXTRA_TEXT, message);
        //need this to prompts email client only
        intent_Generico.setType("text/plain");

        if (intent_Generico.resolveActivity(getPackageManager()) != null) {
            startActivity(Intent.createChooser(intent_Generico, getResources().getString(R.string.selecciona_una_aplicacion)));
        }
    }
    //****************************************************************
    //****************************************************************
    //     FIN de 9 Junio 2015: Codigo relacionado con el contextual action mode para
    //    insetar un evento en la lista de favoritos
    //****************************************************************
    //****************************************************************

    //****************************************************************
    //****************************************************************
    //     11 junio 2015, Instalacion
    //     Inicio de todo lo relacionado con la instalacion
    //****************************************************************
    //****************************************************************
    IConfigurationManager myCmT_Instalacion =null;

    private void method_Gestion_De_Instalacion_de_Pueblos(){

        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"  En metodo method_Gestion_De_Instalacion_de_Pueblos");

        }

        ConfigurationFactory cmTFactory = new ConfigurationFactory();

        try {
            myCmT_Instalacion = cmTFactory.getConfigurationManager(TypeConfiguration.INSTALLATION_DEFAULT_WITH_AUTONOMOUS_COMMUNITY);

            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(xxx, xxx +" Esta bien configurado_2 [" + myCmT_Instalacion.isRightConfigured() + "]");
                Log.d(xxx, xxx +" El cmt esta configurado_2[" + myCmT_Instalacion.isRightConfigured() + "] numero AU["
                        + myCmT_Instalacion.getAutonomousCommunityList().size() + "]");

                for (int i = 0; i < myCmT_Instalacion.getAutonomousCommunityList().size(); i++) {
                    Log.d(xxx, xxx + " :Comunidad " + i + ": "
                            + myCmT_Instalacion.getAutonomousCommunityList().get(i).getAutonomousCommunityName()
                            + "\n");
                    for (int i2 = 0; i2 < myCmT_Instalacion.getAutonomousCommunityList().get(i).getEventPlannerList().size(); i2++) {
                        Log.d(xxx, xxx + " :Pueblo " + i2 + ": "
                                +myCmT_Instalacion.getAutonomousCommunityList().get(i).getEventPlannerList().size()
                                        + "\n"
                                        + myCmT_Instalacion.getAutonomousCommunityList().get(i).getEventPlannerList().get(i2).getName()
                                        + "\n"
                                        + myCmT_Instalacion.getAutonomousCommunityList().get(i).getEventPlannerList().get(i2).getSortDescription()

                        );
                    }

                }
            }




        }
        catch(eefException eef)
        {
            Log.e("onCreate","Error inciando la conficuracion: " +eef.getLocalizedMessage());
        }
        catch (Exception ex){
            Log.e("onCreate","Excepotion no eef inciando la configuracion");
        }

    }//Fin de method_Gestion_De_Instalacion_de_Pueblos


    //****************************************************************
    //****************************************************************
    //     FIN  de Inicio de todo lo relacionado con la instalacion
    //****************************************************************
    //****************************************************************


    //****************************************************************
    //****************************************************************
    //     22 junio 2015, Mostrar eventos del dia con swipe views
    //     Inicio de Mostrar eventos del dia con swipe views
    //****************************************************************
    //****************************************************************

    private void method_Recuperar_Intent() {

        Bundle args_Datos_Para_SwipeView_De_Eventos_Del_Dia = new Bundle();

//        Intent intent = this.getIntent();
        if (this.getIntent() == null){
//            if (intent == null){

                Log.d(xxx, xxx +" Intent es null");

            }else//El intent NO es null, lanzo el adapter de swipe views
            {
                args_Datos_Para_SwipeView_De_Eventos_Del_Dia = this.getIntent().getExtras();

//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
                arrayList_Programa = (ArrayList<Program>)args_Datos_Para_SwipeView_De_Eventos_Del_Dia.getSerializable("Fiesta_Serializada");

                int_Dia_Seleccionado = args_Datos_Para_SwipeView_De_Eventos_Del_Dia.getInt("int_Dia_Seleccionado");
            }



    }//FIN DE method_Recuperar_Intent
ArrayList<Program> arrayList_Programa = new ArrayList<Program>();
int int_Dia_Seleccionado;

    private void method_Generar_SwipeViews_Eventos_Del_Dia() {

        // ViewPager and its adapters use support library
        // fragments, so use getSupportFragmentManager.
        adaptadorDeSwipeViews_Eventos_Del_Dia =
                new AdaptadorDeSwipeViews_Eventos_Del_Dia(getSupportFragmentManager(), arrayList_Programa, int_Dia_Seleccionado);

        mViewPager = (ViewPager) findViewById(R.id.viewpager_1);
        mViewPager.setAdapter(adaptadorDeSwipeViews_Eventos_Del_Dia);


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs_1);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setupWithViewPager(mViewPager);

//        mViewPager.setCurrentItem(int_Dia_Seleccionado);
//        mViewPager.setCurrentItem(int_Dia_Seleccionado, true);

        //Juan 25 agosto, se debe de abrir los eventos en el dia actual cuando se selecciona la fiesta
        mViewPager.setCurrentItem(methodSetCurrentDay());



    }//FIN DE method_Generar_SwipeViews_Eventos_Del_Dia
    AdaptadorDeSwipeViews_Eventos_Del_Dia  adaptadorDeSwipeViews_Eventos_Del_Dia;

    private int methodSetCurrentDay() {

        //Juan 25 agosto, se debe de abrir los eventos en el dia actual cuando se selecciona la fiesta
        SimpleDateFormat sfd = new SimpleDateFormat("E d LLLL yyyy");

        for (int i = 0; i < arrayList_Programa.get(0).getEventByDay().size(); i++) {
            if( methodComparaDias(sfd.format(arrayList_Programa.get(0).getEventByDay().get(i).getProgramEventDate()))) {
                return i;
            }
        }
        return 0;
    }

    private boolean methodComparaDias(String stringDiaConEventos) {
        SimpleDateFormat sfd = new SimpleDateFormat("E d LLLL yyyy");
        String stringFechaActual = sfd.format(new Date());

        if (stringFechaActual.equals(stringDiaConEventos)) {
            return true;
        }else {
            return false;
        }
    }



    //3 oct 2015: nuevo interfaz para gestionar el colorear un evento agregado a favoritos
    MiFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView miFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView;

}//Fin de la clase
