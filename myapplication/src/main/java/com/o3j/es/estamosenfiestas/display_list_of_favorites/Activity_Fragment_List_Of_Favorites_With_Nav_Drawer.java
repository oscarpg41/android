package com.o3j.es.estamosenfiestas.display_list_of_favorites;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.eef.data.dataelements.DayEvents;
import com.eef.data.dataelements.Event;
import com.eef.data.dataelements.Program;
import com.eef.data.eefExceptions.eefException;
import com.eef.data.eeftypes.TypeConfiguration;
import com.eef.data.eeftypes.TypeState;
import com.eef.data.managers.IConfigurationManager;
import com.eef.data.managers.impl.ProgamListManager;
import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper;
import com.o3j.es.estamosenfiestas.dialogs.ClaseDialogEventoGuardado;
import com.o3j.es.estamosenfiestas.display_frame_layout.Class_Modelo_De_Datos_Lista_Favoritos;
import com.o3j.es.estamosenfiestas.display_frame_layout.CustomAdapter_RecyclerView_Evento_Detalle;
import com.o3j.es.estamosenfiestas.display_frame_layout.CustomAdapter_RecyclerView_Fiesta_List_Por_Dias;
import com.o3j.es.estamosenfiestas.display_frame_layout.CustomAdapter_RecyclerView_Fiestas;
import com.o3j.es.estamosenfiestas.display_frame_layout.CustomAdapter_RecyclerView_List_De_Eventos;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Evento;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Fiesta_Seleccionada_Con_RecycleView;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Fiestas_Con_RecycleView;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView;
import com.o3j.es.estamosenfiestas.display_lists_package.AdaptadorDeSwipeViews_2;
import com.o3j.es.estamosenfiestas.factory.impl.ConfigurationFactory;
import com.o3j.es.estamosenfiestas.nav_drawer_utilities.ClassMenuItemsNavigationDrawer;
import com.o3j.es.estamosenfiestas.nav_drawer_utilities.CustomAdapter_NavigationDrawer_2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import managers.DatabaseServerManager;


// Juan 12 Junio 2015: Esta clase usa frame layout para ir poniendo y quitando los fragments con la info de los eventos
// FAVORITOS usando listas con este orden y sin salirse de esta actividad:
// 1: Se muestra la lista con todas los eventos marcados como favoritos
// 2: Se muestra el detalle del evento seleccionado (Se puede borrar el evento)

public class Activity_Fragment_List_Of_Favorites_With_Nav_Drawer extends ActionBarActivity
        implements CustomAdapter_RecyclerView_Fiestas.OnItemClickListenerFiestaSeleccionada,
        CustomAdapter_RecyclerView_Fiesta_List_Por_Dias.OnItemClickListenerDiaSeleccionado,
        CustomAdapter_RecyclerView_List_De_Eventos.OnItemClickListenerEventoSeleccionado,
        CustomAdapter_RecyclerView_Evento_Detalle.OnItemClickListenerEventoDetalle,
        CustomAdapter_NavigationDrawer_2.OnItemClickListenerElementoDeMenuSeccionado,
        CustomAdapter_RecyclerView_List_De_Favoritos.OnItemClickListenerEventoFavoritoSeleccionado,
        CustomAdapter_RecyclerView_Evento_Favorito_Detalle.OnItemClickListenerEventoFavoritoDetalle,
        MiFragment_Fiestas_Con_RecycleView.OnFragmentInteractionListener,
        MiFragment_Fiesta_Seleccionada_Con_RecycleView.OnFragmentInteractionListener,
        MiFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView.OnFragmentInteractionListener,
        MiFragment_Evento.OnFragmentInteractionListener,
        MiFragment_Lista_De_Favoritos_Con_RecycleView.OnFragmentInteractionListener,
        MiFragment_Evento_Favorito_Con_RecycleView.OnFragmentInteractionListener,
        ClaseDialogEventoGuardado.InterfaceClaseDialogEventoGuardado{
    //Original con fragment sin loader
//    public class Activity_Fragment_Base_2 extends ActionBarActivity implements MiFragment_1.OnFragmentInteractionListener {
    AdaptadorDeSwipeViews_2 adaptadorDeSwipeViews;
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        xxx = this.getClass().getSimpleName();

//        Nota: Juan 2 junio: para que funcione el view pager y los tabs tengo que usar el layout activity_fragment_lists_with_nav_drawer_3
//        setContentView(R.layout.activity_fragment_lists_with_nav_drawer_3);


//        Este layout funciona pero al tener el drawer como raiz, tapa la toolbar
        setContentView(R.layout.activity_fragment_frame_layout_with_nav_drawer_3);


//        4 junio 2015, Uso este layout para que el navigation drawer no tape la roolbar
//        setContentView(R.layout.activity_fragment_list_of_favorites);


//        Toolbar toolBar_Actionbar = (Toolbar) findViewById (R.id.activity_my_toolbar);
        Toolbar toolBar_Actionbar = (Toolbar) findViewById(R.id.view);
        //Toolbar will now take on default Action Bar characteristics

//        Le cambio el titulo por el ayuntamiento en method_Data_To_Show_In_Fragments_2
//        Para cambiar el tama�o del titulo lo hice con styles como en
//        http://stackoverflow.com/questions/28487312/how-to-change-the-toolbar-text-size-in-android


//        Tengo que poner este titulo para que aparezca el ayuntamiento en method_Data_To_Show_In_Fragments_2
        toolBar_Actionbar.setTitle(getResources().getString(R.string.pantalla_de_favoritos));
//        toolBar_Actionbar.setTitle("acénto");

//        No uso el subtitulo
//        toolBar_Actionbar.setSubtitle(getResources().getString(R.string.sub_titulo_4));


        setSupportActionBar(toolBar_Actionbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Toolbar bottom_toolbar = (Toolbar) findViewById(R.id.bottom_toolbar);
        bottom_toolbar.setLogo(R.drawable.ic_launcher);
        bottom_toolbar.setTitle(getResources().getString(R.string.texto_1));
        bottom_toolbar.setSubtitle(getResources().getString(R.string.texto_2));
        bottom_toolbar.setNavigationIcon(R.drawable.ic_launcher);
        //Toolbar que pongo abajo

        // Set an OnMenuItemClickListener to handle menu item clicks
        bottom_toolbar.setOnMenuItemClickListener(
                new Toolbar.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // Handle the menu item

                        int id = item.getItemId();

                        //noinspection SimplifiableIfStatement
                        if (id == R.id.action_settings) {
                            Toast.makeText(Activity_Fragment_List_Of_Favorites_With_Nav_Drawer.this, "Bottom toolbar pressed: ", Toast.LENGTH_LONG).show();
                            return true;
                        }
                        return true;
                    }
                });


        bottom_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Activity_Fragment_List_Of_Favorites_With_Nav_Drawer.this, "Navigation Icon pressed: ", Toast.LENGTH_LONG).show();
                // Para esta practica, al presionar el navigation icon lo quito.
                bottom_toolbar.setVisibility(View.GONE);
            }
        });

        // Inflate a menu to be displayed in the toolbar
        bottom_toolbar.inflateMenu(R.menu.menu_main);



        //Por ahora utilizo este metodo para jugar con el API de Jose Luis, 21 Mayo 2015
//        method_Data_To_Show_In_Fragments();

        //4 Junio 2015,
        method_Data_To_Show_In_Fragments_2();

//        method_Gestion_De_Instalacion_de_Pueblos();

        //****************************************************************
        //   Juan, 2 junio 2015: No uso los swipe views, quedan comentados
        //****************************************************************
        // ViewPager and its adapters use support library
        // fragments, so use getSupportFragmentManager.
//        adaptadorDeSwipeViews =
//                new AdaptadorDeSwipeViews_2(
//                        getSupportFragmentManager(), int_Number_Of_Fragments_To_Show, List_Programs);
//        mViewPager = (ViewPager) findViewById(R.id.viewpager_1);
//        mViewPager.setAdapter(adaptadorDeSwipeViews);
//
//        //Poner tabs del swipe view
//        metodoPonerTabs_1(mViewPager);
        //****************************************************************
        //   FIN de Juan, 2 junio 2015: No uso los swipe views, quedan comentados
        //****************************************************************



        //Inicializar el nav drawer, 21 Mayo 2015
        method_Init_NavigationDrawer();





    }//Fin del onCreate


//    Juan 2 junio 2015, no uso el view pager ni los tabs, dejo el metodo metodoPonerTabs_1 comentado

//    private void metodoPonerTabs_1(ViewPager mViewPager) {
//
////        Hecho como en: http://guides.codepath.com/android/Google-Play-Style-Tabs-using-SlidingTabLayout
//// Give the SlidingTabLayout the ViewPager
//        SlidingTabLayout slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
//        // Center the tabs in the layout
//        slidingTabLayout.setDistributeEvenly(true);
//        slidingTabLayout.setViewPager(mViewPager);
//        //Colorear el tab
//        slidingTabLayout.setBackgroundColor(getResources().getColor(R.color.primary_color));
//        // Customize tab indicator color
//        slidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
//            @Override
//            public int getIndicatorColor(int position) {
//                return getResources().getColor(R.color.accent_color);
//            }
//        });
//    }

    //**************************************************************************
    //**************************************************************************
    //  24 junio 2015: Setting correcto para no mostrar menu en la toolbar
    //  24 junio 2015: Solo se muestra el Nav Drawer
    //**************************************************************************
    //**************************************************************************


//    Me ayude con:
//    https://github.com/codepath/android_guides/wiki/Fragment-Navigation-Drawer

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;

        // Uncomment to inflate menu items to Action Bar
        // inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = drawerLayout.isDrawerOpen(navigationDrawerRecyclerView);

        //No he inflado el menu, lo dejo comentado
//        menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }// Fin de onPrepareOptionsMenu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this. 21 Mayo 2015
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }//Codigo del nav drawer


        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        if (id == R.id.action_settings) {
//            Toast.makeText(Activity_Fragment_FrameLayout_WithNavDrawer_2.this, " toolbar pressed: ", Toast.LENGTH_LONG).show();
//
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }
    //**************************************************************************
    //**************************************************************************
    //  FIN DE 24 junio 2015: Setting correcto para no mostrar menu en la toolbar
    //  24 junio 2015: Solo se muestra el Nav Drawer
    //**************************************************************************
    //**************************************************************************

    //Implementa la interfaz de la clase MiFragment_Fiestas_Con_RecycleView
//    y la clase MiFragment_Fiesta_Seleccionada_Con_RecycleView

//    OJO: NO ESTOY HACIENDO USO DE ESTA INTERFAZ
    public void onFragmentInteraction(String string) {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"Fragmento activo: " +string);

        }
    }




    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //  Inicio de Codigo relacionado con el navigation drawer, 21 Mayo 2015
    //****************************************************************
    //****************************************************************
    private void method_Init_NavigationDrawer() {

        //****************************************************************
        //   Codigo relacionado con recycler view del navigation drawer
        //****************************************************************


        //Inicializar los datos del array del menu a mostrar en el navigationDrawer
        init_arrayList_NavigationDrawer();

        navigationDrawerRecyclerView = (RecyclerView) findViewById(R.id.left_drawer_recycle_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        navigationDrawerRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        navigationDrawerLayoutManager = new LinearLayoutManager(this);
        navigationDrawerRecyclerView.setLayoutManager(navigationDrawerLayoutManager);
//        navigationDrawerRecyclerView.setAdapter(new CustomAdapter_NavigationDrawer(

//        Adapter del navigation drawer con la cabecera
        navigationDrawerRecyclerView.setAdapter(new CustomAdapter_NavigationDrawer_2(
                arrayListItemsNavigationDrawer, "0",
                (CustomAdapter_NavigationDrawer_2.OnItemClickListenerElementoDeMenuSeccionado) this));

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_1);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        //Este objeto permite que el navigation Drawer interactue correctamente con el action bar (la toolbar en mi caso)
        //Tengo que usar el actionBarDrawerToggle de V7 support por que el de V4 esta deprecated
        actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                drawerLayout,         /* DrawerLayout object */
                //Deprecated en V7, esto era valido en V4
//                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
//                getSupportActionBar().setTitle(getResources().getString(R.string.title_activity_main_activity_navigation_drawer));
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()

            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                getActionBar().setTitle(mDrawerTitle);
//                getSupportActionBar().setTitle(getTitle());
//                getSupportActionBar().setTitle(getResources().getString(R.string.titulo_navigation_drawer));
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()


            }
        };//Fin de actionBarDrawerToggle

        // Set the drawer toggle as the DrawerListener
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //****************************************************************
        //  FIN Codigo relacionado con recycler view del navigation drawer
        //****************************************************************
    }//Fin de method_Init_NavigationDrawer





    //Datos del menu del navigation drawer
    private void init_arrayList_NavigationDrawer() {//Relacionado con recycler view
        String[] array_menu = {getString(R.string.menu_1),getString(R.string.menu_2),getString(R.string.menu_3)
                ,getString(R.string.menu_4),getString(R.string.menu_6),getString(R.string.menu_7), getString(R.string.menu_5)};

        arrayListItemsNavigationDrawer = new ArrayList<ClassMenuItemsNavigationDrawer>();
        for (int i = 0; i < DATASET_COUNT; i++) {
            arrayListItemsNavigationDrawer.add(
                    ClassMenuItemsNavigationDrawer.newInstance(array_menu[i],
                            " ",
                            R.drawable.ic_launcher));
        }
    }



    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    //Si estos metodos no se llaman, no aparece la hamburguesa
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    //Variables para el recycler view
    private DrawerLayout drawerLayout;
    private RecyclerView navigationDrawerRecyclerView;
    private RecyclerView.Adapter navigationDrawerAdapter;
    private RecyclerView.LayoutManager navigationDrawerLayoutManager;
    protected ArrayList<ClassMenuItemsNavigationDrawer> arrayListItemsNavigationDrawer = null;
    private static final int DATASET_COUNT = 7;
    //Este objeto permite que el navigation Drawer interactue correctamente con el action bar (la toolbar en mi caso)
    private ActionBarDrawerToggle actionBarDrawerToggle;
    //FIN Variables para el recycler view

    //****************************************************************
    //****************************************************************
    //     FIN del Codigo relacionado con el navigation drawer
    //****************************************************************
    //****************************************************************







    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //  Inicio de Codigo relacionado con el API de JL, 21 Mayo 2015
    //****************************************************************
    //****************************************************************
    private void method_Data_To_Show_In_Fragments() {
//        ESte metodo obtiene datos del api de JL, pero habra que cambiar cosas.

        ConfigurationFactory cmTFactory = new ConfigurationFactory();

        try {
            myCmT = cmTFactory.getConfigurationManager(TypeConfiguration.INSTALLATION_DEFAULT_WITH_AUTONOMOUS_COMMUNITY);

            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(xxx, xxx +" Esta bien configurado [" + myCmT.isRightConfigured() + "]");
                Log.d(xxx, xxx +" El cmt esta configurado[" + myCmT.isRightConfigured() + "] numero AU[" + myCmT.getAutonomousCommunityList().size() + "]");
                Log.d(xxx, xxx +" La comunidad Autonoma Configurada[" + myCmT.getAutonomousCommunityConfigured().getAutonomousCommunityName()
                        + "] y el pueblo configurado[" + myCmT.getEventPlannerConfigured().getName() + "]");
            }

            ProgamListManager pmT = new ProgamListManager();
            pmT.loadData(myCmT.getAutonomousCommunityConfigured(),myCmT.getEventPlannerConfigured());
            if(pmT.existCurrentProgram()){
                Program p = pmT.getCurrentProgram();

                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(xxx, xxx +"HAY PROGRAMA EN MARCHA ["+p.getProgramSortDescription()+"]");

                }
                //Obtener la lista de eventos del programa por dias
                List<DayEvents> ltd = p.getEventByDay();
                //Obtener la lista de eventos completa
                List<Event> ltf =p.getFullList();
            }



            if(pmT.existCurrentProgram() || pmT.existFuturetPrograms() || pmT.existPastPrograms()) {
                 List_Programs = pmT.getFullProgramList();


                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(xxx, "HAY  [" + List_Programs.size() + "]" +" PROGRAMAS EN MARCHA");
                    for (int i=0; i<List_Programs.size(); i++){
                        Log.d(xxx, xxx +" Codigo del programa numero  [" +i + ": "
                                +List_Programs.get(i).getState().getCode() + "]" +" PROGRAMAS EN MARCHA");

                        Log.d(xxx, xxx +" Codigo del programa numero  [" +i + ": "
                                +List_Programs.get(i).getProgramSortDescription() + "]" +" PROGRAMAS EN MARCHA");

                        if(List_Programs.get(i).getState().getCode().equals(TypeState.ENDED)){
                            int_Number_Of_Fragments_To_Show++;
                            List_Programs.remove(i);
                        }

                    }
                }



                for (int i=0; i<List_Programs.size(); i++){
                    arrayList_Lista_De_Fiesta_Serializable.add(List_Programs.get(i));
                }
                method_Muestra_Fragment_Con_La_Lista_De_Las_Fiestas(int_Nivel_De_Info_Fiestas);


            }



//            Juan, 10 junio 2015, VER ESTA FORMA DE PASAR PARAMETROS CON PUNTOS SUSPENSIVOS Y EL FOR CON LOS : PUNTOS

//            private static void toggleVisibility(View... views) {
//                for (View view : views) {
//                    boolean isVisible = view.getVisibility() == View.VISIBLE;
//                    view.setVisibility(isVisible ? View.INVISIBLE : View.VISIBLE);
//                }
//            }

        }
        catch(eefException eef)
        {
            Log.e("onCreate","Error inciando la conficuracion");
        }
        catch (Exception ex){
            Log.e("onCreate","Excepotion no eef inciando la configuracion");
        }


    }//Fin de method_Data_To_Show_In_Fragments


    IConfigurationManager myCmT=null;
    private static String xxx;
    List<Program> List_Programs;
    int int_Number_Of_Fragments_To_Show = 0;
    ArrayList<Program> arrayList_Lista_De_Fiesta_Serializable = new ArrayList<Program>();

    int int_Nivel_De_Info_Fiestas = 1;
    int int_Nivel_De_Info_Fiesta_Por_Dias = 2;
    int int_Nivel_De_Info_Fiesta_Eventos_Del_Dia = 3;
    int int_Nivel_De_Info_Fiesta_Evento_Detalle = 4;


    //****************************************************************
    //****************************************************************
    //     FIN del Codigo relacionado con el API de JL
    //****************************************************************
    //****************************************************************

    public void method_Muestra_Fragment_Con_La_Lista_De_Las_Fiestas(int int_Nivel_De_Info) {
// Create new fragment and transaction


        Fragment newFragment = getNewFragmnet(int_Nivel_De_Info);
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack

        if (int_Nivel_De_Info == int_Nivel_De_Info_Fiestas) {
            transaction.add(R.id.details, newFragment);
        }
        else{
            transaction.replace(R.id.details, newFragment);
            transaction.addToBackStack(null);
        }

// Commit the transaction
        transaction.commit();
    }// Fin de method_Muestra_Fragment_Con_La_Lista_De_Las_Fiestas

    public Fragment getNewFragmnet(int i) {
        Fragment fragment = new MiFragment_Fiestas_Con_RecycleView();
        Bundle args = new Bundle();
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
        args.putString(MiFragment_Fiestas_Con_RecycleView.ARG_OBJECT, "fragment#" +(i + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiestas_Con_RecycleView.ARG_PARAM2, String.valueOf(i + 1));

        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiestas_Con_RecycleView.TIPO_DE_LISTA, i);


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
        args.putSerializable("Lista de Programas Serializado", arrayList_Lista_De_Fiesta_Serializable);


        fragment.setArguments(args);

        return fragment;
    }

    //    Interfaz de la clase CustomAdapter_RecyclerView_Fiestas.
    @Override
    public void onClick(Program m_Programa_seleccionado) {
        Fragment newFragment = getNewFragmentForFiesta(m_Programa_seleccionado);
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack

        transaction.replace(R.id.details, newFragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();

    }

    public Fragment getNewFragmentForFiesta(Program m_Programa_seleccionado) {
        Fragment fragment = new MiFragment_Fiesta_Seleccionada_Con_RecycleView();
        Bundle args = new Bundle();
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "fragment#" +(int_Nivel_De_Info_Fiesta_Por_Dias + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, String.valueOf(int_Nivel_De_Info_Fiesta_Por_Dias + 1));

        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, int_Nivel_De_Info_Fiesta_Por_Dias);


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
        args.putSerializable("Programa Serializado", m_Programa_seleccionado);


        fragment.setArguments(args);

        return fragment;
    }


//    Interfaz de la clase CustomAdapter_RecyclerView_Fiesta_List_Por_Dias.
    @Override
    public void onClickDia(DayEvents m_DayEvents) {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"He llegado a onClickDia" );
        }
        Fragment newFragment = getNewFragmentForDayEvents(m_DayEvents);
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack

        transaction.replace(R.id.details, newFragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }

    public Fragment getNewFragmentForDayEvents(DayEvents m_DayEvents) {
        Fragment fragment = new MiFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView();
        Bundle args = new Bundle();
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "fragment#" +(int_Nivel_De_Info_Fiesta_Eventos_Del_Dia + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, String.valueOf(int_Nivel_De_Info_Fiesta_Eventos_Del_Dia + 1));

        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, int_Nivel_De_Info_Fiesta_Eventos_Del_Dia);

//        Hago esto por que la lista de eventos no es serializable, asi que lo pongo en array para
//                pasarlo al fragment con la lista de los eventos del dia seleccionado
        ArrayList<Event> arrayList_Lista_De_Eventos_Del_Dia = new ArrayList<Event>();
        for (int i=0; i<m_DayEvents.getListaEventos().size(); i++){
            arrayList_Lista_De_Eventos_Del_Dia.add(m_DayEvents.getListaEventos().get(i));
        }


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
        args.putSerializable("Eventos del dia Serializado", arrayList_Lista_De_Eventos_Del_Dia);


        fragment.setArguments(args);

        return fragment;
    }

    //    Interfaz de la clase CustomAdapter_RecyclerView_List_De_Eventos.
    @Override
    public void onClickEvento(Event m_Event) {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"He llegado a onClickEvento" );
        }
        Fragment newFragment = getNewFragmentToShowEvent(m_Event);
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack

        transaction.replace(R.id.details, newFragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }

    public Fragment getNewFragmentToShowEvent(Event m_Event) {
        Fragment fragment = new MiFragment_Evento();
        Bundle args = new Bundle();
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "fragment#" + (int_Nivel_De_Info_Fiesta_Evento_Detalle + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, String.valueOf(int_Nivel_De_Info_Fiesta_Evento_Detalle + 1));
        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, int_Nivel_De_Info_Fiesta_Evento_Detalle);

        args.putSerializable("Datos del Evento Serializado", m_Event);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onClickEventoDetalle(Event m_Event) {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"He llegado a onClickEventoDetalle" );
        }
    }



    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //  13 junio 2015: Manejo de fragmentos de favoritos
    //****************************************************************
    //****************************************************************
    private void method_Data_To_Show_In_Fragments_2() {

//        13 junio 2015: Se chequea si hay elementos a mostrar en la lista de favoritos
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(xxx, xxx +"en metodo  method_Data_To_Show_In_Fragments_2");
        }

//        if (Class_ConstantsHelper.arraylist_Datos_Lista_Favoritos.size() != 0) {
//            method_ShowFragmentWithListOfFavoritesFAKE();
//        }else{
////            Si no hay favoritos, POR ahora termino la actividad y vuelvo a la actividad llamante
////            finish();
//            method_ShowFragmentWithListOfFavoritesFAKE();
//
//        }

        //19 agosto 2015: metodo fake para recuperar los favoritos
//        method_ShowFragmentWithListOfFavoritesFAKE();
        //19 agosto 2015: metodo definitivo para recuperar favoritos
        method_ShowFragmentWithListOfFavorites();
    }//Fin de method_Data_To_Show_In_Fragments_2

    public void method_ShowFragmentWithListOfFavorites() {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, xxx +" en metodo method_ShowFragmentWithListOfFavorites" );
        }
//        Fragment newFragment = getNewFragmentToShowListOfFavoritesFAKE();
        MiFragment_Lista_De_Favoritos_Con_RecycleView newFragment =
                (MiFragment_Lista_De_Favoritos_Con_RecycleView) getNewFragmentToShowListOfFavorites();

//        Uso la instancia m_MiFragment_Lista_De_Favoritos_Con_RecycleView para borrar de la lista de favoritos con el longClick,
        //Pero antes, debo borrar de la base datos.
        m_MiFragment_Lista_De_Favoritos_Con_RecycleView = newFragment;

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack
        transaction.add(R.id.details, newFragment, "list_favourites_Tag");
        // Commit the transaction
        transaction.commit();

    }// Fin de method_ShowFragmentWithListOfFavorites

    ArrayList<Class_Modelo_De_Datos_Lista_Favoritos> arraylist_Datos_Lista_Favoritos =
            new ArrayList<Class_Modelo_De_Datos_Lista_Favoritos>();
    public Fragment getNewFragmentToShowListOfFavorites() {

        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, xxx +" en metodo getNewFragmentToShowListOfFavorites" );
        }

        List<Program> List_Programs = null;
        //**********************************************************************
        //19 agosto 2015: recupero los datos de favoritos de sqlite
        DatabaseServerManager databaseServerManager = new DatabaseServerManager();
        databaseServerManager.initManager(this);
        try {
             List_Programs = databaseServerManager.getFullProgramListOnlyFavoritos();
        } catch (eefException eef) {
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(xxx, xxx +" en metodo getNewFragmentToShowListOfFavorites Exception: " +eef.getLocalizedMessage());
            }
//            Toast.makeText(this, "Error recuperando lista de favoritos", Toast.LENGTH_LONG).show();
        }
        //************************************************************************

        //Sigo el patron original, pasando un array list: arraylist_Datos_Lista_Favoritos al fragment
        DateFormat df2 = new SimpleDateFormat("HH:mm");

        arraylist_Datos_Lista_Favoritos =
                new ArrayList<Class_Modelo_De_Datos_Lista_Favoritos>();

//        Class_Modelo_De_Datos_Lista_Favoritos class_modelo_de_datos_lista_favoritos = new Class_Modelo_De_Datos_Lista_Favoritos();
        //relleno el array list:

        if(List_Programs != null) {
            for (int i = 0; i < List_Programs.size(); i++) {
                for (int i2 = 0; i2 < List_Programs.get(i).getFullList().size(); i2++) {


                    Class_Modelo_De_Datos_Lista_Favoritos class_modelo_de_datos_lista_favoritos = new Class_Modelo_De_Datos_Lista_Favoritos();

                    class_modelo_de_datos_lista_favoritos.
                            setEventEventoFavorito(List_Programs.get(i).getFullList().get(i2));
                    class_modelo_de_datos_lista_favoritos.
                            setStringFechaDelEvento(df2.format(List_Programs.get(i).getFullList().get(i2).getStartDate()));
                    class_modelo_de_datos_lista_favoritos.
                            setStringTituloDeLaFiesta(List_Programs.get(i).getProgramSortDescription());
                    arraylist_Datos_Lista_Favoritos.add(class_modelo_de_datos_lista_favoritos);


                }
            }
        }else{
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(xxx, xxx +" en metodo getNewFragmentToShowListOfFavorites List_Programs == null");
            }
        }

        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, xxx +" en metodo getNewFragmentToShowListOfFavorites DATOS" );

            for (int i=0; i<arraylist_Datos_Lista_Favoritos.size(); i++){
                Log.e(xxx, xxx + " en metodo getNewFragmentToShowListOfFavorites DATOS" +
                        arraylist_Datos_Lista_Favoritos.get(i).getStringFechaDelEvento() +
                        arraylist_Datos_Lista_Favoritos.get(i).getStringTituloDeLaFiesta() +
                        arraylist_Datos_Lista_Favoritos.get(i).getEventEventoFavorito().getSortText());
            }



        }


        Fragment fragment = new MiFragment_Lista_De_Favoritos_Con_RecycleView();
        Bundle args = new Bundle();
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "No lo uso");
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, "1");
        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, 1);

//        args.putSerializable("Datos de lista de favoritos", Class_ConstantsHelper.arraylist_Datos_Lista_Favoritos);
        args.putSerializable("Datos de lista de favoritos", arraylist_Datos_Lista_Favoritos);

        fragment.setArguments(args);

        return fragment;
    }//Fin de getNewFragmentToShowListOfFavorites

    MiFragment_Lista_De_Favoritos_Con_RecycleView m_MiFragment_Lista_De_Favoritos_Con_RecycleView;

    public void method_ShowFragmentWithListOfFavoritesFAKE() {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +" He llegado a method_ShowFragmentWithListOfFavoritesFAKE" );
        }
//        Fragment newFragment = getNewFragmentToShowListOfFavoritesFAKE();
        MiFragment_Lista_De_Favoritos_Con_RecycleView newFragment =
                (MiFragment_Lista_De_Favoritos_Con_RecycleView) getNewFragmentToShowListOfFavoritesFAKE();

//        Uso m_MiFragment_Lista_De_Favoritos_Con_RecycleView para borrar de la lista de favoritos con el longClick
        m_MiFragment_Lista_De_Favoritos_Con_RecycleView = newFragment;

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack


//        Si pongo replace, y le doy al boton back, hace una cosa rara, se queda con un fragment vacio
//        transaction.replace(R.id.details, newFragment);
//        transaction.addToBackStack(null);

//        transaction.add(R.id.details, newFragment);


        transaction.add(R.id.details, newFragment, "list_favourites_Tag");

        // Commit the transaction
        transaction.commit();


    }// Fin de method_ShowFragmentWithListOfFavoritesFAKE

    public Fragment getNewFragmentToShowListOfFavoritesFAKE() {
        Fragment fragment = new MiFragment_Lista_De_Favoritos_Con_RecycleView();
        Bundle args = new Bundle();
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "No lo uso");
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, "1");
        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, 1);

        args.putSerializable("Datos de lista de favoritos", Class_ConstantsHelper.arraylist_Datos_Lista_Favoritos);

        fragment.setArguments(args);

        return fragment;
    }//Fin de getNewFragmentToShowListOfFavoritesFAKE

    @Override
    public void onClickEventoFavorito(int m_position) {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +" He llegado a onClickEventoFavorito" );
        }
//        Mostrar el evento favorito seleccionado
//        method_ShowFragmentWithEventoFavoritoFAKE(m_position);
        method_ShowFragmentWithEventoFavorito(m_position);
    }

    public void method_ShowFragmentWithEventoFavorito(int int_Evento) {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"He llegado a method_ShowFragmentWithEventoFavoritoFAKE" );
        }
        Fragment newFragment = getNewFragmentToShowEventoFavorito(int_Evento);
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.replace(R.id.details, newFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();

    }// Fin de method_ShowFragmentWithEventoFavorito

    public Fragment getNewFragmentToShowEventoFavorito(int int_Evento) {
        Fragment fragment = new MiFragment_Evento_Favorito_Con_RecycleView();
        Bundle args = new Bundle();
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "No lo uso");
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, "1");
        //Le paso el evento clickado
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, int_Evento);

        args.putSerializable("Datos de lista de favoritos",arraylist_Datos_Lista_Favoritos );

        fragment.setArguments(args);

        return fragment;
    }//Fin de getNewFragmentToShowEventoFavorito

    int m_position_favorito_a_borrar;


    //Este on long click es para borrar un evento de la lista de favoritos
    @Override
    public boolean onLongClickEventoFavorito(int m_position_favorito_a_borrar, View m_View_Del_Evento_Favorito) {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +" He llegado a onLongClickEventoFavorito" );
        }

        //Cuando aparece el contextual action bar, oculto la tool bar con Invisible para que haga el efecto de movimiento
        Toolbar toolBar_Actionbar = (Toolbar) findViewById(R.id.view);
        toolBar_Actionbar.setVisibility(View.GONE);

        this.m_position_favorito_a_borrar = m_position_favorito_a_borrar;

        if (mActionMode != null) {
            return false;
        }

        // Start the CAB using the ActionMode.Callback defined above
        mActionMode = this.startActionMode(mActionModeCallback);
        m_View_Del_Evento_Favorito.setSelected(true);


        return true;
    }

    public void method_ShowFragmentWithEventoFavoritoFAKE(int int_Evento) {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"He llegado a method_ShowFragmentWithEventoFavoritoFAKE" );
        }
        Fragment newFragment = getNewFragmentToShowEventoFavoritoFAKE(int_Evento);
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.replace(R.id.details, newFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();

    }// Fin de method_ShowFragmentWithEventoFavoritoFAKE

    public Fragment getNewFragmentToShowEventoFavoritoFAKE(int int_Evento) {
        Fragment fragment = new MiFragment_Evento_Favorito_Con_RecycleView();
        Bundle args = new Bundle();
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "No lo uso");
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, "1");
        //Le paso el evento clickado
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, int_Evento);

        args.putSerializable("Datos de lista de favoritos", Class_ConstantsHelper.arraylist_Datos_Lista_Favoritos);

        fragment.setArguments(args);

        return fragment;
    }//Fin de getNewFragmentToShowEventoFavoritoFAKE

    @Override
    public void onClickEventoFavoritoDetalle(Event m_Event) {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +" He llegado a onClickEventoFavoritoDetalle" );
        }
    }


    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //  FIN de Manejo de fragmentos de favoritos
    //****************************************************************
    //****************************************************************





    @Override
    public void onClickMenuItenDelNavDrawer(int position_Del_Menu_Seleccionada) {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"He llegado a onClickMenuItenDelNavDrawer  con click: " +position_Del_Menu_Seleccionada );
        }

//        Intent intent = new Intent(this, com.o3j.es.estamosenfiestas.activity_actualizar_fiestas.
//                Activity_Fragment_Actualizar_Fiesta_WithNavDrawer_2.class);
//        startActivity(intent);


        Intent intent;


        switch (position_Del_Menu_Seleccionada)
        {
            case 0:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_frame_layout.
                        Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 1://Mis Eventos
//                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_list_of_favorites.
//                        Activity_Fragment_List_Of_Favorites_With_Nav_Drawer.class);
//                startActivity(intent);

                drawerLayout.closeDrawer(Gravity.LEFT);

                break;
            case 2:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.activity_actualizar_fiestas.
                        Activity_Fragment_Actualizar_Fiesta_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 3:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.application_configuration.
                        Activity_Application_Configuration_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 4:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_event_planner_information.
                        Activity_Display_Event_Planner_Information_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 5:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.activity_compartir.
                        Activity_Fragment_Compartir_App_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 6:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_informacion.
                        Activity_Display_Developer_Information_WithNavDrawer_2.class);
                startActivity(intent);
                break;
        }

        if (position_Del_Menu_Seleccionada != 1) finish();


//        if(position_Del_Menu_Seleccionada == 3 || position_Del_Menu_Seleccionada == 2){


//          if(position_Del_Menu_Seleccionada == 2){
//
//            //Solo muestra el toast
//            Toast.makeText(this, "Opción no disponible", Toast.LENGTH_LONG).show();
//        }else{
//            if (position_Del_Menu_Seleccionada != 1) finish();
//        }
    }




    //****************************************************************
    //****************************************************************
    //     9 Junio 2015: Codigo relacionado con el contextual action mode para
    //    insetar un evento en la lista de favoritos.
    //    Este interfaz es de la clase: CustomAdapter_RecyclerView_List_De_Eventos
    //    Al hacer long click en un evento
    //****************************************************************
    //****************************************************************
    @Override
    public boolean onLongClickEvento_CustomA_List_De_Eventos(Event m_Event, View m_View_Del_Evento) {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +" He llegado a onLongClickEvento_CustomA_List_De_Eventos  con click: " );
        }


        m_Event_Poner_en_favoritos = m_Event;

        if (mActionMode != null) {
            return false;
        }

        // Start the CAB using the ActionMode.Callback defined above
        mActionMode = this.startActionMode(mActionModeCallback);
        m_View_Del_Evento.setSelected(true);


        return true;
    }




    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_contextual_action_mode_lista_favoritos, menu);
            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_borrar_de_favoritos:
                    if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                        Log.d(xxx, xxx +" action_borrar_favorito " );
                    }

//                    methodFake_Add_To_List_Of_Favorites();
//                    methodFake_Remove_From_List_Of_Favorites();

                    //Juan 19 agosto 2015, metodo definitivo para borrar.
                    method_Remove_From_List_Of_Favorites();
//                    shareCurrentItem();
                    mode.finish(); // Action picked, so close the CAB

                    //Cuando se ejecuta el contextual action bar, se hace visible la toolbar
                    Toolbar toolBar_Actionbar = (Toolbar) findViewById(R.id.view);
                    toolBar_Actionbar.setVisibility(View.VISIBLE);

                    return true;
                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
            //Cuando se ejecuta el contextual action bar, se hace visible la toolbar
            Toolbar toolBar_Actionbar_2 = (Toolbar) findViewById(R.id.view);
            toolBar_Actionbar_2.setVisibility(View.VISIBLE);
        }
    };


    ActionMode mActionMode;



    //Juan 19 agosto 2015, metodo definitivo para borrar.

    private void method_Remove_From_List_Of_Favorites() {

        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, xxx +" En metodo method_Remove_From_List_Of_Favorites " );
        }


        //Borrar de la pantalla de lista de favoritos
//        m_MiFragment_Lista_De_Favoritos_Con_RecycleView.pruebaDeBorrar(m_position_favorito_a_borrar);


        //19 agosto 2015: Uso la clase Clase_borraEventoComoFavorito que tiene el metodo method_Add_To_List_Of_Favorites
        Clase_borraEventoComoFavorito clase_borraEventoComoFavorito =
                Clase_borraEventoComoFavorito.newInstance(arraylist_Datos_Lista_Favoritos.
                                get(m_position_favorito_a_borrar).getEventEventoFavorito(),
                        Activity_Fragment_List_Of_Favorites_With_Nav_Drawer.this,
                        m_MiFragment_Lista_De_Favoritos_Con_RecycleView,
                        m_position_favorito_a_borrar);

        clase_borraEventoComoFavorito.method_borraEventoComoFavorito();
    }//Fin de method_Remove_From_List_Of_Favorites

//    13 junio 2015 metodo fake para quitar elementos a la lista de favoritos
//    Este metodo no lo uso cuando la api de JL este terminada

    private void methodFake_Remove_From_List_Of_Favorites() {

        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +" He llegado a methodFake_Remove_From_List_Of_Favorites " );
        }

        m_MiFragment_Lista_De_Favoritos_Con_RecycleView.pruebaDeBorrar(m_position_favorito_a_borrar);

    }

    //    12 junio 2015 metodo fake para agregar elementos a la lista de favoritos
//    Este metodo no lo uso cuando la api de JL este terminada

    Event m_Event_Poner_en_favoritos;
    private void methodFake_Add_To_List_Of_Favorites() {

        Class_Modelo_De_Datos_Lista_Favoritos m_Class_Modelo_De_Datos_Lista_Favoritos =
                new Class_Modelo_De_Datos_Lista_Favoritos();

        m_Class_Modelo_De_Datos_Lista_Favoritos.setStringTituloDeLaFiesta(Class_ConstantsHelper.titulo_De_La_Fiesta);
        m_Class_Modelo_De_Datos_Lista_Favoritos.setStringFechaDelEvento(Class_ConstantsHelper.fecha_Del_Dia);
        m_Class_Modelo_De_Datos_Lista_Favoritos.setEventEventoFavorito(m_Event_Poner_en_favoritos);
        Class_ConstantsHelper.arraylist_Datos_Lista_Favoritos.add(m_Class_Modelo_De_Datos_Lista_Favoritos);

    }

    //****************************************************************
    //****************************************************************
    //     FIN de 9 Junio 2015: Codigo relacionado con el contextual action mode para
    //    insetar un evento en la lista de favoritos
    //****************************************************************
    //****************************************************************

    //****************************************************************
    //****************************************************************
    //     11 junio 2015, Instalacion
    //     Inicio de todo lo relacionado con la instalacion
    //****************************************************************
    //****************************************************************
    IConfigurationManager myCmT_Instalacion =null;

    private void method_Gestion_De_Instalacion_de_Pueblos(){

        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx +"  En metodo method_Gestion_De_Instalacion_de_Pueblos");

        }

        ConfigurationFactory cmTFactory = new ConfigurationFactory();

        try {
            myCmT_Instalacion = cmTFactory.getConfigurationManager(TypeConfiguration.INSTALLATION_DEFAULT_WITH_AUTONOMOUS_COMMUNITY);

            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(xxx, xxx +" Esta bien configurado_2 [" + myCmT_Instalacion.isRightConfigured() + "]");
                Log.d(xxx, xxx +" El cmt esta configurado_2[" + myCmT_Instalacion.isRightConfigured() + "] numero AU["
                        + myCmT_Instalacion.getAutonomousCommunityList().size() + "]");

                for (int i = 0; i < myCmT_Instalacion.getAutonomousCommunityList().size(); i++) {
                    Log.d(xxx, xxx + " :Comunidad " + i + ": "
                            + myCmT_Instalacion.getAutonomousCommunityList().get(i).getAutonomousCommunityName()
                            + "\n");
                    for (int i2 = 0; i2 < myCmT_Instalacion.getAutonomousCommunityList().get(i).getEventPlannerList().size(); i2++) {
                        Log.d(xxx, xxx + " :Pueblo " + i2 + ": "
                                +myCmT_Instalacion.getAutonomousCommunityList().get(i).getEventPlannerList().size()
                                        + "\n"
                                        + myCmT_Instalacion.getAutonomousCommunityList().get(i).getEventPlannerList().get(i2).getName()
                                        + "\n"
                                        + myCmT_Instalacion.getAutonomousCommunityList().get(i).getEventPlannerList().get(i2).getSortDescription()

                        );
                    }

                }
            }




        }
        catch(eefException eef)
        {
            Log.e("onCreate","Error inciando la conficuracion: " +eef.getLocalizedMessage());
        }
        catch (Exception ex){
            Log.e("onCreate","Excepotion no eef inciando la configuracion: " +ex.getLocalizedMessage());
        }

    }//Fin de method_Gestion_De_Instalacion_de_Pueblos


    //****************************************************************
    //****************************************************************
    //     FIN  de Inicio de todo lo relacionado con la instalacion
    //****************************************************************
    //****************************************************************

    //Interface de dialogo de evento guardado. No hago nada en estos metodos

    @Override
    public void onDialogPositiveClick_ClaseDialogEventoGuardado(DialogFragment dialog) {

    }

    @Override
    public void onDialogNegativeClick_ClaseDialogEventoGuardado(DialogFragment dialog) {

    }

    @Override
    public void onDialogNeutralClick_ClaseDialogEventoGuardado(DialogFragment dialog) {

    }
}//Fin de la clase
