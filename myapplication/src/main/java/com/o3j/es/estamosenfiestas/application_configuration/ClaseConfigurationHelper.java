package com.o3j.es.estamosenfiestas.application_configuration;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by Juan on 03/09/2015.
 */
public class ClaseConfigurationHelper {
    //Esta es una clase de ayuda para gestionar los casos:
    //1.-Cuando se esta instalando la aplicacion
    //2.-Cuando se esta instalando la aplicacion y no hay pueblos con fiestas.
    private String TAG;
    private Context context;


    public static ClaseConfigurationHelper newInstance(Context context) {

        ClaseConfigurationHelper claseConfigurationHelper =
                new ClaseConfigurationHelper();


        claseConfigurationHelper.TAG = claseConfigurationHelper.getClass().getSimpleName();
        claseConfigurationHelper.context = context;

        return claseConfigurationHelper;
    }//Fin de newInstance

    public void methodSetAppInstalada() {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, " En metodo methodSetAppInstalada:  ");
        }

        SharedPreferences prefs_AppInstalada = context.getSharedPreferences("prefs_AppInstalada", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs_AppInstalada.edit();
        editor.putString("app_instalada", "si");
        editor.commit();
    }

    public boolean methodEstoyConfigurando() {
        SharedPreferences prefs_AppInstalada = context.getSharedPreferences("prefs_AppInstalada", Context.MODE_PRIVATE);
        String app_instalada = prefs_AppInstalada.getString("app_instalada", "no");
        if (app_instalada.equals("si")) {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG, " En metodo methodEstoyConfigurando:  Estoy reconfigurando");
            }
            return true;//Estoy reconfigurando
        }else{
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG, " En metodo methodEstoyConfigurando:   Estoy instalando ");
            }
            return false;//Estoy instalando
        }
    }
}
