package com.o3j.es.estamosenfiestas.activity_actualizar_fiestas;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.format.DateUtils;
import android.util.Log;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Juan on 24/08/2015.
 */
public class ClaseActualizacionAutomatica {
    //Esta clase es para actualizar de forma automatica los datos de las fiestas
    private String TAG;
    private Context context;
    private String fechaInicial = "fechaInicial";
    private int intMinutosElapse = 5;



    public static ClaseActualizacionAutomatica newInstance(Context context) {

        ClaseActualizacionAutomatica claseActualizacionAutomatica =
                new ClaseActualizacionAutomatica();


        claseActualizacionAutomatica.TAG = claseActualizacionAutomatica.getClass().getSimpleName();
        claseActualizacionAutomatica.context = context;

        return claseActualizacionAutomatica;
    }//Fin de newInstance

    public void methodGuardaFechaInicial(Date dateFinal) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, " En metodo methodGuardaCurrentDate, Fecha inicial:  " +dateFinal.getTime());
        }


        SharedPreferences prefs_Actualizar = context.getSharedPreferences("prefs_Actualizar", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs_Actualizar.edit();
        editor.putString("primera_Instalacion", "no");
        editor.putLong(fechaInicial, dateFinal.getTime());
        editor.commit();
    }

    public long methodRecuperaFechaInicial() {
        long longFechaInicial = 0L;
        SharedPreferences prefs_Actualizar = context.getSharedPreferences("prefs_Actualizar", Context.MODE_PRIVATE);
        String primera_Instalacion = prefs_Actualizar.getString("primera_Instalacion", "default");
        if (primera_Instalacion.equals("no")) {
            longFechaInicial = prefs_Actualizar.getLong(fechaInicial, 0L);
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG, " En metodo methodRecuperaFechaInicial:  " +longFechaInicial);
            }
            return longFechaInicial;
        }else{
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG, " En metodo methodRecuperaFechaInicial:   primera_Instalacion es default, devuelve 0L: "
                        +primera_Instalacion);
            }
            return longFechaInicial;
        }
    }

    public long methodCalculaDiff(Date dateFinal) {

        if (methodRecuperaFechaInicial() != 0L) {
            long longDiferenciaEnMiliSegundos = dateFinal.getTime() - methodRecuperaFechaInicial();
            return longDiferenciaEnMiliSegundos;
        }else
        {
            return 0L;//No se lanzara la actualizacion en este caso
        }
    }

    public boolean methodGetBooleanActualizarAhora(Date dateFinal) {
        //Este es el metodo al que hay que llamar para disparar la actualizacion automatica

        long longDiferenciaEnMiliSegundos = methodCalculaDiff(dateFinal);

        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, " En metodo methodGetBooleanActualizarAhora longDiferenciaEnMiliSegundos:  " +longDiferenciaEnMiliSegundos);
        }


        //************************************************************************************************************************
        long differenceInSeconds = longDiferenciaEnMiliSegundos / DateUtils.SECOND_IN_MILLIS;
        // formatted will be HH:MM:SS or MM:SS
        String formatted = DateUtils.formatElapsedTime(differenceInSeconds);
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, " En metodo methodGetBooleanActualizarAhora differenceInSeconds:  " +formatted);
        }
        //*************************************************************************************************************************

        //Nota: intMinutosElapse siempre estara en minutos
        if (longDiferenciaEnMiliSegundos >= (1000 * intMinutosElapse *60)) {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG, " En metodo methodGetBooleanActualizarAhora: retorna true ");
            }
            return true;
        }else {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG, " En metodo methodGetBooleanActualizarAhora:  retorna false ");
            }
            return false;
        }
    }

    public void methodIniciaActualizacionAutomatica(String param1, String param2) {
        //Juan 31 agosto 2015: metodo para inicializar actualizacion automatica
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, " En metodo methodIniciaActualizacionAutomatica:  ");
        }

//        IntentserviceActualizacionesAutomaticas.startActionFoo(context, param1, param2);

        //Aqui gestiono el envio del pending intent con la alarma


        //Como en HandyMobilecare, no es una alarma repetitiva
//        Intent intentNuevaBusquedaDeAlarmas = new Intent(this, GestorAlarmasRebootIntentService.class);
//        PendingIntent pendingIntent = PendingIntent.getService(this,
//                0, intentNuevaBusquedaDeAlarmas, PendingIntent.FLAG_CANCEL_CURRENT);
//        AlarmManager alarmManager = (AlarmManager)getSystemService(Activity.ALARM_SERVICE);
//        alarmManager.set(AlarmManager.RTC_WAKEUP, calendaDiaSiguienteCeroHoras.getTimeInMillis(), pendingIntent);

        //Como en android developers:
        //http://developer.android.com/training/scheduling/alarms.html
        //Wake up the device to fire the alarm at precisely 8:30 a.m., and every 20 minutes thereafter:
//        AlarmManager alarmMgr;
//        PendingIntent alarmIntent;
//
//        alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
//        Intent intent = new Intent(context, AlarmReceiver.class);
//        alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
//
//// Set the alarm to start at 8:30 a.m.
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTimeInMillis(System.currentTimeMillis());
//        calendar.set(Calendar.HOUR_OF_DAY, 8);
//        calendar.set(Calendar.MINUTE, 30);
//
//// setRepeating() lets you specify a precise custom interval--in this case,
//// 20 minutes.
//        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
//                1000 * 60 * 20, alarmIntent);



        //Antes de lanzar la alarma (ahora la lanzo cada vez que abro la app en act programs
        methodDetenerActualizacionAutomaticaFake("fake1", "fake2");





        //Por fin, MI ALARMA, repite cada minuto
        AlarmManager alarmMgr;
        PendingIntent alarmIntent;

        alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, IntentserviceActualizacionesAutomaticas.class);
        intent.setAction(IntentserviceActualizacionesAutomaticas.ACTION_FOO);
        intent.putExtra(IntentserviceActualizacionesAutomaticas.EXTRA_PARAM1, param1);
        intent.putExtra(IntentserviceActualizacionesAutomaticas.EXTRA_PARAM2, param2);
        alarmIntent = PendingIntent.getService(context, 0, intent, 0);

// Set the alarm to start at 8:30 a.m.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.MINUTE, 1);

// setRepeating() lets you specify a precise custom interval--in this case,
// 2405 minutes o cada 4 horas.
        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                1000 * 60 * 240, alarmIntent);

    }

    //************************************************************************************
    //9 sept 2015: aqui empieza lo nuevo de la actualizacion automatica con el checkbox
    //************************************************************************************

    public void methodDetenerActualizacionAutomaticaFake(String param1, String param2) {
        //Metodo para cancelar la alarma

        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, " En metodo methodDetenerActualizacionAutomaticaFake:  ");
        }
        AlarmManager alarmMgr;
        PendingIntent alarmIntent;

        alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, IntentserviceActualizacionesAutomaticas.class);
        intent.setAction(IntentserviceActualizacionesAutomaticas.ACTION_FOO);
        intent.putExtra(IntentserviceActualizacionesAutomaticas.EXTRA_PARAM1, param1);
        intent.putExtra(IntentserviceActualizacionesAutomaticas.EXTRA_PARAM2, param2);
        alarmIntent = PendingIntent.getService(context, 0, intent, 0);

        if (alarmMgr!= null) {
            alarmMgr.cancel(alarmIntent);
        }
    }//Fin de methodDetenerActualizacionAutomaticaFake





    public boolean methodGetBooleanCheckBoxChecked() {

        //***********************************************************************************************
        //Juan 9 oct de 2015:
        //Hay que tratar el caso de la instalacion o de actualizacion de la app
        //Para poner por defecto el chequeo automatico
        SharedPreferences prefs_Primera_Instalacion_O_Actualizacion =
                context.getSharedPreferences("prefs_Primera_Instalacion_O_Actualizacion", Context.MODE_PRIVATE);
        boolean BooleanVersionNueva_O_PrimeraDescarga =
                prefs_Primera_Instalacion_O_Actualizacion.getBoolean("BooleanVersionNueva_O_PrimeraDescarga", true);

        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, " En metodo methodGetBooleanCheckBoxChecked, " +
                    "BooleanVersionNueva_O_PrimeraDescarga :  " +BooleanVersionNueva_O_PrimeraDescarga);
        }

        if (BooleanVersionNueva_O_PrimeraDescarga) {
            //Nota: en method_Set_To_False_BooleanVersionNueva_O_PrimeraDescarga hay que llamar a
            //methodSetCheckBox con true para la activacion por defecto.
            method_Set_To_False_BooleanVersionNueva_O_PrimeraDescarga();
        }else{
            //No hago nada, la app ya tiene el valor correcto
        }
        //FIN de Juan 9 oct de 2015:
        //*************************************************************************************************

        //Sigo con el flujo normal de chequeo del check box
        //Metodo para averiguar el estado del checkbox
        SharedPreferences prefs_Actualizar = context.getSharedPreferences("prefs_Actualizar", Context.MODE_PRIVATE);
        boolean BooleanCheckBoxChecked = prefs_Actualizar.getBoolean("BooleanCheckBoxChecked", false);

        if (BooleanCheckBoxChecked) {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG, " En metodo methodGetBooleanCheckBoxChecked, BooleanCheckBoxChecked :  " +BooleanCheckBoxChecked);
            }
            return true;
        }else{
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG, " En metodo methodGetBooleanCheckBoxChecked, BooleanCheckBoxChecked :  " +BooleanCheckBoxChecked);
            }
            return false;
        }
    }//Fin de methodGetBooleanCheckBoxChecked


    //JUan 9 oct 2015: metodo para indicar en el xml que ya no es primera instalacion o actualizacion
    public void method_Set_To_False_BooleanVersionNueva_O_PrimeraDescarga() {
        //Metodo para poner el estado del checkbox checked o unchecked segun el valor de myBoolean
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, " En metodo method_Set_To_False_BooleanVersionNueva_O_PrimeraDescarga");
        }

        SharedPreferences prefs_Primera_Instalacion_O_Actualizacion =
                context.getSharedPreferences("prefs_Primera_Instalacion_O_Actualizacion", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs_Primera_Instalacion_O_Actualizacion.edit();
        editor.putBoolean("BooleanVersionNueva_O_PrimeraDescarga", false);
        editor.commit();

        //Llama a methodSetCheckBox con true para la activacion por defecto de la actualizacion automatica
        //Poner BooleanCheckBoxChecked a true
        methodSetCheckBox(true);



    }//Fin de method_Set_To_False_BooleanVersionNueva_O_PrimeraDescarga




    public void methodSetCheckBox(boolean myBoolean) {
        //Metodo para poner el estado del checkbox checked o unchecked segun el valor de myBoolean
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, " En metodo methodSetCheckBox, myBoolean:  " +myBoolean );
        }

        SharedPreferences prefs_Actualizar = context.getSharedPreferences("prefs_Actualizar", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs_Actualizar.edit();
        editor.putBoolean("BooleanCheckBoxChecked", myBoolean);
        editor.commit();
    }//Fin de methodSetCheckBox

    //************************************************************************************
    //FIN DE 9 sept 2015: aqui empieza lo nuevo de la actualizacion automatica con el checkbox
    //************************************************************************************
}
