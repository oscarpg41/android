package com.o3j.es.estamosenfiestas.servercall.pojos;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by jluis on 27/07/15.
 */
public class ProgramModifiedPojo {
    @Expose
    private String idProgram;
    @Expose
    private String modification;
    @SerializedName("ff_modification")
    @Expose
    private String ffModification;

    /**
     *
     * @return
     * The idProgram
     */
    public String getIdProgram() {
        return idProgram;
    }

    /**
     *
     * @param idProgram
     * The idProgram
     */
    public void setIdProgram(String idProgram) {
        this.idProgram = idProgram;
    }

    /**
     *
     * @return
     * The modification
     */
    public String getModification() {
        return modification;
    }

    /**
     *
     * @param modification
     * The modification
     */
    public void setModification(String modification) {
        this.modification = modification;
    }

    /**
     *
     * @return
     * The ffModification
     */
    public String getFfModification() {
        return ffModification;
    }

    /**
     *
     * @param ffModification
     * The ff_modification
     */
    public void setFfModification(String ffModification) {
        this.ffModification = ffModification;
    }
}
