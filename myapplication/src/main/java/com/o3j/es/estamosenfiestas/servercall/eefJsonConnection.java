package com.o3j.es.estamosenfiestas.servercall;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.util.Log;

import com.eef.data.dataelements.AutonomousCommunity;
import com.eef.data.dataelements.EventPlanner;
import com.eef.data.eefExceptions.eefException;
import com.eef.data.eeftypes.TypeEefError;

import java.util.List;

/**
 * Created by jluis on 21/03/15.
 */
public class eefJsonConnection {

    Context appContext;

    /*javadoc*/
    public static  void checkConnection(Context context) throws eefException{
        ConnectivityManager cmN = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nIM = cmN.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo nIW =cmN.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if(nIM==null && nIW==null)
            throw new eefException(TypeEefError.NETWORK_NOT_AVAILABLE);
        if(!nIM.isAvailable() && !nIM.isConnected() && nIW.isAvailable() && ! nIW.isConnected())
            throw new eefException(TypeEefError.NETWORK_NOT_AVAILABLE);
    }

    public eefJsonConnection() {
    }

    public List<EventPlanner> getEventPlannerFromServer(AutonomousCommunity ac) throws eefException
    {
        eefJsonConnection.checkConnection(appContext);
        String URL="http://www.estamosenfiestas.es/api/towns/" + ac.getAutonomousCommunityId();
        return null;
    }


}
