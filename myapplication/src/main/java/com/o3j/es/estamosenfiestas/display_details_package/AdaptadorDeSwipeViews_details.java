package com.o3j.es.estamosenfiestas.display_details_package;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.eef.data.dataelements.DayEvents;

/**
 * Created by Juan on 14/02/2015.
 */


public class AdaptadorDeSwipeViews_details extends FragmentStatePagerAdapter {

    int int_Number_Of_Fragments_To_Show;

    DayEvents day_Events;

    public AdaptadorDeSwipeViews_details(FragmentManager fm, DayEvents eventos_Del_Dia) {
        super(fm);
        this.day_Events = eventos_Del_Dia;
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = new MiFragment_Con_Loader_Details();
        Bundle args = new Bundle();
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
        args.putString(MiFragment_Con_Loader_Details.ARG_OBJECT, "fragment#" +(i + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Con_Loader_Details.ARG_PARAM2, String.valueOf(i + 1));

//        Estoy probando con la clase Progarm como serializable
        args.putSerializable("Programa Serializado", day_Events);
        fragment.setArguments(args);






        return fragment;
    }


    //GetCount retorna el numero de fragmentos que va a mostrar el swipeView
    @Override
    public int getCount() {

//        No devuelvo un valor fijo
        return 1;
//        return day_Events.getListaEventos().size();
    }

    //Original
//    public int getCount() {
//        return 5;
//    }

    @Override
    public CharSequence getPageTitle(int position) {

        return "Lista de Eventos  " + (position + 1);
    }
}
