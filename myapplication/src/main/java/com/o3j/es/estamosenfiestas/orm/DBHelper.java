package com.o3j.es.estamosenfiestas.orm;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.eef.data.dataelements.Program;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.o3j.es.estamosenfiestas.orm.pojosDB.Configuracion;
import com.o3j.es.estamosenfiestas.orm.pojosDB.Eventos;
import com.o3j.es.estamosenfiestas.orm.pojosDB.Organizador;
import com.o3j.es.estamosenfiestas.orm.pojosDB.Programa;

import java.sql.SQLException;

/**
 * Created by jluis on 26/07/15.
 */
public class DBHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "eef_ormlite.db";
    private static final int DATABASE_VERSION = 1;

    private Dao<Configuracion, Integer> configuracionDao;
    private Dao<Organizador, Integer> organizadorDao;
    private Dao<Programa, Integer> programaDao;
    private Dao<Eventos, Integer> eventosDao;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try{
            TableUtils.createTable(connectionSource, Configuracion.class);
            TableUtils.createTable(connectionSource, Organizador.class);
            TableUtils.createTable(connectionSource, Programa.class);
            TableUtils.createTable(connectionSource, Eventos.class);

        }catch (SQLException ex){
            throw new RuntimeException(ex);
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        //onCreate(database,connectionSource);
       /* try {

            TableUtils.clearTable(connectionSource, Configuracion.class);
            TableUtils.clearTable(connectionSource, Organizador.class);
            TableUtils.createTable(connectionSource, Programa.class);
            TableUtils.createTable(connectionSource, Eventos.class);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }*/
    }

    public Dao<Configuracion,Integer> getConfiguracionDao()  throws SQLException{
        if (configuracionDao == null)
            configuracionDao = getDao(Configuracion.class);
        return configuracionDao;
    }

    public Dao<Organizador,Integer> getOrganizadorDao() throws SQLException{
        if (organizadorDao == null)
            organizadorDao = getDao(Organizador.class);
        return organizadorDao;
    }

    public Dao<Programa,Integer> getProgramaDao() throws SQLException{
        if (programaDao == null)
            programaDao = getDao(Programa.class);
        return programaDao;
    }

    public Dao<Eventos,Integer> getEventosDao() throws SQLException{
        if (eventosDao == null)
            eventosDao = getDao(Eventos.class);
        return eventosDao;
    }
    @Override
    public void close()
    {
        super.close();
        configuracionDao=null;
    }
}
