package com.o3j.es.estamosenfiestas;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.o3j.es.estamosenfiestas.display_frame_layout.Activity_Fragment_FrameLayout_WithNavDrawer_2;

public class JuanTest_Activity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_juan_test_);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_juan_test_, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick_1(View view) {
        // Do something in response to button
//        Actividad original sin nav darwer
//        Intent intent = new Intent(this, com.o3j.es.estamosenfiestas.activity_fragment_base_2.Activity_Fragment_Base_2.class);
//        Intent intent = new Intent(this, com.o3j.es.estamosenfiestas.display_lists_package.Activity_Fragment_Lists_WithNavDrawer.class);
//        Intent intent = new Intent(this, Activity_Fragment_Lists_WithNavDrawer_2.class);
        Intent intent = new Intent(this, Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
        startActivity(intent);
    }
}
