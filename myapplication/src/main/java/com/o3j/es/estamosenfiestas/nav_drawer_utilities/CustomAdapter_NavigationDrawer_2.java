/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.o3j.es.estamosenfiestas.nav_drawer_utilities;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper;

import java.util.ArrayList;

/**
 * Provide views to RecyclerView with data from mDataSet.
 */
//creado el 24 feb 2014  para nuevo layout del contenido del navigation drawer


//He usado esta web
//        http://www.android4devs.com/2014/12/how-to-make-material-design-navigation-drawer.html
//para poner el logo de estamos en fiestas como primer elemento del drawer

public class CustomAdapter_NavigationDrawer_2 extends RecyclerView.Adapter<CustomAdapter_NavigationDrawer_2.ViewHolder> {
//    private static final String TAG = "CustomAdapter";
    private static  String TAG;


    private static OnItemClickListenerElementoDeMenuSeccionado actividad;


    //Original
//    private String[] mDataSet;

    //21 Feb 2015 modificado para funcionar con el array list
    private ArrayList<ClassMenuItemsNavigationDrawer> arrayListItemsNavigationDrawer;


//    6 junio 2015: variables para lo de la cabecera
    private static final int TYPE_HEADER = 0;  // Declaring Variable to Understand which View is being worked on
    // IF the view under inflation and population is header or Item
    private static final int TYPE_ITEM = 1;

    // BEGIN_INCLUDE(recyclerViewSampleViewHolder)
    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        //Los nombres como en Class_Evento_1
        int Holderid;

        private  TextView textView_Titulo;
        private  TextView textView_Descripcion;
        private  ImageView imageView;
        private  ImageView imageView__De_La_Cabecera;
        private  ImageView imageView_en_nav_drawer_abajo;



        private  View m_View;


        public TextView getTextView_Titulo() {
            return textView_Titulo;
        }

        public TextView getTextView_Descripcion() {
            return textView_Descripcion;
        }


        public ImageView getImageView() {
            return imageView;
        }
        public ImageView getImageView_Cabecera() {
            return imageView__De_La_Cabecera;
        }
        public ImageView get_imageView_en_nav_drawer_abajo() {
            return imageView_en_nav_drawer_abajo;
        }


        public View getView() {
            return m_View;
        }




        public ViewHolder(View v, int ViewType) {
            super(v);
            // Define click listener for the ViewHolder's View.
//            v.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
//                        Log.d(TAG, "Element " + getPosition() + " clicked.");
//                    }
//                }
//            });
//            textView_Titulo = (TextView) v.findViewById(R.id.textView6);
//            textView_Descripcion = (TextView) v.findViewById(R.id.textView7);
//            imageView = (ImageView) v.findViewById(R.id.imageView);


            //************************************************************************************************
            //************************************************************************************************
            //************************************************************************************************
            //************************************************************************************************
//            7 Junio 2015: codigo para poder gestionar la cabecera como en:
            //        http://www.android4devs.com/2014/12/how-to-make-material-design-navigation-drawer.html



            // Here we set the appropriate view in accordance with the the view type as passed when the holder object is created

            if(ViewType == TYPE_ITEM) {//Se presenta la fila con el elemento del menu
//                textView = (TextView) itemView.findViewById(R.id.rowText); // Creating TextView object with the id of textView from item_row.xml
//                imageView = (ImageView) itemView.findViewById(R.id.rowIcon);// Creating ImageView object with the id of ImageView from item_row.xml

                Holderid = 1;                                               // setting holder id as 1 as the object being populated are of type item row
                textView_Titulo = (TextView) v.findViewById(R.id.textView6);
                textView_Descripcion = (TextView) v.findViewById(R.id.textView7);
                imageView = (ImageView) v.findViewById(R.id.imageView);
                imageView_en_nav_drawer_abajo = (ImageView) v.findViewById(R.id.imageView_en_nav_drawer_abajo);
                m_View = v;





            }
            else{//Esto vale solo para la fila 0, que es la cabecera


//                Name = (TextView) itemView.findViewById(R.id.name);         // Creating Text View object from header.xml for name
//                email = (TextView) itemView.findViewById(R.id.email);       // Creating Text View object from header.xml for email
//                profile = (ImageView) itemView.findViewById(R.id.circleView);// Creating Image view object from header.xml for profile pic


                imageView__De_La_Cabecera = (ImageView) v.findViewById(R.id.imageView_cabecera);
                Holderid = 0;    // Setting holder id = 0 as the object being populated are of type header view

                //Quito el logo haciendolo invisible
                imageView__De_La_Cabecera.setVisibility(View.GONE);
            }
            //************************************************************************************************
            //************************************************************************************************
            //************************************************************************************************
            //************************************************************************************************
        }//Fin del constructor del view holder

    }//Fin de la clase ViewHolder
    // END_INCLUDE(recyclerViewSampleViewHolder)


    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used by RecyclerView.
     */
    //Original
//    public CustomAdapter_RecyclerView_3(String[] dataSet) {

    //21 Feb 2015 modificado para funcionar con el array list
//    public CustomAdapter_RecyclerView_3(ArrayList<Class_Evento_1> dataSet) {
    //Original
//    public CustomAdapter_RecyclerView_3(ArrayList<Class_Evento_1> dataSet, String intNumeroAdapter) {

    //Le paso el context para poder llamar a la actividad que muestra el detalle
    public CustomAdapter_NavigationDrawer_2(ArrayList<ClassMenuItemsNavigationDrawer> dataSet,
                                            String intNumeroAdapter,
                                            OnItemClickListenerElementoDeMenuSeccionado actividad) {


//        mDataSet = dataSet;
        arrayListItemsNavigationDrawer = dataSet;
//        TAG = this.getClass().getName();
        //Con simple name por que con name el string es muy largo por que nombra el paquete tambien
        TAG = this.getClass().getSimpleName();
            this.intNumeroAdapter = intNumeroAdapter;
            this.stringNumeroAdapter = TAG +" " + this.intNumeroAdapter +", ";

        this.actividad = actividad;


    }//Fin del constructor


    //Uso eete integer para saber que instancia estoy viendo en el log
    private String intNumeroAdapter;
    private String stringNumeroAdapter;

    // BEGIN_INCLUDE(recyclerViewOnCreateViewHolder)
    // Create new views (invoked by the layout manager)
    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        //6 junio 2015, dejo comentado el codigo original
//        View v = LayoutInflater.from(viewGroup.getContext())
//                  .inflate(R.layout.fila_menu_navigationdrawer_1, viewGroup, false);
//
////        return new ViewHolder(v);
//        return new ViewHolder(v);

        //************************************************************************************************
        //************************************************************************************************
        //************************************************************************************************
        //************************************************************************************************
//        Codigo nuevo

        //Below first we ovverride the method onCreateViewHolder which is called when the ViewHolder is
        //Created, In this method we inflate the item_row.xml layout if the viewType is Type_ITEM or else we inflate header.xml
        // if the viewType is TYPE_HEADER
        // and pass it to the view holder


            if (viewType == TYPE_ITEM) {
                View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fila_menu_navigationdrawer_1,viewGroup,false); //Inflating the layout

                ViewHolder vhItem = new ViewHolder(v,viewType); //Creating ViewHolder and passing the object of type view

                return vhItem; // Returning the created object

                //inflate your layout and pass it to view holder

            } else if (viewType == TYPE_HEADER) {

                View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cabecera_del_navigationdrawer,viewGroup,false); //Inflating the layout

                ViewHolder vhHeader = new ViewHolder(v,viewType); //Creating ViewHolder and passing the object of type view

                return vhHeader; //returning the object created


            }
            return null;

        //************************************************************************************************
        //************************************************************************************************
        //************************************************************************************************
        //************************************************************************************************
    }//Fin de onCreateViewHolder
    // END_INCLUDE(recyclerViewOnCreateViewHolder)

    // BEGIN_INCLUDE(recyclerViewOnBindViewHolder)
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(TAG, stringNumeroAdapter +"Element " + position + " set.");
        }


        //Prueba de uso de loader
        //Hago un chequeo de null, y regreso sin imprimir nada
        if(arrayListItemsNavigationDrawer == null) return;


        // Get element from your dataset at this position and replace the contents of the view
        // with that element
//        viewHolder.getTextView().setText(mDataSet[position]);
//        viewHolder.getTextView_Titulo().
//                setText(arrayListItemsNavigationDrawer.get(position).getMenuItemTitle());
//        viewHolder.getTextView_Descripcion().
//                setText(arrayListItemsNavigationDrawer.get(position).getItemTitleDescrption());
//        viewHolder.getImageView().
//                setImageResource(arrayListItemsNavigationDrawer.get(position).getSrc_Del_Image_View());
        //************************************************************************************************
        //************************************************************************************************
        //************************************************************************************************
        //************************************************************************************************

//        Codigo nuevo

        //Next we override a method which is called when the item in a row is needed to be displayed, here the int position
        // Tells us item at which position is being constructed to be displayed and the holder id of the holder object tell us
        // which view type is being created 1 for item row
//        @Override
//        public void onBindViewHolder(MyAdapter.ViewHolder holder, int position) {
            if(viewHolder.Holderid ==1) {//Fila normal del drawer                              // as the list view is going to be called after the header view so we decrement the
                // position by 1 and pass it to the holder while setting the text and image
//                holder.textView.setText(mNavTitles[position - 1]); // Setting the Text with the array of our Titles
//                holder.imageView.setImageResource(mIcons[position -1]);// Settimg the image with array of our icons

//                OJO CON LO DE position-1
                viewHolder.getTextView_Titulo().
                        setText(arrayListItemsNavigationDrawer.get(position - 1).getMenuItemTitle());
                viewHolder.getTextView_Descripcion().
                        setText(arrayListItemsNavigationDrawer.get(position - 1).getItemTitleDescrption());
                viewHolder.getImageView().
                        setImageResource(arrayListItemsNavigationDrawer.get(position - 1).getSrc_Del_Image_View());


                // Define click listener for the ViewHolder's View.
                viewHolder.getView().setOnClickListener(new View.OnClickListener() {
                @Override
                     public void onClick(View v) {
                          if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                              Log.d(TAG, "Element " + (position-1) + " clicked.");
                           }
                         actividad.onClickMenuItenDelNavDrawer(position-1);
                     }
                 });

                switch (position - 1)
                {
                    case 0:
                        viewHolder.getTextView_Titulo().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_estamosenfiestas_nav_drawer_notif, 0, 0, 0);
                        viewHolder.get_imageView_en_nav_drawer_abajo().setVisibility(View.GONE);

                        break;
                    case 1:
                        viewHolder.getTextView_Titulo().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_miseventos_nav_drawer_notif, 0, 0, 0);
                        viewHolder.get_imageView_en_nav_drawer_abajo().setVisibility(View.GONE);

                        break;
                    case 2:
                        viewHolder.getTextView_Titulo().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_actualizarfiestas_nav_drawer_notif, 0, 0, 0);
                        viewHolder.get_imageView_en_nav_drawer_abajo().setVisibility(View.GONE);

                        break;
                    case 3:
                        viewHolder.getTextView_Titulo().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_reconfigurar_nav_drawer_notif, 0, 0, 0);
                        viewHolder.get_imageView_en_nav_drawer_abajo().setVisibility(View.GONE);

                        break;
                    case 4:
                        viewHolder.getTextView_Titulo().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_organizador_nav_drawer_notif, 0, 0, 0);
                        viewHolder.get_imageView_en_nav_drawer_abajo().setVisibility(View.GONE);

                        break;
                    case 5:
                        viewHolder.getTextView_Titulo().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_800, 0, 0, 0);
                        viewHolder.get_imageView_en_nav_drawer_abajo().setVisibility(View.GONE);

                        break;
                    case 6:
                        viewHolder.getTextView_Titulo().setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_informacion_nav_drawer_notif, 0, 0, 0);
                        //8 sept 2015:ya no se muestra el logo, esta en el menu "acerca de"
                        viewHolder.get_imageView_en_nav_drawer_abajo().setVisibility(View.GONE);
                        break;
                    //JUAN 26 AGOSTO 2015: PONGO LA IMAGEN GONE DE 0 A 5 POR QUE SI NO AL ROTAR LA PANTALLA
                    //Y REUTILIZAR LAS VISTAS, DIBUJA ALEATORIAMENTE LA IMAGEN. CON GONE DE 0 A 5 YA NO OCURRE
                }


            }
            else{//Fila 1 con la cabecera

//                holder.profile.setImageResource(profile);           // Similarly we set the resources for header view
//                holder.Name.setText(name);
//                holder.email.setText(email);

                viewHolder.getImageView_Cabecera().
                        setImageResource(R.drawable.logo_grande_01);
            }

        //************************************************************************************************
        //************************************************************************************************
        //************************************************************************************************
        //************************************************************************************************
    }//Fin de onBindViewHolder
    // END_INCLUDE(recyclerViewOnBindViewHolder)

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
//        return mDataSet.length;

//        Original
//        if (arrayListItemsNavigationDrawer == null) {
//            return 0;
//        }
//        return arrayListItemsNavigationDrawer.size();
        //************************************************************************************************
        //************************************************************************************************
        //************************************************************************************************
        //************************************************************************************************
        // the number of items in the list will be +1 the titles including the header view.
        if (arrayListItemsNavigationDrawer == null) {
            return 0+1;
        }

        return arrayListItemsNavigationDrawer.size() + 1;
        //************************************************************************************************
        //************************************************************************************************
        //************************************************************************************************
        //************************************************************************************************
    }//Fin de getItemCount

    //************************************************************************************************
    //************************************************************************************************
    //************************************************************************************************
    //************************************************************************************************
// Witht the following method we check what type of view is being passed
    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;//Retorna true or false
    }
    //************************************************************************************************
    //************************************************************************************************
    //************************************************************************************************
    //************************************************************************************************

    /**
     * Interface para recibir en Activity_Fragment_FrameLayout_WithNavDrawer_2 el click de la fiesta seleccionada
     */
    public interface OnItemClickListenerElementoDeMenuSeccionado {
        public void onClickMenuItenDelNavDrawer(int  position_Del_Menu_Seleccionada);
    }
}
