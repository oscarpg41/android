package com.o3j.es.estamosenfiestas.application_configuration;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.eef.data.dataelements.AutonomousCommunity;
import com.eef.data.dataelements.DayEvents;
import com.eef.data.dataelements.Event;
import com.eef.data.dataelements.EventPlanner;
import com.eef.data.dataelements.Program;
import com.eef.data.eefExceptions.eefException;
import com.eef.data.eeftypes.TypeConfiguration;
import com.eef.data.eeftypes.TypeState;
import com.eef.data.managers.IConfigurationManager;
import com.eef.data.managers.impl.ProgamListManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.activity_actualizar_fiestas.ClaseActualizacionAutomatica;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.ClaseChequeaAcceso_A_Internet;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper;
import com.o3j.es.estamosenfiestas.dialogs.ClaseDialogGenericaDosTresBotones;
import com.o3j.es.estamosenfiestas.dialogs.ClaseDialogNoHayInternet;
import com.o3j.es.estamosenfiestas.display_frame_layout.CustomAdapter_RecyclerView_Evento_Detalle;
import com.o3j.es.estamosenfiestas.display_frame_layout.CustomAdapter_RecyclerView_Fiesta_List_Por_Dias;
import com.o3j.es.estamosenfiestas.display_frame_layout.CustomAdapter_RecyclerView_Fiestas;
import com.o3j.es.estamosenfiestas.display_frame_layout.CustomAdapter_RecyclerView_List_De_Eventos;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Evento;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Fiesta_Seleccionada_Con_RecycleView;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Fiestas_Con_RecycleView;
import com.o3j.es.estamosenfiestas.display_frame_layout.MiFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView;
import com.o3j.es.estamosenfiestas.display_lists_package.AdaptadorDeSwipeViews_2;
import com.o3j.es.estamosenfiestas.factory.impl.ConfigurationFactory;
import com.o3j.es.estamosenfiestas.nav_drawer_utilities.ClassMenuItemsNavigationDrawer;
import com.o3j.es.estamosenfiestas.nav_drawer_utilities.CustomAdapter_NavigationDrawer_2;
import com.o3j.es.estamosenfiestas.orm.DBHelper;
import com.o3j.es.estamosenfiestas.orm.pojosDB.Configuracion;
import com.o3j.es.estamosenfiestas.servercall.forbroadcaster.ConfigurationFromServer;
import com.o3j.es.estamosenfiestas.servercall.forbroadcaster.FullInfoEventPlannerFromServer;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import managers.DatabaseServerManager;
import managers.DownloadInfoFromServerManager;


// Juan 11 junio 2015, primera version. Es una copia de Activity_Fragment_FrameLayout_WithNavDrawer_2
//usando listas con este orden y sin salirse de esta actividad:
// 1: Se muestra la lista con todas las fiestas validas de ese ayuntamiento.
// 2: Se muestra la lista de la fiesta seleccionada en 1 por dias.
// 3: Se muestra la lista de eventos del dia seleccionado en 2.
// 4: Se muestra el detalle del evento seleccionado en 3, por ahora sin swipe.
public class Activity_Application_Configuration_WithNavDrawer_2 extends ActionBarActivity
        implements
        CustomAdapter_RecyclerView_Lista_De_Pueblos.OnItemClickListenerPuebloSeleccionado,
        CustomAdapter_RecyclerView_Lista_De_Comunidades.OnItemClickListenerComunidadSeleccionada,
        CustomAdapter_RecyclerView_Fiestas.OnItemClickListenerFiestaSeleccionada,
        CustomAdapter_RecyclerView_Fiesta_List_Por_Dias.OnItemClickListenerDiaSeleccionado,
        CustomAdapter_RecyclerView_List_De_Eventos.OnItemClickListenerEventoSeleccionado,
        CustomAdapter_RecyclerView_Evento_Detalle.OnItemClickListenerEventoDetalle,
        CustomAdapter_NavigationDrawer_2.OnItemClickListenerElementoDeMenuSeccionado,
        MiFragment_Fiestas_Con_RecycleView.OnFragmentInteractionListener,
        MiFragment_Fiesta_Seleccionada_Con_RecycleView.OnFragmentInteractionListener,
        MiFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView.OnFragmentInteractionListener,
        MiFragment_Evento.OnFragmentInteractionListener,
        MiFragment_Lista_de_Comunidades__Con_RecycleView.OnFragmentInteractionListener,
        MiFragment_Lista_de_Pueblos_Con_RecycleView.OnFragmentInteractionListener,
        ClaseDialogGenericaDosTresBotones.InterfaceClaseDialogGenericaDosTresBotonesListener,
        ClaseDialogNoHayInternet.InterfaceClaseDialogNoHayInternet,
        MiFragmentReconfiguracionBotones.OnFragmentInteractionListenerReconfiguracion{
    //Original con fragment sin loader
//    public class Activity_Fragment_Base_2 extends ActionBarActivity implements MiFragment_1.OnFragmentInteractionListener {
    AdaptadorDeSwipeViews_2 adaptadorDeSwipeViews;
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        xxx = this.getClass().getSimpleName();

        //*************************************************************************************
        //Lo paso al onCreate, si, peta la app
        //Inicializa los broadcast receivers
        myBroadcastReceiver_Progress_Bar = new MyBroadcastReceiver_Progress_Bar();
        intentFilter_1 = new IntentFilter(progressBarBR);
        myBroadcastReceiver_Show_Configuration = new MyBroadcastReceiver_Show_Configuration();
        intentFilter_2 = new IntentFilter(showconfigurationBR);

        //BRs de la descarga de programas y eventos
        myBroadcastReceiver_Progress_Bar_Programas_y_Eventos = new MyBroadcastReceiver_Progress_Bar_Programas_y_Eventos();
        intentFilter_3 = new IntentFilter(broadcast_Programas_Y_Eventos);
        myBroadcastReceiver_Fin_Carga_Programs_Y_Eventos_From_Server = new MyBroadcastReceiver_Fin_Carga_Programs_Y_Eventos_From_Server();
        intentFilter_4 = new IntentFilter(broadcast_Fin_Descarga_De_Programas_Y_Eventos);

        //***********************************************************************


//        Nota: Juan 2 junio: para que funcione el view pager y los tabs tengo que usar el layout activity_fragment_lists_with_nav_drawer_3
//        setContentView(R.layout.activity_fragment_lists_with_nav_drawer_3);


//        Este layout funciona pero al tener el drawer como raiz, tapa la toolbar
//        setContentView(R.layout.activity_fragment_frame_layout_with_nav_drawer);


//        4 junio 2015, Uso este layout para que el navigation drawer no tape la roolbar
        setContentView(R.layout.activity_fragment_frame_layout_with_nav_drawer_3);


//        Toolbar toolBar_Actionbar = (Toolbar) findViewById (R.id.activity_my_toolbar);
        Toolbar toolBar_Actionbar = (Toolbar) findViewById(R.id.view);
        //Toolbar will now take on default Action Bar characteristics

//        Le cambio el titulo por el ayuntamiento en method_Data_To_Show_In_Fragments_2
//        Para cambiar el tama�o del titulo lo hice con styles como en
//        http://stackoverflow.com/questions/28487312/how-to-change-the-toolbar-text-size-in-android


//        Tengo que poner este titulo para que aparezca el ayuntamiento en method_Data_To_Show_In_Fragments_2
        toolBar_Actionbar.setTitle(getResources().getString(R.string.pantalla_de_configuracion));
//        toolBar_Actionbar.setTitle("acénto");

//        No uso el subtitulo
//        toolBar_Actionbar.setSubtitle(getResources().getString(R.string.sub_titulo_4));


        setSupportActionBar(toolBar_Actionbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Toolbar bottom_toolbar = (Toolbar) findViewById(R.id.bottom_toolbar);
        bottom_toolbar.setLogo(R.drawable.ic_launcher);
        bottom_toolbar.setTitle(getResources().getString(R.string.texto_1));
        bottom_toolbar.setSubtitle(getResources().getString(R.string.texto_2));
        bottom_toolbar.setNavigationIcon(R.drawable.ic_launcher);
        //Toolbar que pongo abajo

        // Set an OnMenuItemClickListener to handle menu item clicks
        bottom_toolbar.setOnMenuItemClickListener(
                new Toolbar.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // Handle the menu item

                        int id = item.getItemId();

                        //noinspection SimplifiableIfStatement
                        if (id == R.id.action_settings) {
                            Toast.makeText(Activity_Application_Configuration_WithNavDrawer_2.this, "Bottom toolbar pressed: ", Toast.LENGTH_LONG).show();
                            return true;
                        }
                        return true;
                    }
                });


        bottom_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Activity_Application_Configuration_WithNavDrawer_2.this, "Navigation Icon pressed: ", Toast.LENGTH_LONG).show();
                // Para esta practica, al presionar el navigation icon lo quito.
                bottom_toolbar.setVisibility(View.GONE);
            }
        });

        // Inflate a menu to be displayed in the toolbar
        bottom_toolbar.inflateMenu(R.menu.menu_main);


        //Por ahora utilizo este metodo para jugar con el API de Jose Luis, 21 Mayo 2015
//        method_Data_To_Show_In_Fragments();

        //4 Junio 2015,
//        method_Data_To_Show_In_Fragments_2(toolBar_Actionbar);

        //Metodo con los datos de prueba del API estatico
//        method_Gestion_De_Instalacion_de_Pueblos();

        //24 julio 2015, nuevo metodo, ahora usamos retrofit y receivers
//        method_Gestion_De_Instalacion_de_Pueblos_2();

        //****************************************************************
        //   Juan, 2 junio 2015: No uso los swipe views, quedan comentados
        //****************************************************************
        // ViewPager and its adapters use support library
        // fragments, so use getSupportFragmentManager.
//        adaptadorDeSwipeViews =
//                new AdaptadorDeSwipeViews_2(
//                        getSupportFragmentManager(), int_Number_Of_Fragments_To_Show, List_Programs);
//        mViewPager = (ViewPager) findViewById(R.id.viewpager_1);
//        mViewPager.setAdapter(adaptadorDeSwipeViews);
//
//        //Poner tabs del swipe view
//        metodoPonerTabs_1(mViewPager);
        //****************************************************************
        //   FIN de Juan, 2 junio 2015: No uso los swipe views, quedan comentados
        //****************************************************************


        //Inicializar el nav drawer, 21 Mayo 2015
        method_Init_NavigationDrawer();


        //Juan 25 agosto 2015: prueba llamando solo una vez al metodo para actualizar la fecha actual desde aqui
        //Asi me ahorro ponerlo en un monton de sitios y ponerlo aqui vale para si sale bien o sale mal
        //o me voy por el navigation drawer
//        methodActivarConfiguracionAutomatica_SOLO_PARA_PRUEBAS();


        //*******************************************************************************************
        //Juan 26 agosto 2015, pruebas de los dialogos de error en la instalacion y configuracion
        //Prueba instalacion
        //Prueba conpletada con exito para instalacion y para configurar. Dejo la linea abajo comentada
//        method_Mostar_Error_De_Descarga_De_Datos_2();


        //*******************************************************************************************




        //4 sept 2015: Ahora llamo a method_Fuera_Del_On_Create_Chequea_Acceso_A_Internet
        // cuando haga click en el boton iniciar configuracion
        //que esta en el fragment MiFragmentReconfiguracionBotones
//        method_Fuera_Del_On_Create_Chequea_Acceso_A_Internet();

        //4 septiembre 2015: lo primero es mostrar el fragment con los botones
//        method_Muestra_Fragment_Con_Botones();


        //4 septiembre 2015: compruebo se istoy instalando o reconfigurando
        if (Class_ConstantsHelper.boolean_estoy_instalando) {//Empieza directamente la instalacion
            method_Fuera_Del_On_Create_Chequea_Acceso_A_Internet();
        }else{//Muestra la pantalla con el boton y el texto descriptivo
            method_Muestra_Fragment_Con_Botones();
        }

    }//Fin del onCreate

    public void method_Muestra_Fragment_Con_Botones() {
        //4 septiembre 2015: ahoro primero se muestra un fragmento con los botones
        //Ahora pongo el boton de Reconfigurar, mas adelante pongo los de actualizacion automatica

// Create new fragment and transaction


        Fragment newFragment = getNewFragmnet_Botones();
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack
// El replace vale aunque sea el primer fragment, asi es como lo tienen en android developers

//        NO, ASI FUNCIONA MAL, EL PRIMERO TIENE QUE SER ADD
//        transaction.replace(R.id.details, newFragment);
//        transaction.addToBackStack(null);

        transaction.add(R.id.details, newFragment);


// Commit the transaction
        transaction.commit();

        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx + "He llegado a method_Muestra_Fragment_Con_Botones");
        }
    }// Fin de method_Muestra_Fragment_Con_Botones_


    public Fragment getNewFragmnet_Botones() {
        return MiFragmentReconfiguracionBotones.newInstance("xxxxxxx", "zzzzzzzz");
    }//Fin de getNewFragmnet_Botones

    public void method_Fuera_Del_On_Create_Chequea_Acceso_A_Internet() {
        //*************************************************************************************************
        if(method_Dime_Si_Hay_Red()) {//Cuequeamos si hay red
            //Si hay red, sigue ejecutando
            //24 julio 2015, nuevo metodo, ahora usamos retrofit y receivers
            method_Gestion_De_Instalacion_de_Pueblos_2();

        }else{//NO hay red
            // Create an instance of the dialog fragment and show it
            DialogFragment dialog =  ClaseDialogNoHayInternet.newInstance
                    (R.string.titulo_dialog_error_no_hay_internet_reconfigurar, R.string.message_dialog_error_no_hay_internet_reconfigurar, R.string.positiveButton_error_no_hay_internet_reconfigurar,
                            R.string.negativeButton_error_no_hay_internet_reconfigurar, 2, R.string.boton_neutral_evento_guardado);
            dialog.show(getSupportFragmentManager(), "ClaseDialogNoHayInternet");
        }
        //*************************************************************************************************
    }

    @Override
    public void onFragmentInteractionIniciarReconfiguracion() {
        method_Fuera_Del_On_Create_Chequea_Acceso_A_Internet();
    }

    //    Juan 2 junio 2015, no uso el view pager ni los tabs, dejo el metodo metodoPonerTabs_1 comentado

//    private void metodoPonerTabs_1(ViewPager mViewPager) {
//
////        Hecho como en: http://guides.codepath.com/android/Google-Play-Style-Tabs-using-SlidingTabLayout
//// Give the SlidingTabLayout the ViewPager
//        SlidingTabLayout slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
//        // Center the tabs in the layout
//        slidingTabLayout.setDistributeEvenly(true);
//        slidingTabLayout.setViewPager(mViewPager);
//        //Colorear el tab
//        slidingTabLayout.setBackgroundColor(getResources().getColor(R.color.primary_color));
//        // Customize tab indicator color
//        slidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
//            @Override
//            public int getIndicatorColor(int position) {
//                return getResources().getColor(R.color.accent_color);
//            }
//        });
//    }

    //**************************************************************************
    //**************************************************************************
    //  24 junio 2015: Setting correcto para no mostrar menu en la toolbar
    //  24 junio 2015: Solo se muestra el Nav Drawer
    //**************************************************************************
    //**************************************************************************


//    Me ayude con:
//    https://github.com/codepath/android_guides/wiki/Fragment-Navigation-Drawer

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;

        // Uncomment to inflate menu items to Action Bar
        // inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = drawerLayout.isDrawerOpen(navigationDrawerRecyclerView);

        //No he inflado el menu, lo dejo comentado
//        menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }// Fin de onPrepareOptionsMenu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this. 21 Mayo 2015
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }//Codigo del nav drawer


        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        if (id == R.id.action_settings) {
//            Toast.makeText(Activity_Fragment_FrameLayout_WithNavDrawer_2.this, " toolbar pressed: ", Toast.LENGTH_LONG).show();
//
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }
    //**************************************************************************
    //**************************************************************************
    //  FIN DE 24 junio 2015: Setting correcto para no mostrar menu en la toolbar
    //  24 junio 2015: Solo se muestra el Nav Drawer
    //**************************************************************************
    //**************************************************************************

    //Implementa la interfaz de la clase MiFragment_Fiestas_Con_RecycleView
//    y la clase MiFragment_Fiesta_Seleccionada_Con_RecycleView

    //    OJO: NO ESTOY HACIENDO USO DE ESTA INTERFAZ
    public void onFragmentInteraction(String string) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx + "Fragmento activo: " + string);

        }
    }


    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //  Inicio de Codigo relacionado con el navigation drawer, 21 Mayo 2015
    //****************************************************************
    //****************************************************************
    private void method_Init_NavigationDrawer() {

        //****************************************************************
        //   Codigo relacionado con recycler view del navigation drawer
        //****************************************************************


        //Inicializar los datos del array del menu a mostrar en el navigationDrawer
        init_arrayList_NavigationDrawer();

        navigationDrawerRecyclerView = (RecyclerView) findViewById(R.id.left_drawer_recycle_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        navigationDrawerRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        navigationDrawerLayoutManager = new LinearLayoutManager(this);
        navigationDrawerRecyclerView.setLayoutManager(navigationDrawerLayoutManager);
//        navigationDrawerRecyclerView.setAdapter(new CustomAdapter_NavigationDrawer(

//        Adapter del navigation drawer con la cabecera
        navigationDrawerRecyclerView.setAdapter(new CustomAdapter_NavigationDrawer_2(
                arrayListItemsNavigationDrawer, "0",
                (CustomAdapter_NavigationDrawer_2.OnItemClickListenerElementoDeMenuSeccionado) this));

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_1);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        //Este objeto permite que el navigation Drawer interactue correctamente con el action bar (la toolbar en mi caso)
        //Tengo que usar el actionBarDrawerToggle de V7 support por que el de V4 esta deprecated
        actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                drawerLayout,         /* DrawerLayout object */
                //Deprecated en V7, esto era valido en V4
//                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
//                getSupportActionBar().setTitle(getResources().getString(R.string.title_activity_main_activity_navigation_drawer));
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()

            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                getActionBar().setTitle(mDrawerTitle);
//                getSupportActionBar().setTitle(getTitle());
//                getSupportActionBar().setTitle(getResources().getString(R.string.titulo_navigation_drawer));
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()


            }
        };//Fin de actionBarDrawerToggle

        // Set the drawer toggle as the DrawerListener
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //****************************************************************
        //  FIN Codigo relacionado con recycler view del navigation drawer
        //****************************************************************
    }//Fin de method_Init_NavigationDrawer





    //Datos del menu del navigation drawer
    private void init_arrayList_NavigationDrawer() {//Relacionado con recycler view
        String[] array_menu = {getString(R.string.menu_1),getString(R.string.menu_2),getString(R.string.menu_3)
                ,getString(R.string.menu_4),getString(R.string.menu_6),getString(R.string.menu_7), getString(R.string.menu_5)};
        arrayListItemsNavigationDrawer = new ArrayList<ClassMenuItemsNavigationDrawer>();
        for (int i = 0; i < DATASET_COUNT; i++) {
            arrayListItemsNavigationDrawer.add(
                    ClassMenuItemsNavigationDrawer.newInstance(array_menu[i],
                            " ",
                            R.drawable.ic_launcher));
        }
    }


    //Datos del menu del navigation drawer original, ya no lo uso, 6 junio 2015
    private void init_arrayList_NavigationDrawer_Original() {//Relacionado con recycler view
        arrayListItemsNavigationDrawer = new ArrayList<ClassMenuItemsNavigationDrawer>();
        for (int i = 0; i < DATASET_COUNT; i++) {
            arrayListItemsNavigationDrawer.add(
                    ClassMenuItemsNavigationDrawer.newInstance("Titulo # " + i,
                            "Descripcion #" + i,
                            R.drawable.ic_launcher));
        }
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    //Si estos metodos no se llaman, no aparece la hamburguesa
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    //Variables para el recycler view
    private DrawerLayout drawerLayout;
    private RecyclerView navigationDrawerRecyclerView;
    private RecyclerView.Adapter navigationDrawerAdapter;
    private RecyclerView.LayoutManager navigationDrawerLayoutManager;
    protected ArrayList<ClassMenuItemsNavigationDrawer> arrayListItemsNavigationDrawer = null;
    private static final int DATASET_COUNT = 7;
    //Este objeto permite que el navigation Drawer interactue correctamente con el action bar (la toolbar en mi caso)
    private ActionBarDrawerToggle actionBarDrawerToggle;
    //FIN Variables para el recycler view

    //****************************************************************
    //****************************************************************
    //     FIN del Codigo relacionado con el navigation drawer
    //****************************************************************
    //****************************************************************


    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //  Inicio de Codigo relacionado con el API de JL, 21 Mayo 2015
    //****************************************************************
    //****************************************************************
    private void method_Data_To_Show_In_Fragments() {
//        ESte metodo obtiene datos del api de JL, pero habra que cambiar cosas.

        ConfigurationFactory cmTFactory = new ConfigurationFactory();

        try {
            myCmT = cmTFactory.getConfigurationManager(TypeConfiguration.INSTALLATION_DEFAULT_WITH_AUTONOMOUS_COMMUNITY);

            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(xxx, xxx + " Esta bien configurado [" + myCmT.isRightConfigured() + "]");
                Log.d(xxx, xxx + " El cmt esta configurado[" + myCmT.isRightConfigured() + "] numero AU[" + myCmT.getAutonomousCommunityList().size() + "]");
                Log.d(xxx, xxx + " La comunidad Autonoma Configurada[" + myCmT.getAutonomousCommunityConfigured().getAutonomousCommunityName()
                        + "] y el pueblo configurado[" + myCmT.getEventPlannerConfigured().getName() + "]");
            }

            ProgamListManager pmT = new ProgamListManager();
            pmT.loadData(myCmT.getAutonomousCommunityConfigured(), myCmT.getEventPlannerConfigured());
            if (pmT.existCurrentProgram()) {
                Program p = pmT.getCurrentProgram();

                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(xxx, xxx + "HAY PROGRAMA EN MARCHA [" + p.getProgramSortDescription() + "]");

                }
                //Obtener la lista de eventos del programa por dias
                List<DayEvents> ltd = p.getEventByDay();
                //Obtener la lista de eventos completa
                List<Event> ltf = p.getFullList();
            }


            if (pmT.existCurrentProgram() || pmT.existFuturetPrograms() || pmT.existPastPrograms()) {
                List_Programs = pmT.getFullProgramList();


                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(xxx, "HAY  [" + List_Programs.size() + "]" + " PROGRAMAS EN MARCHA");
                    for (int i = 0; i < List_Programs.size(); i++) {
                        Log.d(xxx, xxx + " Codigo del programa numero  [" + i + ": "
                                + List_Programs.get(i).getState().getCode() + "]" + " PROGRAMAS EN MARCHA");

                        Log.d(xxx, xxx + " Codigo del programa numero  [" + i + ": "
                                + List_Programs.get(i).getProgramSortDescription() + "]" + " PROGRAMAS EN MARCHA");

                        if (List_Programs.get(i).getState().getCode().equals(TypeState.ENDED)) {
                            int_Number_Of_Fragments_To_Show++;
                            List_Programs.remove(i);
                        }

                    }
                }


                for (int i = 0; i < List_Programs.size(); i++) {
                    arrayList_Lista_De_Fiesta_Serializable.add(List_Programs.get(i));
                }
                method_Muestra_Fragment_Con_La_Lista_De_Las_Fiestas(int_Nivel_De_Info_Fiestas);


            }


//            Juan, 10 junio 2015, VER ESTA FORMA DE PASAR PARAMETROS CON PUNTOS SUSPENSIVOS Y EL FOR CON LOS : PUNTOS

//            private static void toggleVisibility(View... views) {
//                for (View view : views) {
//                    boolean isVisible = view.getVisibility() == View.VISIBLE;
//                    view.setVisibility(isVisible ? View.INVISIBLE : View.VISIBLE);
//                }
//            }

        } catch (eefException eef) {
            Log.e("onCreate", "Error inciando la conficuracion");
        } catch (Exception ex) {
            Log.e("onCreate", "Excepotion no eef inciando la configuracion");
        }


    }//Fin de method_Data_To_Show_In_Fragments


    IConfigurationManager myCmT = null;
    private static String xxx;
    List<Program> List_Programs;
    int int_Number_Of_Fragments_To_Show = 0;
    ArrayList<Program> arrayList_Lista_De_Fiesta_Serializable = new ArrayList<Program>();

    int int_Nivel_De_Info_Fiestas = 1;
    int int_Nivel_De_Info_Fiesta_Por_Dias = 2;
    int int_Nivel_De_Info_Fiesta_Eventos_Del_Dia = 3;
    int int_Nivel_De_Info_Fiesta_Evento_Detalle = 4;


    //****************************************************************
    //****************************************************************
    //     FIN del Codigo relacionado con el API de JL
    //****************************************************************
    //****************************************************************

    public void method_Muestra_Fragment_Con_La_Lista_De_Las_Fiestas(int int_Nivel_De_Info) {
// Create new fragment and transaction


        Fragment newFragment = getNewFragmnet(int_Nivel_De_Info);
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack

        if (int_Nivel_De_Info == int_Nivel_De_Info_Fiestas) {
            transaction.add(R.id.details, newFragment);
        } else {
            transaction.replace(R.id.details, newFragment);
            transaction.addToBackStack(null);
        }

// Commit the transaction
        transaction.commit();
    }// Fin de method_Muestra_Fragment_Con_La_Lista_De_Las_Fiestas

    public Fragment getNewFragmnet(int i) {
        Fragment fragment = new MiFragment_Fiestas_Con_RecycleView();
        Bundle args = new Bundle();
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
        args.putString(MiFragment_Fiestas_Con_RecycleView.ARG_OBJECT, "fragment#" + (i + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiestas_Con_RecycleView.ARG_PARAM2, String.valueOf(i + 1));

        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiestas_Con_RecycleView.TIPO_DE_LISTA, i);


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
        args.putSerializable("Lista de Programas Serializado", arrayList_Lista_De_Fiesta_Serializable);


        fragment.setArguments(args);

        return fragment;
    }

    //    Interfaz de la clase CustomAdapter_RecyclerView_Fiestas.
    @Override
    public void onClick(Program m_Programa_seleccionado) {
        Fragment newFragment = getNewFragmentForFiesta(m_Programa_seleccionado);
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack

        transaction.replace(R.id.details, newFragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();

    }

    public Fragment getNewFragmentForFiesta(Program m_Programa_seleccionado) {
        Fragment fragment = new MiFragment_Fiesta_Seleccionada_Con_RecycleView();
        Bundle args = new Bundle();
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "fragment#" + (int_Nivel_De_Info_Fiesta_Por_Dias + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, String.valueOf(int_Nivel_De_Info_Fiesta_Por_Dias + 1));

        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, int_Nivel_De_Info_Fiesta_Por_Dias);


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
        args.putSerializable("Programa Serializado", m_Programa_seleccionado);


        fragment.setArguments(args);

        return fragment;
    }


    //    Interfaz de la clase CustomAdapter_RecyclerView_Fiesta_List_Por_Dias.
    @Override
    public void onClickDia(DayEvents m_DayEvents) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx + "He llegado a onClickDia");
        }
        Fragment newFragment = getNewFragmentForDayEvents(m_DayEvents);
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack

        transaction.replace(R.id.details, newFragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }

    public Fragment getNewFragmentForDayEvents(DayEvents m_DayEvents) {
        Fragment fragment = new MiFragment_Lista_De_Eventos_Del_Dia_Con_RecycleView();
        Bundle args = new Bundle();
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "fragment#" + (int_Nivel_De_Info_Fiesta_Eventos_Del_Dia + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, String.valueOf(int_Nivel_De_Info_Fiesta_Eventos_Del_Dia + 1));

        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, int_Nivel_De_Info_Fiesta_Eventos_Del_Dia);

//        Hago esto por que la lista de eventos no es serializable, asi que lo pongo en array para
//                pasarlo al fragment con la lista de los eventos del dia seleccionado
        ArrayList<Event> arrayList_Lista_De_Eventos_Del_Dia = new ArrayList<Event>();
        for (int i = 0; i < m_DayEvents.getListaEventos().size(); i++) {
            arrayList_Lista_De_Eventos_Del_Dia.add(m_DayEvents.getListaEventos().get(i));
        }


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
        args.putSerializable("Eventos del dia Serializado", arrayList_Lista_De_Eventos_Del_Dia);


        fragment.setArguments(args);

        return fragment;
    }

    //    Interfaz de la clase CustomAdapter_RecyclerView_List_De_Eventos.
    @Override
    public void onClickEvento(Event m_Event) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx + "He llegado a onClickEvento");
        }
        Fragment newFragment = getNewFragmentToShowEvent(m_Event);
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack

        transaction.replace(R.id.details, newFragment);
        transaction.addToBackStack(null);
        // Commit the transaction
        transaction.commit();
    }

    public Fragment getNewFragmentToShowEvent(Event m_Event) {
        Fragment fragment = new MiFragment_Evento();
        Bundle args = new Bundle();
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_OBJECT, "fragment#" + (int_Nivel_De_Info_Fiesta_Evento_Detalle + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiesta_Seleccionada_Con_RecycleView.ARG_PARAM2, String.valueOf(int_Nivel_De_Info_Fiesta_Evento_Detalle + 1));
        //Le paso el tipo de lista a presentar
        args.putInt(MiFragment_Fiesta_Seleccionada_Con_RecycleView.TIPO_DE_LISTA, int_Nivel_De_Info_Fiesta_Evento_Detalle);

        args.putSerializable("Datos del Evento Serializado", m_Event);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onClickEventoDetalle(Event m_Event) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx + "He llegado a onClickEventoDetalle");
        }
    }


    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //****************************************************************
    //  4 Junio 2015 method_Data_To_Show_In_Fragments_2
    //****************************************************************
    //****************************************************************
    private void method_Data_To_Show_In_Fragments_2(Toolbar toolBar_Actionbar) {
//        ESte metodo obtiene datos del api de JL, pero habra que cambiar cosas.

        ConfigurationFactory cmTFactory = new ConfigurationFactory();

        try {
            myCmT = cmTFactory.getConfigurationManager(TypeConfiguration.INSTALLATION_DEFAULT_WITH_AUTONOMOUS_COMMUNITY);

            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(xxx, xxx + " Esta bien configurado [" + myCmT.isRightConfigured() + "]");
                Log.d(xxx, xxx + " El cmt esta configurado[" + myCmT.isRightConfigured() + "] numero AU[" + myCmT.getAutonomousCommunityList().size() + "]");
                Log.d(xxx, xxx + " La comunidad Autonoma Configurada[" + myCmT.getAutonomousCommunityConfigured().getAutonomousCommunityName()
                        + "] y el pueblo configurado[" + myCmT.getEventPlannerConfigured().getName() + "]");
            }

            ProgamListManager pmT = new ProgamListManager();
            pmT.loadData(myCmT.getAutonomousCommunityConfigured(), myCmT.getEventPlannerConfigured());
            if (pmT.existCurrentProgram()) {
                Program p = pmT.getCurrentProgram();

                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(xxx, xxx + "HAY PROGRAMA EN MARCHA [" + p.getProgramSortDescription() + "]");

                }
                //Obtener la lista de eventos del programa por dias
                List<DayEvents> ltd = p.getEventByDay();
                //Obtener la lista de eventos completa
                List<Event> ltf = p.getFullList();
            }


            if (pmT.existCurrentProgram() || pmT.existFuturetPrograms() || pmT.existPastPrograms()) {
                List_Programs = pmT.getFullProgramList();


                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(xxx, "HAY  [" + List_Programs.size() + "]" + " PROGRAMAS EN MARCHA");
                    for (int i = 0; i < List_Programs.size(); i++) {
                        Log.d(xxx, xxx + " Codigo del programa numero  [" + i + ": "
                                + List_Programs.get(i).getState().getCode() + "]" + " PROGRAMAS EN MARCHA");

                        Log.d(xxx, xxx + " Codigo del programa numero  [" + i + ": "
                                + List_Programs.get(i).getProgramSortDescription() + "]" + " PROGRAMAS EN MARCHA");


//                        if(List_Programs.get(i).getState().getCode().equals(TypeState.ENDED)){
//                            int_Number_Of_Fragments_To_Show++;
//                            List_Programs.remove(i);
//                        }

                    }
                }


                toolBar_Actionbar.setTitle(myCmT.getEventPlannerConfigured().getName());


                for (int i = 0; i < List_Programs.size(); i++) {
                    arrayList_Lista_De_Fiesta_Serializable.add(List_Programs.get(i));
                }
                method_Muestra_Fragment_Con_La_Lista_De_Las_Fiestas(int_Nivel_De_Info_Fiestas);


            }


        } catch (eefException eef) {
            Log.e("onCreate", "Error inciando la conficuracion");
        } catch (Exception ex) {
            Log.e("onCreate", "Excepotion no eef inciando la configuracion");
        }


    }//Fin de method_Data_To_Show_In_Fragments_2

    @Override
    public void onClickMenuItenDelNavDrawer(int position_Del_Menu_Seleccionada) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx + "He llegado a onClickMenuItenDelNavDrawer  con click: " + position_Del_Menu_Seleccionada);
        }

        Intent intent;


        switch (position_Del_Menu_Seleccionada)
        {
            case 0:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_frame_layout.
                        Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 1://Mis Eventos
                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_list_of_favorites.
                        Activity_Fragment_List_Of_Favorites_With_Nav_Drawer.class);
                startActivity(intent);
                break;
            case 2:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.activity_actualizar_fiestas.
                        Activity_Fragment_Actualizar_Fiesta_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 3:
//                intent = new Intent(this, com.o3j.es.estamosenfiestas.application_configuration.
//                        Activity_Application_Configuration_WithNavDrawer_2.class);
//                startActivity(intent);

                drawerLayout.closeDrawer(Gravity.LEFT);

                break;
            case 4:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_event_planner_information.
                        Activity_Display_Event_Planner_Information_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 5:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.activity_compartir.
                        Activity_Fragment_Compartir_App_WithNavDrawer_2.class);
                startActivity(intent);
                break;
            case 6:
                intent = new Intent(this, com.o3j.es.estamosenfiestas.display_informacion.
                        Activity_Display_Developer_Information_WithNavDrawer_2.class);
                startActivity(intent);
                break;
        }


        //JUan 25 agosto 2015, esta llamada es solo para pruebas, hay que ponerla en actualizar tambien
//        methodActivarConfiguracionAutomatica_SOLO_PARA_PRUEBAS();


        if (position_Del_Menu_Seleccionada != 3) finish();

//        if(position_Del_Menu_Seleccionada == 2){
//            //Solo muestra el toast
//            Toast.makeText(this, "Opción no disponible", Toast.LENGTH_LONG).show();
//        }else{
//            if (position_Del_Menu_Seleccionada != 3) finish();
//        }
    }


    //****************************************************************
    //****************************************************************
    //     9 Junio 2015: Codigo relacionado con el contextual action mode para
    //    insetar un evento en la lista de favoritos.
    //    Este interfaz es de la clase: CustomAdapter_RecyclerView_List_De_Eventos
    //    Al hacer long click en un evento
    //****************************************************************
    //****************************************************************
    @Override
    public boolean onLongClickEvento_CustomA_List_De_Eventos(Event m_Event, View m_View_Del_Evento) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx + " He llegado a onLongClickEvento_CustomA_List_De_Eventos  con click: ");
        }


        if (mActionMode != null) {
            return false;
        }

        // Start the CAB using the ActionMode.Callback defined above
        mActionMode = this.startActionMode(mActionModeCallback);
        m_View_Del_Evento.setSelected(true);


        return true;
    }


    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_contextual_action_mode_evento_de_la_lista, menu);
            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_agregar_a_favoritos:
                    if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                        Log.d(xxx, xxx + " action_agregar_a_favorito ");
                    }

//                    shareCurrentItem();
                    mode.finish(); // Action picked, so close the CAB


                    return true;
                default:
                    return false;
            }
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };


    ActionMode mActionMode;
    //****************************************************************
    //****************************************************************
    //     FIN de 9 Junio 2015: Codigo relacionado con el contextual action mode para
    //    insetar un evento en la lista de favoritos
    //****************************************************************
    //****************************************************************

    //****************************************************************
    //****************************************************************
    //     11 junio 2015, Instalacion
    //     Inicio de todo lo relacionado con la instalacion
    //****************************************************************
    //****************************************************************
    IConfigurationManager myCmT_Instalacion = null;

    private void method_Gestion_De_Instalacion_de_Pueblos() {

        //Inicializa los broadcast receivers
        myBroadcastReceiver_Progress_Bar = new MyBroadcastReceiver_Progress_Bar();
        intentFilter_1 = new IntentFilter(progressBarBR);
        myBroadcastReceiver_Show_Configuration = new MyBroadcastReceiver_Show_Configuration();
        intentFilter_2 = new IntentFilter(showconfigurationBR);

        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx + "  En metodo method_Gestion_De_Instalacion_de_Pueblos");

        }

        ConfigurationFactory cmTFactory = new ConfigurationFactory();

        try {
            myCmT_Instalacion = cmTFactory.getConfigurationManager(TypeConfiguration.INSTALLATION_DEFAULT_WITH_AUTONOMOUS_COMMUNITY);

            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(xxx, xxx + " Esta bien configurado_2 [" + myCmT_Instalacion.isRightConfigured() + "]");
                Log.d(xxx, xxx + " El cmt esta configurado_2[" + myCmT_Instalacion.isRightConfigured() + "] numero AU["
                        + myCmT_Instalacion.getAutonomousCommunityList().size() + "]");

                for (int i = 0; i < myCmT_Instalacion.getAutonomousCommunityList().size(); i++) {
                    Log.d(xxx, xxx + " :Comunidad " + i + ": "
                            + myCmT_Instalacion.getAutonomousCommunityList().get(i).getAutonomousCommunityName()
                            + "\n");
                    for (int i2 = 0; i2 < myCmT_Instalacion.getAutonomousCommunityList().get(i).getEventPlannerList().size(); i2++) {
                        Log.d(xxx, xxx + " :Pueblo " + i2 + ": "
                                        + myCmT_Instalacion.getAutonomousCommunityList().get(i).getEventPlannerList().size()
                                        + "\n"
                                        + myCmT_Instalacion.getAutonomousCommunityList().get(i).getEventPlannerList().get(i2).getName()
                                        + "\n"
                                        + myCmT_Instalacion.getAutonomousCommunityList().get(i).getEventPlannerList().get(i2).getSortDescription()

                        );
                    }

                }
            }


            for (int i = 0; i < myCmT_Instalacion.getAutonomousCommunityList().size(); i++) {
                arrayList_Lista_De_Comunidades_Serializable.add(myCmT_Instalacion.getAutonomousCommunityList().get(i));
            }
            method_Muestra_Fragment_Con_La_Lista_De_Las_Comunidades();


        } catch (eefException eef) {
            Log.e(xxx, "Error inciando la conficuracion: " + eef.getLocalizedMessage());
        } catch (Exception ex) {
            Log.e(xxx, "Excepotion no eef inciando la configuracion");
        }

    }//Fin de method_Gestion_De_Instalacion_de_Pueblos



    public void method_Muestra_Fragment_Con_La_Lista_De_Las_Comunidades() {
// Create new fragment and transaction


        Fragment newFragment = getNewFragmnet_List_Of_Communities();
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack
// El replace vale aunque sea el primer fragment, asi es como lo tienen en android developers

//        NO, ASI FUNCIONA MAL, EL PRIMERO TIENE QUE SER ADD Y NO HAY QUE USAR addToBackStack.
//        transaction.replace(R.id.details, newFragment);
//        transaction.addToBackStack(null);


        //4 septiembre 2015, cambio el add por replace por que ahora el primer fragment
        // es MiFragmentReconfiguracionBotones, y agrego addToBackStack
//        transaction.add(R.id.details, newFragment);
//        transaction.replace(R.id.details, newFragment);
//        transaction.addToBackStack(null);

        if (Class_ConstantsHelper.boolean_estoy_instalando) {//Este es el primer fragment
        transaction.add(R.id.details, newFragment);
        }else{//Muestra la pantalla con el boton y el texto descriptivo
            //4 septiembre 2015, cambio el add por replace por que ahora el primer fragment
            // es MiFragmentReconfiguracionBotones, y agrego addToBackStack
//        transaction.add(R.id.details, newFragment);
            //Ya ha mostrado el fragment con el boton, asi que hago un replace y add to back stack
            transaction.replace(R.id.details, newFragment);
            transaction.addToBackStack(null);
        }



// Commit the transaction
        transaction.commit();

        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx + "He llegado a method_Muestra_Fragment_Con_La_Lista_De_Las_Comunidades");
        }
    }// Fin de method_Muestra_Fragment_Con_La_Lista_De_Las_Fiestas

    public Fragment getNewFragmnet_List_Of_Communities() {
        Fragment fragment = new MiFragment_Lista_de_Comunidades__Con_RecycleView();
        Bundle args = new Bundle();
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
        args.putString(MiFragment_Fiestas_Con_RecycleView.ARG_OBJECT, "MiFragment_Lista_de_Comunidades__Con_RecycleView");
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiestas_Con_RecycleView.ARG_PARAM2, "1");

        //Le paso el tipo de lista a presentar, pongo cualquier cosa
        args.putInt(MiFragment_Fiestas_Con_RecycleView.TIPO_DE_LISTA, 1);


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
        args.putSerializable("Lista de Comunidades Serializado", arrayList_Lista_De_Comunidades_Serializable);


        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onClickComunidadAutonomaSeleccionada(List<EventPlanner> m_EventPlannerList,
                                                     AutonomousCommunity m_AutonomousCommunity) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx + " He llegado a onClickComunidad AutonomaSeleccionada");
            Log.d(xxx, xxx + " Comunidad AutonomaSeleccionada: " +m_AutonomousCommunity.getAutonomousCommunityName());
        }


        this.m_AutonomousCommunity = m_AutonomousCommunity;

//        Limpio el array para no acumular pueblos
        arrayList_Lista_De_EventPlanner_Serializable.clear();
//        Paso los EventPlanner a un array list que esta serializado
        for (int i = 0; i < m_EventPlannerList.size(); i++) {
            arrayList_Lista_De_EventPlanner_Serializable.add(m_EventPlannerList.get(i));
        }

        Fragment newFragment = getNewFragmnet_List_Of_Towns();
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

// Replace whatever is in the fragment_container view with this fragment,
// and add the transaction to the back stack
// El replace vale aunque sea el primer fragment, asi es como lo tienen en android developers
        transaction.replace(R.id.details, newFragment);
        transaction.addToBackStack(null);

//        transaction.add(R.id.details, newFragment);


// Commit the transaction
        transaction.commit();
    }

    public Fragment getNewFragmnet_List_Of_Towns() {
        Fragment fragment = new MiFragment_Lista_de_Pueblos_Con_RecycleView();
        Bundle args = new Bundle();
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
        args.putString(MiFragment_Fiestas_Con_RecycleView.ARG_OBJECT, "MiFragment_Lista_de_Pueblos_Con_RecycleView");
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(MiFragment_Fiestas_Con_RecycleView.ARG_PARAM2, "1");

        //Le paso el tipo de lista a presentar, pongo cualquier cosa
        args.putInt(MiFragment_Fiestas_Con_RecycleView.TIPO_DE_LISTA, 1);


//        Estoy probando con la clase Progarm como serializable
//        args.putSerializable("Programa Serializado", List_Programs);
        args.putSerializable("Lista de Pueblos Serializado", arrayList_Lista_De_EventPlanner_Serializable);


        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onClickPuebloSeleccionado(EventPlanner m_EventPlanner) {
        //NOTA: mas abajo tengo una copia del original con el api de JL a capon
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, xxx + " He llegado a onClickPuebloSeleccionado");
            Log.e(xxx, xxx + " Ayuntamiento seleccionado: " +m_EventPlanner.getName());
            Log.e(xxx, xxx + " Pueblo seleccionado: " +m_EventPlanner.getSortDescription());
            Log.e(xxx, xxx + " Pueblo longDescription seleccionado: " +m_EventPlanner.getLongDescription());
        }

        //LO dejo comentado, lo use para pruebas antes de modificar la clase Configuration Factory
//           try {
               //Dejo esto comentado, NO LO NECESITO
//               myCmT_Instalacion.persistsConfiguration();
//               myCmT_Instalacion.changeAutonomousCommunityConfigured(m_AutonomousCommunity, m_EventPlanner);
//               myCmT_Instalacion.changeEventPlannerConfigured(m_EventPlanner);
//               myCmT_Instalacion.persistsConfiguration();

//               my_Persist_Configuration_In_DB_CCAA(m_EventPlanner);
//               my_Recupera_Configuration_In_DB_CCAA(m_EventPlanner);

//           }catch (eefException eef) {
//               Log.e(xxx, "Error en changeAutonomous CommunityConfigured1146 : " + eef.getLocalizedMessage());
//           }

        //Ahora hay que lanzar la busqueda de los programas y los eventos con el api de JL
        method_Buscar_Programas_Y_Eventos(m_EventPlanner);

        //    ************************************************************************************************************
//        todo esto lo dejo comentado y descomento linea 1156
//        try {
//
//            myCmT_Instalacion.changeAutonomousCommunityConfigured(m_AutonomousCommunity, m_EventPlanner);
//            myCmT_Instalacion.persistsConfiguration();
//
//            Class_ConstantsHelper.boolean_Hay_Una_nueva_configuracion = true;
//            //Limpio la configuracion si la habia
//            Class_ConstantsHelper.m_IConfigurationManager = null;
//            Class_ConstantsHelper.m_IConfigurationManager = myCmT_Instalacion;
//
//            Intent intent = new Intent(this, com.o3j.es.estamosenfiestas.display_frame_layout.
//                    Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
//            startActivity(intent);
//
////               Limpio el array list de favoritos para que no muestre datos de otros ayuntamientos
//            Class_ConstantsHelper.arraylist_Datos_Lista_Favoritos.clear();
//
//
//            finish();
//
//
//        }catch (eefException eef) {
//            Log.e(xxx, "Error en changeAutonomous CommunityConfigured: " + eef.getLocalizedMessage());
//        }
//    *************************************************************************************************************

    }//Fin de onClickPuebloSeleccionado

//    public void my_Persist_Configuration_In_DB_CCAA(IConfigurationManager myCmT_Instalacion) throws eefException {
        public void my_Persist_Configuration_In_DB_CCAA(EventPlanner m_EventPlanner) throws eefException {

        //Prueba de Juan Miguel 27 julio 2015
        Dao configuracionDao;
        try {
            configuracionDao = getHelper().getConfiguracionDao();
            Configuracion configuration = new Configuracion();
//            configuration.setCcaaId(myCmT_Instalacion.getAutonomousCommunityConfigured().getAutonomousCommunityId());
//            configuration.setEventPlannerId(myCmT_Instalacion.getEventPlannerConfigured().getEventPlannerId());
            configuration.setCcaaId(m_EventPlanner.getAutonomousCommunityId());
            configuration.setEventPlannerId(m_EventPlanner.getEventPlannerId());
            configuration.setFechaConfiguracion(new Date());
            configuracionDao.create(configuration);


        } catch (SQLException e) {
            Log.e(xxx, xxx + "Error creando comunidad: " +e);
        }



        //***************************************************************************************
        /*configuration.setAutonomousCommunityList(SpainAutonomousCommunity.getAutonomousCommunityList());
            configuration.setEventPlannerId(3);*/
           /* ConfigurationManager cmT = new ConfigurationManager(TypeConfiguration.AUTONOMOUS_COMMUNITY_ENABLED);
            cmT = getTestCommunity2(cmT);
            return (IConfigurationManager) cmT;*/
//        if(!isconfigured())
//            return new ConfigurationManager(configurationType);
//
//                }
//                //If there is not defined a Configuration Manager for the Configuration Type throws an Exception
//                throw new eefException(TypeEefError.NO_CONFIGURATION_FOR_TYPE);
//            }
//
//                private boolean isconfigured()
//                {
//                    t
//                    return false;
//                }
        //***************************************************************************************

    }

//    public void my_Recupera_Configuration_In_DB_CCAA(IConfigurationManager myCmT_Instalacion) throws eefException {
        public void my_Recupera_Configuration_In_DB_CCAA(EventPlanner m_EventPlanner) throws eefException {

        //Prueba de Juan Miguel 27 julio 2015


        Dao configuracionDao;
        try {
            configuracionDao = getHelper().getConfiguracionDao();
            Configuracion configuration = (Configuracion) configuracionDao.queryForId(1);
            List<Configuracion> todas_CCAA =  configuracionDao.queryForAll();

            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                if (configuration == null) {
                    Log.e(xxx, xxx + ": No hay configuracion con  id = 1");
                } else {
                    Log.e(xxx, xxx + ": Recuperado configuracion con id = 1: " + configuration.getCcaaId());
                }

                if (todas_CCAA == null) {
                    Log.e(xxx, xxx + ": No hay CCAA");
                } else {
                    for (int i = 0; i < todas_CCAA.size(); i++) {
                        Log.e(xxx, xxx + ": Recuperado CCAA IDs: " + todas_CCAA.get(i).getCcaaId());
                        Log.e(xxx, xxx + ": Recuperado event planner IDs: " + todas_CCAA.get(i).getEventPlannerId());
                    }
                    Log.e(xxx, xxx + ": numero de CCAAs= " +todas_CCAA.size());

                }
            }

            //Borro la info de la tabla configuracion
            if (!todas_CCAA.isEmpty()) {
                int int_registros_borrados = configuracionDao.delete(todas_CCAA);
                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.e(xxx, xxx + ": int_registros_borrados= " + int_registros_borrados);
                }
            }

            //Que pasa si despues de borrar intento recuperar otra vez?

            //Si no hay registros, nunca me devuelve un null, devuelve la lista vacia
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                List<Configuracion> todas_CCAA_despues_de_borrar =  configuracionDao.queryForAll();
                if (todas_CCAA_despues_de_borrar == null) {
                    Log.e(xxx, xxx + ": todas_CCAA_despues_de_borrar es NULL");
                    if (todas_CCAA_despues_de_borrar.isEmpty()) {
                        Log.e(xxx, xxx + ": todas_CCAA_despues_de_borrar.isEmpty(), y funciona si es null");
                    }
                } else {
                    for (int i = 0; i < todas_CCAA_despues_de_borrar.size(); i++) {
                        Log.e(xxx, xxx + ": Recuperado todas_CCAA_despues_de_borrar.isEmpty() IDs: " + todas_CCAA_despues_de_borrar.get(i).getCcaaId());
                        Log.e(xxx, xxx + ": Recuperado todas_CCAA_despues_de_borrar.isEmpty() event planner IDs: " + todas_CCAA_despues_de_borrar.get(i).getEventPlannerId());
                    }

                    Log.e(xxx, xxx + ": numero de todas_CCAA_despues_de_borrar= " +todas_CCAA_despues_de_borrar.size());

                }
            }

            //Si no hay registros, nunca me devuelve un null, devuelve la lista vacia
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                List<Configuracion> todas_CCAA_despues_de_borrar =  configuracionDao.queryForAll();
                    if (todas_CCAA_despues_de_borrar.isEmpty()) {
                        Log.e(xxx, xxx + ": todas_CCAA_despues_de_borrar.isEmpty()__paso 2, y funciona si es null");
                    }

            }

        } catch (SQLException e) {
            Log.e(xxx, xxx + "Error recuperando configuracion: " +e);
        }
    }

    private DBHelper mDBHelper;

    private DBHelper getHelper() {
        if (mDBHelper == null) {
            mDBHelper = OpenHelperManager.getHelper(this, DBHelper.class);
        }
        return mDBHelper;
    }

    //****************************************************************
    //****************************************************************
    //     27 julio 2015: copia de seguridad de este metodo.
    //     ASI ESTABA FUNCIONANDO CON EL API VIEJO DE JL
    //****************************************************************
    //****************************************************************

//    @Override
//    public void onClickPuebloSeleccionado(EventPlanner m_EventPlanner) {
//        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
//            Log.e(xxx, xxx + " He llegado a onClickPuebloSeleccionado");
//            Log.e(xxx, xxx + " Ayuntamiento seleccionado: " +m_EventPlanner.getName());
//            Log.e(xxx, xxx + " Pueblo seleccionado: " +m_EventPlanner.getSortDescription());
//            Log.e(xxx, xxx + " Pueblo longDescription seleccionado: " +m_EventPlanner.getLongDescription());
//        }
//
////        Salvar la nueva configuracion
//
//    ************************************************************************************************************
//        try {
//
//            myCmT_Instalacion.changeAutonomousCommunityConfigured(m_AutonomousCommunity, m_EventPlanner);
//            myCmT_Instalacion.persistsConfiguration();
//
//            Class_ConstantsHelper.boolean_Hay_Una_nueva_configuracion = true;
//            //Limpio la configuracion si la habia
//            Class_ConstantsHelper.m_IConfigurationManager = null;
//            Class_ConstantsHelper.m_IConfigurationManager = myCmT_Instalacion;
//
//            Intent intent = new Intent(this, com.o3j.es.estamosenfiestas.display_frame_layout.
//                    Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
//            startActivity(intent);
//
////               Limpio el array list de favoritos para que no muestre datos de otros ayuntamientos
//            Class_ConstantsHelper.arraylist_Datos_Lista_Favoritos.clear();
//
//
//            finish();
//
//
//        }catch (eefException eef) {
//            Log.e(xxx, "Error en changeAutonomous CommunityConfigured: " + eef.getLocalizedMessage());
//        }
//    *************************************************************************************************************
//
//    }

    private ArrayList<AutonomousCommunity> arrayList_Lista_De_Comunidades_Serializable = new ArrayList<AutonomousCommunity>();

//    Lista con los pueblos de la comunidad clickada
    private ArrayList<EventPlanner> arrayList_Lista_De_EventPlanner_Serializable = new ArrayList<EventPlanner>();

    private AutonomousCommunity m_AutonomousCommunity;


    //****************************************************************
    //****************************************************************
    //     FIN  de Inicio de todo lo relacionado con la instalacion
    //****************************************************************
    //****************************************************************




    //****************************************************************
    //****************************************************************
    //     24 julio 2015, Instalacion
    //     Inicio de todo lo relacionado con el loader y retrofit
    //****************************************************************
    //****************************************************************
    ConfigurationFactory cmTFactory;
    private void method_Gestion_De_Instalacion_de_Pueblos_2() {

        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx + "  En metodo method_Gestion_De_Instalacion_de_Pueblos_2");

        }


//        ConfigurationFactory cmTFactory = new ConfigurationFactory();
        cmTFactory = new ConfigurationFactory();
        cmTFactory.initFactory(this, progressBarBR, showconfigurationBR );


        //25 julio 2015: lo comento para probar solo el progress bar
        try {

            cmTFactory.getConfigurationManagerFromServer(TypeConfiguration.INSTALLATION_DEFAULT_WITH_AUTONOMOUS_COMMUNITY);
//            method_Iniciar_Progress_Dialog();



        } catch (eefException eef) {
            Log.e(xxx, "Error inciando la conficuracion: " + eef.getLocalizedMessage());
            method_Mostar_Error_De_Descarga_De_Datos_2();

        }

        //25 julio 2015: probando solamente el progress bar
//        method_Iniciar_Progress_Dialog();






    }//Fin de method_Gestion_De_Instalacion_de_Pueblos_2

    //ID DEL BROADCAST PARA INDICAR EL FIN DE LA DESCARGA DE UNA COMUNIDAD
    String progressBarBR = "PROGRESS_BAR_1";
    //ID DEL BROADCAST PARA INDICAR QUE LA CONFIGURACION ESTA PREPARADA.
    String showconfigurationBR = "SHOW_CONFIGURATION_1";

    MyBroadcastReceiver_Progress_Bar myBroadcastReceiver_Progress_Bar;
    IntentFilter intentFilter_1;
    boolean boolean_Init_Progress_Dialog = true;
    public class MyBroadcastReceiver_Progress_Bar extends BroadcastReceiver {


        @Override
        public void onReceive(Context context, Intent intent) {

            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
            {
                Log.d(xxx, xxx + " MyBroadcastReceiver_Progress_Bar");
            }

            Bundle bundle = intent.getExtras();
            if (bundle != null) {

                int int_1 = bundle.getInt("comunidades_acabadas");
                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
                {
                    Log.e(xxx, xxx + " MyBroadcastReceiver_Progress_Bar: " +int_1);
                }



                if (boolean_Init_Progress_Dialog) {
                    boolean_Init_Progress_Dialog = false;
                    method_Iniciar_Progress_Dialog();
                }

                method_Actualiza_Progress_Dialog();

            }
        }
    }//Fin de MyBroadcastReceiver_Progress_Bar

    MyBroadcastReceiver_Show_Configuration myBroadcastReceiver_Show_Configuration;
    IntentFilter intentFilter_2;
    public class MyBroadcastReceiver_Show_Configuration extends BroadcastReceiver {


        @Override
        public void onReceive(Context context, Intent intent) {

            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
            {
                Log.e(xxx, xxx + "  MyBroadcastReceiver_Show_Configuration");
            }

            //4 septiembre: siempre limpio el array antes de rellenarlo
            arrayList_Lista_De_Comunidades_Serializable.clear();



            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                //4 septiembre 2015: Escribo en el xml que ya esta configurado, independiente de si
                //hay o no programas de fiestas que instalar, y lo hago siempre que entro en reconfigurar,
                //ya sea la primera instalacion o no.
                ClaseConfigurationHelper claseConfigurationHelper =
                        ClaseConfigurationHelper.newInstance(Activity_Application_Configuration_WithNavDrawer_2.this);
                claseConfigurationHelper.methodSetAppInstalada();


                //25 julio: el serializable cmT no funciona en el intent, da error, lo dejo comentado
//                ConfigurationManager myCmT_Instalacion = (ConfigurationManager) bundle.getSerializable("cmt");


                ConfigurationFromServer configurationFromServer =
                        (ConfigurationFromServer) bundle.getParcelable("configuration");

                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.e(xxx, xxx + "  MyBroadcastReceiver_Show_Configuration configurationFromServer: "

                            + configurationFromServer.areConfigurationAvailable());
                }


//                ConfigurationFromServer configurationFromServer =
//                        (ConfigurationFromServer) intent.getParcelableExtra("configuration");

                if (configurationFromServer == null) {
                    if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                        Log.e(xxx, xxx + "  MyBroadcastReceiver_Show_Configuration: " + "configurationFromServer == null");
                    }
                }

                //25 julio: hasta que JL lo arregle, me mando un string
//                broadcastIntent.putExtra("cmt", "me estoy enviando un string");
                int string_1 = bundle.getInt("comunidades_acabadas");
                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.e(xxx, xxx + "  MyBroadcastReceiver_Show_Configuration: " + string_1);
                }

                progressDialog.dismiss();

                //Ya tengo el cmTFactory desde method_Gestion_De_Instalacion_de_Pueblos_2
//                ConfigurationFactory cmTFactory = new ConfigurationFactory();

                try {

                    myCmT_Instalacion = cmTFactory.loadConfigrationManagerFromConfigurationFromServer
                            (configurationFromServer, TypeConfiguration.INSTALLATION_DEFAULT_WITH_AUTONOMOUS_COMMUNITY);


                } catch (eefException eef) {
                    Log.e(xxx, "MyBroadcastReceiver_Show_Configuration, Error inciando la conficuracion: " + eef.getLocalizedMessage());

                    method_Mostar_Error_De_Descarga_De_Datos_2();
                }


                //25 julio, esto lo dejo comentado hasta que funcione el serializable
                try {
//
//                    if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
//                        Log.d(xxx, xxx + " Esta bien configurado_2 [" + myCmT_Instalacion.isRightConfigured() + "]");
//                        Log.d(xxx, xxx + " El cmt esta configurado_2[" + myCmT_Instalacion.isRightConfigured() + "] numero AU["
//                                + myCmT_Instalacion.getAutonomousCommunityList().size() + "]");
//
//                        for (int i = 0; i < myCmT_Instalacion.getAutonomousCommunityList().size(); i++) {
//                            Log.d(xxx, xxx + " :Comunidad " + i + ": "
//                                    + myCmT_Instalacion.getAutonomousCommunityList().get(i).getAutonomousCommunityName()
//                                    + "\n");
//                            for (int i2 = 0; i2 < myCmT_Instalacion.getAutonomousCommunityList().get(i).getEventPlannerList().size(); i2++) {
//                                Log.d(xxx, xxx + " :Pueblo " + i2 + ": "
//                                                + myCmT_Instalacion.getAutonomousCommunityList().get(i).getEventPlannerList().size()
//                                                + "\n"
//                                                + myCmT_Instalacion.getAutonomousCommunityList().get(i).getEventPlannerList().get(i2).getName()
//                                                + "\n"
//                                                + myCmT_Instalacion.getAutonomousCommunityList().get(i).getEventPlannerList().get(i2).getSortDescription()
//
//                                );
//                            }
//
//                        }
//                    }
//
//

                    //4 septiembre: siempre limpio el array antes de rellenarlo
                    for (int i = 0; i < myCmT_Instalacion.getAutonomousCommunityList().size(); i++) {
                        arrayList_Lista_De_Comunidades_Serializable.add(myCmT_Instalacion.getAutonomousCommunityList().get(i));
                    }


                    method_Muestra_Fragment_Con_La_Lista_De_Las_Comunidades();
//
//
//                } catch (eefException eef) {
//                    Log.e(xxx, "MyBroadcastReceiver_Show_Configuration, Error inciando la conficuracion: " + eef.getLocalizedMessage());
                } catch (Exception ex) {
                    Log.e(xxx, "MyBroadcastReceiver_Show_Configuration, Excepotion no eef inciando la configuracion");
                    method_Mostar_Error_De_Descarga_De_Datos_2();

                }


            }
        }
    }//FIN DE MyBroadcastReceiver_Show_Configuration nnnnn


    @Override
    public void onResume() {
        super.onResume();
        //Registra los receptores
        registerReceiver(myBroadcastReceiver_Progress_Bar, intentFilter_1);
        registerReceiver(myBroadcastReceiver_Show_Configuration, intentFilter_2);
        registerReceiver(myBroadcastReceiver_Progress_Bar_Programas_y_Eventos, intentFilter_3);
        registerReceiver(myBroadcastReceiver_Fin_Carga_Programs_Y_Eventos_From_Server, intentFilter_4);
    }

    @Override
    public void onPause() {
        super.onPause();
        //Elimina el registro de los receptores
        unregisterReceiver(myBroadcastReceiver_Progress_Bar);
        unregisterReceiver(myBroadcastReceiver_Show_Configuration);
        unregisterReceiver(myBroadcastReceiver_Progress_Bar_Programas_y_Eventos);
        unregisterReceiver(myBroadcastReceiver_Fin_Carga_Programs_Y_Eventos_From_Server);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Destruyo el helper
        if (mDBHelper != null) {
            OpenHelperManager.releaseHelper();
            mDBHelper = null;
        }
    }
    ProgressDialog progressDialog;
    private void method_Iniciar_Progress_Dialog() {
//        Como en:
//        http://www.compiletimeerror.com/2013/09/android-progress-dialog-example.html#.VbO_KvntmM0
        progressDialog = new ProgressDialog(this);
        progressDialog.setMax(100);
//        progressDialog.setMessage("Cargando datos....");
//        progressDialog.setTitle("Configuración de la aplicación");
        progressDialog.setMessage(getResources().getString(R.string.progress_bar_mensaje_1));
        progressDialog.setTitle(getResources().getString(R.string.progress_bar_titulo_1));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.show();

        //LO comento, actualizo el progress bar con el receiver, use el thread para probarlo
//        method_Thread_Progress_Dialog();
    }


    private void method_Actualiza_Progress_Dialog() {
//        while (progressDialog.getProgress() <= progressDialog
//                .getMax()) {
//            progressDialog.incrementProgressBy(6);
//            if (progressDialog.getProgress() >= progressDialog
//                    .getMax())
//            {
//                progressDialog.dismiss();
//            }
//        }

        progressDialog.incrementProgressBy(4);

    }

    private void method_Thread_Progress_Dialog() {
       final Thread thread =  new Thread(new Runnable() {

           Handler handle = new Handler() {
               @Override
               public void handleMessage(Message msg) {
                   super.handleMessage(msg);
                   progressDialog.incrementProgressBy(1);
               }
           };

            @Override
            public void run() {
                try {
                    while (progressDialog.getProgress() <= progressDialog
                            .getMax()) {
                        Thread.sleep(200);
                        handle.sendMessage(handle.obtainMessage());
                        if (progressDialog.getProgress() == progressDialog
                                .getMax()) {
                            progressDialog.dismiss();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
       });
//        }).start();

        thread.start();
    }

    //****************************************************************
    //****************************************************************
    //     Fin de todo lo relacionado con el loader y retrofit
    //****************************************************************
    //****************************************************************

    //****************************************************************
    //****************************************************************
    //     28 julio 2015, Instalacion
    //     Inicio de todo lo relacionado con el loader y los dos broadcast receiver
    //     de descargar los programas y eventos y guardar en sqlite
    //****************************************************************
    //****************************************************************

    //ID DEL BROADCAST PARA INDICAR EL PROGRESO DE LA DESCARGA DE PROGRAMAS Y EVENTOS
    String broadcast_Programas_Y_Eventos = "broadcast_Programas_Y_Eventos";
    //ID DEL BROADCAST PARA INDICAR EL FIN DE LA DESCARGA DE PROGRAMAS Y EVENTOS
    String broadcast_Fin_Descarga_De_Programas_Y_Eventos = "broadcast_Fin_Descarga_De_Programas_Y_Eventos";

    MyBroadcastReceiver_Progress_Bar_Programas_y_Eventos myBroadcastReceiver_Progress_Bar_Programas_y_Eventos;
    IntentFilter intentFilter_3;
    boolean boolean_Init_Progress_Dialog_2 = true;
    public class MyBroadcastReceiver_Progress_Bar_Programas_y_Eventos extends BroadcastReceiver {


        @Override
        public void onReceive(Context context, Intent intent) {

            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
            {
                Log.e(xxx, xxx + " MyBroadcastReceiver_Progress_Bar_Programa_y_Eventos");
            }

            Bundle bundle = intent.getExtras();
            if (bundle != null) {

                int NUMERO_PROGRAMAS_A_DESCARGA = bundle.getInt(DownloadInfoFromServerManager.NUMERO_PROGRAMAS_A_DESCARGA, 0);
                String DESCARGADO_INFO_EVENT_PLANNER = bundle.getString(DownloadInfoFromServerManager.DESCARGADO_INFO_EVENT_PLANNER, "string por defecto");
                String DESCARGADAS_LISTAS_DE_PROGRAMAS = bundle.getString(DownloadInfoFromServerManager.DESCARGADAS_LISTAS_DE_PROGRAMAS, "string por defecto");
                String CONTINUAR_DESCARGA = bundle.getString(DownloadInfoFromServerManager.CONTINUAR_DESCARGA, "string por defecto");
                String PROGRAMA_DESCARGADO = bundle.getString(DownloadInfoFromServerManager.PROGRAMA_DESCARGADO, "string por defecto");
                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
                {
                    Log.e(xxx, xxx + " MyBroadcastReceiver_Progress_Bar_Programa_y_Eventos: " +"\n"
                                    +"NUMERO_PROGRAMAS_A_DESCARGA" +NUMERO_PROGRAMAS_A_DESCARGA +"\n"
                                    +"DESCARGADO_INFO_EVENT_PLANNER" +DESCARGADO_INFO_EVENT_PLANNER +"\n"
                                    +"DESCARGADAS_LISTAS_DE_PROGRAMAS" +DESCARGADAS_LISTAS_DE_PROGRAMAS +"\n"
                                    +"CONTINUAR_DESCARGA" +CONTINUAR_DESCARGA +"\n"
                                    +"PROGRAMA_DESCARGADO" +PROGRAMA_DESCARGADO);
                }




                if (boolean_Init_Progress_Dialog_2) {
                    boolean_Init_Progress_Dialog_2 = false;
                    method_Iniciar_Progress_Dialog_Programas_Y_Eventos();
                }

                //Por ahora no llamo a incrementar y uso el spinner
//                method_Actualiza_Progress_Dialog_Programas_Y_Eventos();


            }
        }
    }//Fin de MyBroadcastReceiver_Progress_Bar_Programas_y_Eventos

    MyBroadcastReceiver_Fin_Carga_Programs_Y_Eventos_From_Server myBroadcastReceiver_Fin_Carga_Programs_Y_Eventos_From_Server;
    IntentFilter intentFilter_4;
    public class MyBroadcastReceiver_Fin_Carga_Programs_Y_Eventos_From_Server extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            progressDialog.dismiss();

            //Vuelvo a poner true esta variable por si este pueblo no tiene nada y selecciono otro pueblo
            boolean_Init_Progress_Dialog_2 = true;


            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
            {
                Log.e(xxx, xxx + "  MyBroadcastReceiver_Fin_Carga_Programs_Y_Eventos_From_Server");
            }



            Bundle bundle = intent.getExtras();
            if (bundle != null) {

                //Este broadcast es llamado desde DownloadInfoFromServerManager.sendShowConfigurationBRBroadcast

                boolean boolean_Programas_Y_Eventos_Descargados_De_Internet_Correctos = true;
                String string_Error_En_La_Descarga = bundle.getString(DownloadInfoFromServerManager.ERROR_DESCARGA, "no_hay_datos_de_error");
                String CONFIGURACION_PROGRAMAS_EN_DB = bundle.getString(DownloadInfoFromServerManager.CONFIGURACION_PROGRAMAS_EN_DB);

                if(string_Error_En_La_Descarga.equals("no_hay_datos_de_error")) {
                     if(CONFIGURACION_PROGRAMAS_EN_DB.equals("true")){
                          //Los datos del pueblo seleccionado ya estan guardados en sqlite.
                         boolean_Programas_Y_Eventos_Descargados_De_Internet_Correctos = true;
                         if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
                         {
                             Log.e(xxx, xxx + "  MyBroadcastReceiver_Fin_Carga_Programs_Y_Eventos_From_Server: "
                             +"Valor de CONFIGURACION_PROGRAMAS_EN_DB = " +CONFIGURACION_PROGRAMAS_EN_DB);
                         }
                     }else{
                         boolean_Programas_Y_Eventos_Descargados_De_Internet_Correctos = false;
                         if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
                         {
                             Log.e(xxx, xxx + "  MyBroadcastReceiver_Fin_Carga_Programs_Y_Eventos_From_Server: "
                                     +"Valor de CONFIGURACION_PROGRAMAS_EN_DB = " +CONFIGURACION_PROGRAMAS_EN_DB);
                         }
                     }
                }else{
                    boolean_Programas_Y_Eventos_Descargados_De_Internet_Correctos = false;
                    if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
                    {
                        Log.e(xxx, xxx + "  MyBroadcastReceiver_Fin_Carga_Programs_Y_Eventos_From_Server: "
                                +"Valor de string_Error_En_La_Descarga = " +string_Error_En_La_Descarga);
                    }
                }


                if (boolean_Programas_Y_Eventos_Descargados_De_Internet_Correctos) {

                    //Los datos del pueblo seleccionado ya estan guardados en sqlite. LLamar a la activity para
                    //presentar los datos
                    method_Muestra_Actividad_Programas_De_Las_Fiestas();

                }else {
                    method_Mostar_Error_De_Descarga_De_Datos_2();
                }

            }
        }
    }//FIN DE MyBroadcastReceiver_Fin_Carga_Programs_Y_Eventos_From_Server

    private void method_Muestra_Actividad_Programas_De_Las_Fiestas() {

        //Aqui he terminado la configuracion. Llamar a la activity de programas de fiestas
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
        {
            Log.e(xxx, xxx + ": He llegado a  method_Muestra_Actividad_Programas_De_Las_Fiestas, la configuracion esta correcta ");
        }

        //Si la instalacion o reconfiguracion ha terminado bien, reseteo la variable de
        Class_ConstantsHelper.boolean_estoy_instalando = false;

        Intent intent;
        intent = new Intent(this, com.o3j.es.estamosenfiestas.display_frame_layout.
                Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
        startActivity(intent);
        finish();
    }



    private void method_Iniciar_Progress_Dialog_Programas_Y_Eventos() {
        progressDialog = null;
//        Como en:
//        http://www.compiletimeerror.com/2013/09/android-progress-dialog-example.html#.VbO_KvntmM0
        progressDialog = new ProgressDialog(this);
        progressDialog.setMax(100);
//        progressDialog.setMessage("Cargando datos....");
//        progressDialog.setTitle("Configuración de la aplicación");
        progressDialog.setMessage(getResources().getString(R.string.progress_bar_mensaje_1));
        progressDialog.setTitle(getResources().getString(R.string.progress_bar_titulo_1));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        //LO comento, actualizo el progress bar con el receiver, use el thread para probarlo
//        method_Thread_Progress_Dialog();
    }

    private void method_Actualiza_Progress_Dialog_Programas_Y_Eventos() {

        progressDialog.incrementProgressBy(1);

    }



    private void method_Buscar_Programas_Y_Eventos(EventPlanner m_EventPlanner) {
        //Juan, 24 agosto 2015: ahora aqui tengo que averiguar si tengo una configuracion o no
        //para diferenciar si llamo a
        //Primera instalacion: DownloadInfoFromServerManager.getOrganizerFullInformationFromServer
        //reconfiguracion: DownloadInfoFromServerManager.getOrganizerFullInformationForReconfigureFromServer
        //hay que usar databaseServerManager.getConfiguracion();
        //Si salta la exception, tengo que instalar, si no, tengo que reconfigurar

//        EventPlanner eventPlanner = null;

        //Si salta la exception, voy a method_Buscar_Programas_Y_Eventos_2 para instalar el programa

        DatabaseServerManager databaseServerManager = new DatabaseServerManager();
        databaseServerManager.initManager(this);

        try {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
            {
                Log.e(xxx, xxx + ":  En metodo  method_Buscar_Programas_Y_Eventos ");
            }
            databaseServerManager.getConfiguracion();
            //Si no salta la exception, llamo a method_Buscar_Programas_Y_Eventos_Reconfigurar
            method_Buscar_Programas_Y_Eventos_Reconfigurar(m_EventPlanner);
        } catch (eefException eef) {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
            {
                Log.e(xxx, xxx + ": en metodo  method_Buscar_Programas_Y_Eventos Hay un ERROR Catch 2" +
                        "databaseServerManager.getConfiguracion(): " +eef.getLocalizedMessage());
            }
            //Si salta la exception, voy a method_Buscar_Programas_Y_Eventos_Instalacion para instalar el programa
            method_Buscar_Programas_Y_Eventos_Instalacion(m_EventPlanner);
        }


    }//Fin de method_Buscar_Programas_Y_Eventos


    private void method_Buscar_Programas_Y_Eventos_Instalacion(EventPlanner m_EventPlanner) {


//        EventPlanner eventPlanner = null;


            DownloadInfoFromServerManager downloadInfoFromServerManager = new DownloadInfoFromServerManager();
            downloadInfoFromServerManager.initManager(this, broadcast_Programas_Y_Eventos, broadcast_Fin_Descarga_De_Programas_Y_Eventos);
//            method_Iniciar_Progress_Dialog_Programas_Y_Eventos();
            try {
                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
                {
                    Log.e(xxx, xxx + ":  En metodo  method_Buscar_Programas_Y_Eventos_Instalacion ");
                }
                downloadInfoFromServerManager.getOrganizerFullInformationFromServer(m_EventPlanner);

//                methodActivarConfiguracionAutomatica_SOLO_PARA_PRUEBAS();

//                method_Iniciar_Progress_Dialog_Programas_Y_Eventos();
            } catch (eefException eef) {
                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
                {
                    Log.e(xxx, xxx + ": en metodo  method_Buscar_Programas_Y_Eventos_Instalacion Hay un ERROR Catch " +
                            "getOrganizerFullInformationFromServer: " +eef.getLocalizedMessage());
                }
            }


    }//Fin de method_Buscar_Programas_Y_Eventos_Instalacion

    private void method_Buscar_Programas_Y_Eventos_Reconfigurar(EventPlanner m_EventPlanner) {


//        EventPlanner eventPlanner = null;


        DownloadInfoFromServerManager downloadInfoFromServerManager = new DownloadInfoFromServerManager();
        downloadInfoFromServerManager.initManager(this, broadcast_Programas_Y_Eventos, broadcast_Fin_Descarga_De_Programas_Y_Eventos);
//            method_Iniciar_Progress_Dialog_Programas_Y_Eventos();
        try {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
            {
                Log.e(xxx, xxx + ":  En metodo  method_Buscar_Programas_Y_Eventos_Reconfigurar ");
            }
            downloadInfoFromServerManager.getOrganizerFullInformationForReconfigureFromServer(m_EventPlanner);

//            methodActivarConfiguracionAutomatica_SOLO_PARA_PRUEBAS();

//                method_Iniciar_Progress_Dialog_Programas_Y_Eventos();
        } catch (eefException eef) {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
            {
                Log.e(xxx, xxx + ": en metodo  method_Buscar_Programas_Y_Eventos_Reconfigurar Hay un ERROR Catch " +
                        "getOrganizerFullInformationForReconfigureFromServer: " +eef.getLocalizedMessage());
            }
        }


    }//Fin de method_Buscar_Programas_Y_Eventos_Reconfigurar

    private void methodActivarConfiguracionAutomatica_SOLO_PARA_PRUEBAS() {
        Log.e(xxx, xxx + ":  En metodo  methodActivarConfiguracionAutomatica_SOLO_PARA_PRUEBAS ");

        //Esto es solo para probar, tambien hay que ponerla en actualizar
        ClaseActualizacionAutomatica claseActualizacionAutomatica = ClaseActualizacionAutomatica.newInstance(this);
        claseActualizacionAutomatica.methodGuardaFechaInicial(new Date());
    }

    private void method_Guardar_Datos_En_DB(FullInfoEventPlannerFromServer fullInfoEventPlannerFromServer) {
        //AQUIAUIAUIAUIAIUAIUAIUAIUAIUAIUAIUAIUAIUAIUAIUA

        //Juan, 16 agosto 2015: este metodo ya no se usa, ahora jose luis guarda en sqlite en un solo paso

        //Aqui tengo que inicializar el loader para guardar en sqlite
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
        {
            Log.e(xxx, xxx + ": He llegado a  method_Guardar_Datos_En_DB con el fullInfoEventPlannerFromServer");
            metodoPresentaDialogDePrueba();
        }
    }



    private void method_Mostar_Error_De_Descarga_De_Datos_2() {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
        {
            Log.e(xxx, xxx + ": He llegado a  method_Mostar_Error_De_Descarga_De_Datos_2: "
                    + "ERROR en la descarga de datos: " );
        }

        //Juan, 26 agosto 2015: compruebo se istoy instalando o reconfigurando
        if (Class_ConstantsHelper.boolean_estoy_instalando) {
            //Reseteo esta variable
            Class_ConstantsHelper.boolean_estoy_instalando = false;

            // Create an instance of the dialog fragment and show it
            DialogFragment dialog = ClaseDialogGenericaDosTresBotones.newInstance
                    (R.string.titulo_dialog_method_Mostar_Error_De_Descarga_De_Datos_2,
                            R.string.message_instalacion_dialog_method_Mostar_Error_De_Descarga_De_Datos_2, R.string.positiveButton,
                            R.string.negativeButton, 1, R.string.neutralButton_dialog_method_Mostar_Error_De_Descarga_De_Datos_2);
            dialog.show(getSupportFragmentManager(), "ClaseDialogGenericaDosTresBotones");

        } else {

            // Create an instance of the dialog fragment and show it
            DialogFragment dialog =  ClaseDialogNoHayInternet.newInstance
                    (R.string.titulo_dialog_method_Mostar_Error_De_Descarga_De_Datos_2,
                            R.string.message_configuracion_dialog_method_Mostar_Error_De_Descarga_De_Datos_2, R.string.positiveButton_error_no_hay_internet_reconfigurar,
                            R.string.negativeButton_error_no_hay_internet_reconfigurar, 1,
                            R.string.neutralButton_dialog_method_Mostar_Error_De_Descarga_De_Datos_2);
            dialog.show(getSupportFragmentManager(), "ClaseDialogNoHayInternet");
        }

    }//Fin de method_Mostar_Error_De_Descarga_De_Datos_2

    private void metodoPresentaDialogDePrueba() {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
        {
            Log.e(xxx, xxx + ": He llegado a  metodoPresentaDialogDePrueba: ");
        }
        // Create an instance of the dialog fragment and show it
        DialogFragment dialog =  ClaseDialogGenericaDosTresBotones.newInstance
                (R.string.title, R.string.description, R.string.positiveButton,
                        R.string.negativeButton, 3, R.string.boton_neutral);
        dialog.show(getSupportFragmentManager(), "ClaseDialogGenericaDosTresBotones");
    }

    //Metodos de la interface del dialogo de la clase ClaseDialogGenericaDosTresBotones
    @Override
    public void onDialogPositiveClick_ClaseDialogGenericaDosTresBotones(DialogFragment dialog) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
        {
            Log.e(xxx, xxx + ": He llegado a  onDialogPositiveClick_ClaseDialogGenericaDosTresBotones: ");
        }
    }

    @Override
    public void onDialogNegativeClick_ClaseDialogGenericaDosTresBotones(DialogFragment dialog) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
        {
            Log.e(xxx, xxx + ": He llegado a  onDialogNegativeClick_ClaseDialogGenericaDosTresBotones: ");
        }
    }

    @Override
    public void onDialogNeutralClick_ClaseDialogGenericaDosTresBotones(DialogFragment dialog) {
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
        {
            Log.e(xxx, xxx + ": He llegado a  onDialogNeutralClick_ClaseDialogGenericaDosTresBotones: ");
        }


        //Termina la app sin instalar
//        finish();

        //Devuelve al usuario a los programas, lo puse asi el 4 de septiembre
        Intent intent;
        intent = new Intent(this, com.o3j.es.estamosenfiestas.display_frame_layout.
                Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
        startActivity(intent);
        finish();
    }

    //****************************************************************
    //****************************************************************
    //     FIN DE 28 julio 2015, Instalacion
    //     Inicio de todo lo relacionado con el loader y los dos broadcast receiver
    //     de descargar los programas y eventos y guardar en sqlite
    //****************************************************************
    //****************************************************************

    public boolean method_Dime_Si_Hay_Red() {

        //antes de lanzar cualquier servicio web al server, verifico si hay acceso a internet
        ClaseChequeaAcceso_A_Internet claseChequeaAcceso_A_Internet = new ClaseChequeaAcceso_A_Internet(this);
        if(claseChequeaAcceso_A_Internet.isOnline()) {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(xxx, xxx +"He llegado a method_Dime_Si_Hay_Red: SI" );
            }
            return true;
        }else {
            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.d(xxx, xxx +"He llegado a method_Dime_Si_Hay_Red: NO" );
            }
            return false;
        }
    }

    //Interface de dialogo de evento No hay Internet


    @Override
    public void onDialogPositiveClick_InterfaceClaseDialogNoHayInternet(DialogFragment dialog) {
        //Reintento mostrar la info
        method_Fuera_Del_On_Create_Chequea_Acceso_A_Internet();

    }

    @Override
    public void onDialogNegativeClick_InterfaceClaseDialogNoHayInternet(DialogFragment dialog) {

        //Devuelve al usuario a los programas
        Intent intent;
        intent = new Intent(this, com.o3j.es.estamosenfiestas.display_frame_layout.
                Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onDialogNeutralClick_InterfaceClaseDialogNoHayInternet(DialogFragment dialog) {

        //Juan 26 agosto 2015, lo uso desde metodo method_Mostar_Error_De_Descarga_De_Datos_2 si falla la reconfiguracion
        //Devuelve al usuario a los programas
        Intent intent;
        intent = new Intent(this, com.o3j.es.estamosenfiestas.display_frame_layout.
                Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
        startActivity(intent);
        finish();
    }
}//Fin de la clase
