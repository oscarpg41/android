package com.o3j.es.estamosenfiestas.display_galeria;


import android.content.Context;
import android.util.Log;

import com.eef.data.dataelements.Event;
import com.eef.data.dataelements.MultimediaElement;
import com.eef.data.dataelements.Program;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper;

import java.util.ArrayList;

/**
 * Created by Juan on 24/08/2015.
 */
public class ClaseCrearListasDeImagenesDeLasFiestas {
    //Esta clase es para actualizar de forma automatica los datos de las fiestas
    private String TAG;
    private Context context;




    public static ClaseCrearListasDeImagenesDeLasFiestas newInstance(Context context) {

        ClaseCrearListasDeImagenesDeLasFiestas claseAvisoAutomaticoDeEventosFavoritos =
                new ClaseCrearListasDeImagenesDeLasFiestas();


        claseAvisoAutomaticoDeEventosFavoritos.TAG = claseAvisoAutomaticoDeEventosFavoritos.getClass().getSimpleName();
        claseAvisoAutomaticoDeEventosFavoritos.context = context;

        return claseAvisoAutomaticoDeEventosFavoritos;
    }//Fin de newInstance

    public ArrayList<Event> devuelveArrayDeEventos(Program program) {
        ArrayList<Event> arrayListEvent = new ArrayList<Event>();

        methodPonerUrlCartelDeLaFiesta(program, arrayListEvent);
        methodPonerUrlCartelImagenDelEvento(program, arrayListEvent);

        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, ": En metodo devuelveArrayDeEventos: el tamaño de arrayListEvent= " +arrayListEvent.size());
        }

        return arrayListEvent;
    }//Fin de devuelveArrayDeEventos

    public void methodPonerUrlCartelImagenDelEvento(Program program, ArrayList<Event> arrayListEvent) {

        //Poner el primer elemento del array de eventos con la url del cartel de las fiestas
        //pero solo si es una url valida.

        //Recorro todo el array de eventos
        for (int i = 0; i < program.getFullList().size(); i++) {
            if (methodChequeoUrlValida(program.getFullList().get(i).getMultimediaElementList().get(0).getMultimediaElementLocation())) {
                arrayListEvent.add(program.getFullList().get(i));
            }
        }

    }//Fin de methodPonerUrlCartelImagenDelEvento

    public void methodPonerUrlCartelDeLaFiesta(Program program, ArrayList<Event> arrayListEvent) {

        //Poner el primer elemento del array de eventos con la url del cartel de las fiestas
        //pero solo si es una url valida.

        if (methodChequeoUrlValida(program.getListMultimediaElement().get(0).getMultimediaElementLocation())) {
            Event event = new Event();
            event.setEventId(program.getProgramId());
            event.setSortText(program.getProgramSortDescription());
            ArrayList<MultimediaElement> multimediaElementLis = new ArrayList<MultimediaElement>();
            multimediaElementLis.add(program.getListMultimediaElement().get(0));
                    event.setMultimediaElementList(multimediaElementLis);
            arrayListEvent.add(event);
        }


    }//Fin de methodPonerUrlCartelDeLaFiesta

    public boolean methodChequeoUrlValida(String url) {

        if(!Class_ConstantsHelper.methodStaticChequeaURL(url)){//url invalida
//            if(url == null || url.isEmpty()){
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG, ": En metodo method_Descarga_Imagen_De_Internet: url es NULL o esta VACIO" +url);
            }

            return false;

        }else {//url es valida
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG, ": En metodo method_Descarga_Imagen_De_Internet: url OK" +url);
            }
            return true;
        }
    }//Fin de methodChequeoUrlValida

}//Fin de la clase
