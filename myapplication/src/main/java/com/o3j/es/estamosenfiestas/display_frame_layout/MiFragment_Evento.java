package com.o3j.es.estamosenfiestas.display_frame_layout;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.eef.data.dataelements.Event;
import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper;
import com.o3j.es.estamosenfiestas.dialogs.ClaseDialogEventoGuardado;
import com.o3j.es.estamosenfiestas.display_lists_package.DividerItemDecoration;
import com.o3j.es.estamosenfiestas.display_vista_deslizante_2.Activity_Fragment_SwipeView_EventoDetalle_BackToParent;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MiFragment_Evento.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MiFragment_Evento#newInstance} factory method to
 * create an instance of this fragment.
 */

//Este fragment muesta la lista de los eventos del dia seleccionado de la fiesta
public class MiFragment_Evento extends android.support.v4.app.Fragment
                implements ClaseDialogEventoGuardado.InterfaceClaseDialogEventoGuardado{

    private static String xxx;

    public static String ARG_OBJECT = "lista ";

    private static final String ARG_PARAM1 = "param1";
    public static final String ARG_PARAM2 = "param2";
    public static final String TIPO_DE_LISTA = "tipo_De_Lista";

    private String mParam1;
    private String mParam2;
    private int int_Tipo_De_Lista;

    private OnFragmentInteractionListener mListener;

    private Event m_Event;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MiFragment_1.
     */

    //Este builder no lo uso por ahora
    public static MiFragment_Evento newInstance(String param1, String param2) {
        MiFragment_Evento fragment = new MiFragment_Evento();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public MiFragment_Evento() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

            mParam1 = getArguments().getString(ARG_OBJECT);
            int_Tipo_De_Lista = getArguments().getInt(TIPO_DE_LISTA);
//            mProgram = (Program )getArguments().getSerializable("Programa Serializado");
//            m_Lista_De_Fiestas = (Program )getArguments().getSerializable("Programa Serializado");
            m_Event = (Event)getArguments().getSerializable("Datos del Evento Serializado");

            m_Event_Poner_en_favoritos = m_Event;
        }

        xxx = this.getClass().getSimpleName();

    }//Fin del onCreate



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_mi_fragment_1, container, false);
        //****************************************************************
        //****************************************************************
        //             Codigo relacionado con recycler view
        //****************************************************************
        //****************************************************************
        View rootView = inflater.inflate(R.layout.fragment_mi_fragment_1, container, false);


        rootView.setTag(TAG);
        // BEGIN_INCLUDE(initializeRecyclerView)
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view_1);

        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        mLayoutManager = new LinearLayoutManager(getActivity());

        mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;

        if (savedInstanceState != null) {
            // Restore saved layout manager type.
            mCurrentLayoutManagerType = (LayoutManagerType) savedInstanceState
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);

//      4 junio 2015: Aunque solo hay que mostrar un evento, voy a hacerlo siguiendo el patron
//        de recycler view, como en los fragmenntos de lista de fiestas, lista de dias y lista de eventos.
//        Con lo que arrayList_Lista_Con_Evento solo tiene un elemento que es el evento con sus detalles
        ArrayList<Event> arrayList_Lista_Con_Evento = new ArrayList<Event>();
        arrayList_Lista_Con_Evento.add(m_Event);//Este array list solo tiene un elemento Evento.

//        Juan, 3 junio  2015, uso CustomAdapter_RecyclerView_Evento_Detalle y le paso el List<Events>
//        con un solo evento que es el seleccionado para mostrar sus detalles
                mAdapter = new CustomAdapter_RecyclerView_Evento_Detalle(arrayList_Lista_Con_Evento, mParam1,
                        (CustomAdapter_RecyclerView_Evento_Detalle.OnItemClickListenerEventoDetalle) getActivity()
                        ,this,
                        (Activity_Fragment_SwipeView_EventoDetalle_BackToParent) getActivity());
                // Set CustomAdapter as the adapter for RecyclerView.
                mRecyclerView.setAdapter(mAdapter);

        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL);
        mRecyclerView.addItemDecoration(itemDecoration);
        // END_INCLUDE(initializeRecyclerView)
//        ****************************************************************************

        //Manejo de los radio buttons
        mLinearLayoutRadioButton = (RadioButton) rootView.findViewById(R.id.linear_layout_rb);
        mLinearLayoutRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRecyclerViewLayoutManager(LayoutManagerType.LINEAR_LAYOUT_MANAGER);
            }
        });

        mGridLayoutRadioButton = (RadioButton) rootView.findViewById(R.id.grid_layout_rb);
        mGridLayoutRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRecyclerViewLayoutManager(LayoutManagerType.GRID_LAYOUT_MANAGER);
            }
        });
        //****************************************************************
        //****************************************************************
        //             FIN Codigo relacionado con recycler view
        //****************************************************************
        //****************************************************************

        //        Texto para mostrar el titulo de la fiesta

        //24 junio 2015: No muestro los textos. Se muestran en Activity_Fragment_SwipeView_EventoDetalle_BackToParent
        //como me pidio Oscar
//        Texto para mostrar el titulo de la fiesta
        TextView textView_Titulo = (TextView) rootView.findViewById(R.id.titulo_del_fragment);

        textView_Titulo.setVisibility(View.GONE);

        textView_Titulo.setText(Class_ConstantsHelper.titulo_De_La_Fiesta);

//        Texto para mostrar la fecha del dia de los eventos de ese dia
        TextView textView_Fecha_Del_Dia = (TextView) rootView.findViewById(R.id.fecha_del_dia);

        textView_Fecha_Del_Dia.setVisibility(View.GONE);

//        textView_Fecha_Del_Dia.setText(Class_ConstantsHelper.fecha_Del_Dia);


        //Pongo como fecha del dia la del evento
        SimpleDateFormat sfd = new SimpleDateFormat("EEEE dd LLLL yyyy");
//        sfd.format(this.m_Event.getStartDate());
        textView_Fecha_Del_Dia.setText(sfd.format(this.m_Event.getStartDate()));


        //La referencia a la toolbar se crea en el onCreate

        // 10 julio 2015: he pasado todo el manejo de la contextual tool bar a CustomAdapter_RecyclerView_Evento_Detalle
        //como me pidio oscar. Ahora agregar a favoritos y enviar se gestiona en el adapter.
        //Dejo estas tres lineas comentadas
//         Toolbar toolbar_acciones_del_detailed_view = (Toolbar) rootView.findViewById(R.id.tollbar_contextual_action_bar);
//        toolbar_acciones_del_detailed_view.setVisibility(View.VISIBLE);
//        method_generar_toolbar_para_contextual_action_mode(toolbar_acciones_del_detailed_view);

        return rootView;
    }//Fin de onCreateView

    public void method_generar_toolbar_para_contextual_action_mode(Toolbar toolbar_acciones_del_detailed_view) {


        // Set an OnMenuItemClickListener to handle menu item clicks
        toolbar_acciones_del_detailed_view.setOnMenuItemClickListener(
                new Toolbar.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        // Handle the menu item

                        int id = item.getItemId();

//                        //noinspection SimplifiableIfStatement
//                        if (id == R.id.action_settings) {
//                            Toast.makeText(Activity_Fragment_FrameLayout_WithNavDrawer_2.this, "Bottom toolbar pressed: ", Toast.LENGTH_LONG).show();
//                            return true;
//                        }
//                        return true;


                        //*******************************************************************************

                        switch (id)

                        {
                            case R.id.action_agregar_a_favoritos:
                                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                                    Log.d(xxx, xxx + " action_agregar_a_favorito ");
                                }

                                methodFake_Add_To_List_Of_Favorites();

                                return true;


                            case R.id.action_enviar_mail:
                                if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                                    Log.d(xxx, xxx + " action_enviar_mail ");
                                }

                                methodFake_Enviar_Mail();


                                return true;


                            default:
                                return false;
                        }
                        //*******************************************************************************
                    }


                });


//        toolbar_acciones_del_detailed_view.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(Activity_Fragment_FrameLayout_WithNavDrawer_2.this, "Navigation Icon pressed: ", Toast.LENGTH_LONG).show();
//                // Para esta practica, al presionar el navigation icon lo quito.
//                toolbar_acciones_del_detailed_view.setVisibility(View.GONE);
//            }
//        });

        // Inflate a menu to be displayed in the toolbar
        toolbar_acciones_del_detailed_view.inflateMenu(R.menu.menu_contextual_action_mode_evento_de_la_lista);

    }//Fin de method_generar_toolbar_para_contextual_action_mode


    //    12 junio 2015 metodo fake para agregar elementos a la lista de favoritos
//    Este metodo no lo uso cuando la api de JL este terminada

    Event m_Event_Poner_en_favoritos;
    private void methodFake_Add_To_List_Of_Favorites() {

        Class_Modelo_De_Datos_Lista_Favoritos m_Class_Modelo_De_Datos_Lista_Favoritos =
                new Class_Modelo_De_Datos_Lista_Favoritos();

        m_Class_Modelo_De_Datos_Lista_Favoritos.setStringTituloDeLaFiesta(Class_ConstantsHelper.titulo_De_La_Fiesta);
        m_Class_Modelo_De_Datos_Lista_Favoritos.setStringFechaDelEvento(Class_ConstantsHelper.fecha_Del_Dia);
        m_Class_Modelo_De_Datos_Lista_Favoritos.setEventEventoFavorito(m_Event_Poner_en_favoritos);
        Class_ConstantsHelper.arraylist_Datos_Lista_Favoritos.add(m_Class_Modelo_De_Datos_Lista_Favoritos);
        //*************************************************************************************************
//        Mostrar el dialogo de has guardado en favoritos
        boolean boolean_evento_guardado = true;

        if (boolean_evento_guardado) {

            // Create an instance of the dialog fragment and show it
            DialogFragment dialog =  ClaseDialogEventoGuardado.newInstance
                    (R.string.titulo_dialog_evento_guardado, R.string.message_dialog_evento_guardado, R.string.positiveButton,
                            R.string.negativeButton, 1, R.string.boton_neutral_evento_guardado);
            dialog.show(getFragmentManager(), "ClaseDialogGenericaDosTresBotones");
//            dialog.show(getSupportFragmentManager(), "ClaseDialogGenericaDosTresBotones");
        }else{
            DialogFragment dialog =  ClaseDialogEventoGuardado.newInstance
                    (R.string.titulo_dialog_evento_guardado, R.string.message_dialog_evento_guardado_Error, R.string.positiveButton,
                            R.string.negativeButton, 1, R.string.boton_neutral_evento_guardado);
            dialog.show(getFragmentManager(), "ClaseDialogGenericaDosTresBotones");
        }

        //*************************************************************************************************
    }

    private void methodFake_Enviar_Mail() {

//        String to = textTo.getText().toString();
//        String subject = textSubject.getText().toString();
//        String message = textMessage.getText().toString();

        String to = "";
        String subject = Class_ConstantsHelper.titulo_De_La_Fiesta;

        StringBuilder strBuilder = new StringBuilder(m_Event_Poner_en_favoritos.getSortText());
        strBuilder.append("\n");
        strBuilder.append(Class_ConstantsHelper.fecha_Del_Dia);
        strBuilder.append("\n");
        strBuilder.append("Hora del evento: ");

        DateFormat df2 = new SimpleDateFormat("HH:mm");
//        df2.format(m_Event_Poner_en_favoritos.getStartDate());
        strBuilder.append(df2.format(m_Event_Poner_en_favoritos.getStartDate()));


        strBuilder.append("\n");
        strBuilder.append(m_Event_Poner_en_favoritos.getLongText());
        String message = strBuilder.toString();


        Intent intent_email = new Intent(Intent.ACTION_SEND);
        intent_email.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
        //email.putExtra(Intent.EXTRA_CC, new String[]{ to});
        //email.putExtra(Intent.EXTRA_BCC, new String[]{to});
        intent_email.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent_email.putExtra(Intent.EXTRA_TEXT, message);

        //need this to prompts email client only
        intent_email.setType("message/rfc822");

//                startActivity(Intent.createChooser(intent_email, "Choose an Email client :"));

        if (intent_email.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(Intent.createChooser(intent_email, "Choose an Email client :"));
        }
        //**************************************************************************************

    }//Fin de methodFake_Enviar_Mail

    //Variables para que funcione el recycle view
    private static final String TAG = "RecyclerViewFragment";
    protected RecyclerView mRecyclerView;
    protected LayoutManagerType mCurrentLayoutManagerType;//Tipo enum


    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }

    protected RecyclerView.LayoutManager mLayoutManager;
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    //Original
//    protected CustomAdapter_RecyclerView_2 mAdapter;
    //Nuevo adapter para el array list
//    protected com.o3j.es.estamosenfiestas.activity_fragment_base_2.CustomAdapter_RecyclerView_3 mAdapter;
//    protected CustomAdapter_RecyclerView_3 mAdapter;
    protected CustomAdapter_RecyclerView_Evento_Detalle mAdapter;
    protected RadioButton mLinearLayoutRadioButton;
    protected RadioButton mGridLayoutRadioButton;


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save currently selected layout manager.
        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, mCurrentLayoutManagerType);
        super.onSaveInstanceState(savedInstanceState);
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction("ahora no lo uso");
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(String string);
    }

    /**
     * Set RecyclerView's LayoutManager to the one given.
     *
     * @param layoutManagerType Type of layout manager to switch to.
     */
    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                mLayoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
                mCurrentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }

    private static final int SPAN_COUNT = 2;

    //Interface de dialogo de evento guardado. No hago nada en estos metodos

    @Override
    public void onDialogPositiveClick_ClaseDialogEventoGuardado(DialogFragment dialog) {

    }

    @Override
    public void onDialogNegativeClick_ClaseDialogEventoGuardado(DialogFragment dialog) {

    }

    @Override
    public void onDialogNeutralClick_ClaseDialogEventoGuardado(DialogFragment dialog) {

    }

    public void methodResaltarEventoFavorito(Event m_Event) {

        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, xxx +" En metodo a pruebaResaltarEventoFavorito: ");
        }

        mAdapter.methodResaltarEventoFavorito(m_Event);
    }

}//Fin de la clase
