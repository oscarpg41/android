package com.o3j.es.estamosenfiestas.orm.pojosDB;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by jluis on 27/07/15.
 */
@DatabaseTable
public class Eventos {
    public final static String ID ="_id";
    public final static String ORGANIZADOR="organizador";
    public final static String PROGRAMA="programa";
    public final static String FECHA_EVENTO="fecha_evento";
    public final static String TITULO="titulo";
    public final static String DESCRIPCION="descripcion";
    public final static String URL_IMAGEN="url_imagen";
    public final static String LOCAL_IMAGEN="local_imagen";
    public final static String POPULAR="popular";
    public final static String FAVORITO="favorito";
    public final static String FECHA_FAVORITO="fecha_favorito";
    public final static String FECHA_FIN_FAVORITO="fecha_fin_favorito";
    public final static String FECHA_ULTIMA_DESCARGA="fecha_descarga";
    public final static String FECHA_ULTIMA_ACTUALIZACION="fecha_ultima_actualizacion";


    @DatabaseField(id=true, columnName = ID)
    private int id;
    @DatabaseField(foreign = true, columnName = ORGANIZADOR)
    Organizador organizador;
    @DatabaseField(foreign = true, columnName = PROGRAMA)
    Programa programa;
    @DatabaseField(columnName = FECHA_EVENTO)
    Date fechaEvento;
    @DatabaseField(columnName = TITULO, width = 255)
    String titulo;
    @DatabaseField(columnName = DESCRIPCION, width = 1000)
    String descripcion;
    @DatabaseField(columnName = URL_IMAGEN, width = 255)
    String url_imagen;
    @DatabaseField(columnName = LOCAL_IMAGEN, width = 255)
    String local_imagen;
    @DatabaseField(columnName = POPULAR)
    int popular;
    @DatabaseField(columnName = FAVORITO)
    int favorito;
    @DatabaseField(columnName = FECHA_FAVORITO)
    Date fecha_favorito;
    @DatabaseField(columnName = FECHA_FIN_FAVORITO)
    Date fecha_fin_favorito;
    @DatabaseField(columnName = FECHA_ULTIMA_DESCARGA)
    private Date fecha_descarga;
    @DatabaseField(columnName = FECHA_ULTIMA_ACTUALIZACION)
    private Date fecha_ultima_actualizacion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Organizador getOrganizador() {
        return organizador;
    }

    public void setOrganizador(Organizador organizador) {
        this.organizador = organizador;
    }

    public Programa getPrograma() {
        return programa;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }

    public Date getFechaEvento() {
        return fechaEvento;
    }

    public void setFechaEvento(Date fechaEvento) {
        this.fechaEvento = fechaEvento;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUrl_imagen() {
        return url_imagen;
    }

    public void setUrl_imagen(String url_imagen) {
        this.url_imagen = url_imagen;
    }

    public String getLocal_imagen() {
        return local_imagen;
    }

    public void setLocal_imagen(String local_imagen) {
        this.local_imagen = local_imagen;
    }

    public int getPopular() {
        return popular;
    }

    public void setPopular(int popular) {
        this.popular = popular;
    }

    public int getFavorito() {
        return favorito;
    }

    public void setFavorito(int favorito) {
        this.favorito = favorito;
    }

    public Date getFecha_favorito() {
        return fecha_favorito;
    }

    public void setFecha_favorito(Date fecha_favorito) {
        this.fecha_favorito = fecha_favorito;
    }

    public Date getFecha_fin_favorito() {
        return fecha_fin_favorito;
    }

    public void setFecha_fin_favorito(Date fecha_fin_favorito) {
        this.fecha_fin_favorito = fecha_fin_favorito;
    }

    public Date getFecha_descarga() {
        return fecha_descarga;
    }

    public void setFecha_descarga(Date fecha_descarga) {
        this.fecha_descarga = fecha_descarga;
    }

    public Date getFecha_ultima_actualizacion() {
        return fecha_ultima_actualizacion;
    }

    public void setFecha_ultima_actualizacion(Date fecha_ultima_actualizacion) {
        this.fecha_ultima_actualizacion = fecha_ultima_actualizacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Eventos)) return false;

        Eventos eventos = (Eventos) o;

        return getId() == eventos.getId();

    }

    @Override
    public int hashCode() {
        return getId();
    }
}
