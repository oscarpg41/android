package com.o3j.es.estamosenfiestas.servercall.forbroadcaster;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.eef.data.dataelements.Event;
import com.eef.data.dataelements.EventPlanner;
import com.eef.data.dataelements.MultimediaElement;
import com.eef.data.dataelements.Program;
import com.eef.data.eefExceptions.eefException;
import com.eef.data.eeftypes.TypeEefError;
import com.eef.data.eeftypes.TypeMultimediaElement;
import com.eef.data.eeftypes.TypeState;
import com.o3j.es.estamosenfiestas.servercall.pojos.InfoOrganizerPojo;
import com.o3j.es.estamosenfiestas.servercall.pojos.ProgramEventPojo;
import com.o3j.es.estamosenfiestas.servercall.pojos.ProgramPojo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jluis on 28/07/15.
 */
public class UpdateProgramsFromServer{

    Boolean rightDownloaded;


    private static EventPlanner eventPlanner;
    private static List<Program> listaProgramasToCheck;
    private static List<Program> listaProgramasToUpdate;
    private static List<Program> listaProgramasNoToUpdate;
    private static List<Program> listaProgramasNew;
    private static List<ProgramPojo> listaProgramasToCompare;

    HashMap<Program, List<ProgramEventPojo>> programasDescargados;
    HashMap<Program,List<Event>> programsDownladed;

    HashMap<Program, List<ProgramEventPojo>> programasNuevosDescargados;
    HashMap<Program,List<Event>> programsNewDownladed;

    public UpdateProgramsFromServer() {
        rightDownloaded= false;
        eventPlanner=null;
        listaProgramasToUpdate= new ArrayList<Program>();
        listaProgramasNoToUpdate= new ArrayList<Program>();
        listaProgramasNew=new ArrayList<Program>();
        programasDescargados = new HashMap<Program, List<ProgramEventPojo>>();
        programsDownladed = new HashMap<Program,List<Event>>();
        programasNuevosDescargados = new HashMap<Program, List<ProgramEventPojo>>();
        programsNewDownladed = new HashMap<Program,List<Event>>();
    }

    public Boolean getRightDownloaded() {
        return rightDownloaded;
    }


    public EventPlanner getEventPlanner() {
        return eventPlanner;
    }

    public void setEventPlanner(EventPlanner eP){
        this.eventPlanner = eP;
    }

    public HashMap<Program, List<Event>> getProgramsDownladed() {
        return programsDownladed;
    }

    public HashMap<Program,List<Event>> getNewProgramsDownloaded(){
        return programsNewDownladed;
    }

    public HashMap<Program, List<ProgramEventPojo>> getProgramasDescargados() {
        return programasDescargados;
    }

    public  List<Program> getListaProgramasToCheck() {
        return listaProgramasToCheck;
    }

    public  void setListaProgramasToCheck(List<Program> listaProgramas) {
        this.listaProgramasToCheck = listaProgramas;
        if(listaProgramasToCheck==null || listaProgramasToCheck.size()==0)
            return;
        listaProgramasToCompare = new ArrayList<ProgramPojo>();
        for(Program program : listaProgramas)
            listaProgramasToCompare.add(programToProgramPojo(program));
    }


    public  List<Program> getListaProgramasToUpdate() {
        return listaProgramasToUpdate;
    }

    public  void setListaProgramasToUpdate(List<Program> listaProgramasToUpdate) {
        UpdateProgramsFromServer.listaProgramasToUpdate = listaProgramasToUpdate;
    }

    private List<MultimediaElement> getImageAsMultimediaList(String img) {
        MultimediaElement logo = new MultimediaElement(img, TypeMultimediaElement.IMAGE_URL);
        ArrayList<MultimediaElement> logoList = new ArrayList<MultimediaElement>();
        logoList.add(logo);
        return logoList;
    }

    public synchronized void addProgramToBeUpdated(Program program)
    {
        listaProgramasToUpdate.add(program);
    }

    public synchronized void addProgramNotToBeUpdated(Program program)
    {
        listaProgramasNoToUpdate.add(program);
    }

    public synchronized void addProgramNew(Program program)
    {
        listaProgramasNew.add(program);
    }

    public List<Program> getListaProgramasNew() {
        return listaProgramasNew;
    }

    public  synchronized void addProgramToUpdateEventDownload(Program programa, List<ProgramEventPojo> eventosPrograma)
    {
        if(programasDescargados.containsKey(programa))
            return;
        programasDescargados.put(programa, eventosPrograma);
        try {

            List<Event> listEvent = programEventPojoListToEventList(eventosPrograma, programa, eventPlanner);
            programsDownladed.put(programa,listEvent);
        } catch (eefException e) {
            Log.e("ERROR POR FECHAS","LAS FECHAS DEL PROGRAMA O DE LOS EVENTOS SON ERRONEAS");
        }
        //programaFull.
    }

    public synchronized void addNewProgramEventDownload(Program programa, List<ProgramEventPojo> eventosPrograma)
    {
        if(programasNuevosDescargados.containsKey(programa))
            return;
        programasNuevosDescargados.put(programa, eventosPrograma);
        try {
            List<Event> listEvent = programEventPojoListToEventList(eventosPrograma, programa, eventPlanner);
            programsNewDownladed.put(programa,listEvent);
        } catch (eefException e) {
            Log.e("ERROR POR FECHAS","LAS FECHAS DEL PROGRAMA O DE LOS EVENTOS SON ERRONEAS");
        }
        //programaFull.
    }

    public void processProgramsDownload(List<ProgramPojo> programasToProcess)
    {
        final String LOG_SEED = "UPD-->processProgramsDownload";

        //Hay que recorrer la lista de programas descargados

        for(ProgramPojo pP: programasToProcess)
        {
            try {
                //Se comprueba si el programa es Nuevo
                Program program = programPojoToProgram(pP);
                if(isnewProgram(pP)) {
                    addProgramNew(program);
                    continue;
                }
                //Si necesita se actualizado
                if(needToBeUpdated(pP)) {
                    addProgramToBeUpdated(program);
                    continue;
                }
                //Si No necesita ser actualizado
                addProgramNotToBeUpdated(program);

            } catch (eefException e) {
                Log.e(LOG_SEED,"Error al convertir a Program ["+pP.getName()+"]");
                continue;
            }
        }

    }


    private boolean isnewProgram(ProgramPojo programaToCheck)
    {
        if(!listaProgramasToCompare.contains(programaToCheck))
            return true;
        return false;
    }

    private boolean needToBeUpdated(ProgramPojo programFromServer)
    {
        ProgramPojo pPCompare =  listaProgramasToCompare.get(listaProgramasToCompare.indexOf(programFromServer));

        return comparaFechaPosterios(pPCompare.getFfModification(),programFromServer.getFfModification());
    }

    private boolean comparaFechaPosterios(String fechaPrograma, String fechaProgramaServer)
    {
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date fechaEnApp = formatoDelTexto.parse(fechaPrograma);
            Date fechaEnServer = formatoDelTexto.parse(fechaProgramaServer);
            if(fechaEnApp.before(fechaEnServer))
            {
                Log.d("NEEDUPDATE","LA fecha del programa en el app["+fechaPrograma+"] es anterior a la del servidor["+fechaProgramaServer+"] SE VA A ACTUALIZAR");
                return true;
            }
            Log.d("NEEDUPDATE","LA fecha del programa en el app["+fechaPrograma+"] es anterior a la del servidor["+fechaProgramaServer+"] NO SE VA A ACTUALIZAR");
            return false;
        } catch (ParseException e) {
            Log.e("NEEDUPDATE", "LA fecha del programa en el app["+fechaPrograma+"] es anterior a la del servidor"+fechaProgramaServer+"] no tiene el formato yyyy-MM-dd HH:mm:ss");
            return false;
        }
    }

    //CHEQUEA SI SE HA RECIBIDO LOS EVENTOS DE TODOS LOS PROGRAMAS A DESCARGA
    public Boolean allProgramsDownladed(){

        //Si la lista de programas to check es null o vacia
        if( listaProgramasToCheck.size()==0)
            return false;
        //si hay programas para actualizar y no se han descargado todavía.
        if( listaProgramasToUpdate!= null && listaProgramasToUpdate.size() > 0) {
            //recorro de la lista de programas y compruebo si ya tengo la descarga
            for (Program programa : listaProgramasToUpdate)
                if (!programasDescargados.containsKey(programa))
                    return false;
        }
        //si hay programas nuevos y no se han descargado todavía.
        if( listaProgramasNew!= null && listaProgramasNew.size() > 0) {
            //recorro de la lista de programas y compruebo si ya tengo la descarga
            for (Program programa : listaProgramasNew)
                if (!programsNewDownladed.containsKey(programa))
                    return false;
        }        //Si para todos los programas ya tengo la lista de eventos marco la configuración como
        //bien configurada.
        rightDownloaded=true;
        return true;
    }


    private List<Event> programEventPojoListToEventList(List<ProgramEventPojo> eventosP, Program program, EventPlanner organizer) throws eefException
    {
        final String LOG_SEED = "UPD-->programPojoToProgram-->";
        ArrayList<Event> eventos = new ArrayList<Event>();
        for(ProgramEventPojo evento : eventosP) {
            Event event = new Event();
            event.setProgramId(program.getProgramId());
            event.setAutonomousCommunityId(organizer.getEventPlannerId());
            event.setEventId(Integer.parseInt(evento.getIdEvent()));
            event.setSortText(evento.getTitle());
            event.setLongText(evento.getDescription());
            event.setMultimediaElementList(getImageAsMultimediaList(evento.getImage()));
            event.setState(TypeState.SCHEDULED);
            SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String fechaEvento = evento.getFfEvent() + " " + evento.getHour();
            try {

                event.setStartDate(formatoDelTexto.parse(fechaEvento));

            } catch (ParseException e) {
                Log.e(LOG_SEED, "Error parser de las fechas de eventp[" + fechaEvento +"]  no cadena [yyyy-MM-dd HH:mm:ss]");
                throw new eefException(TypeEefError.PROGRAMS_DATE_PARSE_FORMAT_EXCEPTION);
            }
            eventos.add(event);
        }
        return eventos;
    }



    private Program programPojoToProgram(ProgramPojo programa) throws eefException
    {
        final String LOG_SEED = "FULL-->programPojoToProgram-->";
        Program program = new Program();
        program.setAutonomousCommunityId(eventPlanner.getAutonomousCommunityId());
        program.setEventPlannerId(eventPlanner.getEventPlannerId());
        program.setProgramId(Integer.parseInt(programa.getIdProgram()));
        program.setProgramLongDescription(programa.getName());
        program.setProgramSortDescription(programa.getName());
        program.setListMultimediaElement(getImageAsMultimediaList(programa.getImage()));
        Date fecha = new GregorianCalendar().getTime();
        program.setDownloadedDay(fecha);
        program.setLastUpdateDay(fecha);
        //Se convierten a fecha los campos de incio y fin
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            program.setProgramStartDate(formatoDelTexto.parse(programa.getFfIni()));
            program.setProgramEndDate(formatoDelTexto.parse(programa.getFfEnd()));
        } catch (ParseException e) {
            Log.e(LOG_SEED, "Error parser de las fechas de programa fecha_ini[" + programa.getFfIni() + "] o fecha fin[" + programa.getFfEnd() + "]");
            throw new eefException(TypeEefError.PROGRAMS_DATE_PARSE_FORMAT_EXCEPTION);
        }
        return program;
    }


    private  ProgramPojo programToProgramPojo(Program program)
    {

        final String LOG_SEED = "FULL-->programPojoToProgram-->";
        ProgramPojo programPojo = new ProgramPojo();
        programPojo.setIdProgram(program.getProgramId().toString());
        programPojo.setName(program.getProgramSortDescription());
        programPojo.setImage(program.getListMultimediaElement().get(0).getMultimediaElementLocation());
        //Se convierten a fecha los campos de incio y fin
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatofechaActualizacion = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        programPojo.setFfIni(formatoDelTexto.format(program.getProgramStartDate()));
        programPojo.setFfEnd(formatoDelTexto.format(program.getProgramEndDate()));
        programPojo.setFfModification(formatofechaActualizacion.format(program.getLastUpdateDay()));

        return programPojo;
    }

}
