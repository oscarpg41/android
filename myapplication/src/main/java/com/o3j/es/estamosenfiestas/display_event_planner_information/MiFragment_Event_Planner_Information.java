package com.o3j.es.estamosenfiestas.display_event_planner_information;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.eef.data.dataelements.Event;
import com.eef.data.dataelements.EventPlanner;
import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.display_lists_package.DividerItemDecoration;

import java.util.ArrayList;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MiFragment_Event_Planner_Information.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MiFragment_Event_Planner_Information#newInstance} factory method to
 * create an instance of this fragment.
 */

//Este fragment muesta la lista de los eventos del dia seleccionado de la fiesta
public class MiFragment_Event_Planner_Information extends android.support.v4.app.Fragment {

    private static String xxx;

    public static String ARG_OBJECT = "lista ";

    private static final String ARG_PARAM1 = "param1";
    public static final String ARG_PARAM2 = "param2";
    public static final String TIPO_DE_LISTA = "tipo_De_Lista";

    private String mParam1;
    private String mParam2;
    private int int_Tipo_De_Lista;

    private OnFragmentInteractionListener mListener;

    private Event m_Event;

    private ArrayList<EventPlanner> arrayList_Event_Planner_Information = new ArrayList<EventPlanner>();


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MiFragment_1.
     */

    //Este builder no lo uso por ahora
    public static MiFragment_Event_Planner_Information newInstance(String param1, String param2) {
        MiFragment_Event_Planner_Information fragment = new MiFragment_Event_Planner_Information();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public MiFragment_Event_Planner_Information() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

            mParam1 = getArguments().getString(ARG_OBJECT);
            int_Tipo_De_Lista = getArguments().getInt(TIPO_DE_LISTA);
//            mProgram = (Program )getArguments().getSerializable("Programa Serializado");
//            m_Lista_De_Fiestas = (Program )getArguments().getSerializable("Programa Serializado");
            arrayList_Event_Planner_Information = (ArrayList<EventPlanner>)getArguments().getSerializable("Lista de Event_Planner_Information");
        }

        xxx = this.getClass().getSimpleName();

    }//Fin del onCreate



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_mi_fragment_1, container, false);
        //****************************************************************
        //****************************************************************
        //             Codigo relacionado con recycler view
        //****************************************************************
        //****************************************************************
        View rootView = inflater.inflate(R.layout.fragment_mi_fragment_1, container, false);
        rootView.setTag(TAG);
        // BEGIN_INCLUDE(initializeRecyclerView)
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view_1);

        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        mLayoutManager = new LinearLayoutManager(getActivity());

        mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;

        if (savedInstanceState != null) {
            // Restore saved layout manager type.
            mCurrentLayoutManagerType = (LayoutManagerType) savedInstanceState
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);


                mAdapter = new CustomAdapter_RecyclerView_Event_Planner_Information(arrayList_Event_Planner_Information, mParam1,
                        (CustomAdapter_RecyclerView_Event_Planner_Information.OnItemClickListenerEventPlannerInformation) getActivity(),
                        (Activity_Display_Event_Planner_Information_WithNavDrawer_2) getActivity());
                // Set CustomAdapter as the adapter for RecyclerView.
                mRecyclerView.setAdapter(mAdapter);

        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL);
        mRecyclerView.addItemDecoration(itemDecoration);
        // END_INCLUDE(initializeRecyclerView)
//        ****************************************************************************

        //Manejo de los radio buttons
        mLinearLayoutRadioButton = (RadioButton) rootView.findViewById(R.id.linear_layout_rb);
        mLinearLayoutRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRecyclerViewLayoutManager(LayoutManagerType.LINEAR_LAYOUT_MANAGER);
            }
        });

        mGridLayoutRadioButton = (RadioButton) rootView.findViewById(R.id.grid_layout_rb);
        mGridLayoutRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRecyclerViewLayoutManager(LayoutManagerType.GRID_LAYOUT_MANAGER);
            }
        });
        //****************************************************************
        //****************************************************************
        //             FIN Codigo relacionado con recycler view
        //****************************************************************
        //****************************************************************

//        //        Texto para mostrar el titulo de la fiesta
//        TextView textView_Titulo = (TextView) rootView.findViewById(R.id.titulo_del_fragment);
//
//        textView_Titulo.setVisibility(View.VISIBLE);
//
//        textView_Titulo.setText(Class_ConstantsHelper.titulo_De_La_Fiesta);
//
////        Texto para mostrar la fecha del dia de los eventos de ese dia
//        TextView textView_Fecha_Del_Dia = (TextView) rootView.findViewById(R.id.fecha_del_dia);
//
//        textView_Fecha_Del_Dia.setVisibility(View.VISIBLE);
//
//        textView_Fecha_Del_Dia.setText(Class_ConstantsHelper.fecha_Del_Dia);

        //4 sept: si el array de event planner esta vacio, muestro el texto de no organizador
        if(arrayList_Event_Planner_Information.isEmpty()){
            TextView textView_Titulo = (TextView) rootView.findViewById(R.id.titulo_del_fragment);
            textView_Titulo.setVisibility(View.VISIBLE);
            textView_Titulo.setText(R.string.info_del_organizador_no_disponible);
        }

        return rootView;
    }//Fin de onCreateView

    //Variables para que funcione el recycle view
    private static final String TAG = "RecyclerViewFragment";
    protected RecyclerView mRecyclerView;
    protected LayoutManagerType mCurrentLayoutManagerType;//Tipo enum


    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }

    protected RecyclerView.LayoutManager mLayoutManager;
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    //Original
//    protected CustomAdapter_RecyclerView_2 mAdapter;
    //Nuevo adapter para el array list
//    protected com.o3j.es.estamosenfiestas.activity_fragment_base_2.CustomAdapter_RecyclerView_3 mAdapter;
//    protected CustomAdapter_RecyclerView_3 mAdapter;
    protected CustomAdapter_RecyclerView_Event_Planner_Information mAdapter;
    protected RadioButton mLinearLayoutRadioButton;
    protected RadioButton mGridLayoutRadioButton;


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save currently selected layout manager.
        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, mCurrentLayoutManagerType);
        super.onSaveInstanceState(savedInstanceState);
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction("ahora no lo uso");
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(String string);
    }

    /**
     * Set RecyclerView's LayoutManager to the one given.
     *
     * @param layoutManagerType Type of layout manager to switch to.
     */
    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                mLayoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
                mCurrentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }

    private static final int SPAN_COUNT = 2;


}//Fin de la clase
