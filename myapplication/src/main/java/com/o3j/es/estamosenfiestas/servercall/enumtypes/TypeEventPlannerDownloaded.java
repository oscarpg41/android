package com.o3j.es.estamosenfiestas.servercall.enumtypes;

/**
 * Created by jluis on 28/07/15.
 */
public enum TypeEventPlannerDownloaded {
    EVENTPLANNER_DOWNLOADED(1),
    EVENTPLANNER_PROGRAMLIST_DOWNLOADED(2),
    EVENT_PROGRAM_DOWNLOADED(3),
    EVENTPLANNER_PROGRAMLIST_FOR_UPDATE_DOWNLOADED(4),
    EVENT_PLANNER_DOWNLOADED_FOR_RECONFIGURE(5),
    PROGRAM_NEED_UPADTE(6),
    PROGRAM_NO_NEED_UPDATE(8),
    PROGRAM_NEW(7);

    Integer value;

    TypeEventPlannerDownloaded(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    static TypeEventPlannerDownloaded getErrorTypeFromErrorCode(Integer code)
    {
        Integer longitud= TypeEventPlannerDownloaded.values().length;
        for(int i=0;i<longitud;i++)
        {
            if(TypeEventPlannerDownloaded.values()[i].getValue()==code)
                return TypeEventPlannerDownloaded.values()[i];
            i++;
        }
        return null;
    }
}
