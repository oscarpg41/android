/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.o3j.es.estamosenfiestas.display_frame_layout;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eef.data.dataelements.Program;
import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Provide views to RecyclerView with data from mDataSet.
 */
//creado el 21 feb 2014  para nuevo layout fila_info_eventos
public class CustomAdapter_RecyclerView_Fiestas extends RecyclerView.Adapter<CustomAdapter_RecyclerView_Fiestas.ViewHolder> {
//    private static final String TAG = "CustomAdapter";
    private static  String TAG;



//    private static FragmentActivity actividad;


    //Original
//    private String[] mDataSet;

    //21 Feb 2015 modificado para funcionar con el array list
//    private ArrayList<Class_Evento_1> arrayList_De_Eventos_De_la_fiesta;
    private static  List<Program> arrayList_De_Eventos_De_la_fiesta;

//    Agrego la interface para recoger los clicks en la actividad
    private OnItemClickListenerFiestaSeleccionada mListener;

    //Juan 10 oct 2015
    private OnItemClickListenerImagenDeLaFiesta mListenerClickImagenDeLaFiesta;


    // BEGIN_INCLUDE(recyclerViewSampleViewHolder)
    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */


    public static class ViewHolder extends RecyclerView.ViewHolder {
        //Los nombres como en Class_Evento_1
        private final TextView textView_Titulo;
        private final TextView textView_Descripcion;
        private final TextView textView_Descripcion2;
        private final ImageView imageView;
        private final ImageView imageViewGrande;
        private final View m_View;



        public TextView getTextView_Titulo() {

            return textView_Titulo;
        }

        public TextView getTextView_Descripcion() {

            return textView_Descripcion;
        }

        public TextView getTextView_Descripcion2() {

            return textView_Descripcion2;
        }

        public ImageView getImageView() {
            return imageView;
        }

        public ImageView getImageView_Grande() {
            return imageViewGrande;
        }

        public View getView() {
            return m_View;
        }



        public ViewHolder(View v) {
            super(v);
            // Define click listener for the ViewHolder's View.


//            OJO: ver en http://www.truiton.com/2015/02/android-recyclerview-tutorial/
//            como poner el listener en el fragment

//        Juan, 3 Junio 2015, pongo los onclick en onBindViewHolder
//        (como en el proyecto NavigationDrawer) y no en el constructor del viewHolder
//            Comento estos que son los originales

//            v.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
//                        Log.d(TAG, "Element " + getPosition() + " clicked.");
//                    }
//
//
//                    //23 Feb 2015: Llamo a la actividad que muestra el detalle del evento
//                    // Do something in response to button
//
//
//                }
//            });


            // Juan, 26 Mayo 2015: Solo detecto el click, por ahora
//            v.setOnLongClickListener(new View.OnLongClickListener(){
//
//                @Override
//                public boolean onLongClick(View v) {
//                    if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
//                        Log.d(TAG, "Element " + getPosition() + " LONG clicked.");
//                    }
////                    OJO: Si retorno false, entonces se dispara el onClick!!
////                    return false;
////                    Por eso si solo quiero detectar el long click, retorno true
//                    return true;
//                }
//            });


            m_View = v;
            textView_Titulo = (TextView) v.findViewById(R.id.textView6);
            textView_Descripcion = (TextView) v.findViewById(R.id.textView7);
            textView_Descripcion2 = (TextView) v.findViewById(R.id.textView8);
            imageView = (ImageView) v.findViewById(R.id.imageView);
            imageViewGrande = (ImageView) v.findViewById(R.id.imageView_foto_Grande);
            imageViewGrande.setVisibility(View.VISIBLE);


//            Juan, 26 Mayo 2015: Este adapter solo muestra la fecha que esta en el textView_Titulo
//            y las fechas de inicio y fin
//            El resto de controles los oculto por ahora
            textView_Descripcion2.setVisibility(View.GONE);
//            imageView.setVisibility(View.GONE);
        }

    }//Fin de la clase ViewHolder
    // END_INCLUDE(recyclerViewSampleViewHolder)


    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used by RecyclerView.
     */
    //Original
//    public CustomAdapter_RecyclerView_3(String[] dataSet) {

    //21 Feb 2015 modificado para funcionar con el array list
//    public CustomAdapter_RecyclerView_3(ArrayList<Class_Evento_1> dataSet) {
    //Original
//    public CustomAdapter_RecyclerView_3(ArrayList<Class_Evento_1> dataSet, String intNumeroAdapter) {

    //Le paso el context para poder llamar a la actividad que muestra el detalle
    public CustomAdapter_RecyclerView_Fiestas(List<Program> dataSet,
                                              String intNumeroAdapter,
                                              OnItemClickListenerFiestaSeleccionada mListener,
                                              Activity_Fragment_FrameLayout_WithNavDrawer_2
                                                      activity_fragment_frameLayout_withNavDrawer_2) {
//        mDataSet = dataSet;
        arrayList_De_Eventos_De_la_fiesta = dataSet;
//        TAG = this.getClass().getName();
        //Con simple name por que con name el string es muy largo por que nombra el paquete tambien
        TAG = this.getClass().getSimpleName();
            this.intNumeroAdapter = intNumeroAdapter;
            this.stringNumeroAdapter = TAG +" " + this.intNumeroAdapter +", ";

        this.mListener = mListener;

        this.activity_fragment_frameLayout_withNavDrawer_2 = activity_fragment_frameLayout_withNavDrawer_2;


        this.mListenerClickImagenDeLaFiesta = activity_fragment_frameLayout_withNavDrawer_2;
    }//Fin del constructor

    Activity_Fragment_FrameLayout_WithNavDrawer_2 activity_fragment_frameLayout_withNavDrawer_2;

    //Uso eete integer para saber que instancia estoy viendo en el log
    private String intNumeroAdapter;
    private String stringNumeroAdapter;

    // BEGIN_INCLUDE(recyclerViewOnCreateViewHolder)
    // Create new views (invoked by the layout manager)
    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
//                .inflate(R.layout.fila_info_eventos_1, viewGroup, false);
                    .inflate(R.layout.fila_info_fiesta_con_cartel_de_la_fiesta, viewGroup, false);



//        NOTA IMPORTANTE: ver selectableItemBackground en fila_info_eventos_1
//        To display visual responses like ripples on screen when a click event is detected add selectableItemBackground
//        resource in the layout (highlighted above).



//        return new ViewHolder(v);
        return new ViewHolder(v);

    }
    // END_INCLUDE(recyclerViewOnCreateViewHolder)

    // BEGIN_INCLUDE(recyclerViewOnBindViewHolder)
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(TAG, stringNumeroAdapter +"Element " + position + " set.");
        }

        //Prueba de uso de loader
        //Hago un chequeo de null, y regreso sin imprimir nada
        if(arrayList_De_Eventos_De_la_fiesta == null) return;


//        Juan, 26 Mayo 2015: Relleno el ViewHolder con los datos de los dias:

        // Get element from your dataset at this position and replace the contents of the view
        // with that element
//        Por ahora, solo muestro el string con el nombre de la fiesta

        viewHolder.getTextView_Titulo().
                setText(arrayList_De_Eventos_De_la_fiesta.get(position).getProgramSortDescription());
        method_Insertar_Fechas_De_La_Fiesta_2(viewHolder, position);

//        viewHolder.getTextView_Descripcion().
//                setText(arrayList_De_Eventos_De_la_fiesta.get(position).getDescripcion());
//        viewHolder.getTextView_Descripcion2().
//                setText(arrayList_De_Eventos_De_la_fiesta.get(position).getDescripcion2());

        //Esta imagen no hay que cargarla, lo dejo comentado, 18 agosto 2015
//        viewHolder.getImageView().
//                setImageResource(R.drawable.ic_launcher);

//        Juan, 3 Junio 2015, pongo los onclick aqui (como en el proyecto NavigationDrawer) y no en el constructor del viewHolder
        viewHolder.getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(TAG, "Element " + position + " clicked.");
                }


                //23 Feb 2015: Llamo a la actividad que muestra el detalle del evento
                // Do something in response to button

                mListener.onClick(arrayList_De_Eventos_De_la_fiesta.get(position));

            }
        });


        viewHolder.getView().setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(TAG, "Element " + position + " LONG clicked.");
                }
//                    OJO: Si retorno false, entonces se dispara el onClick!!
//                    return false;
//                    Por eso si solo quiero detectar el long click, retorno true
                return true;
            }
        });



        //11 julio 2015: Oscar queria meter aqui el cartel de la fiesta

//        OJO: aqui se cuelga a veces por falta de memoria para la imagen
//        Failed to allocate a 8814572 byte allocation with 1070104 free bytes and 1045KB until OOM
        //Voy a usar el metodo method_Descarga_Imagen_De_Internet
//        viewHolder.getImageView_Grande().setImageResource(R.drawable.mercaobarroco);

        //************************************************************************************
        //Juan 18 agosto 2015: presento la imagen como en:
        //http://themakeinfo.com/2015/04/android-retrofit-images-tutorial/
        //Con esta instruccion:
        //Picasso.with(context).load("http://i.imgur.com/DvpvklR.png").into(imageView);
        method_Descarga_Imagen_De_Internet(viewHolder.getImageView_Grande(),
                arrayList_De_Eventos_De_la_fiesta.get(position));
        //*************************************************************************************


        //******************************************************************************
        //Juan 27 junio 16: Oscar me ha pedido quitar esta funcion:
        //mostrar el cartel de las fiestas al hacer click.
        //Lo dejo comentado
        //*****************************************************************************

        //Juan 10 oct 2015, para coger el click en la imagen de las fiestas
//        viewHolder.getImageView_Grande().setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
//                    Log.d(TAG, "Element " + position + " clicked.");
//                }
//                mListenerClickImagenDeLaFiesta.onClickImagenDeLaFiesta(arrayList_De_Eventos_De_la_fiesta.get(position));
//            }
//        });

        //******************************************************************************
        //FIN DE:
        //Juan 27 junio 16: Oscar me ha pedido quitar esta funcion:
        //mostrar el cartel de las fiestas al hacer click.
        //Lo dejo comentado
        //*****************************************************************************

    }
    // END_INCLUDE(recyclerViewOnBindViewHolder)

    public void method_Descarga_Imagen_De_Internet(ImageView imageView, Program program) {
        //Juan 17 agosto 2015: presento la imagen como en:
        //http://themakeinfo.com/2015/04/android-retrofit-images-tutorial/
        //Con esta instruccion:
        //Picasso.with(context).load("http://i.imgur.com/DvpvklR.png").into(imageView);
        //O esta:
        //Picasso.with(getContext()).load(url+flower.getPhoto()).resize(100,100).into(img);

        String url = null;

        url = program.getListMultimediaElement().get(0).getMultimediaElementLocation();
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, ": En metodo method_Descarga_Imagen_De_Internet, url: " +url);
        }
        if(!Class_ConstantsHelper.methodStaticChequeaURL(url)){//url invalida
//            if(url == null || url.isEmpty()){
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG, ": En metodo method_Descarga_Imagen_De_Internet: url es NULL o esta VACIO");
            }
            imageView.setVisibility(View.GONE);
        }else {//url es valido, invoca la descarga de la imagen

            //Tengo que hacer esto en un asyntask en segundo plano
            //por si no la app se para, vamos, ni raranca
            //java.lang.IllegalStateException: Method call should not happen from the main thread.
//            method_Tamaño_De_La_Imagen_Descargada(url);

            //SE puede ver deformada
//            Picasso.with(activity_fragment_frameLayout_withNavDrawer_2).load(url).resize(180, 180).into(imageView);
            //Asi se ven bien, aunque algo recortadas, depende del tamaño
            //todas las imagene se ven del mismo tamaño, pero recortadas
            //Entonces un cartel que tenga texto, se puede ver fatal
//            Picasso.with(activity_fragment_frameLayout_withNavDrawer_2).load(url).fit().centerCrop().into(imageView);

            //asi se ve bien, la imagen sale entera pero dependiendo de la imagen, saldra mas vertical,
            //O mas horizontal, y cada imagen, de diferente tamaño.
            //O sea, que muestra toda la imagen respetando sus proporciones
            Picasso.with(activity_fragment_frameLayout_withNavDrawer_2).load(url).fit().centerInside().into(imageView);


            //ESto casca, me da puntero nulo, LOGICO
//             int origW = 0;
//             int origH = 0;
//            origW = imageView.getDrawable().getIntrinsicHeight();
//            origH = imageView.getDrawable().getIntrinsicWidth();
//            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
//                Log.e(TAG, ": En metodo method_Descarga_Imagen_De_Internet: ANCHO: " +origW);
//                Log.e(TAG, ": En metodo method_Descarga_Imagen_De_Internet: ALTO: " +origH);
//            }

        }
    }

    private void method_Tamaño_De_La_Imagen_Descargada(String url) {
        //Como en:
        //http://stackoverflow.com/questions/25522847/getting-image-width-and-height-with-picasso-library

        int width = 0;
        int height = 0;

        try {//Da error por que no puedo llamar a picasso desde el main thread
            //La app se para
            final Bitmap image = Picasso.with(activity_fragment_frameLayout_withNavDrawer_2).load(url).get();
            width = image.getWidth();
            height = image.getHeight();

        } catch (IOException e) {
            //No hago nada
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG, ": En metodo method_Tamaño_De_La_Imagen_Descargada: IOException: " +e);

            }

        }


        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, ": En metodo method_Tamaño_De_La_Imagen_Descargada: ANCHO: " +width);
            Log.e(TAG, ": En metodo method_Tamaño_De_La_Imagen_Descargada: ALTO: " +height);
        }
        //After this you can call again load with same url (It will be fetched from cache):

    }//Fin de method_Tamaño_De_La_Imagen_Descargada

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
//        return mDataSet.length;

        if (arrayList_De_Eventos_De_la_fiesta == null) {
            return 0;
        }

        return arrayList_De_Eventos_De_la_fiesta.size();
    }

    /**
     * Interface para recibir en Activity_Fragment_FrameLayout_WithNavDrawer_2 el click de la fiesta seleccionada
     */
    public interface OnItemClickListenerFiestaSeleccionada {
        public void onClick(Program m_Programa_seleccionado);
    }

    /**
     * Interface para recibir en Activity_Fragment_FrameLayout_WithNavDrawer_2 el click de la imagen de la fiesta
     */
    public interface OnItemClickListenerImagenDeLaFiesta {
        public void onClickImagenDeLaFiesta(Program m_Programa_seleccionado);
    }


    // Pone las fechas de inicio y fin de las fiestas
    public void method_Insertar_Fechas_De_La_Fiesta(ViewHolder viewHolder, int position) {
        //8 septiembre 2015: Ya no uso este metodo, uso method_Insertar_Fechas_De_La_Fiesta_2

    //        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
//        DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//        DateFormat df3 = new SimpleDateFormat("dd-MMM-yyyy");
//        DateFormat df4 = new SimpleDateFormat("MMMM dd, yyyy");
    DateFormat df4 = new SimpleDateFormat("MMMM d, yyyy");
//        DateFormat df5 = new SimpleDateFormat("E, MMM dd yyyy");
//        DateFormat df6 = new SimpleDateFormat("E, MMM dd yyyy HH:mm:ss");


//        String Desde = "De ";
//        String hasta = "a ";
//
//        viewHolder.getTextView_Descripcion().
//                append(df4.format("De ");
//
//        viewHolder.getTextView_Descripcion().
//                append(df4.format("De " + arrayList_De_Eventos_De_la_fiesta.get(position).getProgramStartDate()));
//
//        viewHolder.getTextView_Descripcion().
//                append("\n");
//
//        viewHolder.getTextView_Descripcion().
//                append("a ");
//
//        viewHolder.getTextView_Descripcion().
//                append(df4.format("a " +arrayList_De_Eventos_De_la_fiesta.get(position).getProgramEndDate()));

    viewHolder.getTextView_Descripcion().
    setText("De ");

    viewHolder.getTextView_Descripcion().
    append(df4.format(arrayList_De_Eventos_De_la_fiesta.get(position).getProgramStartDate()));

    viewHolder.getTextView_Descripcion().
    append("\n");

    viewHolder.getTextView_Descripcion().
    append("a ");

    viewHolder.getTextView_Descripcion().
    append(df4.format(arrayList_De_Eventos_De_la_fiesta.get(position).getProgramEndDate()));
}//Fin de method_Insertar_Fechas_De_La_Fiesta

    public void method_Insertar_Fechas_De_La_Fiesta_2(ViewHolder viewHolder, int position) {
        //8 sept 2015: Nuevo formato que pidio oscar

        DateFormat df4 = new SimpleDateFormat("d ");
        DateFormat df5 = new SimpleDateFormat("MMMM, yyyy");

        viewHolder.getTextView_Descripcion().
                setText("De ");

        viewHolder.getTextView_Descripcion().
                append(df4.format(arrayList_De_Eventos_De_la_fiesta.get(position).getProgramStartDate()));

        viewHolder.getTextView_Descripcion().
                append("de ");

        viewHolder.getTextView_Descripcion().
                append(df5.format(arrayList_De_Eventos_De_la_fiesta.get(position).getProgramStartDate()));

        viewHolder.getTextView_Descripcion().
                append("\n");

        viewHolder.getTextView_Descripcion().
                append("a ");

        viewHolder.getTextView_Descripcion().
                append(df4.format(arrayList_De_Eventos_De_la_fiesta.get(position).getProgramEndDate()));

        viewHolder.getTextView_Descripcion().
                append("de ");

        viewHolder.getTextView_Descripcion().
                append(df5.format(arrayList_De_Eventos_De_la_fiesta.get(position).getProgramEndDate()));


    }//Fin de method_Insertar_Fechas_De_La_Fiesta_2
}//Fin de la clase
