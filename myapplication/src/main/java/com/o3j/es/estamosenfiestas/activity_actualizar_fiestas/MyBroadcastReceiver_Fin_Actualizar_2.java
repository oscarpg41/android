package com.o3j.es.estamosenfiestas.activity_actualizar_fiestas;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.display_frame_layout.Activity_Fragment_FrameLayout_WithNavDrawer_2;

import managers.DownloadInfoFromServerForUpdateManager;

/**
 * Created by Juan on 31/08/2015.
 */
public class MyBroadcastReceiver_Fin_Actualizar_2 extends BroadcastReceiver {
    private static String xxx;
    Context context;

    //No hago nada en este broadcast
    @Override
    public void onReceive(Context context, Intent intent) {
        //**********************************************************************************
        // acquire the wake lock como lo explican en android developers
        //para el caso de que el movil este dormido
        PowerManager powerManager = (PowerManager) context.getSystemService(context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                "MyWakelockTag");
        wakeLock.acquire();

        //release the wake lock como lo explican en android developers
//        wakeLock.release();




        //**********************************************************************************


        xxx = this.getClass().getSimpleName();

        this.context = context;

        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
        {
            Log.e(xxx, " en metodo onReceive de MyBroadcastReceiver_Fin_Actualizar_2");
        }
        Bundle bundle = intent.getExtras();
        if (bundle != null) {

            Boolean Boolean_UPDATE_PROGRAMS_PROCESS_ERROR = bundle.getBoolean(DownloadInfoFromServerForUpdateManager.UPDATE_PROGRAMS_PROCESS_ERROR);
            Boolean UPDATE_HAY_PROGRAMAS_NUEVOS = bundle.getBoolean(DownloadInfoFromServerForUpdateManager.UPDATE_HAY_PROGRAMAS_NUEVOS);
            Boolean boolean_UPDATE_HAY_PROGRAMAS_QUE_ACTUALIZAR = bundle.getBoolean(DownloadInfoFromServerForUpdateManager.UPDATE_HAY_PROGRAMAS_QUE_ACTUALIZAR);
            int UPDATE_NUMERO_PROGRAMAS_ACTUALIZAR = bundle.getInt(DownloadInfoFromServerForUpdateManager.UPDATE_NUMERO_PROGRAMAS_ACTUALIZAR);
            int UPDATE_NUMERO_PROGRAMAS_NUEVOS = bundle.getInt(DownloadInfoFromServerForUpdateManager.UPDATE_NUMERO_PROGRAMAS_NUEVOS);



            if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D)
            {
                Log.e(xxx,   "  en metodo onReceive de MyBroadcastReceiver_Fin_Actualizar_2" +"\n" +
                        "Boolean_UPDATE_PROGRAMS_PROCESS_ERROR: " +Boolean_UPDATE_PROGRAMS_PROCESS_ERROR +"\n" +
                        " UPDATE_HAY_PROGRAMAS_NUEVOS  " +UPDATE_HAY_PROGRAMAS_NUEVOS +"\n" +
                        "boolean_UPDATE_HAY_PROGRAMAS_QUE_ACTUALIZAR: " +boolean_UPDATE_HAY_PROGRAMAS_QUE_ACTUALIZAR +"\n" +
                        "UPDATE_NUMERO_PROGRAMAS_ACTUALIZAR: " +UPDATE_NUMERO_PROGRAMAS_ACTUALIZAR +"\n" +
                        "UPDATE_NUMERO_PROGRAMAS_NUEVOS: " +UPDATE_NUMERO_PROGRAMAS_NUEVOS);
            }


            if(!Boolean_UPDATE_PROGRAMS_PROCESS_ERROR) {
                if(boolean_UPDATE_HAY_PROGRAMAS_QUE_ACTUALIZAR){
                    methodEnviarNotificacionDeProgramasActualizados();
                }else{
                    if(UPDATE_HAY_PROGRAMAS_NUEVOS){
                        methodEnviarNotificacionDeHayUnProgramaNuevo();
                    }else{//No hay nada que actualizar
                        //No hago nada, si no hay algo actualizado, NO se notifica al usuario
                    }
                }

            }else{//Si hay un error, no hago nada

            }
        }

        //**********************************************************************************
        // acquire the wake lock como lo explican en android developers
        //para el caso de que el movil este dormido
//        PowerManager powerManager = (PowerManager) context.getSystemService(context.POWER_SERVICE);
//        PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
//                "MyWakelockTag");
//        wakeLock.acquire();

        //release the wake lock como lo explican en android developers
        wakeLock.release();
        //**********************************************************************************

    }

    public void methodEnviarNotificacionDeProgramasActualizados() {
        //*************************************************************************************************
        //*************************************************************************************************
        //*************************************************************************************************
        //Juan, 6 oct 2015:Chequeo si la version de android es menor a lollipop para mandar un tipo
        //de notificacion u otra

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            methodEnviarNotificacionDeProgramasActualizados_1();
        } else {//es lollipop o superior
            methodEnviarNotificacionDeProgramasActualizados_2();
        }

        //Fin del chequeo de madrugada
        //*************************************************************************************************
        //*************************************************************************************************
        //*************************************************************************************************

    }

    public void methodEnviarNotificacionDeProgramasActualizados_2() {//>= Lollipop
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, xxx + "En metodo  methodEnviarNotificacionDeProgramasActualizados_2 ");
        }
        Bitmap notificationLargeIconBitmap = BitmapFactory.decodeResource(
                context.getResources(),
                R.drawable.logo01);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
//                        .setSmallIcon(R.drawable.logo01)
                        //Juan 6 oct 2015: Para versiones >= lollipop, uso el icono con alpha en small y el normal de 512 en large
                        .setSmallIcon(R.drawable.logo_01_alpha_2_gimp)
                        .setContentTitle("Actualización de fiestas")
                        .setContentText("Fiestas actualizadas")
                        .setAutoCancel(true)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
//                        .setDefaults(NotificationCompat.DEFAULT_SOUND | NotificationCompat.DEFAULT_VIBRATE
//                                | NotificationCompat.DEFAULT_LIGHTS)
                        .setDefaults(NotificationCompat.DEFAULT_SOUND)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setLargeIcon(notificationLargeIconBitmap);

// Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
        //**********************
        //**********************
        resultIntent.setAction(Intent.ACTION_SCREEN_ON);
        //**********************
        //**********************

        // The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from the Activity leads out of
// your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
// Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
// Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
        mNotificationManager.notify(0, mBuilder.build());
    }//Fin de methodEnviarNotificacionDeProgramasActualizados_2

    public void methodEnviarNotificacionDeProgramasActualizados_1() {//< Lollipop
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, xxx + "En metodo  methodEnviarNotificacionDeProgramasActualizados_1 ");
        }

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
//                        .setSmallIcon(R.drawable.logo01)
                        //Juan 6 oct 2015: Para versiones < lollipop, uso el icono de 72x72
                        .setSmallIcon(R.drawable.logo_01_72x72)
                        .setContentTitle("Actualización de fiestas")
//                        .setContentText("Revisa los programas de tus fiestas, hay actualizaciones")
                        .setContentText("Fiestas actualizadas")
                        .setAutoCancel(true)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
//                        .setDefaults(NotificationCompat.DEFAULT_SOUND | NotificationCompat.DEFAULT_VIBRATE
//                                | NotificationCompat.DEFAULT_LIGHTS)
                        .setDefaults(NotificationCompat.DEFAULT_SOUND)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT);
// Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
        //**********************
        //**********************
        resultIntent.setAction(Intent.ACTION_SCREEN_ON);
        //**********************
        //**********************

        // The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from the Activity leads out of
// your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
// Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
// Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
        mNotificationManager.notify(0, mBuilder.build());
    }//Fin de methodEnviarNotificacionDeProgramasActualizados_1






    public void methodEnviarNotificacionDeHayUnProgramaNuevo() {
        //*************************************************************************************************
        //*************************************************************************************************
        //*************************************************************************************************
        //Juan, 6 oct 2015:Chequeo si la version de android es menor a lollipop para mandar un tipo
        //de notificacion u otra

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            methodEnviarNotificacionDeHayUnProgramaNuevo_1();
        } else {//es lollipop o superior
            methodEnviarNotificacionDeHayUnProgramaNuevo_2();
        }

        //Fin del chequeo de madrugada
        //*************************************************************************************************
        //*************************************************************************************************
        //*************************************************************************************************

    }

    public void methodEnviarNotificacionDeHayUnProgramaNuevo_2() {//>= Lollipop
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, xxx + "En metodo  methodEnviarNotificacionDeHayUnProgramaNuevo_2 ");
        }

        Bitmap notificationLargeIconBitmap = BitmapFactory.decodeResource(
                context.getResources(),
                R.drawable.logo01);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        //Juan 6 oct 2015: Para versiones >= lollipop, uso el icono con alpha en small y el normal de 512 en large
                        .setSmallIcon(R.drawable.logo_01_alpha_2_gimp)
                        .setContentTitle("Actualización de fiestas")
                        .setContentText("Hay un nuevo programa de fiestas")
                        .setAutoCancel(true)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
//                        .setDefaults(NotificationCompat.DEFAULT_SOUND | NotificationCompat.DEFAULT_VIBRATE
//                                | NotificationCompat.DEFAULT_LIGHTS)
                        .setDefaults(NotificationCompat.DEFAULT_SOUND)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setLargeIcon(notificationLargeIconBitmap);

// Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
        //**********************
        //**********************
        resultIntent.setAction(Intent.ACTION_SCREEN_ON);
        //**********************
        //**********************

        // The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from the Activity leads out of
// your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
// Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
// Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
        mNotificationManager.notify(0, mBuilder.build());
    }//Fin de methodEnviarNotificacionDeHayUnProgramaNuevo_2


    public void methodEnviarNotificacionDeHayUnProgramaNuevo_1() {//< Lollipop
        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(xxx, xxx + "En metodo  methodEnviarNotificacionDeHayUnProgramaNuevo_1 ");
        }

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
//                        .setSmallIcon(R.drawable.logo01)
                        //Juan 6 oct 2015: Para versiones < lollipop, uso el icono de 72x72
                        .setSmallIcon(R.drawable.logo_01_72x72)
                        .setContentTitle("Actualización de fiestas")
                        .setContentText("Hay un nuevo programa de fiestas")
                        .setAutoCancel(true)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
//                        .setDefaults(NotificationCompat.DEFAULT_SOUND | NotificationCompat.DEFAULT_VIBRATE
//                                | NotificationCompat.DEFAULT_LIGHTS)
                        .setDefaults(NotificationCompat.DEFAULT_SOUND)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT);
// Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
        //**********************
        //**********************
        resultIntent.setAction(Intent.ACTION_SCREEN_ON);
        //**********************
        //**********************

        // The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from the Activity leads out of
// your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
// Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(Activity_Fragment_FrameLayout_WithNavDrawer_2.class);
// Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
        mNotificationManager.notify(0, mBuilder.build());
    }//Fin de methodEnviarNotificacionDeHayUnProgramaNuevo_1


}
