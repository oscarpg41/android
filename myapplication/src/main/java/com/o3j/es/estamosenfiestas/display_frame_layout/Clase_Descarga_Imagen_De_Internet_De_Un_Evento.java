package com.o3j.es.estamosenfiestas.display_frame_layout;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.eef.data.dataelements.Event;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper;
import com.squareup.picasso.Picasso;


//Juan, 19 agosto 2015, clase para guardar favoritos. es usada por:
//CustomAdapter_RecyclerView_Evento_Detalle
//
public class Clase_Descarga_Imagen_De_Internet_De_Un_Evento {

    private static  String TAG;
    Event event;
    ImageView imageView;
    Context context;

    public static Clase_Descarga_Imagen_De_Internet_De_Un_Evento newInstance(ImageView imageView, Event event, Context context) {



        Clase_Descarga_Imagen_De_Internet_De_Un_Evento frag = new Clase_Descarga_Imagen_De_Internet_De_Un_Evento();

        frag.event = event;
        frag.imageView = imageView;
        frag.context = context;
        TAG = frag.getClass().getSimpleName();

        return frag;
    }//Fin de newInstance




    //******************************************************************************************************
    public void method_Descarga_Imagen_De_Internet() {

        //Juan 17 agosto 2015: presento la imagen como en:
        //http://themakeinfo.com/2015/04/android-retrofit-images-tutorial/
        //Con esta instruccion:
        //Picasso.with(context).load("http://i.imgur.com/DvpvklR.png").into(imageView);
        //O esta:
        //Picasso.with(getContext()).load(url+flower.getPhoto()).resize(100,100).into(img);

        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, " En metodo method_Descarga_Imagen_De_Internet:  ");
        }

        String url = null;
        url = event.getMultimediaElementList().get(0).getMultimediaElementLocation();
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, ": En metodo method_Descarga_Imagen_De_Internet, url: " + url);
        }

        if(!Class_ConstantsHelper.methodStaticChequeaURL(url)){//url invalida
//            if(url == null || url.isEmpty()){
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG, ": En metodo method_Descarga_Imagen_De_Internet: url es NULL o esta VACIO");
            }
            //Oscar quiere que si no hay imagen, se vea mas pequena la cardview
            imageView.setVisibility(View.GONE);
//            imageView.setVisibility(View.INVISIBLE);
        }else{//url es valido, invoca la descarga de la imagen
            //Asi se ven las imagenes deformadas
//            Picasso.with(activity_Fragment_SwipeView_EventoDetalle_BackToParent).load(url).resize(400, 400).into(imageView);
            //Asi se ven bien, aunque algo recortadas, depende del tamaño
            //todas las imagene se ven del mismo tamaño, pero recortadas
            //Entonces un cartel que tenga texto, se puede ver fatal
            Picasso.with(context).load(url).fit().centerCrop().into(imageView);


            //asi se ve bien, la imagen sale entera pero dependiendo de la imagen, saldra mas vertical,
            //O mas horizontal, y cada imagen, de diferente tamaño.
            //O sea, que muestra toda la imagen respetando sus proporciones
//            Picasso.with(context).load(url).error(R.drawable.ic_launcher).fit().centerInside().into(imageView);
            //Sin el error
//            Picasso.with(context).load(url).fit().centerInside().into(imageView);
        }



    }//method_Descarga_Imagen_De_Internet
}
