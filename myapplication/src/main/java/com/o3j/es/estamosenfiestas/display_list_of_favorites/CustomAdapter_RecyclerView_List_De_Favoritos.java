/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.o3j.es.estamosenfiestas.display_list_of_favorites;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper;
import com.o3j.es.estamosenfiestas.display_frame_layout.Class_Modelo_De_Datos_Lista_Favoritos;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Provide views to RecyclerView with data from mDataSet.
 */
//creado el 21 feb 2014  para nuevo layout fila_info_eventos
public class CustomAdapter_RecyclerView_List_De_Favoritos extends RecyclerView.Adapter<CustomAdapter_RecyclerView_List_De_Favoritos.ViewHolder> {
//    private static final String TAG = "CustomAdapter";
    private static  String TAG;



//    private static FragmentActivity actividad;


    //Original
//    private String[] mDataSet;


    private List<Class_Modelo_De_Datos_Lista_Favoritos> List_De_Eventos_Favoritos;



//    Agrego la interface para recoger los clicks en la actividad
    private OnItemClickListenerEventoFavoritoSeleccionado mListener;

    // BEGIN_INCLUDE(recyclerViewSampleViewHolder)
    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */


    public static class ViewHolder extends RecyclerView.ViewHolder {
        //Los nombres como en Class_Evento_1
        private final TextView textView_nombre_de_la_fiesta;
        private final TextView textView_Titulo;
        private final TextView textView_Descripcion;
        private final TextView textView_Descripcion2;
        private final ImageView imageView;
        private final View m_View;

//        4 Junio 2015: uso este textview para mostrar la hora del evento
        private final TextView textView_Hora_Del_Evento;

        public TextView get_textView_nombre_de_la_fiesta() {

            return textView_nombre_de_la_fiesta;
        }

        public TextView getTextView_Titulo() {

            return textView_Titulo;
        }

        public TextView getTextView_Descripcion() {

            return textView_Descripcion;
        }

        public TextView getTextView_Descripcion2() {

            return textView_Descripcion2;
        }

        public ImageView getImageView() {
            return imageView;
        }

        public View getView() {
            return m_View;
        }


        public TextView getTextView_Hora_Del_Evento() {
            return textView_Hora_Del_Evento;
        }


        public ViewHolder(View v) {
            super(v);
            // Define click listener for the ViewHolder's View.


//            OJO: ver en http://www.truiton.com/2015/02/android-recyclerview-tutorial/
//            como poner el listener en el fragment

//        Juan, 3 Junio 2015, pongo los onclick en onBindViewHolder
//        (como en el proyecto NavigationDrawer) y no en el constructor del viewHolder
//            Comento estos que son los originales

//            v.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
//                        Log.d(TAG, "Element " + getPosition() + " clicked.");
//                    }
//
//
//                    //23 Feb 2015: Llamo a la actividad que muestra el detalle del evento
//                    // Do something in response to button
//
//
//                }
//            });


            // Juan, 26 Mayo 2015: Solo detecto el click, por ahora
//            v.setOnLongClickListener(new View.OnLongClickListener(){
//
//                @Override
//                public boolean onLongClick(View v) {
//                    if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
//                        Log.d(TAG, "Element " + getPosition() + " LONG clicked.");
//                    }
////                    OJO: Si retorno false, entonces se dispara el onClick!!
////                    return false;
////                    Por eso si solo quiero detectar el long click, retorno true
//                    return true;
//                }
//            });



            m_View = v;

            textView_nombre_de_la_fiesta = (TextView) v.findViewById(R.id.textView_nombre_de_la_fiesta);
            textView_Titulo = (TextView) v.findViewById(R.id.textView6);
            textView_Descripcion = (TextView) v.findViewById(R.id.textView7);
            textView_Descripcion2 = (TextView) v.findViewById(R.id.textView8);
            imageView = (ImageView) v.findViewById(R.id.imageView);

//            Juan, 26 Mayo 2015: Este adapter solo muestra la fecha que esta en el textView_Titulo
//            El resto de controles los oculto por ahora
            textView_Descripcion.setVisibility(View.GONE);
            textView_Descripcion2.setVisibility(View.GONE);
//            imageView.setVisibility(View.GONE);

//            4 Junio 2015: uso este textview para mostrar la hora del evento
            textView_Hora_Del_Evento = (TextView) v.findViewById(R.id.textView_hora_del_evento);
            textView_Hora_Del_Evento.setVisibility(View.VISIBLE);

        }

    }//Fin de la clase ViewHolder
    // END_INCLUDE(recyclerViewSampleViewHolder)


    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used by RecyclerView.
     */
    //Original
//    public CustomAdapter_RecyclerView_3(String[] dataSet) {

    //21 Feb 2015 modificado para funcionar con el array list
//    public CustomAdapter_RecyclerView_3(ArrayList<Class_Evento_1> dataSet) {
    //Original
//    public CustomAdapter_RecyclerView_3(ArrayList<Class_Evento_1> dataSet, String intNumeroAdapter) {

    //Le paso el context para poder llamar a la actividad que muestra el detalle
    public CustomAdapter_RecyclerView_List_De_Favoritos(List<Class_Modelo_De_Datos_Lista_Favoritos> dataSet,
                                                        String intNumeroAdapter,
                                                        OnItemClickListenerEventoFavoritoSeleccionado mListener) {
//        mDataSet = dataSet;
        List_De_Eventos_Favoritos = dataSet;
//        TAG = this.getClass().getName();
        //Con simple name por que con name el string es muy largo por que nombra el paquete tambien
        TAG = this.getClass().getSimpleName();
            this.intNumeroAdapter = intNumeroAdapter;
            this.stringNumeroAdapter = TAG +" " + this.intNumeroAdapter +", ";

        this.mListener = mListener;
    }//Fin del constructor


    //Uso eete integer para saber que instancia estoy viendo en el log
    private String intNumeroAdapter;
    private String stringNumeroAdapter;

    // BEGIN_INCLUDE(recyclerViewOnCreateViewHolder)
    // Create new views (invoked by the layout manager)
    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
//                .inflate(R.layout.text_row_item, viewGroup, false);
//                .inflate(R.layout.fila_info_eventos_1, viewGroup, false);
                  .inflate(R.layout.fila_info_eventos_favoritos, viewGroup, false);

//        NOTA IMPORTANTE: ver selectableItemBackground en fila_info_eventos_1
//        To display visual responses like ripples on screen when a click event is detected add selectableItemBackground
//        resource in the layout (highlighted above).



//        return new ViewHolder(v);
        return new ViewHolder(v);

    }
    // END_INCLUDE(recyclerViewOnCreateViewHolder)

    // BEGIN_INCLUDE(recyclerViewOnBindViewHolder)
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder( ViewHolder viewHolder, final int position) {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(TAG, stringNumeroAdapter +"Element " + position + " set.");
        }


        //Prueba de uso de loader
        //Hago un chequeo de null, y regreso sin imprimir nada
        if(List_De_Eventos_Favoritos == null) return;


//        Juan, 26 Mayo 2015: Relleno el ViewHolder con los datos de los dias:

        // Get element from your dataset at this position and replace the contents of the view
        // with that element
//        Por ahora, solo muestro el string con el dia


        viewHolder.get_textView_nombre_de_la_fiesta().setText(List_De_Eventos_Favoritos.get(position).getStringTituloDeLaFiesta());

        //Pongo el nombre de la fiesta resaltado en negrita
        viewHolder.get_textView_nombre_de_la_fiesta().
                setTypeface(null, Typeface.BOLD);


        viewHolder.getTextView_Titulo().
                setText(List_De_Eventos_Favoritos.get(position).getEventEventoFavorito().getSortText());

        //***********************************************************************************************
        //Prueba para poner el titulo de la fiesta
//        viewHolder.getTextView_Titulo().
//                append("\n" +List_De_Eventos_Favoritos.get(position).getStringTituloDeLaFiesta());
        //*************************************************************************************************

//        viewHolder.getTextView_Titulo().
//                setText(arrayList_De_Eventos_De_la_fiesta.get(position).toString());
//        viewHolder.getTextView_Descripcion().
//                setText(arrayList_De_Eventos_De_la_fiesta.get(position).getDescripcion());
//        viewHolder.getTextView_Descripcion2().
//                setText(arrayList_De_Eventos_De_la_fiesta.get(position).getDescripcion2());


//        viewHolder.getImageView().
//                setImageResource(R.drawable.ic_launcher);

        //            4 Junio 2015: uso este textview para mostrar la hora del evento
        DateFormat df2 = new SimpleDateFormat("HH:mm");

        DateFormat df4 = new SimpleDateFormat("MMMM dd, yyyy");
        DateFormat df5 = new SimpleDateFormat("MMMM dd yyyy, HH:mm");


        viewHolder.getTextView_Hora_Del_Evento().
                setText(df5.format(List_De_Eventos_Favoritos.get(position).getEventEventoFavorito().getStartDate()));

        viewHolder.getTextView_Hora_Del_Evento().
                append("  ");//Hago esto para despegar la hora del texto

//        Juan, 3 Junio 2015, pongo los onclick aqui (como en el proyecto NavigationDrawer) y no en el constructor del viewHolder
        viewHolder.getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(TAG, "Element " + position + " clicked.");
                }


                //23 Feb 2015: Llamo a la actividad que muestra el detalle del evento
                // Do something in response to button

//                Esta llamada obtiene un objeto Events que tiene todos los datos de ese evento
                mListener.onClickEventoFavorito(position);

            }
        });


        int int_Id_De_La_View_Clickada = viewHolder.getView().getId();

        viewHolder.getView().setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                    Log.d(TAG, "Element " + position + " LONG clicked.");
                }

                boolean boolean_contextual_action_mode = mListener.onLongClickEventoFavorito(position, v);
//                    OJO: Si retorno false, entonces se dispara el onClick!!
//                    return false;
//                    Por eso si solo quiero detectar el long click, retorno true
                return boolean_contextual_action_mode;
            }
        });
    }
    // END_INCLUDE(recyclerViewOnBindViewHolder)

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
//        return mDataSet.length;


        if (List_De_Eventos_Favoritos == null) {
            return 0;
        }

        return List_De_Eventos_Favoritos.size();
    }

    /**
     * Interface para recibir en Activity_Fragment_FrameLayout_WithNavDrawer_2 el click de la fiesta seleccionada
     */
    public interface OnItemClickListenerEventoFavoritoSeleccionado {
        public void onClickEventoFavorito(int  m_position);

//        Este metodo lo puedo usar para borrar el favorito de la lista
        public boolean onLongClickEventoFavorito(int  m_position, View m_View_Del_Evento_Favorito);

    }


    public void removeItem(int position) {

        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, " metodo He llegado a removeItem");
        }

        List_De_Eventos_Favoritos.remove(position);
        notifyItemRemoved(position);
//        notifyItemRangeChanged(getPosition, mDataSet.size());

        notifyDataSetChanged();

    }

}//Fin de la clase
