/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.o3j.es.estamosenfiestas.nav_drawer_utilities;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper;
import com.o3j.es.estamosenfiestas.R;

import java.util.ArrayList;

/**
 * Provide views to RecyclerView with data from mDataSet.
 */
//creado el 24 feb 2014  para nuevo layout del contenido del navigation drawer
public class CustomAdapter_NavigationDrawer extends RecyclerView.Adapter<CustomAdapter_NavigationDrawer.ViewHolder> {
//    private static final String TAG = "CustomAdapter";
    private static  String TAG;


    private static ActionBarActivity actividad;


    //Original
//    private String[] mDataSet;

    //21 Feb 2015 modificado para funcionar con el array list
    private ArrayList<ClassMenuItemsNavigationDrawer> arrayListItemsNavigationDrawer;


    // BEGIN_INCLUDE(recyclerViewSampleViewHolder)
    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        //Los nombres como en Class_Evento_1
        private final TextView textView_Titulo;
        private final TextView textView_Descripcion;
        private final ImageView imageView;

        public TextView getTextView_Titulo() {
            return textView_Titulo;
        }

        public TextView getTextView_Descripcion() {
            return textView_Descripcion;
        }


        public ImageView getImageView() {
            return imageView;
        }



        public ViewHolder(View v) {
            super(v);
            // Define click listener for the ViewHolder's View.
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                        Log.d(TAG, "Element " + getPosition() + " clicked.");
                    }

                    //23 Feb 2015: Llamo a la actividad que muestra el detalle del evento
                    // Do something in response to button
//                    Intent intent = new Intent(actividad,
//                            com.o3j.es.activity_fragment_base_2.Activity_Fragment_Evento_Detalle.class);
//                    actividad.startActivity(intent);
                }
            });
            textView_Titulo = (TextView) v.findViewById(R.id.textView6);
            textView_Descripcion = (TextView) v.findViewById(R.id.textView7);
            imageView = (ImageView) v.findViewById(R.id.imageView);
        }

    }//Fin de la clase ViewHolder
    // END_INCLUDE(recyclerViewSampleViewHolder)


    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used by RecyclerView.
     */
    //Original
//    public CustomAdapter_RecyclerView_3(String[] dataSet) {

    //21 Feb 2015 modificado para funcionar con el array list
//    public CustomAdapter_RecyclerView_3(ArrayList<Class_Evento_1> dataSet) {
    //Original
//    public CustomAdapter_RecyclerView_3(ArrayList<Class_Evento_1> dataSet, String intNumeroAdapter) {

    //Le paso el context para poder llamar a la actividad que muestra el detalle
    public CustomAdapter_NavigationDrawer(ArrayList<ClassMenuItemsNavigationDrawer> dataSet,
                                          String intNumeroAdapter,
                                          ActionBarActivity actividad) {
//        mDataSet = dataSet;
        arrayListItemsNavigationDrawer = dataSet;
//        TAG = this.getClass().getName();
        //Con simple name por que con name el string es muy largo por que nombra el paquete tambien
        TAG = this.getClass().getSimpleName();
            this.intNumeroAdapter = intNumeroAdapter;
            this.stringNumeroAdapter = TAG +" " + this.intNumeroAdapter +", ";

        this.actividad = actividad;


    }//Fin del constructor


    //Uso eete integer para saber que instancia estoy viendo en el log
    private String intNumeroAdapter;
    private String stringNumeroAdapter;

    // BEGIN_INCLUDE(recyclerViewOnCreateViewHolder)
    // Create new views (invoked by the layout manager)
    @Override
//    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                  .inflate(R.layout.fila_menu_navigationdrawer_1, viewGroup, false);

//        return new ViewHolder(v);
        return new ViewHolder(v);

    }
    // END_INCLUDE(recyclerViewOnCreateViewHolder)

    // BEGIN_INCLUDE(recyclerViewOnBindViewHolder)
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(TAG, stringNumeroAdapter +"Element " + position + " set.");
        }


        //Prueba de uso de loader
        //Hago un chequeo de null, y regreso sin imprimir nada
        if(arrayListItemsNavigationDrawer == null) return;


        // Get element from your dataset at this position and replace the contents of the view
        // with that element
//        viewHolder.getTextView().setText(mDataSet[position]);
        viewHolder.getTextView_Titulo().
                setText(arrayListItemsNavigationDrawer.get(position).getMenuItemTitle());
        viewHolder.getTextView_Descripcion().
                setText(arrayListItemsNavigationDrawer.get(position).getItemTitleDescrption());
        viewHolder.getImageView().
                setImageResource(arrayListItemsNavigationDrawer.get(position).getSrc_Del_Image_View());
    }
    // END_INCLUDE(recyclerViewOnBindViewHolder)

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
//        return mDataSet.length;

        if (arrayListItemsNavigationDrawer == null) {
            return 0;
        }

        return arrayListItemsNavigationDrawer.size();
    }
}
