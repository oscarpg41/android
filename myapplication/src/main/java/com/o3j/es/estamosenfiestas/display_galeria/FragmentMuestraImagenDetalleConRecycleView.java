package com.o3j.es.estamosenfiestas.display_galeria;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.eef.data.dataelements.Event;
import com.o3j.es.estamosenfiestas.R;
import com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper;
import com.o3j.es.estamosenfiestas.display_lists_package.DividerItemDecoration;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentMuestraImagenDetalleConRecycleView.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentMuestraImagenDetalleConRecycleView#newInstance} factory method to
 * create an instance of this fragment.
 */

//Este fragment muesta la lista de los eventos del dia seleccionado de la fiesta
public class FragmentMuestraImagenDetalleConRecycleView extends android.support.v4.app.Fragment
//        implements LoaderManager.LoaderCallbacks<ArrayList<Class_Evento_1>>{
        implements android.support.v4.app.LoaderManager.LoaderCallbacks<ArrayList<com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_Evento_1>> {
    //        implements android.support.v4.app.LoaderManager.LoaderCallbacks<ArrayList<Class_Evento_1>>{
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static String xxx;

    //    public static final String ARG_OBJECT = "object";
//    public static String ARG_OBJECT = "object";
    public static String ARG_OBJECT = "lista ";

    //ARG_OBJECT no puede ser static por que entonces coje el valor del ultimo objeto instanciado
//    public  String ARG_OBJECT = "object";


    private static final String ARG_PARAM1 = "param1";
    public static final String ARG_PARAM2 = "param2";
    public static final String TIPO_DE_LISTA = "tipo_De_Lista";

    private String mParam1;
    private String mParam2;
    private int int_Tipo_De_Lista;
    private int intPosicionDeLaImagenEnElArray;

    private OnFragmentInteractionListener mListener;

    private List<Event> arrayListDeEventos;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MiFragment_1.
     */

    //Este builder no lo uso por ahora
    public static FragmentMuestraImagenDetalleConRecycleView newInstance(String param1, String param2) {
        FragmentMuestraImagenDetalleConRecycleView fragment = new FragmentMuestraImagenDetalleConRecycleView();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentMuestraImagenDetalleConRecycleView() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

            mParam1 = getArguments().getString(ARG_OBJECT);
            int_Tipo_De_Lista = getArguments().getInt(TIPO_DE_LISTA);
//            mProgram = (Program )getArguments().getSerializable("Programa Serializado");
//            m_Lista_De_Fiestas = (Program )getArguments().getSerializable("Programa Serializado");
            arrayListDeEventos = (List<Event>)getArguments().getSerializable("arrayListDeEventos");

            intPosicionDeLaImagenEnElArray = getArguments().getInt(TIPO_DE_LISTA);
        }

        xxx = this.getClass().getSimpleName();


        //Cuidado con esto que produce mProgram es nulo
//        if (com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
//            Log.d(xxx, xxx + " " + mParam1 + " onCreate");
//            Log.d(xxx, xxx + " " + mParam1 + " onCreate: " + mProgram.getProgramSortDescription());
//            Log.d(xxx, xxx + " " + mParam1 + " onCreate: " + mProgram.getProgramLongDescription());
//        }

        //****************************************************************
        //****************************************************************
        //             Codigo relacionado con recycler view
        //****************************************************************
        //****************************************************************
        // Initialize dataset, this data would usually come from a local content provider or
        // remote server.
//        initDataset();

        //Iniciar el array con los eventos de la fiesta

        //Lo comento por que ahora se hace con el loader
//        init_arrayList_De_Eventos_De_la_fiesta();
        //****************************************************************
        //****************************************************************
        //             FIN Codigo relacionado con recycler view
        //****************************************************************
        //****************************************************************
    }//Fin del onCreate


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_mi_fragment_1, container, false);
        //****************************************************************
        //****************************************************************
        //             Codigo relacionado con recycler view
        //****************************************************************
        //****************************************************************
        View rootView = inflater.inflate(R.layout.fragment_mi_fragment_1, container, false);
        rootView.setTag(TAG);
        // BEGIN_INCLUDE(initializeRecyclerView)
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view_1);

        //************************************************************
        //Juan, 14 oct 2015: Se muestran las imagenes en un image view
        //ya no uso recyclerView, lo pongo a gone

        mRecyclerView.setVisibility(View.GONE);

        ImageView mImageViewImagenDetalle = (ImageView) rootView.findViewById((R.id.imageView_detalle_imagen_de_la_fiesta));
        mImageViewImagenDetalle.setVisibility(View.VISIBLE);
        method_Descarga_Imagen_De_Internet(mImageViewImagenDetalle,
                arrayListDeEventos.get(0));
        //Juan, 14 oct 2015: Se muestran las imagenes en un image view
        //*************************************************************


        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.

        //Con LINEAR_LAYOUT_MANAGER
        mLayoutManager = new LinearLayoutManager(getActivity());
        mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;

        //Con GRID_LAYOUT_MANAGER
//        mLayoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
//        mCurrentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;



        if (savedInstanceState != null) {
            // Restore saved layout manager type.
            mCurrentLayoutManagerType = (LayoutManagerType) savedInstanceState
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);


        mAdapter = new CustomAdapterRecyclerViewListaDeImagenDetalle(arrayListDeEventos, mParam1,
                (CustomAdapterRecyclerViewListaDeImagenDetalle.OnItemClickListenerImagenSeleccionada)getActivity(), this);

                // Set CustomAdapter as the adapter for RecyclerView.
                mRecyclerView.setAdapter(mAdapter);

        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL);
        mRecyclerView.addItemDecoration(itemDecoration);
        // END_INCLUDE(initializeRecyclerView)
//        ****************************************************************************

        //Manejo de los radio buttons
        mLinearLayoutRadioButton = (RadioButton) rootView.findViewById(R.id.linear_layout_rb);
        mLinearLayoutRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRecyclerViewLayoutManager(LayoutManagerType.LINEAR_LAYOUT_MANAGER);
            }
        });

        mGridLayoutRadioButton = (RadioButton) rootView.findViewById(R.id.grid_layout_rb);
        mGridLayoutRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRecyclerViewLayoutManager(LayoutManagerType.GRID_LAYOUT_MANAGER);
            }
        });
        //****************************************************************
        //****************************************************************
        //             FIN Codigo relacionado con recycler view
        //****************************************************************
        //****************************************************************




        //24 junio 2015: No muestro los textos. Se muestran en Activity_Fragment_SwipeView_EventoDetalle_BackToParent
        //como me pidio Oscar
//        Texto para mostrar el titulo de la fiesta
        TextView textView_Titulo = (TextView) rootView.findViewById(R.id.titulo_del_fragment);

        textView_Titulo.setVisibility(View.GONE);

        textView_Titulo.setText(Class_ConstantsHelper.titulo_De_La_Fiesta);

//        Texto para mostrar la fecha del dia de los eventos de ese dia
        TextView textView_Fecha_Del_Dia = (TextView) rootView.findViewById(R.id.fecha_del_dia);

        textView_Fecha_Del_Dia.setVisibility(View.GONE);

//        textView_Fecha_Del_Dia.setText(Class_ConstantsHelper.fecha_Del_Dia);


        //Pongo como fecha del dia la del evento
        SimpleDateFormat sfd = new SimpleDateFormat("EEEE dd LLLL yyyy");
//        sfd.format(this.m_Event.getStartDate());

        //OJO: esto da null pointer exception, lo comento
//        textView_Fecha_Del_Dia.setText(sfd.format(arrayListDeEventos.get(0).getStartDate()));



        return rootView;
    }//Fin de onCreateView

//    public void onClick_ir_a_favoritos(View view) {
//
//        Intent intent = new Intent(getActivity(), Activity_Fragment_List_Of_Favorites.class);
//        startActivity(intent);
//    }

    //Variables para que funcione el recycle view
    private static final String TAG = "RecyclerViewFragment";
    protected RecyclerView mRecyclerView;
    protected LayoutManagerType mCurrentLayoutManagerType;//Tipo enum


    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }

    protected RecyclerView.LayoutManager mLayoutManager;
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    //Original
//    protected CustomAdapter_RecyclerView_2 mAdapter;
    //Nuevo adapter para el array list
//    protected com.o3j.es.estamosenfiestas.activity_fragment_base_2.CustomAdapter_RecyclerView_3 mAdapter;
//    protected CustomAdapter_RecyclerView_3 mAdapter;
    protected CustomAdapterRecyclerViewListaDeImagenDetalle mAdapter;
    protected RadioButton mLinearLayoutRadioButton;
    protected RadioButton mGridLayoutRadioButton;


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save currently selected layout manager.
        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, mCurrentLayoutManagerType);
        super.onSaveInstanceState(savedInstanceState);
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction("ahora no lo uso");
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(String string);
    }

    /**
     * Set RecyclerView's LayoutManager to the one given.
     *
     * @param layoutManagerType Type of layout manager to switch to.
     */
    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        switch (layoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                mLayoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
                mCurrentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }

    private static final int SPAN_COUNT = 2;

    //****************************************************************
    //****************************************************************
    //             Codigo relacionado con el loader
    //****************************************************************
    //****************************************************************

//    @Override
//    public Loader<ArrayList<Class_Evento_1>> onCreateLoader(int id, Bundle args) {
//        ClaseLoaderDeListaDeFiesta_Loader_1 claseLoaderDeListaDeFiesta_Loader_1 =
//                new ClaseLoaderDeListaDeFiesta_Loader_1(getActivity());
//        return claseLoaderDeListaDeFiesta_Loader_1;
//    }

    @Override
    public android.support.v4.content.Loader<ArrayList<com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_Evento_1>> onCreateLoader(int id, Bundle args) {
        com.o3j.es.estamosenfiestas.activity_fragment_base_2.ClaseLoaderDeListaDeFiesta_Loader_1 claseLoaderDeListaDeFiesta_Loader_1 =
                new com.o3j.es.estamosenfiestas.activity_fragment_base_2.ClaseLoaderDeListaDeFiesta_Loader_1(getActivity());
        return claseLoaderDeListaDeFiesta_Loader_1;

        //Nota, 23 feb 2015: funciona ok con support.v4.content.Loader, con standard funciona mal
        //y no pinta las listas en vertical cuando giro la tablet.
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<ArrayList<com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_Evento_1>> loader, ArrayList<com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_Evento_1> data) {


////        Por ahora lo hago asi a ver que pasa
//        mAdapter = new CustomAdapter_RecyclerView_3(arrayList_De_Eventos_De_la_fiesta, mParam1,
//                                getActivity());

//        mAdapter = new CustomAdapter_RecyclerView_4(arrayList_De_Eventos_De_la_fiesta, mParam1,
//                getActivity());
//        // Set CustomAdapter as the adapter for RecyclerView.
//        mRecyclerView.setAdapter(mAdapter);

        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(xxx, xxx + " " + mParam1 + " onloadfinished");
        }
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<ArrayList<com.o3j.es.estamosenfiestas.activity_fragment_base_2.Class_Evento_1>> loader) {

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Prepare the loader.  Either re-connect with an existing one,
        // or start a new one.
        //Para pasar el this de la actividad, hay que usar el get activity por que este fragment
        //no esta embebido en la actividad.
        intIdentificadorDelLoaderDelFragment = Integer.parseInt(mParam2);

        //Original
//        getActivity().getLoaderManager().initLoader(0, null, this);

        //En cada fragment, asigno un unico loader, con un ID unico
        //No funciona cuando cambio la orientacion
//        getActivity().getSupportLoaderManager().initLoader(intIdentificadorDelLoaderDelFragment, null, this);

        //Usando

//        Juan, 26 Mayo 2014: No uso el loader por que los datos se reciben de la actividad en program
//        getActivity().getSupportLoaderManager().
//                initLoader(intIdentificadorDelLoaderDelFragment, null, this);
    }
//****************************************************************
    //****************************************************************
    //             FIN del Codigo relacionado con el loader
    //****************************************************************
    //****************************************************************

    private int intIdentificadorDelLoaderDelFragment;




    @Override
    public void onResume() {
        super.onResume();
    }



    public void method_Descarga_Imagen_De_Internet(ImageView imageView, Event event) {
        //Juan 17 agosto 2015: presento la imagen como en:
        //http://themakeinfo.com/2015/04/android-retrofit-images-tutorial/
        //Con esta instruccion:
        //Picasso.with(context).load("http://i.imgur.com/DvpvklR.png").into(imageView);
        //O esta:
        //Picasso.with(getContext()).load(url+flower.getPhoto()).resize(100,100).into(img);

        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.d(TAG, ": En metodo method_Descarga_Imagen_De_Internet");
        }

        String url = null;

        url = event.getMultimediaElementList().get(0).getMultimediaElementLocation();
        if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
            Log.e(TAG, ": En metodo method_Descarga_Imagen_De_Internet, url: " + url);
        }
        if (!Class_ConstantsHelper.methodStaticChequeaURL(url)) {//url invalida
//            if(url == null || url.isEmpty()){
            if (Class_ConstantsHelper.BOOLEAN_SHOW_LOG_D) {
                Log.e(TAG, ": En metodo method_Descarga_Imagen_De_Internet: url es NULL o esta VACIO");
            }
            //Oscar quiere que si no hay imagen, se vea mas pequena la cardview
            imageView.setVisibility(View.GONE);
//            imageView.setVisibility(View.INVISIBLE);
        } else {//url es valido, invoca la descarga de la imagen
            //Asi se ven las imagenes deformadas
//            Picasso.with(activity_Fragment_SwipeView_EventoDetalle_BackToParent).load(url).resize(400, 400).into(imageView);
            //Asi se ven bien, aunque algo recortadas, depende del tamaño
            //todas las imagene se ven del mismo tamaño, pero recortadas
            //Entonces un cartel que tenga texto, se puede ver fatal
//            Picasso.with(miFragmentMuestraGaleriaConRecycleView.getActivity()).load(url).fit().centerCrop().into(imageView);

            //asi se ve bien, la imagen sale entera pero dependiendo de la imagen, saldra mas vertical,
            //O mas horizontal, y cada imagen, de diferente tamaño.
            //O sea, que muestra toda la imagen respetando sus proporciones
            Picasso.with(getActivity()).load(url).fit().centerInside().into(imageView);
        }
    }

}//Fin de la clase
