package com.o3j.es.estamosenfiestas.activity_fragment_base_2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Juan on 14/02/2015.
 */

//Juan, 22 mayo 2015: Ya no uso esta clase.
//AHORA ESTOY USANDO AdaptadorDeSwipeViews_2
public class AdaptadorDeSwipeViews extends FragmentStatePagerAdapter {

    public AdaptadorDeSwipeViews(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
//        Fragment fragment = new MiFragment_1();
        Fragment fragment = new com.o3j.es.estamosenfiestas.activity_fragment_base_2.MiFragment_Con_Loader_1();
        Bundle args = new Bundle();
        // Our object is just an integer :-P
//        args.putInt(MiFragment_Con_Loader_1.ARG_OBJECT, i + 1);
        args.putString(com.o3j.es.estamosenfiestas.activity_fragment_base_2.MiFragment_Con_Loader_1.ARG_OBJECT, "fragment#" +(i + 1));
        //Paso el numero del fragment que estoy creando como un string, para usarlo en
        //el fragment para crear el loader con un unico ID para cada fragment
        args.putString(com.o3j.es.estamosenfiestas.activity_fragment_base_2.MiFragment_Con_Loader_1.ARG_PARAM2, String.valueOf(i + 1));
        fragment.setArguments(args);
        return fragment;
    }


    //GetCount retorna el numero de fragmentos que va a mostrar el swipeView
    @Override
    public int getCount() {
        return 10;
    }

    //Original
//    public int getCount() {
//        return 5;
//    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Lista  " + (position + 1);
    }
}
