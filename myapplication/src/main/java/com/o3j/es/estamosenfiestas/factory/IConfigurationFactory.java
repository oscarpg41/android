package com.o3j.es.estamosenfiestas.factory;

import android.content.Context;

import com.eef.data.eefExceptions.eefException;
import com.eef.data.managers.IConfigurationManager;
import com.eef.data.managers.impl.ConfigurationManager;
import com.eef.data.eeftypes.TypeConfiguration;
import com.o3j.es.estamosenfiestas.servercall.forbroadcaster.ConfigurationFromServer;

/**
 * Created by jluis on 19/02/15.
 */
public interface IConfigurationFactory {

    public IConfigurationManager getConfigurationManager(TypeConfiguration configurationType) throws eefException;

    public IConfigurationManager saveConfiguration(ConfigurationManager configuration) throws eefException;

    public void getConfigurationManagerFromServer(TypeConfiguration configurationType) throws eefException;

    public void initFactory(Context contexto, String progressBarBR, String showconfigurationBR);

    public IConfigurationManager loadConfigrationManagerFromConfigurationFromServer(ConfigurationFromServer configuration, TypeConfiguration configurationType) throws eefException;

    }
