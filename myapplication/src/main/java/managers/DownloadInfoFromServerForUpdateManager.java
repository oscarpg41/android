package managers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.eef.data.dataelements.EventPlanner;
import com.eef.data.dataelements.Program;
import com.eef.data.eefExceptions.eefException;
import com.eef.data.eeftypes.TypeEefError;
import com.o3j.es.estamosenfiestas.orm.pojosDB.Configuracion;
import com.o3j.es.estamosenfiestas.servercall.enumtypes.TypeEventPlannerDownloaded;
import com.o3j.es.estamosenfiestas.servercall.forbroadcaster.UpdateProgramsFromServer;
import com.o3j.es.estamosenfiestas.servercall.netInterface.IeefRetrofit;
import com.o3j.es.estamosenfiestas.servercall.pojos.ProgramEventPojo;
import com.o3j.es.estamosenfiestas.servercall.pojos.ProgramModifiedPojo;
import com.o3j.es.estamosenfiestas.servercall.pojos.ProgramPojo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by jluis on 24/08/15.
 */
public class DownloadInfoFromServerForUpdateManager extends DownloadInfoFromServerManager {

    //PUT_EXTRAS VARIABLES PARA EL PROCESO DE UPDATE
    public static String UPDATE_HAY_PROGRAMAS_QUE_ACTUALIZAR = "programas_para_actualizar";
    public static String UPDATE_HAY_PROGRAMAS_NUEVOS = "programas nuevos";
    public static String UPDATE_NUMERO_PROGRAMAS_ACTUALIZAR = "numero_programas_actualizar";
    public static String UPDATE_NUMERO_PROGRAMAS_NUEVOS = "numero_programas_";
    public static String UPDATE_NUMERO_PROGRAMAS_ACTUALIZADOS = "numero_programas_actualizados";
    public static String UPDATE_NUMERO_PROGRAMAS_NUEVOS_DESCARGADOS = "";
    public static String UPDATE_PROGRAMS_PROCESS_ERROR = "error_update_programs";

    //CONTEXT DE LA APLICACION ANDROID
    private static Context myContext;

    private UpdateProgramsFromServer updatedProgramInfo;

    public void initManager(Context contexto, String progressBarBR, String showconfigurationBR)
    {
        this.myContext=contexto;
        this.progressBarBR=progressBarBR;
        this.showconfigurationBR=showconfigurationBR;
    }

    private void sendProgressBarForUpdateBroadcast(TypeEventPlannerDownloaded type)
    {

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(progressBarBR);

        broadcastIntent.putExtra(UPDATE_HAY_PROGRAMAS_QUE_ACTUALIZAR, "true");
        broadcastIntent.putExtra(UPDATE_NUMERO_PROGRAMAS_ACTUALIZAR,updatedProgramInfo.getListaProgramasToUpdate().size());
        broadcastIntent.putExtra(UPDATE_NUMERO_PROGRAMAS_NUEVOS, updatedProgramInfo.getListaProgramasNew().size());

        broadcastIntent.putExtra(CONTINUAR_DESCARGA, "true");
        myContext.sendBroadcast(broadcastIntent);
    }

    private void sendBrodcasterror(TypeEefError error)
    {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(showconfigurationBR);
        //Juan 2 oct 2015: Hay que enviar boolean false, si no da error en
        //clase MyBroadcastReceiver_Fin_Actualizar_2, linea 58
//        broadcastIntent.putExtra(UPDATE_PROGRAMS_PROCESS_ERROR, error.getErrorText());
        broadcastIntent.putExtra(UPDATE_PROGRAMS_PROCESS_ERROR, false);
        myContext.sendBroadcast(broadcastIntent);
        return;
    }

    private void sendShowUpdateResultBroadcast(TypeEventPlannerDownloaded type)
    {
        //Inicializo la configurcion descargada
        //confDescargada.setListComWithActiveServers(listaCD);

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(showconfigurationBR);
        DatabaseServerManager dbM = new DatabaseServerManager();
        dbM.initManager(myContext);
        try
        {
            dbM.actualizarProgramas(updatedProgramInfo);
            broadcastIntent.putExtra(CONFIGURACION_PROGRAMAS_EN_DB, "true");
            broadcastIntent.putExtra(UPDATE_HAY_PROGRAMAS_QUE_ACTUALIZAR, updatedProgramInfo.getListaProgramasToUpdate().size() > 0?new Boolean(true):new Boolean(false));
            broadcastIntent.putExtra(UPDATE_HAY_PROGRAMAS_NUEVOS,updatedProgramInfo.getListaProgramasNew().size() > 0?new Boolean(true): new Boolean(false));
            broadcastIntent.putExtra(UPDATE_NUMERO_PROGRAMAS_ACTUALIZAR, updatedProgramInfo.getListaProgramasToUpdate().size());
            broadcastIntent.putExtra(UPDATE_NUMERO_PROGRAMAS_NUEVOS,updatedProgramInfo.getListaProgramasNew().size());
            broadcastIntent.putExtra(UPDATE_PROGRAMS_PROCESS_ERROR, new Boolean(false));
        } catch (eefException eefE) {
           // Log.e("EEF_ERROR_SAVE_DB", "LA DESCARGA DE PROGRAMAS DE [" + fullInfo.getEventPlannerInfo().getTown() + "] ha fallado [" + eefE.getMessage() + "]");
            broadcastIntent.putExtra(UPDATE_PROGRAMS_PROCESS_ERROR,new Boolean(true));
        } catch (Exception ex){
            //Log.e("EEF_ERROR_SAVE_DB","LA DESCARGA DE PROGRAMAS DE ["+fullInfo.getEventPlannerInfo().getTown()+"] ha fallado Excepción no controlada[" + ex +"]");
            broadcastIntent.putExtra(UPDATE_PROGRAMS_PROCESS_ERROR,new Boolean(true));
        }
        finally {
            myContext.sendBroadcast(broadcastIntent);
        }
    }

    public void updateProgramsFromServer() throws eefException
    {
        final String SEED_LOG = "DPE-->checkProgramsUpdated";
        DatabaseServerManager dbM = new DatabaseServerManager();
        dbM.initManager(myContext);
        updatedProgramInfo = new UpdateProgramsFromServer();

        try{
            //Obtengo la configuracion El organizador y la lista de programas para comprobar
            //Si hay que actualizarlo.
            Configuracion configuracion = dbM.getConfiguracion();
            EventPlanner eventPlanner = dbM.getEventPLanner(configuracion);
            List<Program> listaprogramas = dbM.getFullProgramList();
            updatedProgramInfo.setEventPlanner(eventPlanner);
            updatedProgramInfo.setListaProgramasToCheck(listaprogramas);

            //Se inicializa RETROFIT PARA PODER DESCARGAR LA LISTA DE PROGRAMAS DEL ORGANIZER
            final Integer evtId = updatedProgramInfo.getEventPlanner().getEventPlannerId();


            RestAdapter myRest =  new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(URL_ORGANIZADOR_PROGRAMS).build();
            IeefRetrofit mySer = myRest.create(IeefRetrofit.class);
            mySer.getOrganizerProgramsListInfo(evtId, new Callback<List<ProgramPojo>>() {
                @Override
                public void success(List<ProgramPojo> programPojos, Response response) {
                    String SEED_SUCCESS = SEED_LOG + "success ->";
                    if (programPojos == null) {
                        Log.i(SEED_SUCCESS, "URL[" + response.getUrl() + "] LA LISA DE PROGRAMS ES NULL");
                        sendBrodcasterror(TypeEefError.EVENT_PLANNER_PROGRAM_LIST_DOWNLOAD_ERROR);
                        return;
                    }
                    if (programPojos.size() == 0) {
                        Log.e(SEED_SUCCESS, "El size de la lista de Pojos es 0 [" + programPojos.size() + "]");
                        sendBrodcasterror(TypeEefError.EVENT_PLANNER_PROGRAM_LIST_DOWNLOAD_ERROR);
                        return;
                    }
                    //Hay una lista de programas hay que realizar la descarga de cada uno de ellos.
                    updatedProgramInfo.processProgramsDownload(programPojos);
                    if(hayProgramasQueDescargar()) {
                        descargaProgramasNuevos();
                        descargaProgramasAActualizar();
                    }
                    else{
                        sendShowUpdateResultBroadcast(TypeEventPlannerDownloaded.PROGRAM_NEW);
                    }

                }

                //NO SE HA PODIDO DESCARGAR EL LISTADO DE PROGRAMAS POR LO QUE SE MANDA EL
                //BROADCAST MESSAGE DE ERROR PARA MOSTRAR PANTALLA DE ERROR.
                @Override
                public void failure(RetrofitError error) {
                    String SEED_FALIURE = SEED_LOG + "faliure ->";
                    Log.e(SEED_FALIURE, "LA  DESCARGA LISTA DE PROGRAMAS DEL EVNTPLANER[" + evtId + "] HA DADO ERROR");
                    sendBrodcasterror(TypeEefError.EVENT_PLANNER_PROGRAM_LIST_DOWNLOAD_ERROR);
                }
            });
        } catch (eefException e) {
            Log.e(SEED_LOG,"ERROR AL RECUPERAR EL PROGRAMA DE LA BD");
        }
    }

    public void descargaProgramasNuevos()
    {
        if(updatedProgramInfo.getListaProgramasNew() == null || updatedProgramInfo.getListaProgramasNew().size()==0) {
            Log.d("DESCARGAPROGRAMASNUEVOS","NO HAY PROGRAMAS NUEVOS QUE DESCARGAR");
            return;
        }
        for(Program program : updatedProgramInfo.getListaProgramasNew()){
            descargaEventosPrograma(program,TypeEventPlannerDownloaded.PROGRAM_NEW);
        }
    }

    public void descargaProgramasAActualizar()
    {
        if(updatedProgramInfo.getListaProgramasToUpdate() == null || updatedProgramInfo.getListaProgramasToUpdate().size()==0){
            Log.d("DESC_PROGRAMAS_UPDATE","NO HAY PROGRAMAS ACTUALIZADOS QUE DESCARGAR");
            return;
        }
        for(Program program : updatedProgramInfo.getListaProgramasToUpdate()){
            descargaEventosPrograma(program,TypeEventPlannerDownloaded.PROGRAM_NEED_UPADTE);
        }
    }

    public boolean hayProgramasQueDescargar()
    {
        if( (updatedProgramInfo.getListaProgramasNew() == null
                || updatedProgramInfo.getListaProgramasNew().size()==0)
            &&
                (updatedProgramInfo.getListaProgramasToUpdate() == null
                        || updatedProgramInfo.getListaProgramasToUpdate().size()==0) )
            return false;
        return true;
    }
    private void descargaEventosPrograma(final Program programa, final TypeEventPlannerDownloaded typeDownload)
    {
        if(programa == null || programa.getProgramId()==null) {
            Log.e("DEP","ERROR EL PROGRAMA USADO PARA DESCARGA LA LISTA DE EVENETOS NO ESTA BIEN POPULADO");
            sendBrodcasterror(TypeEefError.PROGRAM_TO_DOWNLOAD_ERRONEOUS);
        }
        //Se inicializa RETROFIT PARA PODER DESCARGAR LA LISTA DE EVENTOS DEL PROGRAMA
        final Integer programId = programa.getProgramId();
        final String SEED_LOG = "DPEP["+programId+"]-> ";

        RestAdapter myRest =  new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(URL_EVENTSPROGRAM).build();
        IeefRetrofit mySer = myRest.create(IeefRetrofit.class);

        mySer.getprogramEvents(programId, new Callback<List<ProgramEventPojo>>() {
            @Override
            public void success(List<ProgramEventPojo> programEventPojos, Response response) {
                String SEED_SUCCESS = SEED_LOG + "success ->";
                //Si para iun programa no puedo obtener la lista de eventos mando Broadcasta de ERROR.
                if (programEventPojos == null) {
                    Log.i(SEED_SUCCESS, "URL[" + response.getUrl() + "] LA LISA DE EVENTOS DEL PROGRAMA[" + programId + " ES NULL");
                    sendBrodcasterror(TypeEefError.PROGRAM_EVENT_DOWNLAD_ERROR);
                    return;
                }
                if (programEventPojos.size() == 0) {
                    Log.e(SEED_SUCCESS, "El size de la LA LISA DE EVENTOS DEL PROGRAMA[" + programId + "] es 0 [" + programEventPojos.size() + "]");
                    sendBrodcasterror(TypeEefError.PROGRAM_EVENT_DOWNLAD_EMPTY);
                    return;
                }
                if(typeDownload == TypeEventPlannerDownloaded.PROGRAM_NEED_UPADTE)
                    updatedProgramInfo.addProgramToUpdateEventDownload(programa,programEventPojos);
                if(typeDownload == TypeEventPlannerDownloaded.PROGRAM_NEW)
                    updatedProgramInfo.addNewProgramEventDownload(programa,programEventPojos);

                if (!updatedProgramInfo.allProgramsDownladed())
                    sendProgressBarForUpdateBroadcast(typeDownload);
                else
                    sendShowUpdateResultBroadcast(typeDownload);
            }

            @Override
            public void failure(RetrofitError error) {
                String SEED_FALIURE = SEED_LOG + "faliure ->";
                Log.e(SEED_FALIURE, "LA DESACARGA DE LOS EVENTOS DEL PROGRAMAS [" + programId + "] ESTA VACIA");
                sendBrodcasterror(TypeEefError.EVENT_PLANNER_PROGRAM_LIST_DOWNLOAD_ERROR);
            }
        });


    }


    //USA EL SERVICIO REST PARA SABER SI TIENE QUE ACTUALIZAR
    public void checkProgram(final Program programa){

        if(programa == null || programa.getProgramId()==null) {
            Log.e("DEP","ERROR EL PROGRAMA USADO PARA ACTUAKIZAR LA LISTA DE EVENETOS NO ESTA BIEN POPULADO");
            sendBrodcasterror(TypeEefError.PROGRAM_TO_UPDATE_ERRONEOUS);
        }
        //Se inicializa RETROFIT PARA PODER DESCARGAR LA LISTA DE EVENTOS DEL PROGRAMA
        final Integer programId = programa.getProgramId();
        final String SEED_LOG = "DIP CHECKUPDATE["+programId+"]-> ";

        RestAdapter myRest =  new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(URL_CHECK_IF_UPDATE_PROGRAM).build();
        IeefRetrofit mySer = myRest.create(IeefRetrofit.class);
        // mySer.getprogramEvents(programId, new Callback<List<ProgramEventPojo>>()
        mySer.checkProgramModified(programId, new Callback<List<ProgramModifiedPojo>>() {
            @Override
            public void success(List<ProgramModifiedPojo> programModifieds, Response response) {
                String SEE_SUCCESS = SEED_LOG + "-->success";
                if (programModifieds == null || programModifieds.isEmpty()) {
                    Log.d(SEE_SUCCESS, "NO HAY DATOS DE ACTUALIZACION[" + programId + "]");
                    updatedProgramInfo.addProgramToBeUpdated(programa);
                    //sendProgressBarForUpdateBroadcast(TypeEefError.);
                }
                try {
                    ProgramModifiedPojo infoProgram = programModifieds.get(0);
                    SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date modServ = formatoDelTexto.parse(infoProgram.getFfModification());
                    Date fechMovil = programa.getLastUpdateDay();
                    if (fechMovil.before(modServ)) {
                        Log.d(SEE_SUCCESS, "EL programa[" + programa.getProgramSortDescription() + "] debe de ser actualizado");
                        updatedProgramInfo.addProgramToBeUpdated(programa);
                    }
                    //downloadProgramAndEventsForUpdate(updatedProgramInfo.getEventPlanner(), programa);
                } catch (Exception ex) {
                    Log.e(SEE_SUCCESS, "Error al comprobar las fechas del programa");
                    return;
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }
}
