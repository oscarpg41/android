package managers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.eef.data.dataelements.EventPlanner;
import com.eef.data.dataelements.Program;
import com.eef.data.eefExceptions.eefException;
import com.eef.data.eeftypes.TypeEefError;
import com.o3j.es.estamosenfiestas.servercall.enumtypes.TypeEventPlannerDownloaded;
import com.o3j.es.estamosenfiestas.servercall.forbroadcaster.FullInfoEventPlannerFromServer;
import com.o3j.es.estamosenfiestas.servercall.forbroadcaster.UpdateProgramsFromServer;
import com.o3j.es.estamosenfiestas.servercall.netInterface.IeefRetrofit;
import com.o3j.es.estamosenfiestas.servercall.pojos.InfoOrganizerPojo;
import com.o3j.es.estamosenfiestas.servercall.pojos.ProgramEventPojo;
import com.o3j.es.estamosenfiestas.servercall.pojos.ProgramPojo;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by jluis on 27/07/15.
 */
public class DownloadInfoFromServerManager {
    //URLS DE COMUNICACION
/*
    public static String URL_TOWNS="http://www.estamosenfiestas.es/api/index.php/towns/";
    public static String URL_CCAA="http://www.estamosenfiestas.es/api/index.php/ccaa/";
    public static String URL_ORGANIZADOR_INFO="http://www.estamosenfiestas.es/api/index.php/infoorganizer/";
    public static String URL_ORGANIZADOR_PROGRAMS="http://www.estamosenfiestas.es/api/index.php/programsorganizer/";
    public static String URL_EVENTSPROGRAM="http://www.estamosenfiestas.es/api/index.php/eventsprogram/";
    public static String URL_CHECK_IF_UPDATE_PROGRAM="http://www.estamosenfiestas.es/api/index.php/modification";
*/


    public static String URL_TOWNS="https://www.estamosenfiestas.es/api/index.php/towns/";
    public static String URL_CCAA="https://www.estamosenfiestas.es/api/index.php/ccaa/";
    public static String URL_ORGANIZADOR_INFO="https://www.estamosenfiestas.es/api/index.php/infoorganizer/";
    public static String URL_ORGANIZADOR_PROGRAMS="https://www.estamosenfiestas.es/api/index.php/programsorganizer/";
    public static String URL_EVENTSPROGRAM="https://www.estamosenfiestas.es/api/index.php/eventsprogram/";
    public static String URL_CHECK_IF_UPDATE_PROGRAM="https://www.estamosenfiestas.es/api/index.php/modification";


    /*

    public static String URL_TOWNS="http://www.estamosenfiestas.es/api/index.php/towns2/";
    public static String URL_CCAA="http://www.estamosenfiestas.es/api/index.php/ccaa2/";
    public static String URL_ORGANIZADOR_INFO="http://www.estamosenfiestas.es/api/index.php/infoorganizer2/";
    public static String URL_ORGANIZADOR_PROGRAMS="http://www.estamosenfiestas.es/api/index.php/programsorganizer2/";
    public static String URL_EVENTSPROGRAM="http://www.estamosenfiestas.es/api/index.php/eventsprogram2/";
    public static String URL_CHECK_IF_UPDATE_PROGRAM="http://www.estamosenfiestas.es/api/index.php/modification2";
*/

//    public static String URL_TOWNS="https://www.estamosenfiestas.es/api/index.php/towns2/";
//    public static String URL_CCAA="https://www.estamosenfiestas.es/api/index.php/ccaa2/";
//    public static String URL_ORGANIZADOR_INFO="https://www.estamosenfiestas.es/api/index.php/infoorganizer2/";
//    public static String URL_ORGANIZADOR_PROGRAMS="https://www.estamosenfiestas.es/api/index.php/programsorganizer2/";
//    public static String URL_EVENTSPROGRAM="https://www.estamosenfiestas.es/api/index.php/eventsprogram2/";
//    public static String URL_CHECK_IF_UPDATE_PROGRAM="https://www.estamosenfiestas.es/api/index.php/modification2";

    //PUT_EXTRAS VARIABLES DE LOS PASOS DE LA DESCARGA. CADA LLAMADA INCLUE SI HA DESCARGADO O NO
    public static String DESCARGADO_INFO_EVENT_PLANNER = "descargado_info_event_planner";
    public static String DESCARGADAS_LISTAS_DE_PROGRAMAS = "descargado_lista_programas";
    public static String NUMERO_PROGRAMAS_A_DESCARGA ="numero_programas_a_descargar";
    public static String PROGRAMA_DESCARGADO="id_programa_descargado";
    public static String DESCARGADAS_EVENTOS_DE_PROGRAMA = "descargando_lista_eventos_programas";

    //PUT_EXTRAS VARIABLES PARA EL PROCESO DE UPDATE
    public static String UPDATE_HAY_PROGRAMAS_QUE_ACTUALIZAR = "programas_para_actualizar";
    public static String UPDATE_NUMERO_PROGRAMAS = "numero_programas";
    public static String UPDATE_NUMERO_PROGRRAMAS_ACTUALIZADOS = "numero_programas_actualizados";
    public static String UPDATE_PROGRAMS_PROCESS_ERROR = "error_update_programs";



    //Este Intent se recibirá solo en el showBroadcast y contendrá la información que se ha conseguido descarga de
    //del servidor para ser entregada al dbmanager y persista la informacion en BD.
    public static String OBJETO_DESCARGA_TOTAL = "info_total_descargada";
    //Se recibe en el intenet del progress para indicar que quedan pasos todavía y controlar el progreso de la barra
    public static String CONTINUAR_DESCARGA = "continuo_DESCARGA";
    public static String CONFIGURACION_PROGRAMAS_EN_DB ="db_ready";

    //SI EL EXTRA AL BROADCAST INCLUYE ESTE ELEMENTO INDICA QUE EN ALGUNA PARTE DE LA DESCARGA
    //SE HA IDO A ERROR Y NO SE PUEDE COMPLETAR LA DESCARGA. MOSTRAR PANTALLA DE ERROR
    public static String ERROR_DESCARGA = "error_descarga";


    //CONTEXT DE LA APLICACION ANDROID
    private static Context myContext;

    private Integer pasos_finalizados;

    private FullInfoEventPlannerFromServer fullInfo;
    private UpdateProgramsFromServer updatedProgramInfo;
    private FullInfoEventPlannerFromServer reconfigureInfo;

    //ID DEL BROADCAST PARA INDICAR EL AVANCE EN LAS DESCARGA DE LOS PROGRAMAS
    String progressBarBR;
    //ID DEL BROADCAST PARA INDICAR QUE SE PUEDE GRABAR
    String showconfigurationBR;




    public void initManager(Context contexto, String progressBarBR, String showconfigurationBR)
    {
        this.myContext=contexto;
        this.progressBarBR=progressBarBR;
        this.showconfigurationBR=showconfigurationBR;
    }

    public void getOrganizerFullInformationFromServer(final EventPlanner organizer) throws eefException
    {
        getOrganizerFullInformationFromServer(organizer,TypeEventPlannerDownloaded.EVENT_PROGRAM_DOWNLOADED);
    }

    public void getOrganizerFullInformationForReconfigureFromServer(final EventPlanner organizer) throws eefException
    {
        getOrganizerFullInformationFromServer(organizer,TypeEventPlannerDownloaded.EVENT_PLANNER_DOWNLOADED_FOR_RECONFIGURE);
    }

    public void getOrganizerFullInformationFromServer(final EventPlanner organizer, final TypeEventPlannerDownloaded typeDonwload) throws eefException {

        //REcojo el id del organidar que hay que descargar
        if(organizer == null || organizer.getEventPlannerId()==null)
            throw new eefException(TypeEefError.EVENT_PLANNER_ERRONEOUS);
        this.fullInfo = new FullInfoEventPlannerFromServer();

        final Integer evtId = organizer.getEventPlannerId();

        //Se inicializa RETROFIT PARA PODER DESCARGAR LA INFO DEL ORGANIZER
        final String SEED_LOG = "OFIF["+evtId+"]-> ";
        RestAdapter myRest =  new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(URL_ORGANIZADOR_INFO).build();
        IeefRetrofit mySer = myRest.create(IeefRetrofit.class);

        //SE INTRODUCE LA LLAMADA EN CALLBACK
        mySer.getOrganizerInfo(evtId, new Callback<List<InfoOrganizerPojo>>() {
            //SI NO HAY PROBLEMAS EN LA DESCARGA DE LA INFO DEL ORGANIZADOR
            @Override
            public void success(List<InfoOrganizerPojo> infoOrganizerPojo, Response response) {
                String SEED_SUCCESS = SEED_LOG + "success ->";
                //Compruebo que la info del organizador es correcta
                if (infoOrganizerPojo == null) {
                    Log.i(SEED_SUCCESS, "URL[" + response.getUrl() + "] LA LISA DE ORGANIZADORES ES NULL");
                    sendBrodcasterror(TypeEefError.EVENT_PLANNER_DOWNLOAD_ERROR);
                    return;
                }
                if (infoOrganizerPojo.size() == 0) {
                    Log.e(SEED_SUCCESS, "El size de la lista de Organizadores es 0 [" + infoOrganizerPojo.size() + "]");
                    sendBrodcasterror(TypeEefError.EVENT_PLANNER_DOWNLOAD_ERROR);
                    return;
                }

                //Añado la info del organizador a la descarga
                fullInfo.setEventPlannerInfo(infoOrganizerPojo.get(0));
                //Inicio la descarga de los programas del organizador
                //Se lanza en su propio hilo y no en paralelo con la info del
                //Organizer para evitar problema
                Log.e(SEED_SUCCESS, "INICIO de la descarga de Programas de organizador[" + evtId + "]");
                descargaProgramasEventPlanner(organizer,typeDonwload);
                sendProgressBarBroadcast(TypeEventPlannerDownloaded.EVENTPLANNER_DOWNLOADED);
                return;
            }

            //Si ha fallado la descarga. Envio broadcast de error para informar al Activity que hay un error
            @Override
            public void failure(RetrofitError error) {
                String SEED_FALIURE = SEED_LOG + "faliure ->";
                sendBrodcasterror(TypeEefError.EVENT_PLANNER_DOWNLOAD_ERROR);
            }
        });

        return;

    }

    //Esta funcion descarga la lista de programas que devuelva el servidor para un organizador
    //Si obtiene una lista de programas incia la descarga de los mismo en paralelo.
    private void descargaProgramasEventPlanner(EventPlanner organizer, final TypeEventPlannerDownloaded typedownload)
    {
        if(organizer == null || organizer.getEventPlannerId()==null) {
            Log.e("DPEP","ERROR EL ORGANIZADOR USADO PARA DESCARGAR LA LISTA DE PROGRAMAS NO ESTA BIEN POPULADO");
            sendBrodcasterror(TypeEefError.EVENT_PLANNER_ERRONEOUS);
        }
        //Se inicializa RETROFIT PARA PODER DESCARGAR LA LISTA DE PROGRAMAS DEL ORGANIZER
        final Integer evtId = organizer.getEventPlannerId();
        final String SEED_LOG = "DPEP["+evtId+"]-> ";

        RestAdapter myRest =  new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(URL_ORGANIZADOR_PROGRAMS).build();
        IeefRetrofit mySer = myRest.create(IeefRetrofit.class);

        //Se introduce la lamada en callback con laimplementación de
        //la calase anonyma para poder dar la

        mySer.getOrganizerProgramsListInfo(evtId, new Callback<List<ProgramPojo>>() {
            @Override
            public void success(List<ProgramPojo> programPojos, Response response) {
                String SEED_SUCCESS = SEED_LOG + "success ->";
                if (programPojos == null) {
                    Log.i(SEED_SUCCESS, "URL[" + response.getUrl() + "] LA LISA DE PROGRAMS ES NULL");
                    sendBrodcasterror(TypeEefError.EVENT_PLANNER_PROGRAM_LIST_DOWNLOAD_ERROR);
                    return;
                }
                if (programPojos.size() == 0) {
                    Log.e(SEED_SUCCESS, "El size de la lista de Pojos es 0 [" + programPojos.size() + "]");
                    sendBrodcasterror(TypeEefError.EVENT_PLANNER_PROGRAM_LIST_DOWNLOAD_ERROR);
                    return;
                }
                //Hay una lista de programas hay que realizar la descarga de cada uno de ellos.
                fullInfo.setListaProgramas(programPojos);
                for (ProgramPojo programa : programPojos) {
                    Log.d(SEED_SUCCESS, "INICIO Descargando eventos del programa del organizador[" + evtId + "] con id de programa[" + programa.getIdProgram() + "]");
                    descargaEventosPrograma(programa,typedownload);

                }
                sendProgressBarBroadcast(TypeEventPlannerDownloaded.EVENTPLANNER_PROGRAMLIST_DOWNLOADED);

            }

            //NO SE HA PODIDO DESCARGAR EL LISTADO DE PROGRAMAS POR LO QUE SE MANDA EL
            //BROADCAST MESSAGE DE ERROR PARA MOSTRAR PANTALLA DE ERROR.
            @Override
            public void failure(RetrofitError error) {
                String SEED_FALIURE = SEED_LOG + "faliure ->";
                Log.e(SEED_FALIURE, "LA  DESCARGA LISTA DE PROGRAMAS DEL EVNTPLANER[" + evtId + "] HA DADO ERROR");
                sendBrodcasterror(TypeEefError.EVENT_PLANNER_PROGRAM_LIST_DOWNLOAD_ERROR);
            }
        });

        return;
    }


    //funcion que descarga los eventos de un programa cuando se estarealizando una descarga de informacion
    //full y no se tiene acceso al programa en el dispositivo. PO0r esa razón espera recibir
    //el pojo del programa descargado.
    private void descargaEventosPrograma(final ProgramPojo programa, final TypeEventPlannerDownloaded typeDownload)
    {
        if(programa == null || programa.getIdProgram()==null) {
            Log.e("DEP","ERROR EL PROGRAMA USADO PARA DESCARGA LA LISTA DE EVENETOS NO ESTA BIEN POPULADO");
            sendBrodcasterror(TypeEefError.PROGRAM_TO_DOWNLOAD_ERRONEOUS);
        }
        //Se inicializa RETROFIT PARA PODER DESCARGAR LA LISTA DE EVENTOS DEL PROGRAMA
        final Integer programId = Integer.parseInt(programa.getIdProgram());
        final String SEED_LOG = "DPEP["+programId+"]-> ";

        RestAdapter myRest =  new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(URL_EVENTSPROGRAM).build();
        IeefRetrofit mySer = myRest.create(IeefRetrofit.class);

        mySer.getprogramEvents(programId, new Callback<List<ProgramEventPojo>>() {
            @Override
            public void success(List<ProgramEventPojo> programEventPojos, Response response) {
                String SEED_SUCCESS = SEED_LOG + "success ->";
                //Si para iun programa no puedo obtener la lista de eventos mando Broadcasta de ERROR.
                if (programEventPojos == null) {
                    Log.i(SEED_SUCCESS, "URL[" + response.getUrl() + "] LA LISA DE EVENTOS DEL PROGRAMA[" + programId + " ES NULL");
                    sendBrodcasterror(TypeEefError.PROGRAM_EVENT_DOWNLAD_ERROR);
                    return;
                }
                if (programEventPojos.size() == 0) {
                    Log.e(SEED_SUCCESS, "El size de la LA LISA DE EVENTOS DEL PROGRAMA[" + programId + "] es 0 [" + programEventPojos.size() + "]");
                    sendBrodcasterror(TypeEefError.PROGRAM_EVENT_DOWNLAD_EMPTY);
                    return;
                }
                fullInfo.addProgramEventDownload(programa, programEventPojos);
                if (fullInfo.allProgramsDownladed())
                    sendShowConfigurationBRBroadcast(typeDownload);
                else
                    sendProgressBarBroadcast(typeDownload);
            }

            @Override
            public void failure(RetrofitError error) {
                String SEED_FALIURE = SEED_LOG + "faliure ->";
                Log.e(SEED_FALIURE, "LA DESACARGA DE LOS EVENTOS DEL PROGRAMAS [" + programId + "] ESTA VACIA");
                sendBrodcasterror(TypeEefError.EVENT_PLANNER_PROGRAM_LIST_DOWNLOAD_ERROR);
            }
        });


    }


    private void sendBrodcasterror(TypeEefError error)
    {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(showconfigurationBR);
        broadcastIntent.putExtra(ERROR_DESCARGA, error.getErrorText());
        myContext.sendBroadcast(broadcastIntent);
    }

    private void sendProgressBarBroadcast(TypeEventPlannerDownloaded type)
    {

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(progressBarBR);
        if(fullInfo.getEventPlannerInfo() != null)
            broadcastIntent.putExtra(DESCARGADO_INFO_EVENT_PLANNER,"true");
        if(fullInfo.getListaProgramas() != null && fullInfo.getListaProgramas().size()!=0)
        {
            broadcastIntent.putExtra(DESCARGADAS_LISTAS_DE_PROGRAMAS,"true");
            broadcastIntent.putExtra(NUMERO_PROGRAMAS_A_DESCARGA,fullInfo.getListaProgramas().size());
            broadcastIntent.putExtra(PROGRAMA_DESCARGADO,"Programa");
        }
        broadcastIntent.putExtra(CONTINUAR_DESCARGA,"true");
        myContext.sendBroadcast(broadcastIntent);
    }

    private void sendBrodcasterrorForUPdate(TypeEefError error)
    {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(showconfigurationBR);
        if(error.getErrorId()==TypeEefError.PROGRAM_TO_UPDATE_NO_PROGRAMS_TO_BE_UPDATED.getErrorId()) {
            broadcastIntent.putExtra(UPDATE_HAY_PROGRAMAS_QUE_ACTUALIZAR, "false");
            broadcastIntent.putExtra(UPDATE_PROGRAMS_PROCESS_ERROR, "NO ERROR");
        }
        else
            broadcastIntent.putExtra(UPDATE_PROGRAMS_PROCESS_ERROR,error.getErrorText());
        myContext.sendBroadcast(broadcastIntent);
        return;
    }

    private void sendShowConfigurationBRBroadcast(TypeEventPlannerDownloaded type)
    {
        //Inicializo la configurcion descargada
        //confDescargada.setListComWithActiveServers(listaCD);

        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(showconfigurationBR);
        DatabaseServerManager dbM = new DatabaseServerManager();
        dbM.initManager(myContext);
        try
        {
            if(type==TypeEventPlannerDownloaded.EVENT_PLANNER_DOWNLOADED_FOR_RECONFIGURE)
                dbM.borraTodosLosDatos();
            dbM.grabarFullEventPLanner(fullInfo);
            broadcastIntent.putExtra(CONFIGURACION_PROGRAMAS_EN_DB, "true");
        } catch (eefException eefE) {
            Log.e("EEF_ERROR_SAVE_DB","LA DESCARGA DE PROGRAMAS DE ["+fullInfo.getEventPlannerInfo().getTown()+"] ha fallado ["+eefE.getMessage()+"]");
            broadcastIntent.putExtra(CONFIGURACION_PROGRAMAS_EN_DB,"false");
        } catch (Exception ex){
            Log.e("EEF_ERROR_SAVE_DB","LA DESCARGA DE PROGRAMAS DE ["+fullInfo.getEventPlannerInfo().getTown()+"] ha fallado Excepción no controlada[" + ex +"]");
            broadcastIntent.putExtra(CONFIGURACION_PROGRAMAS_EN_DB,"false");
        }
        finally {
            myContext.sendBroadcast(broadcastIntent);
        }
    }






    //Esta funcion descarga la lista de programas que devuelva el servidor para un organizador
    //Si obtiene una lista de programas incia la descarga de los mismo en paralelo.
    private void downloadProgramAndEventsForUpdate(EventPlanner organizer, final Program programa)
    {
        if(organizer == null || organizer.getEventPlannerId()==null) {
            Log.e("DPEP","ERROR EL ORGANIZADOR USADO PARA DESCARGAR LA LISTA DE PROGRAMAS NO ESTA BIEN POPULADO");
            sendBrodcasterrorForUPdate(TypeEefError.PROGRAM_TO_UPDATE_NO_PROGRAMS_TO_BE_UPDATED);
        }
        //Se inicializa RETROFIT PARA PODER DESCARGAR LA LISTA DE PROGRAMAS DEL ORGANIZER
        final Integer evtId = organizer.getEventPlannerId();
        final String SEED_LOG = "DPEP["+evtId+"]-> ";

        RestAdapter myRest =  new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(URL_ORGANIZADOR_PROGRAMS).build();
        IeefRetrofit mySer = myRest.create(IeefRetrofit.class);

        //Se introduce la lamada en callback con laimplementación de
        //la calase anonyma para poder dar la

        mySer.getOrganizerProgramsListInfo(evtId, new Callback<List<ProgramPojo>>() {
            @Override
            public void success(List<ProgramPojo> programPojos, Response response) {
                String SEED_SUCCESS = SEED_LOG + "success ->";
                if (programPojos == null) {
                    Log.i(SEED_SUCCESS, "URL[" + response.getUrl() + "] LA LISA DE PROGRAMS ES NULL");
                    sendBrodcasterror(TypeEefError.EVENT_PLANNER_PROGRAM_LIST_DOWNLOAD_ERROR);
                    return;
                }
                if (programPojos.size() == 0) {
                    Log.e(SEED_SUCCESS, "El size de la lista de Pojos es 0 [" + programPojos.size() + "]");
                    sendBrodcasterror(TypeEefError.EVENT_PLANNER_PROGRAM_LIST_DOWNLOAD_ERROR);
                    return;
                }
                //Hay una lista de programas hay que realizar la descarga de cada uno de ellos.
                fullInfo.setListaProgramas(programPojos);
                for (ProgramPojo programaP : programPojos) {
                    Log.d(SEED_SUCCESS, "INICIO Descargando eventos del programa del organizador[" + evtId + "] con id de programa[" + programaP.getIdProgram() + "]");
                    descargaEventosPrograma(programaP,TypeEventPlannerDownloaded.EVENTPLANNER_PROGRAMLIST_FOR_UPDATE_DOWNLOADED);

                }
                sendProgressBarBroadcast(TypeEventPlannerDownloaded.EVENTPLANNER_PROGRAMLIST_DOWNLOADED);

            }

            //NO SE HA PODIDO DESCARGAR EL LISTADO DE PROGRAMAS POR LO QUE SE MANDA EL
            //BROADCAST MESSAGE DE ERROR PARA MOSTRAR PANTALLA DE ERROR.
            @Override
            public void failure(RetrofitError error) {
                String SEED_FALIURE = SEED_LOG + "faliure ->";
                Log.e(SEED_FALIURE, "LA  DESCARGA LISTA DE PROGRAMAS DEL EVNTPLANER[" + evtId + "] HA DADO ERROR");
                sendBrodcasterror(TypeEefError.EVENT_PLANNER_PROGRAM_LIST_DOWNLOAD_ERROR);
            }
        });

        return;
    }


    //funcion que descarga los eventos de un programa cuando se estarealizando una descarga de informacion
    //full y no se tiene acceso al programa en el dispositivo. PO0r esa razón espera recibir
    //el pojo del programa descargado.
    private void descargaEventosProgramaForUpdate(final ProgramPojo programa)
    {
        if(programa == null || programa.getIdProgram()==null) {
            Log.e("DEP","ERROR EL PROGRAMA USADO PARA DESCARGA LA LISTA DE EVENETOS NO ESTA BIEN POPULADO");
            sendBrodcasterror(TypeEefError.PROGRAM_TO_DOWNLOAD_ERRONEOUS);
        }
        //Se inicializa RETROFIT PARA PODER DESCARGAR LA LISTA DE EVENTOS DEL PROGRAMA
        final Integer programId = Integer.parseInt(programa.getIdProgram());
        final String SEED_LOG = "DPEP["+programId+"]-> ";

        RestAdapter myRest =  new RestAdapter.Builder().setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(URL_EVENTSPROGRAM).build();
        IeefRetrofit mySer = myRest.create(IeefRetrofit.class);

        mySer.getprogramEvents(programId, new Callback<List<ProgramEventPojo>>() {
            @Override
            public void success(List<ProgramEventPojo> programEventPojos, Response response) {
                String SEED_SUCCESS = SEED_LOG + "success ->";
                //Si para iun programa no puedo obtener la lista de eventos mando Broadcasta de ERROR.
                if (programEventPojos == null) {
                    Log.i(SEED_SUCCESS, "URL[" + response.getUrl() + "] LA LISA DE EVENTOS DEL PROGRAMA[" + programId + " ES NULL");
                    sendBrodcasterror(TypeEefError.PROGRAM_EVENT_DOWNLAD_ERROR);
                    return;
                }
                if (programEventPojos.size() == 0) {
                    Log.e(SEED_SUCCESS, "El size de la LA LISA DE EVENTOS DEL PROGRAMA[" + programId + "] es 0 [" + programEventPojos.size() + "]");
                    sendBrodcasterror(TypeEefError.PROGRAM_EVENT_DOWNLAD_EMPTY);
                    return;
                }
                fullInfo.addProgramEventDownload(programa, programEventPojos);
                if (fullInfo.allProgramsDownladed())
                    sendShowConfigurationBRBroadcast(TypeEventPlannerDownloaded.EVENT_PROGRAM_DOWNLOADED);
                else
                    sendProgressBarBroadcast(TypeEventPlannerDownloaded.EVENT_PROGRAM_DOWNLOADED);
            }

            @Override
            public void failure(RetrofitError error) {
                String SEED_FALIURE = SEED_LOG + "faliure ->";
                Log.e(SEED_FALIURE, "LA DESACARGA DE LOS EVENTOS DEL PROGRAMAS [" + programId + "] ESTA VACIA");
                sendBrodcasterror(TypeEefError.EVENT_PLANNER_PROGRAM_LIST_DOWNLOAD_ERROR);
            }
        });


    }



}
