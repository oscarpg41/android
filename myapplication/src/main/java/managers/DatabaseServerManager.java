package managers;

import android.content.Context;
import android.util.Log;

import com.eef.data.dataelements.DayEvents;
import com.eef.data.dataelements.Event;
import com.eef.data.dataelements.EventPlanner;
import com.eef.data.dataelements.MultimediaElement;
import com.eef.data.dataelements.Program;
import com.eef.data.eefExceptions.eefException;
import com.eef.data.eeftypes.TypeEefError;
import com.eef.data.eeftypes.TypeMultimediaElement;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.table.TableUtils;
import com.o3j.es.estamosenfiestas.orm.DBHelper;
import com.o3j.es.estamosenfiestas.orm.pojosDB.Configuracion;
import com.o3j.es.estamosenfiestas.orm.pojosDB.Eventos;
import com.o3j.es.estamosenfiestas.orm.pojosDB.Organizador;
import com.o3j.es.estamosenfiestas.orm.pojosDB.Programa;
import com.o3j.es.estamosenfiestas.servercall.forbroadcaster.FullInfoEventPlannerFromServer;
import com.o3j.es.estamosenfiestas.servercall.forbroadcaster.UpdateProgramsFromServer;


import java.sql.SQLException;
import java.sql.Savepoint;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jluis on 29/07/15.
 */
public class DatabaseServerManager {

    //Helper de Acceso a la base de datos
    DBHelper mdbHelper;

    Context myContext;




    public void initManager(Context contexto)
    {
        this.myContext=contexto;
        this.mdbHelper=null;
    }

    //LLAMAR A ESTA FUNCIONA ANTES DE USAR EL HELPER
    private void initDBHelper()
    {
        if(mdbHelper == null)
            this.mdbHelper = OpenHelperManager.getHelper(myContext, DBHelper.class);
    }

    //LAMAR A ESTA FUNCION SIEMPRE QUE
    private void releaseDB()
    {
        if (mdbHelper != null) {
            OpenHelperManager.releaseHelper();
            mdbHelper = null;
        }
    }

    private void borraDBForNewConfiguration()
    {
        initDBHelper();
        try {
            TableUtils.clearTable(mdbHelper.getConnectionSource(),Eventos.class);
            TableUtils.clearTable(mdbHelper.getConnectionSource(),Programa.class);
            TableUtils.clearTable(mdbHelper.getConnectionSource(),Organizador.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        catch (Exception ex)
        {
            Log.e("ERROR CLEAN TABLAS", "EXCEPCION" + ex);
        }
        finally {
            releaseDB();
        }
    }

    private void borraConfiguracion()
    {
        borraConfiguracion(null);
    }
    private void borraConfiguracion(DBHelper db)
    {
        String LOG_SEED = "DBS-> borraConfiguracion";
        try {
            if (db != null)
                TableUtils.clearTable(db.getConnectionSource(), Configuracion.class);
            else {
                initDBHelper();
                TableUtils.clearTable(mdbHelper.getConnectionSource(), Configuracion.class);
            }
        } catch (SQLException e) {
           Log.e(LOG_SEED,"Excepcion sql al borra la tabla de configuracion["+ e);
        }catch (Exception e) {
            Log.e(LOG_SEED,"Excepcion NO CONTROLADA al borra la tabla de configuracion["+ e);
        }
        finally {
            if(db==null)
                releaseDB();
        }
    }



    private Organizador eventPlannerToOrganizador(EventPlanner eventPlanner) throws eefException
    {
        Date fecha = new GregorianCalendar().getTime();
        Organizador organizador = new Organizador();
        organizador.setCcaa_id(eventPlanner.getAutonomousCommunityId());
        organizador.setName(eventPlanner.getName());
        organizador.setFecha_descarga(fecha);
        organizador.setId(eventPlanner.getEventPlannerId());
        organizador.setInfo(eventPlanner.getLongDescription());
        organizador.setUrl_logo(eventPlanner.getMultimediaElementList().get(0).getMultimediaElementLocation());
        //organizador.setNum_programas(eventPlanner.getProgramList().size());
        organizador.setTown(eventPlanner.getSortDescription());
        organizador.setFecha_descarga(eventPlanner.getDownloadedDate());
        organizador.setFecha_ultima_actualizacion(eventPlanner.getUpdatedDate());
        return organizador;
    }

    private List<MultimediaElement> imgToMultimediaList(String img) {
        MultimediaElement logo = new MultimediaElement(img, TypeMultimediaElement.IMAGE_URL);
        ArrayList<MultimediaElement> logoList = new ArrayList<MultimediaElement>();
        logoList.add(logo);
        return logoList;
    }

    public EventPlanner organizadorToEventPlanner(Organizador organizador) throws eefException
    {
        Date fecha = new GregorianCalendar().getTime();
        EventPlanner eventPlanner = new EventPlanner();
        eventPlanner.setAutonomousCommunityId(organizador.getCcaa_id());
        eventPlanner.setName(organizador.getName());
        eventPlanner.setEventPlannerId(organizador.getId());
        eventPlanner.setLongDescription(organizador.getInfo());
        eventPlanner.setMultimediaElementList(imgToMultimediaList(organizador.getUrl_logo()));
        eventPlanner.setSortDescription(organizador.getTown());
        return eventPlanner;
    }


    private Organizador grabarNuevoEventPlanner(EventPlanner eventPlanner) throws eefException{
        initDBHelper();
        String LOG_SEED = "DBS --> grabarEventPlanner -->";
        Dao eventPlannerDao;
        try {
            eventPlannerDao=mdbHelper.getOrganizadorDao();
            Organizador nuevoOrganizador = eventPlannerToOrganizador(eventPlanner);
            eventPlannerDao.create(nuevoOrganizador);
            return nuevoOrganizador;
        } catch (SQLException sqlE) {
            Log.e(LOG_SEED,  "SQLException: GUARDANDO NUEVO ORGANIZADOR EN LAS TABLAS: " +sqlE);
            throw new eefException(TypeEefError.DB_ERROR_SAVE_EVENT_PLANNER_INFO);
        }
        catch (eefException eefE) {
            Log.e(LOG_SEED,  "Exception: DE EEF AL ACCEDER A LA INFORMACION DEL EVENT PLANNER: " +eefE);
            throw eefE;
        }
        catch (Exception e) {
            Log.e(LOG_SEED, "Exception: NO CONTROLADA GUARDANDO ORGANIZADOR: " + e);
            throw new eefException(TypeEefError.DB_ERROR_SAVE_EVENT_PLANNER_INFO);
        }
        finally{
            releaseDB();
        }
    }


    public Programa programToPrograma(Program program)
    {
        Programa programa = new Programa();
        programa.setId(program.getProgramId());
        programa.setFecha_descarga(new GregorianCalendar().getTime());
        programa.setFecha_inicio(program.getProgramStartDate());
        programa.setFecha_fin(program.getProgramEndDate());
        programa.setFecha_ultima_actualizacion(program.getLastUpdateDay());
        programa.setFecha_descarga(program.getLastUpdateDay());
        programa.setName(program.getProgramLongDescription());
        programa.setUrl_logo(program.getListMultimediaElement().get(0).getMultimediaElementLocation());
        return programa;
    }

    public Program programaToProgram(Programa programa)
    {
        Program program= new Program();
        program.setProgramId(programa.getId());
        program.setProgramStartDate(programa.getFecha_inicio());
        program.setProgramEndDate(programa.getFecha_fin());
        program.setProgramLongDescription(programa.getName());
        program.setProgramSortDescription(programa.getName());
        program.setListMultimediaElement(imgToMultimediaList(programa.getUrl_logo()));
        program.setDownloadedDay(programa.getFecha_descarga());
        program.setLastUpdateDay(programa.getFecha_ultima_actualizacion());
        return program;
    }

    private List<Programa> grabarListaProgramas(Organizador organizador, List<Program> listProgram,HashMap<Program,List<Event>> eventosPrograma) throws eefException{
        initDBHelper();
        String LOG_SEED = "DSM-->grabarListaProgramas -->";
        Dao programaDao ;
        ArrayList<Programa> listaProgramasSalvados = new ArrayList<Programa>();
        try {
            programaDao = mdbHelper.getProgramaDao();
            for(Program program : listProgram)
            {
                Programa programa = programToPrograma(program);
                programa.setOrganizador(organizador);
                programaDao.create(programa);
                listaProgramasSalvados.add(programa);
                grabarEventosPrograma(organizador,programa,eventosPrograma.get(program),mdbHelper);
            }
            return listaProgramasSalvados;
        }
        catch (SQLException sqlE) {
            Log.e(LOG_SEED,  "SQLException: GUARDANDO PROGRAMAS EN LAS TABLAS: " +sqlE);
            throw new eefException(TypeEefError.DB_ERROR_SAVE_PROGRAM);
        }
        catch (Exception e) {
            Log.e(LOG_SEED, "Exception: NO CONTROLADA GUARDANDO ORGANIZADOR: " + e);
            throw new eefException(TypeEefError.DB_ERROR_SAVE_PROGRAM);
        }
        finally{
            releaseDB();
        }
    }

    private Eventos eventToEventos(Event event)
    {
        Eventos evento = new Eventos();
        evento.setId(event.getEventId());
        evento.setTitulo(event.getSortText());
        evento.setDescripcion(event.getLongText());
        evento.setFavorito(Event.NO_FAVORITO);
        evento.setFechaEvento(event.getStartDate());
        evento.setFecha_descarga(new GregorianCalendar().getTime());
        evento.setUrl_imagen(event.getMultimediaElementList().get(0).getMultimediaElementLocation());
        return evento;
    }

    private Event eventosToEvent(Eventos evento)
    {
        Event event = new Event();
        event.setEventId(evento.getId());
        event.setSortText(evento.getTitulo());
        event.setLongText(evento.getDescripcion());
        event.setAddedToFavourites(evento.getFavorito() == Event.FAVORITO ? true : false);
        event.setMultimediaElementList(imgToMultimediaList(evento.getUrl_imagen()));
        event.setStartDate(evento.getFechaEvento());
        event.setAddedToFavourites(evento.getFavorito() == 1 ? true : false);
        return event;
    }

    private void grabarEventosPrograma(Organizador organizador,Programa programa, List<Event>  listaEventos, DBHelper mdb) throws eefException
    {
        final String LOG_SEED = "DSM-->grabarEventosPrograma->";
        Log.d(LOG_SEED, "Grabando eventos organizador[" + organizador.getTown() + "] programa[" + programa.getName() + "] numero eventos[" + listaEventos.size() + "]");
        Dao eventoDao;
        try {
            eventoDao = mdbHelper.getEventosDao();
            for (Event event : listaEventos) {
                Log.d(LOG_SEED, "GRabando evento [" + event.getEventId() + "]");
                Eventos evento = eventToEventos(event);
                evento.setOrganizador(organizador);
                evento.setPrograma(programa);
                eventoDao.create(evento);
            }
        }
        catch (SQLException sqlE) {
            Log.e(LOG_SEED,  "SQLException: GUARDANDO EVENTO: " +sqlE);
            throw new eefException(TypeEefError.DB_ERROR_SAVE_PROGRAM_EVENTS);
        }
        catch (Exception e) {
            Log.e(LOG_SEED, "Exception: NO CONTROLADA GUARDANDO EVENTOS programa["+programa.getName()+"]: " + e);
            throw new eefException(TypeEefError.DB_ERROR_SAVE_EVENT_PLANNER_INFO);
        }
    }

    private void grabarConfiguracion(Organizador organizador) throws eefException
    {
        String LOG_SEED = "DSM->grabarConfiguracion->";
        Log.i(LOG_SEED, "Se va a grabar la configuración [" + organizador.getTown() + "]");
        initDBHelper();
        Dao configuracionDao;
        try {
            configuracionDao = mdbHelper.getConfiguracionDao();
            Configuracion configuracion = new Configuracion();
            configuracion.setCcaaId(organizador.getCcaa_id());
            configuracion.setEventPlannerId(organizador.getId());
            configuracion.setDescargadoEventPlanner(1);
            configuracion.setDescargadosProgramas(1);
            Date fecha= new GregorianCalendar().getTime();
            configuracion.setFechaDescargadoEventPlanner(fecha);
            configuracion.setFechaDescargadoProgramas(fecha);
            configuracion.setFechaConfiguracion(fecha);
            configuracionDao.create(configuracion);
        }
        catch (SQLException sqlE) {
            Log.e(LOG_SEED,  "SQLException: GUARDANDO CONFIGURACION EEF: " +sqlE);
            throw new eefException(TypeEefError.DB_ERROR_SAVE_CONFIGURATION);
        }
        catch (Exception e) {
            Log.e(LOG_SEED, "Exception: NO CONTROLADA GUARDANDO CONFIGURACION DE ORGANIZADOR["+organizador.getTown()+"]: " + e);
            throw new eefException(TypeEefError.DB_ERROR_SAVE_CONFIGURATION);
        }
        finally{
            releaseDB();
        }
    }

    public void grabarFullEventPLanner(FullInfoEventPlannerFromServer eventPlannerInformation) throws eefException
    {
        String LOG_SEED = "DSM->grabarFullEventPLanner->";
        Log.i(LOG_SEED, "Se va a grabar la informacio de descarga[" + eventPlannerInformation.getEventPlannerInfo().getTown() + "]");

        EventPlanner organizador= eventPlannerInformation.getEventPlanner();
        try
        {

            //borraDBForNewConfiguration();
            //Hay que guardar en el orden que se necesita por la FK's
            EventPlanner eventPlannerToSave = eventPlannerInformation.getEventPlanner();
            Organizador organizadorSaved= grabarNuevoEventPlanner(eventPlannerToSave);

            //hay que guardar la lista de programas
            grabarListaProgramas(organizadorSaved,eventPlannerInformation.getListPrograms(), eventPlannerInformation.getProgramsDownladed());

            //Si todo ha sido guardado correctamente salvo la configuracion
            grabarConfiguracion(organizadorSaved);

        }catch (eefException ex)
        {
            Log.e(LOG_SEED,"EROR AL GRABAR LOS DATOS DEL EVENET PLANNER["+ex.getMessage()+"]");
            borraDBForNewConfiguration();
            borraConfiguracion();
            throw ex;
        }

    }

    public Configuracion getConfiguracion() throws eefException{
        initDBHelper();
        String LOG_SEED = "DSM --> getConfiguracion -->";
        Dao configuracionDao;
        try {
            configuracionDao = mdbHelper.getConfiguracionDao();
            List<Configuracion> Configuraciones =  configuracionDao.queryForAll();

            //No hay configuración se debería de mandar al activity de configuración
            if (Configuraciones==null || Configuraciones.isEmpty())
                throw new eefException(TypeEefError.EEF_NOT_CONFIGURED);

            Configuracion eefConfiguracion= Configuraciones.listIterator().next();
            return eefConfiguracion;

        } catch (SQLException e) {
            Log.e(LOG_SEED,  "SQLException: ACCEDIENDO A LA TABLA CONFIGURACION: " +e);
            throw new eefException(TypeEefError.DB_ERROR_ACCES_TO_CONFIGURATION);
        }
        catch(eefException eef)
        {
            if(eef.getErrorT() != TypeEefError.EEF_NOT_CONFIGURED)
                Log.e(LOG_SEED,"ERROR CONTROLADO -> NO SE PUEDE ACCEDER A LA CONFIGURACION. NO ES ERROR DE NO ESTAR CONFIGURADA ");
            throw eef;
        }
        catch (Exception e) {
            Log.e(LOG_SEED, "Exception: NO CONTROLADA ACCEDIENDO A LA TABLA CONFIGURACION: " + e);
            throw new eefException(TypeEefError.CONFIGURATION_ERROR);
        }
        finally{
            releaseDB();
        }
    }

    public List<Program> getFullProgramList() throws eefException
    {
        final String LOG_SEED = "DSM-->getPrograms";

        try {
            //Se reupera la configuración
            Configuracion configuracion = getConfiguracion();

            //Se recupera el event Planner
            Organizador organizador = getOrganizador(configuracion);

            //Se recupera la lista de programas
            List<Programa> listaProgramasDB = getListaProgramasSineventos(organizador);

            //Se recupera la lista de programas
            List<Program> listaProgramas = getListProgramsFullInfo(configuracion, organizador, listaProgramasDB);
            return listaProgramas;

        }catch (eefException eef)
        {
            Log.e(LOG_SEED,"eef Exception al recuperar la lista de programas");
            throw eef;
        }
        catch (Exception ex)
        {
            Log.e(LOG_SEED, "Excepcion no controlada al recuperar la lista de pro");
            throw new eefException(TypeEefError.DB_ERROR_LOAD_PROGRAM);
        }

    }

    private List<Program> getListProgramsFullInfo(Configuracion configuracion, Organizador organizador,List<Programa> listaProgramasDB) throws eefException
    {
        final String LOG_SEED = "DSM-->getListProgramsFullInfo";

        //Se inicializa la lista de programas a devolver
        ArrayList<Program> listaPrograms = new ArrayList<Program>();
        try {
            //Para cada programa que se ha recuperado se realiza la incialización y se recupera la información
            for (Programa programa : listaProgramasDB) {
                Program program = programaToProgram(programa);
                program.setAutonomousCommunityId(organizador.getCcaa_id());
                program.setEventPlannerId(organizador.getId());
                List<Eventos> eventos = getEventosDePrograma(organizador,programa);
                program = setEventosToProgram(program, eventos);
                listaPrograms.add(program);
            }
        }catch (eefException eef)
        {
            Log.e(LOG_SEED,"ERROR AL CONSTRUIR LISTA DE PROGRAMAS CON INFO DB");
            throw eef;
        }catch (Exception ex)
        {
            Log.e(LOG_SEED,"ERROR NO CONTROLADO AL MONTAR PROGRAMAS DEL ORGANIZADOR["+organizador.getName()+"DESDE BD");
            throw new eefException(TypeEefError.DB_ERROR_LOAD_PROGRAM_EVENTS);
                    
        }

        return listaPrograms  ;
    }

    private Program setEventosToProgram(Program program, List<Eventos> eventos)
    {
        final String LOG_SEED = "DSM-->setEventosToProgram";
        Log.d(LOG_SEED,"Programa["+program.getProgramSortDescription()+"] procesando sus eventos");
        Date dia_procesando = null;
        List<Event> localFullList = new ArrayList<Event>();
        List<Event> listaEventosDia = new ArrayList<Event>();
        List<DayEvents> listEventDays = new ArrayList<DayEvents>();
        List<Event> eventosAlfinal = new ArrayList<Event>();
        DayEvents dayEvent = new DayEvents();

        for(Eventos evento: eventos)
        {
            Event event=eventosToEvent(evento);
            event.setEventPlannerId(program.getEventPlannerId());
            event.setAutonomousCommunityId(program.getAutonomousCommunityId());
            //Se añade al full list
            localFullList.add(event);

            //Se construye la lista de eventos por dia.
            //Miro si lo añado a la lista del día o ha cambiado.
            Date fecha_evento = event.getStartDate();

            //Si es el primer registro el dia que estoy procesando se incializa
            if(dia_procesando==null) {
                dia_procesando = (Date) fecha_evento.clone();
                dayEvent.setProgramEventDate(dia_procesando);
            }

            //Si el día ha cambiado hay que añadir a la lista de Dias el dia procesado
            if(!comparafechas(dia_procesando,fecha_evento)) {
                Log.d(LOG_SEED, "SE VA A CAMBIAR DE DIA procesando[" + dia_procesando + "] fecha evento[" + fecha_evento + "]");
                for(Event eventoalfinal : eventosAlfinal){
                    listaEventosDia.add(eventoalfinal);
                }
                dayEvent.setListaEventos(listaEventosDia);
                listEventDays.add(dayEvent);

                dia_procesando = (Date)fecha_evento.clone();
                dayEvent = new DayEvents();
                dayEvent.setProgramEventDate(dia_procesando);
                listaEventosDia= new ArrayList<Event>();
                eventosAlfinal = new ArrayList<Event>();
            }
            if(eventoEsAlFinal(event))
                eventosAlfinal.add(event);
            else
                listaEventosDia.add(event);
        }

        if((listaEventosDia!= null && listaEventosDia.size()>0) || (eventosAlfinal!=null && eventosAlfinal.size()>0)) {
            for(Event eventoalfinal : eventosAlfinal){
                listaEventosDia.add(eventoalfinal);
            }
            dayEvent.setListaEventos(listaEventosDia);
            listEventDays.add(dayEvent);
        }

        program.setFullList(localFullList);
        program.setEventByDay(listEventDays);
        return program;
    }

    private boolean eventoEsAlFinal(Event evento)
    {
        Integer limit = 0530;

        SimpleDateFormat format = new SimpleDateFormat("HHmm");
        try {
            Integer evento_time = Integer.parseInt(format.format(evento.getStartDate()));
            if (evento_time < limit)
                return true;
            return false;
        }catch(Exception numEx){
            Log.e("eventoEsAlFinal","ERROR AL PARSEAR FECHA["+evento.getStartDate()+"]");
            return false;
        }

    }

    private boolean comparafechas(Date fecha_evento, Date fecha_dia)
    {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        String f_1 = format.format(fecha_evento);
        String f_2 = format.format(fecha_dia);
        return f_1.equals(f_2);
    }

    private List<Eventos> getEventosDePrograma(Organizador organizador, Programa programa) throws eefException
    {
        String LOG_SEED = "DSM-->getEventosDePrograma-->";
        initDBHelper();
        Dao eventosDao;
        try
        {
            eventosDao = mdbHelper.getEventosDao();
            QueryBuilder<Eventos,Integer>  qb = eventosDao.queryBuilder();
            qb.where().eq(Eventos.PROGRAMA,programa.getId());
            qb.orderBy(Eventos.FECHA_EVENTO, true);
            List<Eventos> listaEventos = qb.query();
            return listaEventos;
        }catch (SQLException eSql) {
            Log.e(LOG_SEED,  "SQLException: ACCEDIENDO  A LOS EVENTOS PROGRAMA["+organizador.getName()+"] del Organizador["+programa.getName()+"]: " +eSql);
            throw new eefException(TypeEefError.DB_ERROR_LOAD_EVENT_PLANNER_INFO);
        }
        catch (Exception ex) {
            Log.e(LOG_SEED, "Exception: NO CONTROLADA  ACCEDIENDO  A LOS EVENTOS PROGRAMA["+organizador.getName()+"] del Organizador["+programa.getName()+"]: " +ex);
            throw new eefException(TypeEefError.DB_ERROR_LOAD_EVENT_PLANNER_INFO);
        }
        finally{
            releaseDB();
        }
    }
    public Organizador getOrganizador(Configuracion configuracion) throws eefException
    {
        String LOG_SEED = "DSM --> getOrganizador -->";
        initDBHelper();
        Dao organizadorDao;
        try
        {
            organizadorDao = mdbHelper.getOrganizadorDao();
            Organizador organizador = (Organizador) organizadorDao.queryForId(configuracion.getEventPlannerId());
            return organizador;
        }catch (SQLException eSql) {
            Log.e(LOG_SEED,  "SQLException: ACCEDIENDO AL EVENT PLANNER CONFIGUIRADO: " +eSql);
            throw new eefException(TypeEefError.DB_ERROR_LOAD_EVENT_PLANNER_INFO);
        }
        catch (Exception ex) {
            Log.e(LOG_SEED, "Exception: NO CONTROLADA ACCEDIENDO AL EVENT PLANNER CONFIGURADO: " + ex);
            throw new eefException(TypeEefError.DB_ERROR_LOAD_EVENT_PLANNER_INFO);
        }
        finally{
            releaseDB();
        }

    }

    public EventPlanner getEventPLanner(Configuracion configuracion) throws eefException
    {
        String LOG_SEED = "DSM --> getEventPLanner -->";

        try
        {
            Organizador organizador = getOrganizador(configuracion);
            EventPlanner eventPlannerDb = organizadorToEventPlanner(organizador);
            return eventPlannerDb;
        }catch (eefException eef) {
            Log.e(LOG_SEED,  "SQLException: ACCEDIENDO AL EVENT PLANNER CONFIGUIRADO organizdor"+configuracion.getEventPlannerId()+"]:"+eef);
            throw new eefException(TypeEefError.DB_ERROR_LOAD_EVENT_PLANNER_INFO);
        }
        catch (Exception ex) {
            Log.e(LOG_SEED, "Exception: NO CONTROLADA ACCEDIENDO AL EVENT PLANNER CONFIGURADO: " + ex);
            throw new eefException(TypeEefError.DB_ERROR_LOAD_EVENT_PLANNER_INFO);
        }
    }


    public List<Programa> getListaProgramasSineventos(Organizador organizador) throws eefException
    {
        String LOG_SEED = "DSM --> getListaProgramasSineventos -->";
        EventPlanner eventPlannerDb = new EventPlanner();
        initDBHelper();
        Dao programaDao;
        try
        {
            programaDao = mdbHelper.getProgramaDao();
            List<Programa> programas =  programaDao.queryForEq(Programa.ORGANIZADOR, organizador);
            return programas;
        }catch (SQLException eSql) {
            Log.e(LOG_SEED,  "SQLException: ACCEDIENDO AL LISTADO DE PROGRAMAS DE ["+organizador.getName()+"]: " +eSql);
            throw new eefException(TypeEefError.DB_ERROR_LOAD_PROGRAM);
        }
        catch (Exception ex) {
            Log.e(LOG_SEED, "Exception: ACCEDIENDO AL LISTADO DE PROGRAMAS DE ["+organizador.getName()+"]: " +ex);
            throw new eefException(TypeEefError.DB_ERROR_LOAD_PROGRAM);
        }
        finally{
            releaseDB();
        }
    }

    public List<Program> getListaProgramSineventos(Organizador organizador) throws eefException
    {
        String LOG_SEED = "DSM --> getListaProgramasSineventos -->";
        EventPlanner eventPlannerDb = new EventPlanner();
        initDBHelper();
        Dao programaDao;
        try
        {
            programaDao = mdbHelper.getProgramaDao();
            List<Programa> programas =  programaDao.queryForEq(Programa.ORGANIZADOR, organizador);
            ArrayList<Program> listaProgram = new ArrayList<Program>();
            if(programas == null || programas.isEmpty())
                return listaProgram;
            for(Programa programa: programas) {
                listaProgram.add(programaToProgram(programa));
            }
            return listaProgram;
        }catch (SQLException eSql) {
            Log.e(LOG_SEED,  "SQLException: ACCEDIENDO AL LISTADO DE PROGRAMAS DE ["+organizador.getName()+"]: " +eSql);
            throw new eefException(TypeEefError.DB_ERROR_LOAD_PROGRAM);
        }
        catch (Exception ex) {
            Log.e(LOG_SEED, "Exception: ACCEDIENDO AL LISTADO DE PROGRAMAS DE ["+organizador.getName()+"]: " +ex);
            throw new eefException(TypeEefError.DB_ERROR_LOAD_PROGRAM);
        }
        finally{
            releaseDB();
        }
    }

    public void grabarEventoComoFavorito(Event evento) throws eefException
    {
        tratarEventoFavorito(evento,Event.FAVORITO);
    }

    public void borraEventoComoFavorito(Event evento) throws eefException
    {
        tratarEventoFavorito(evento,Event.NO_FAVORITO);
    }

    private void tratarEventoFavorito(Event evento, Integer marca) throws eefException
    {
        String LOG_SEED = "DSM-->marcarEventoComoFavorito-->";
        initDBHelper();
        Dao eventosDao;
        try
        {
            Date fecha = new GregorianCalendar().getTime();

            eventosDao = mdbHelper.getEventosDao();
            UpdateBuilder<Eventos,Integer> qb = eventosDao.updateBuilder();
            qb.updateColumnValue(Eventos.FAVORITO, marca);

            if(marca == Event.FAVORITO) {
                qb.updateColumnValue(Eventos.FECHA_FAVORITO,fecha);
                qb.updateColumnValue(Eventos.FECHA_FIN_FAVORITO,null);
            }
            else
            {
                qb.updateColumnValue(Eventos.FECHA_FAVORITO,fecha);
                qb.updateColumnValue(Eventos.FECHA_FIN_FAVORITO,fecha);
            }
            qb.where().eq(Eventos.ID, evento.getEventId());
            int filasActualizadas= qb.update();
            if(filasActualizadas!=1) {
                Log.e(LOG_SEED,"EL NUMERO DE FILAS ACTUALIZADAS ["+filasActualizadas+"]");
                throw new eefException(TypeEefError.DB_ERROR_EVENT_AS_BOOKMARK);
            }
        }catch (eefException eef){
            throw eef;
        }
        catch (SQLException eSql) {
            Log.e(LOG_SEED,  "SQLException: Marcando campo favorito de evento ["+evento.getSortText()+"] campo favorito["+marca+"]: " +eSql);
            throw new eefException(TypeEefError.DB_ERROR_EVENT_AS_BOOKMARK);
        }
        catch (Exception ex) {
            Log.e(LOG_SEED, "Exception: NO CONTROLADA  Marcando campo favorito de evento ["+evento.getSortText()+"] campo favorito["+marca+"]: " +ex);
            throw new eefException(TypeEefError.DB_ERROR_EVENT_AS_BOOKMARK);
        }
        finally{
            releaseDB();
        }
    }


    public List<Program> getFullProgramListOnlyFavoritos() throws eefException
    {
        final String LOG_SEED = "DSM-->getPrograms";

        try {
            //Se reupera la configuración
            Configuracion configuracion = getConfiguracion();

            //Se recupera el event Planner
            Organizador organizador = getOrganizador(configuracion);

            //Se recupera la lista de programas
            List<Programa> listaProgramasDB = getListaProgramasSineventos(organizador);

            //Se recupera la lista de programas
            List<Program> listaProgramas = getListProgramsOnlyFavoritos(configuracion, organizador, listaProgramasDB);
            return listaProgramas;

        }catch (eefException eef)
        {
            Log.e(LOG_SEED,"eef Exception al recuperar la lista de programas solo con Favoritos");
            throw eef;
        }
        catch (Exception ex)
        {
            Log.e(LOG_SEED, "Excepcion no controlada al recuperar la lista de pro");
            throw new eefException(TypeEefError.DB_ERROR_LOAD_PROGRAM);
        }

    }
    private List<Program> getListProgramsOnlyFavoritos(Configuracion configuracion, Organizador organizador,List<Programa> listaProgramasDB) throws eefException
    {
        final String LOG_SEED = "DSM-->getListProgramsFullInfo";

        //Se inicializa la lista de programas a devolver
        ArrayList<Program> listaPrograms = new ArrayList<Program>();
        try {
            //Para cada programa que se ha recuperado se realiza la incialización y se recupera la información
            for (Programa programa : listaProgramasDB) {
                Program program = programaToProgram(programa);
                program.setAutonomousCommunityId(organizador.getCcaa_id());
                program.setEventPlannerId(organizador.getId());
                List<Eventos> eventos = getEventosDeProgramaOnlyFavoritos(organizador,programa);
                program = setEventosToProgram(program, eventos);
                listaPrograms.add(program);
            }
        }catch (eefException eef)
        {
            Log.e(LOG_SEED,"ERROR AL CONSTRUIR LISTA DE PROGRAMAS CON INFO DB");
            throw eef;
        }catch (Exception ex)
        {
            Log.e(LOG_SEED,"ERROR NO CONTROLADO AL MONTAR PROGRAMAS DEL ORGANIZADOR["+organizador.getName()+"DESDE BD");
            throw new eefException(TypeEefError.DB_ERROR_LOAD_PROGRAM_EVENTS);

        }

        return listaPrograms  ;
    }

    private List<Eventos> getEventosDeProgramaOnlyFavoritos(Organizador organizador, Programa programa) throws eefException
    {
        String LOG_SEED = "DSM-->getEventosDePrograma-->";
        initDBHelper();
        Dao eventosDao;
        try
        {
            eventosDao = mdbHelper.getEventosDao();
            QueryBuilder<Eventos,Integer>  qb = eventosDao.queryBuilder();
            qb.where().eq(Eventos.PROGRAMA,programa.getId()).and().eq(Eventos.FAVORITO, Event.FAVORITO);
            qb.orderBy(Eventos.FECHA_EVENTO, true);
            List<Eventos> listaEventos = qb.query();
            return listaEventos;
        }catch (SQLException eSql) {
            Log.e(LOG_SEED, "SQLException: ACCEDIENDO  A LOS EVENTOS PROGRAMA[" + organizador.getName() + "] del Organizador[" + programa.getName() + "]: " + eSql);
            throw new eefException(TypeEefError.DB_ERROR_LOAD_EVENT_PLANNER_INFO);
        }
        catch (Exception ex) {
            Log.e(LOG_SEED, "Exception: NO CONTROLADA  ACCEDIENDO  A LOS EVENTOS PROGRAMA["+organizador.getName()+"] del Organizador["+programa.getName()+"]: " +ex);
            throw new eefException(TypeEefError.DB_ERROR_LOAD_EVENT_PLANNER_INFO);
        }
        finally{
            releaseDB();
        }
    }

    public void borraTodosLosDatos() throws eefException
    {
        String LOG_SEED = "DSM-->borraTodosLosDatos-->";
        Savepoint sv = null;

        try
        {
            Configuracion configuracion = getConfiguracion();

            Organizador organizador = getOrganizador(configuracion);

            List <Programa> listaProgramas = getListaProgramasSineventos(organizador);

            //Preparo el borrado de la base de datos para dejar preparado para la insercción
            initDBHelper();
            mdbHelper.getConnectionSource().getReadWriteConnection().setAutoCommit(false);
            sv=mdbHelper.getConnectionSource().getReadWriteConnection().setSavePoint("INICIO_BORRADO");

            //REalizo el borrado de los mismos

            for(Programa programa : listaProgramas)
            {
                borraprograma(programa,mdbHelper);
            }
            borraOrganizador(organizador, mdbHelper);
            borraConfiguracion(mdbHelper);
            mdbHelper.getConnectionSource().getReadWriteConnection().commit(sv);
            mdbHelper.getConnectionSource().getReadWriteConnection().setAutoCommit(true);

        }catch (eefException eSql) {
            Log.e(LOG_SEED,  "SQLException: BORRANDO TODOS LOS DATOS  DE LA BD PARA RECONFIGURAR" +eSql);
            try {
                mdbHelper.getConnectionSource().getReadWriteConnection().rollback(sv);
            } catch (SQLException sql)
            {
                Log.e(LOG_SEED,"EXCEPCION AL HACER EL ROLLBACK");
            }
            throw new eefException(TypeEefError.DB_ERROR_ERASED_PROGRAMS_FOR_RECONFIGURE);

        }
        catch (Exception ex) {
            Log.e(LOG_SEED, "Exception: BORRANDO TODOS LOS DATOS  DE LA BD PARA RECONFIGURAR" +ex);
            try {
                mdbHelper.getConnectionSource().getReadWriteConnection().rollback(sv);
            } catch (SQLException sql)
            {
                Log.e(LOG_SEED,"EXCEPCION AL HACER EL ROLLBACK");
            }
            throw new eefException(TypeEefError.DB_ERROR_ERASED_PROGRAMS_FOR_RECONFIGURE);
        }
        finally{
            releaseDB();
        }
    }

    public void borraprograma(Programa programa,DBHelper db) throws eefException
    {
        String LOG_SEED = "DSM-->borraprograma-->";
        try{
            borraEventosPrograma(programa, db);
            Dao programaDao = db.getProgramaDao();
            DeleteBuilder<Programa,Integer> dbq = programaDao.deleteBuilder();
            dbq.where().eq(Programa.ID, programa.getId());
            int programas_borrados=dbq.delete();
            if(programas_borrados != 1) {
                Log.e(LOG_SEED, "ERROR AL BORRAR PROGRAMA["+programa.getName()+"] mas de un programa con el mismo id["+programa.getId()+"]");
                throw new eefException(TypeEefError.DB_ERROR_ERASED_PROGRAMS_FOR_RECONFIGURE);
            }
        }catch (SQLException eSql) {
            Log.e(LOG_SEED,  "SQLException: BORRANDO EL PROGRAMA PROGRAMA["+programa.getName()+"]: " +eSql);
            throw new eefException(TypeEefError.DB_ERROR_ERASED_PROGRAMS_FOR_RECONFIGURE);
        }catch (eefException eef) {
            throw  eef;
        }
        catch (Exception ex) {
            Log.e(LOG_SEED, "Exception: BORRANDO EL PROGRAMA PROGRAMA["+programa.getName()+"]: " +ex);
            throw new eefException(TypeEefError.DB_ERROR_LOAD_EVENT_PLANNER_INFO);
        }

    }

    public void borraEventosPrograma(Programa programa, DBHelper db) throws eefException{
        String LOG_SEED = "DSM-->borraEventosPrograma-->";
        try{
            Dao eventosDao = db.getEventosDao();
            DeleteBuilder<Eventos,Integer> dbq = eventosDao.deleteBuilder();
            dbq.where().eq(Eventos.PROGRAMA, programa.getId());
            int eventos_borrados=dbq.delete();
            Log.d(LOG_SEED,"NUMERO DE VENTOS BORRADOS DEL PROGRAMA ["+programa.getName()+"] es de ["+eventos_borrados+"]");
            if( eventos_borrados <0)
            {
                Log.e(LOG_SEED, "ERROR AL BORRAR LOS EVENTOS DEL PROGRAMA["+programa.getName()+"] NO ENCUENTRA LOS EVENTOS");
                throw new eefException(TypeEefError.DB_ERROR_ERASED_PROGRAMS_FOR_RECONFIGURE);
            }
        }catch (SQLException eSql) {
            Log.e(LOG_SEED,  "SQLException: BORRANDO EL PROGRAMA PROGRAMA["+programa.getName()+"]: " +eSql);
            throw new eefException(TypeEefError.DB_ERROR_ERASED_PROGRAMS_FOR_RECONFIGURE);
        }catch (eefException eef) {
            throw  eef;
        }
        catch (Exception ex) {
            Log.e(LOG_SEED, "Exception: BORRANDO EL PROGRAMA PROGRAMA["+programa.getName()+"]: " +ex);
            throw new eefException(TypeEefError.DB_ERROR_ERASED_PROGRAMS_FOR_RECONFIGURE);
        }
    }

    public void borraOrganizador(Organizador organizador, DBHelper db) throws  eefException
    {
        String LOG_SEED = "DSM-->borraprograma-->";
        try{
            Dao organizadorDao = db.getOrganizadorDao();
            DeleteBuilder<Organizador,Integer> dbq = organizadorDao.deleteBuilder();
            dbq.where().eq(Programa.ID, organizador.getId());
            int organizadores_borrados=dbq.delete();
            if(organizadores_borrados != 1) {
                Log.e(LOG_SEED, "ERROR AL BORRAR ORGNIZADOR["+organizador.getName()+"] 0 o mas de un organizador con el mismo id["+organizador.getId()+"]");
                throw new eefException(TypeEefError.DB_ERROR_ERASED_EVENT_PLANNER);
            }
        }catch (SQLException eSql) {
            Log.e(LOG_SEED,  "SQLException: BORRANDO EL PROGRAMA PROGRAMA["+organizador.getName()+"]: " +eSql);
            throw new eefException(TypeEefError.DB_ERROR_ERASED_EVENT_PLANNER);
        }catch (eefException eef) {
            throw  eef;
        }
        catch (Exception ex) {
            Log.e(LOG_SEED, "Exception: BORRANDO EL PROGRAMA PROGRAMA["+organizador.getName()+"]: " +ex);
            throw new eefException(TypeEefError.DB_ERROR_ERASED_EVENT_PLANNER);
        }


    }

    void actualizarProgramas(UpdateProgramsFromServer updateInfo) throws eefException {

        String LOG_SEED = "DSM-->actualizarProgramas";
        Configuracion configuracion = getConfiguracion();

        EventPlanner eventPlanner = getEventPLanner(configuracion);
        Organizador organizador = getOrganizador(configuracion);

        if((updateInfo.getListaProgramasNew()!=null
                && updateInfo.getListaProgramasNew().size() ==0) &&
            (updateInfo.getListaProgramasToUpdate()!=null &&
                    updateInfo.getListaProgramasToUpdate().size()==0))
            return;

        //Actualizamos los programas
        for(Program program : updateInfo.getListaProgramasToUpdate())
        {

            Programa programa = getProgramFromId(program);
            //Pimero se actualiza los eventos por si hay un problema que no se actulice la fecha del programa

            List<Eventos> listaEventosEnDB = getEventosDePrograma(organizador,programa);
            updateEventosPrograma(organizador,programa,updateInfo.getProgramsDownladed().get(program),listaEventosEnDB);

            //se actualiza el programa.
            Log.d(LOG_SEED,"Se va a actualizar el programa["+program.getProgramSortDescription()+"]");
            programa = programaToProgramaActualizado(programa, program);
            updateProgramaEnDB(programa);
        }

        //Insertamos los nuevos si hay nuevos quie insertar
        if(updateInfo.getListaProgramasNew()!=null
                && updateInfo.getListaProgramasNew().size() !=0)
            grabarListaProgramas(organizador,updateInfo.getListaProgramasNew(),updateInfo.getNewProgramsDownloaded());


    }



    private Programa programaToProgramaActualizado(Programa programa, Program program)
    {
        //Como el programa se va a actualizar al menos la fecha de actualización
        //Siempre hay que actualizar.
        programa.setFecha_ultima_actualizacion(new GregorianCalendar().getTime());

        if(!programa.getFecha_inicio().equals(program.getProgramStartDate()))
            programa.setFecha_inicio(program.getProgramStartDate());

        if(!programa.getFecha_fin().equals(program.getProgramEndDate()))
            programa.setFecha_fin(program.getProgramEndDate());

        if(!programa.getName().equals(program.getProgramLongDescription()))
            programa.setName(program.getProgramLongDescription());
        if(!programa.getUrl_logo().equals(program.getListMultimediaElement().get(0).getMultimediaElementLocation()))
            programa.setUrl_logo(program.getListMultimediaElement().get(0).getMultimediaElementLocation());

        return programa;
    }

    public Programa getProgramFromId(Program program) throws eefException {
        String LOG_SEED = "DSM --> getListaProgramasSineventos -->";
        initDBHelper();
        Dao programaDao;
        try
        {
            programaDao = mdbHelper.getProgramaDao();
            List<Programa> programas =  programaDao.queryForEq(Programa.ID, program.getProgramId());
            if(programas == null || programas.isEmpty()) {
                Log.e(LOG_SEED,"El programa con id["+ program.getProgramId()+"] no existe en la base de datos");
                throw new eefException(TypeEefError.PROGRAM_TO_UPDATE_IS_NOT_IN_DB);
            }
            return programas.get(0);
        }
        catch (eefException eef){
            throw eef;
        }
        catch (SQLException eSql) {
            Log.e(LOG_SEED,  "SQLException: ACCEDIENDO AL PROGRAMA  ["+program.getProgramSortDescription()+"]: " +eSql);
            throw new eefException(TypeEefError.DB_ERROR_LOAD_PROGRAM);
        }
        catch (Exception ex) {
            Log.e(LOG_SEED, "Exception: ACCEDIENDO AL PROGRAMA ["+program.getProgramSortDescription()+"]: " +ex);
            throw new eefException(TypeEefError.DB_ERROR_LOAD_PROGRAM);
        }
        finally{
            releaseDB();
        }
    }

    public void updateProgramaEnDB(Programa programa) throws eefException{
        String LOG_SEED = "DSM-->updateProgramaEnDB-->";
        initDBHelper();
        Dao programaDao;
        try
        {
            programaDao = mdbHelper.getProgramaDao();
            int programasActualizados = programaDao.update(programa);
            if(programasActualizados!=1){
                Log.e(LOG_SEED,"ERROR ACTUALIZANO EL PROGRAMA con ID["+programa.getId()+"] devuelve mas de una fila");
                throw new eefException(TypeEefError.DB_ERROR_UPDATE_PROGRAM);
            }
        }
        catch (eefException eef){
            throw eef;
        }
        catch (SQLException eSql) {
            Log.e(LOG_SEED,  "SQLException: ERROR ACTUALIZANDO EL PROGRAMA con ID ["+ programa.getId()+"]: " +eSql);
            throw new eefException(TypeEefError.DB_ERROR_UPDATE_PROGRAM);
        }
        catch (Exception ex) {
            Log.e(LOG_SEED, "Exception: ERROR ACTUALIZANDO EL PROGRAMA con ID ["+ programa.getId()+"]: " +ex);
            throw new eefException(TypeEefError.DB_ERROR_UPDATE_PROGRAM);
        }
        finally{
            releaseDB();
        }
    }

    public void updateEventosPrograma (Organizador organizador, Programa programa, List<Event> listaEventosServidor, List<Eventos> listaEventosMovil) throws eefException
    {
        String LOG_SEED = "DSM-->updateEventosPrograma-->";
        try {
            //Se recore la lista de eventos descargada y se insertan los nuevos y se actualizan
            //los que haga falta.
            for (Event event : listaEventosServidor) {
                //eventos En BD
                Eventos eventoDescargado = eventToEventos(event);
                if (esNuevo(eventoDescargado, listaEventosMovil)) {
                    agregarEventoPrograma(organizador, programa, eventoDescargado);
                    continue;
                }
                if (estaActualizado(eventoDescargado, listaEventosMovil)) {
                    updateEventoEnPrograma(eventoDescargado, listaEventosMovil.get(listaEventosMovil.indexOf(eventoDescargado)));
                    continue;
                }
                Log.d(LOG_SEED, "THE EVENT [" + event.getEventId() + "] IS NOT NEW , DOESN'T NEED TO BE UPDATE");
            }

            //ahora se mira si alguno de los eventos del movil no esta en el programa y se borra.
            for(Eventos evento : listaEventosMovil){
                if(hayQueBorrarEvento(evento, listaEventosServidor))
                    borrarEventoDePrograma(evento);
            }

        } catch (eefException eefE) {
            Log.e(LOG_SEED,  "EefException actualizando el evento: programa["+programa.getName()+"]:" +eefE);
            throw new eefException(TypeEefError.PROGRAM_TO_UPDATE_EVENT_ERROR);
        }catch (Exception e) {
            Log.e(LOG_SEED, "Exception: NO CONTROLADA Actualizando EVENTOS programa["+programa.getName()+"]: " + e);
            throw new eefException(TypeEefError.PROGRAM_TO_UPDATE_EVENT_ERROR);
        }

    }

    private boolean esNuevo(Eventos eventoDescargado, List<Eventos> listaEventosMovil)
    {
        for(Eventos eventoMovil : listaEventosMovil){
            if(eventoMovil.getId() == eventoDescargado.getId())
                return false;
        }
        return true;
    }

    private void agregarEventoPrograma(Organizador organizador, Programa programa, Eventos evento) throws eefException
    {
        final String LOG_SEED = "DSM-->grabarEventosPrograma->";
        Log.d(LOG_SEED, "Agregando eventos organizador[" + organizador.getTown() + "] programa[" + programa.getName()
                + "]  eventos[" + evento.getId() + "]");
        initDBHelper();
        Dao eventoDao;
        try {
            eventoDao = mdbHelper.getEventosDao();
            Log.d(LOG_SEED, "GRabando evento  nuevo descargado[" + evento.getId() + "]");
            evento.setOrganizador(organizador);
            evento.setPrograma(programa);
            eventoDao.create(evento);
        }
        catch (SQLException sqlE) {
            Log.e(LOG_SEED,  "SQLException: GUARDANDO EVENTO: " +sqlE);
            throw new eefException(TypeEefError.DB_ERROR_SAVE_PROGRAM_EVENTS);
        }
        catch (Exception e) {
            Log.e(LOG_SEED, "Exception: NO CONTROLADA AGRGANDO EVENTOS programa["+programa.getName()+"]: " + e);
            throw new eefException(TypeEefError.DB_ERROR_SAVE_PROGRAM_EVENTS);
        }
        finally
        {
            releaseDB();
        }
    }

    private boolean estaActualizado(Eventos eventoDescargado, List<Eventos> listaEventosMovil){
        if(listaEventosMovil.contains(eventoDescargado))
            return true;
        return false;
    }

    private void updateEventoEnPrograma(Eventos eventoDescagado, Eventos eventoMovil) throws eefException {
        final String LOG_SEED = "DSM-->updateEventoEnPrograma->";
        boolean needUpdate=false;

        if(!eventoMovil.getTitulo().equals(eventoDescagado.getTitulo())) {
            eventoMovil.setTitulo(eventoDescagado.getTitulo());
            needUpdate = true;
        }
        if(!eventoMovil.getDescripcion().equals(eventoDescagado.getDescripcion())) {
            eventoMovil.setDescripcion(eventoDescagado.getDescripcion());
            needUpdate = true;
        }

        if(!eventoMovil.getUrl_imagen().equals(eventoDescagado.getUrl_imagen())){
            eventoMovil.setUrl_imagen(eventoDescagado.getUrl_imagen());
            needUpdate=true;
        }
        if(eventoMovil.getFechaEvento().before(eventoDescagado.getFechaEvento())
                || eventoMovil.getFechaEvento().after(eventoDescagado.getFechaEvento())){
            eventoMovil.setFechaEvento(eventoDescagado.getFechaEvento());
            needUpdate=true;
        }

        if(!needUpdate) {
            Log.e(LOG_SEED, "EL EVENTO ["+eventoMovil.getId()+"] del programa ["+eventoMovil.getPrograma().getName()+"NO NECESITA SER ACTUALIZADO");
            return;
        }

        eventoMovil.setFecha_ultima_actualizacion(new GregorianCalendar().getTime());
        upadteEventoEnDB(eventoMovil);
    }

    private void upadteEventoEnDB(Eventos evento) throws eefException {
        String LOG_SEED = "DSM-->updateProgramaEnDB-->";
        initDBHelper();
        Dao eventoDao;
        try
        {
            eventoDao = mdbHelper.getEventosDao();
            int programasActualizados = eventoDao.update(evento);
            if(programasActualizados!=1){
                Log.e(LOG_SEED,"ERROR ACTUALIZANO EL EVENTO con ID["+evento.getId()+"] devuelve mas de una fila");
                throw new eefException(TypeEefError.DB_ERROR_UPDATE_EVENT_IN_PROGRAM);
            }
        }
        catch (eefException eef){
            throw eef;
        }
        catch (SQLException eSql) {
            Log.e(LOG_SEED,  "SQLException: ERROR ACTUALIZANDO EL EVENTO con ID ["+ evento.getId()+"]: " +eSql);
            throw new eefException(TypeEefError.DB_ERROR_UPDATE_EVENT_IN_PROGRAM);
        }
        catch (Exception ex) {
            Log.e(LOG_SEED, "Exception: ERROR ACTUALIZANDO EL EVENTO con ID ["+ evento.getId()+"]: " +ex);
            throw new eefException(TypeEefError.DB_ERROR_UPDATE_EVENT_IN_PROGRAM);
        }
        finally{
            releaseDB();
        }
    }

    private boolean hayQueBorrarEvento(Eventos evento, List<Event> listaEventos)
    {
       for(Event event : listaEventos){
           if(event.getEventId()==evento.getId())
               return false;
       }
        return true;
    }

    private void borrarEventoDePrograma(Eventos evento) throws  eefException {
        String LOG_SEED = "DSM-->borrarEventoDePrograma-->";
        initDBHelper();
        Dao eventoDao;
        try
        {
            eventoDao = mdbHelper.getEventosDao();
            int eventosBorrados = eventoDao.delete(evento);
            if(eventosBorrados!=1){
                Log.e(LOG_SEED,"ERROR ELIMINANDO EL EVENTO con ID["+evento.getId()+"] BORRA mas de una fila");
                throw new eefException(TypeEefError.DB_ERROR_ERASED_EVENT_FROM_PROGRAM);
            }
        }
        catch (eefException eef){
            throw eef;
        }
        catch (SQLException eSql) {
            Log.e(LOG_SEED,  "SQLException: ERROR BORRANDO EL EVENTO con ID ["+ evento.getId()+"]: " +eSql);
            throw new eefException(TypeEefError.DB_ERROR_ERASED_EVENT_FROM_PROGRAM);
        }
        catch (Exception ex) {
            Log.e(LOG_SEED, "Exception: ERROR BORRANDO EL EVENTO con ID ["+ evento.getId()+"]: " +ex);
            throw new eefException(TypeEefError.DB_ERROR_ERASED_EVENT_FROM_PROGRAM);
        }
        finally{
            releaseDB();
        }
    }

}
