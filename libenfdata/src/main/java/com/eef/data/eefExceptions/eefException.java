package com.eef.data.eefExceptions;

import com.eef.data.eeftypes.TypeEefError;

/**
 * Created by jluis on 19/02/15.
 */
public class eefException extends Exception {

    private TypeEefError errorT;

    public eefException( TypeEefError errorT) {
        super(errorT.getErrorText());
        this.errorT = errorT;
    }

    public TypeEefError getErrorT() {
        return errorT;
    }

    public void setErrorT(TypeEefError errorT) {
        this.errorT = errorT;
    }
}
