package com.eef.data.eeftypes;

/**
 * Created by jluis on 7/02/15.
 */
public enum TypeMultimediaElement {
    UNKNOWN(-1),IMAGE_IN_MOBILE(1),VIDEO_IN_MOBILE(2),AUDIO_IN_MOBILE(3),IMAGE_URL(4),VIDEO_URL(5),AUDIO_URL(6);

    Integer meType;

    TypeMultimediaElement(Integer meType) {
        this.meType = meType;
    }


    public Integer getMeType() {
        return meType;
    }

    public void setMeType(Integer meType) {
        this.meType = meType;
    }

    static TypeMultimediaElement getTipoMultimediaFromCode(Integer idTipoMultimedia)
    {
        Integer longitud= TypeMultimediaElement.values().length;
        for(int i=0;i<longitud;i++)
        {
            if(TypeMultimediaElement.values()[i].getMeType()==idTipoMultimedia)
                return TypeMultimediaElement.values()[i];
            i++;
        }
        return TypeMultimediaElement.UNKNOWN;
    }

}
