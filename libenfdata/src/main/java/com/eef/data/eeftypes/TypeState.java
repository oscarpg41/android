package com.eef.data.eeftypes;

/**
 * Created by jluis on 7/02/15.
 */
public enum TypeState {
    NO_KNOWN_STATE(-1),ENDED(1),STARTED(2),EXTENDED(2),DELAYED(3),SCHEDULED(4),SUSPENDED(5);

    Integer code;


    TypeState(Integer code) {
        this.code = code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;

    }

    static TypeState getEstadoFromCode(Integer idEstado)
    {
        Integer longitud= TypeState.values().length;
        for(int i=0;i<longitud;i++)
        {
            if(TypeState.values()[i].getCode()==idEstado)
                return TypeState.values()[i];
            i++;
        }
        return TypeState.NO_KNOWN_STATE;
    }
//
}
