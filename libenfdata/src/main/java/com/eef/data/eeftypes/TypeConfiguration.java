package com.eef.data.eeftypes;

/**
 * Created by jluis on 19/02/15.
 */
public enum TypeConfiguration {
    INSTALLATION_DEFAULT_WITH_AUTONOMOUS_COMMUNITY(1),INSTALLATION_DEFAULT_WITHOUT_AUTONOMOUS_COMMUNITY(2),USER_CONFIGURATION(3),
    AUTONOMOUS_COMMUNITY_ENABLED(4), AUTONOMOUS_COMMUNITY_DISABLED(5);

    private Integer configurationId;

    TypeConfiguration(Integer configurationId) {
        this.configurationId = configurationId;
    }

    public Integer getConfigurationId() {
        return configurationId;
    }

    public void setConfigurationId(Integer configurationId) {
        this.configurationId = configurationId;
    }
}
