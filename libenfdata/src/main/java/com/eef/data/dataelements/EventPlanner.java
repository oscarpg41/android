package com.eef.data.dataelements;

import com.eef.data.eefExceptions.eefException;
import com.eef.data.eeftypes.TypeEefError;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by jluis on 7/02/15.
 */
public class EventPlanner implements Serializable {
    private static final long serialVersionUID = 1L;


    //specific Id for event Planner
    Integer eventPlannerId;
    //Name that identified the event Planner
    String name;
    //Sort description of the event Planner to use for marketing purposes
    String sortDescription;
    //Long description of the event Planner to user for marketing purposes
    String longDescription;
    //List of Multimedia elements that could be showed
    List<MultimediaElement> multimediaElementList;

    //If true the Event Planner is the active for the configuration and its plans should be showed in
    //the application
    Boolean isActiveInConfiguration;



    //If the APK is working with an Active Autonomous Comunity
    //This autonomous community will contains only the id and the name and not
    //the complete information.
    //Only to be used if it's necessary
    Integer autonomousCommunityId;
    //True if the Autonomous community is the loop
    Boolean autonomousCommunityActive;

    //Event program List
    List<Program> programList;

    //Program List Loaded informs if the list of program is loadded of is necessary recover it
    Boolean programListLoaded;

    //Create and uploaded date
    Date  downloadedDate;
    Date  updatedDate;


    public EventPlanner() {
        this.isActiveInConfiguration = new Boolean(false);
        this.programListLoaded = new Boolean(false);
    }

    /*
       Informs about the program list of the event planner is loaded or not
        If not and the list is needed use the ProgramFactory to Obtain it
    */
    public Boolean isProgramListLoaded(){
        return programListLoaded;
    }

    /*
    Use this method to mark the evenet planner as configured
     Only one event Planner could be marked as Active in configuration
     */
    public void activeEventPlannerFromConfiguration(){

        this.isActiveInConfiguration = true;
    }

    public Integer getEventPlannerId() {

        return eventPlannerId;
    }

    public Integer getAutonomousCommunityId() {
        return autonomousCommunityId;
    }

    public void setAutonomousCommunityId(Integer autonomousCommunityId) {
        this.autonomousCommunityId = autonomousCommunityId;
    }

    public void setEventPlannerId(Integer eventPlannerId) {

        this.eventPlannerId = eventPlannerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getSortDescription() {

        return sortDescription;
    }

    public void setSortDescription(String sortDescription) {
        this.sortDescription = sortDescription;
    }

    public String getLongDescription() {

        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public Boolean getAutonomousCommunityActive() {

        return autonomousCommunityActive;
    }

    public void setAutonomousCommunityActive(Boolean autonomousCommunityActive) {
        this.autonomousCommunityActive = autonomousCommunityActive;
    }

    //Returns the program List defined by the event planner
    public List<Program> getProgramList() throws eefException {
        if(programList==null)
            throw new eefException(TypeEefError.COMMUNITY_NON_USED_ERROR);
        return programList;
    }



    public void setProgramList(List<Program> programList) {
        if(programList==null)
            return;
        this.programList = programList;
        this.programListLoaded= new Boolean(true);
    }

    public List<MultimediaElement> getMultimediaElementList() throws  eefException {
        if(multimediaElementList==null | multimediaElementList.size()==0)
            throw new eefException(TypeEefError.THE_MULTIMEDIA_LIST_IS_EMPTY);
        return multimediaElementList;
    }

    public void setMultimediaElementList(List<MultimediaElement> multimediaElementList) {
        this.multimediaElementList = multimediaElementList;
    }

    public Boolean getProgramListLoaded() {
        return programListLoaded;
    }

    public Date getDownloadedDate() {
        return downloadedDate;
    }

    public void setDownloadedDate(Date downloadedDate) {
        this.downloadedDate = downloadedDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EventPlanner)) return false;

        EventPlanner that = (EventPlanner) o;

        if (!autonomousCommunityId.equals(that.autonomousCommunityId)) return false;
        if (!eventPlannerId.equals(that.eventPlannerId)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = eventPlannerId.hashCode();
        result = 31 * result + autonomousCommunityId.hashCode();
        return result;
    }
}
