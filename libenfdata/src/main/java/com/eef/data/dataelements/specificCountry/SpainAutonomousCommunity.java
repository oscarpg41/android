package com.eef.data.dataelements.specificCountry;

import com.eef.data.dataelements.AutonomousCommunity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jluis on 21/02/15.
 */
public  class SpainAutonomousCommunity {

    private static List<AutonomousCommunity> listAC;
    private final static String[] nameList={"Andalucía","Aragón","Asturias, Principado de", "Balears, Illes", "Canarias",
                                            "Cantabría","Castilla La Mancha","Castilla y León","Cataluña","Comunitat Valenciana",
                                            "Extremadura","Galicia","La Rioja", "Madrid, Comunidad de","Navarra, Comunidad Foral de",
                                            "Euskadi","Murcia, Región de","Ceuta", "Melilla"};


    public static List<AutonomousCommunity> getAutonomousCommunityList(){
        if(listAC != null)
            return listAC;
        listAC = new ArrayList<>();
        int communityId=1;
        for (String name : nameList) {
            AutonomousCommunity au= new AutonomousCommunity();
            au.setAutonomousCommunityId(communityId++);
            au.setAutonomousCommunityName(name);
            listAC.add(au);
        }
        return listAC;
    }
}
