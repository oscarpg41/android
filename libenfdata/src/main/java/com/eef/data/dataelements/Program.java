package com.eef.data.dataelements;

import com.eef.data.eeftypes.TypeState;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by jluis on 7/02/15.
 */
public class Program implements Serializable {

//    Hecho serializable para probar por Juan el 22 Mayo 2015
    private static final long serialVersionUID = 1L;



    //If the APK is working with AutonomousCommunity the id of ComAu which the event planner is sited
    private Integer autonomousCommunityId;
    //Id of the event planner which has published the program.
    private Integer eventPlannerId;
    //program's specific id.
    private  Integer programId;

    //Program sort description to be showed in the mobile
    private String programSortDescription;
    //Long Description of the Program to be showed in the mobile
    private String programLongDescription;

    //Start and End date of the program
    private Date programStartDate;
    private Date programEndDate;

    //Multimedia Elements that are associated to the Program
    private List<MultimediaElement> listMultimediaElement;

    //Program State depending of the Dates or specific actions set by Event Planner
    private TypeState state;

    //Lista de Eventos del programa

    List<DayEvents> eventByDay;
    List<Event> fullList;

    //FEcha de update y fecha de ultima actualización
    private Date downloadedDay;
    private Date lastUpdateDay;

    public List<DayEvents> getEventByDay() {
        return eventByDay;
    }

    public void setEventByDay(List<DayEvents> eventByDay) {
        this.eventByDay = eventByDay;
    }

    public List<Event> getFullList() {
        return fullList;
    }

    public void setFullList(List<Event> fullList) {
        this.fullList = fullList;
    }

    public Integer getAutonomousCommunityId() {
        return autonomousCommunityId;
    }

    public void setAutonomousCommunityId(Integer autonomousCommunityId) {
        this.autonomousCommunityId = autonomousCommunityId;
    }

    public Integer getEventPlannerId() {
        return eventPlannerId;
    }

    public void setEventPlannerId(Integer eventPlannerId) {
        this.eventPlannerId = eventPlannerId;
    }

    public Integer getProgramId() {
        return programId;
    }

    public void setProgramId(Integer programId) {
        this.programId = programId;
    }

    public String getProgramSortDescription() {
        return programSortDescription;
    }

    public void setProgramSortDescription(String programSortDescription) {
        this.programSortDescription = programSortDescription;
    }

    public String getProgramLongDescription() {
        return programLongDescription;
    }

    public void setProgramLongDescription(String programLongDescription) {
        this.programLongDescription = programLongDescription;
    }

    public Date getProgramStartDate() {
        return programStartDate;
    }

    public void setProgramStartDate(Date programStartDate) {
        this.programStartDate = programStartDate;
    }

    public Date getProgramEndDate() {
        return programEndDate;
    }

    public void setProgramEndDate(Date programEndDate) {
        this.programEndDate = programEndDate;
    }

    public List<MultimediaElement> getListMultimediaElement() {
        return listMultimediaElement;
    }

    public void setListMultimediaElement(List<MultimediaElement> listMultimediaElement) {
        this.listMultimediaElement = listMultimediaElement;
    }

    public TypeState getState() {
        return state;
    }

    public void setState(TypeState state) {
        this.state = state;
    }

    public Date getDownloadedDay() {
        return downloadedDay;
    }

    public void setDownloadedDay(Date downloadedDay) {
        this.downloadedDay = downloadedDay;
    }

    public Date getLastUpdateDay() {
        return lastUpdateDay;
    }

    public void setLastUpdateDay(Date lastUpdateDay) {
        this.lastUpdateDay = lastUpdateDay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Program)) return false;

        Program program = (Program) o;

        if (!autonomousCommunityId.equals(program.autonomousCommunityId)) return false;
        if (!eventPlannerId.equals(program.eventPlannerId)) return false;
        if (!programId.equals(program.programId)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = autonomousCommunityId.hashCode();
        result = 31 * result + eventPlannerId.hashCode();
        result = 31 * result + programId.hashCode();
        return result;
    }
}
