package com.eef.data.dataelements;

import com.eef.data.eeftypes.TypeState;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by jluis on 7/02/15.
 */
public class Event implements Serializable{

    //    Hecho serializable para probar por Juan el 25 Mayo 2015
    private static final long serialVersionUID = 1L;

    //Constante que dfine el valor de Favorito
    public static final Integer FAVORITO=1;
    public static final Integer NO_FAVORITO=0;

    //If the APK is working with AutonomousCommunity the id of ComAu which the event planner is sited
    private Integer AutonomousCommunityId;

    //Id of the Event planner that manages the program which the event belong to.
    private Integer eventPlannerId;

    // Program Id which identified the program which the event is included
    private  Integer programId;

    //Specific Event Id
    private Integer eventId;

    //Define the state which the event is currently
    private TypeState state;

    //Start date defined for the event
    private Date startDate;

    //Real StatDate defined
    private Date realStartDate;

    //End date defined for the event
    private Date EndDate;

    //Multimedia event list available for a specific event
    private List<MultimediaElement> multimediaElementList;

    //event sort text description. To be used as title
    private String sortText;

    //event long text description. To be used as general description
    private String longText;

    //if the event is added to the favourites list of the user
    Boolean addedToFavourites;

    public Integer getAutonomousCommunityId() {
        return AutonomousCommunityId;
    }

    public void setAutonomousCommunityId(Integer autonomousCommunityId) {
        AutonomousCommunityId = autonomousCommunityId;
    }

    public Integer getEventPlannerId() {
        return eventPlannerId;
    }

    public void setEventPlannerId(Integer eventPlannerId) {
        this.eventPlannerId = eventPlannerId;
    }

    public Integer getProgramId() {
        return programId;
    }

    public void setProgramId(Integer programId) {
        this.programId = programId;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public TypeState getState() {
        return state;
    }

    public void setState(TypeState state) {
        this.state = state;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return EndDate;
    }

    public void setEndDate(Date endDate) {
        EndDate = endDate;
    }

    public List<MultimediaElement> getMultimediaElementList() {
        return multimediaElementList;
    }

    public void setMultimediaElementList(List<MultimediaElement> multimediaElementList) {
        this.multimediaElementList = multimediaElementList;
    }

    public String getSortText() {
        return sortText;
    }

    public void setSortText(String sortText) {
        this.sortText = sortText;
    }

    public String getLongText() {
        return longText;
    }

    public void setLongText(String longText) {
        this.longText = longText;
    }

    public Boolean getAddedToFavourites() {
        return addedToFavourites;
    }

    public void setAddedToFavourites(Boolean addedToFavourites) {
        this.addedToFavourites = addedToFavourites;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Event)) return false;

        Event event = (Event) o;

        if (!AutonomousCommunityId.equals(event.AutonomousCommunityId)) return false;
        if (!eventId.equals(event.eventId)) return false;
        if (!eventPlannerId.equals(event.eventPlannerId)) return false;
        if (!programId.equals(event.programId)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = AutonomousCommunityId.hashCode();
        result = 31 * result + eventPlannerId.hashCode();
        result = 31 * result + programId.hashCode();
        result = 31 * result + eventId.hashCode();
        return result;
    }
}
