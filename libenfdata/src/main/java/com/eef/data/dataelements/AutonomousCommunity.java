package com.eef.data.dataelements;

import com.eef.data.eefExceptions.eefException;
import com.eef.data.eeftypes.TypeEefError;

import java.util.List;

public class AutonomousCommunity {

    //Autonomous Community ID default when it is not enable
    public final static Integer AUTONOMOUS_COMMUNITY_DEFAULT_ID = -1;

    //Specific Autonomous Community id
    private Integer AutonomousCommunityId;

    //Autonomous Community formal name
    private String AutonomousCommunityName;

    //this attribute informs if this is the current configured community,
    Boolean isActiveInConfiguration;

    //List of planner event configured for the community Autonomous.
    List<EventPlanner> eventPlannerList;

    ///If false there is not an list o eventPlanner loaded
    Boolean eventPlannerListLoaded;

    public String getAutonomousCommunityName() {
        return AutonomousCommunityName;
    }

    public void setAutonomousCommunityName(String autonomousCommunityName) {
        this.AutonomousCommunityName = autonomousCommunityName;
    }

    public List<EventPlanner> getEventPlannerList() throws eefException {
        if(!eventPlannerListLoaded)
            throw new eefException(TypeEefError.CONFIGURATION_ERROR);
        return eventPlannerList;
    }

    public void setEventPlannerList(List<EventPlanner> eventPlannerList)    {
        if(eventPlannerList==null)
            return;
        this.eventPlannerList = eventPlannerList;
        this.eventPlannerListLoaded=true;
    }

    public Integer getAutonomousCommunityId() {
        return AutonomousCommunityId;
    }

    public void setAutonomousCommunityId(Integer autonomousCommunityId) {
        this.AutonomousCommunityId = autonomousCommunityId;
    }

    public Boolean getIsActiveInConfiguration() {
        return isActiveInConfiguration;
    }

    public void setIsActiveInConfiguration(Boolean isActiveInConfiguration) {
        this.isActiveInConfiguration = isActiveInConfiguration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AutonomousCommunity)) return false;

        AutonomousCommunity that = (AutonomousCommunity) o;

        if (!AutonomousCommunityId.equals(that.AutonomousCommunityId)) return false;


        return true;
    }

    @Override
    public int hashCode() {
        return AutonomousCommunityId.hashCode();
    }
}
