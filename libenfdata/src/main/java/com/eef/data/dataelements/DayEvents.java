package com.eef.data.dataelements;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by jluis on 16/05/15.
 */
public class DayEvents implements Serializable{






//    Hecho serializable para probar por Juan el 25 Mayo 2015
        private static final long serialVersionUID = 1L;

    Date programEventDate;
    List<Event> ListaEventos;

    public Date getProgramEventDate() {
        return programEventDate;
    }

    public void setProgramEventDate(Date programEventDate) {
        this.programEventDate = programEventDate;

    }

    public List<Event> getListaEventos() {
        return ListaEventos;
    }

    public void setListaEventos(List<Event> listaEventos) {
        ListaEventos = listaEventos;
    }

    @Override
    public String toString(){
        SimpleDateFormat sfd = new SimpleDateFormat("EEEE dd LLLL yyyy");
        return  sfd.format(this.programEventDate);
    }
}
