package com.eef.data.dataelements;

import com.eef.data.eeftypes.TypeMultimediaElement;

import java.io.Serializable;

/**
 * Created by jluis on 7/02/15.
 */
public class MultimediaElement implements Serializable {

    //This attribut provide the location of the multimedia element. A URL, the local device ...
    private String multimediaElementLocation;

    //This informs about the multimedia type to chosse the adecuate program
    private TypeMultimediaElement multimediaElementType;

    public MultimediaElement(String multimediaElement, TypeMultimediaElement multimediaElementType) {
        this.multimediaElementLocation = multimediaElement;
        this.multimediaElementType = multimediaElementType;
    }

    public String getMultimediaElementLocation() {
        return multimediaElementLocation;
    }

    public void setMultimediaElementLocation(String multimediaElementLocation) {
        this.multimediaElementLocation = multimediaElementLocation;
    }

    public TypeMultimediaElement getMultimediaElementType() {
        return multimediaElementType;
    }

    public void setMultimediaElementType(TypeMultimediaElement multimediaElementType) {
        this.multimediaElementType = multimediaElementType;
    }
}
