package com.eef.data.managers;

import com.eef.data.dataelements.AutonomousCommunity;
import com.eef.data.dataelements.EventPlanner;
import com.eef.data.dataelements.Program;
import com.eef.data.eefExceptions.eefException;

import java.util.List;

/**
 * Created by jluis on 16/05/15.
 */
public interface IProgramListManager {

    void loadData( AutonomousCommunity aC, EventPlanner eP) throws eefException;

    Boolean existCurrentProgram();

    Boolean existFuturetPrograms();

    Boolean existPastPrograms();

    Program getCurrentProgram()  throws eefException;

    List<Program> getFutureProgram()  throws eefException;

    List<Program> getCurrentAndFuturePrograms()  throws eefException;

    List<Program> getFullProgramList() throws eefException;

    EventPlanner getEventPlannerInformation();

    AutonomousCommunity getAutonomousCommunityInformation();

}
