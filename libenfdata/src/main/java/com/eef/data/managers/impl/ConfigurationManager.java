package com.eef.data.managers.impl;

import com.eef.data.dataelements.AutonomousCommunity;
import com.eef.data.dataelements.EventPlanner;
import com.eef.data.eefExceptions.eefException;
import com.eef.data.eeftypes.TypeConfiguration;
import com.eef.data.eeftypes.TypeEefError;
import com.eef.data.managers.IConfigurationManager;

import java.io.Serializable;
import java.util.List;


/**
 * Created by jluis on 19/02/15.
 */
public class ConfigurationManager implements IConfigurationManager, Serializable {

    private AutonomousCommunity autonomousCommunityConfigured;
    private EventPlanner eventPlannerConfigured;

    private AutonomousCommunity AUpendingToSave;
    private EventPlanner EPpendingToSave;

    private Boolean isConfigured;
    private Boolean includeCommunityInConfiguration;

    //If Community configuration is enabled the initial search list will be this.
    private List<AutonomousCommunity> autonomousCommunitiesList;

    //If Community configuration is not enabled, the intial search list will be this.
    private List<EventPlanner> onlyEventPlannerList;

    //Configuration type using in the com.o3j.es.estamosenfiestas.factory to obtain the configuration Manager
    TypeConfiguration configurationType;


    private Boolean configurationNotSaved;

    @Override
    public TypeConfiguration checkConfigurationType(){

        return configurationType;
    }

    public ConfigurationManager(TypeConfiguration typeConfiguration) {
        this.configurationType = typeConfiguration;
        this.autonomousCommunitiesList = null;
        this.configurationNotSaved= new Boolean(false);
        this.AUpendingToSave=null;
        this.EPpendingToSave=null;
        this.isConfigured=false;
        if(configurationType == TypeConfiguration.AUTONOMOUS_COMMUNITY_ENABLED)
            this.includeCommunityInConfiguration = new Boolean(true);
        else
            this.includeCommunityInConfiguration = new Boolean(false);

        //Creo la comunidad de Castilla la mancha


    }

    @Override
    public Boolean isCommunityAutonomousEnabled() {
        return includeCommunityInConfiguration;
    }

    @Override
    public List<AutonomousCommunity> getAutonomousCommunityList() throws eefException {
        /*if(!isConfigured)
            throw new eefException(TypeEefError.CONFIGURATION_ERROR);
        checkCommunityEnabled();*/
        return autonomousCommunitiesList;
    }

    @Override
    public List<EventPlanner> getEventPlannerList() throws eefException
    {
        checkValidConfig();
        try {
            checkCommunityEnabled();
        }
        catch(eefException eefE) {
            if (eefE.getErrorT() == TypeEefError.COMMUNITY_NON_USED_ERROR)
                return onlyEventPlannerList;
            else
                throw eefE;
        }
        return autonomousCommunityConfigured.getEventPlannerList();
    }

    @Override
    public void changeAutonomousCommunityConfigured(AutonomousCommunity newAutonomousCommunity,EventPlanner newEventPlanner) throws eefException {
        checkValidConfig();
        if(!isCommunityAutonomousEnabled())
            throw new eefException(TypeEefError.COMMUNITY_NON_USED_ERROR);
        //If the parameters are the same as are configured no changes are needed
//        if(autonomousCommunityConfigured.equals(newAutonomousCommunity) && eventPlannerConfigured.equals(newEventPlanner))
//            return;

        //Checking if the Autonomous Community parameter is a valid and the event planner is included in the EP list
        AUincludesEP(newAutonomousCommunity,newEventPlanner);
//        if(!autonomousCommunityConfigured.equals(newAutonomousCommunity))
            AUpendingToSave = newAutonomousCommunity;
        EPpendingToSave=newEventPlanner;
        configurationNotSaved=true;

        //Cambios de Juan el 15 junio 2015:
        //He comentado las lineas 97, 98 y 102 por que al cambiar la configuracion y volver a
        //Castilla leon, no cogia los datos. Esto hay que revisarlo bien


    }

    @Override
    public void changeEventPlannerConfigured(EventPlanner newEventPlanner) throws eefException {
        //If the autonomous configuration is configured
        checkValidConfig();
        EventPlanner evTemp = getEventPlannerById(newEventPlanner.getEventPlannerId());
        //Checking if the event planner is in the list
        if(evTemp==null) {
            //throws a different error depending on the AU configuration is enabled;
            if(isCommunityAutonomousEnabled())
                throw new eefException(TypeEefError.EVENT_PLANNER_NOT_EXITS_AU);
            throw new eefException(TypeEefError.EVENT_PLANNER_NOT_EXITS);
        }
        EPpendingToSave=evTemp;

    }

    @Override
    public AutonomousCommunity getAutonomousCommunityConfigured() throws eefException {
        checkValidConfig();
        return autonomousCommunityConfigured;
    }

    @Override
    public EventPlanner getEventPlannerConfigured() throws eefException {
        checkValidConfig();
        return eventPlannerConfigured;
    }

    @Override
    public void persistsConfiguration() throws eefException {
        autonomousCommunityConfigured = AUpendingToSave;
        eventPlannerConfigured = EPpendingToSave;
        configurationNotSaved=false;
        AUpendingToSave = null;


    }

    @Override
    public boolean configurationToPersist() {
        return configurationNotSaved;
    }

    @Override
    public AutonomousCommunity getAutonomousCommunityById(Integer id) throws eefException {
        checkCommunityEnabled();
        AutonomousCommunity auTmp = new AutonomousCommunity();
        auTmp.setAutonomousCommunityId(id);
        if(!autonomousCommunitiesList.contains(auTmp))
            throw new eefException(TypeEefError.AUTONOMOUS_COMMUNITY_NOT_EXITS);
        return autonomousCommunitiesList.get(autonomousCommunitiesList.indexOf(auTmp));
    }

    @Override
    public EventPlanner getEventPlannerById(Integer id) throws eefException{
        List<EventPlanner> listEv=null;
        EventPlanner evT = new EventPlanner();
        evT.setEventPlannerId(id);
        try {
            checkCommunityEnabled();
            listEv = autonomousCommunityConfigured.getEventPlannerList();
            evT.setAutonomousCommunityId(autonomousCommunityConfigured.getAutonomousCommunityId());
        }
        catch (eefException eefE)
        {
            listEv=this.onlyEventPlannerList;
            evT.setAutonomousCommunityId(AutonomousCommunity.AUTONOMOUS_COMMUNITY_DEFAULT_ID);
        }
        if(!listEv.contains(evT))
            throw new eefException(TypeEefError.EVENT_PLANNER_NOT_EXITS);
        return listEv.get(listEv.indexOf(evT));
    }

    @Override
    public boolean isRightConfigured() {
        return isConfigured;
    }


    private void AUincludesEP(AutonomousCommunity au, EventPlanner ep) throws eefException
    {
        if(!isCommunityAutonomousEnabled())
            throw new eefException(TypeEefError.COMMUNITY_NON_USED_ERROR);
        if(!autonomousCommunitiesList.contains(au))
            throw new eefException(TypeEefError.AUTONOMOUS_COMMUNITY_NOT_EXITS);
        AutonomousCommunity AUt = autonomousCommunitiesList.get(autonomousCommunitiesList.indexOf(au));
        List<EventPlanner> EPl = AUt.getEventPlannerList();
        if(!EPl.contains(ep))
               throw new eefException(TypeEefError.EVENT_PLANNER_NOT_EXITS);
    }

    private void checkValidConfig() throws eefException
    {
        if(!isRightConfigured())
            throw new eefException(TypeEefError.CONFIGURATION_ERROR);
    }

    private void checkCommunityEnabled() throws eefException {
        if(!isCommunityAutonomousEnabled())
            throw new eefException(TypeEefError.COMMUNITY_NON_USED_ERROR);
    }

    public List<AutonomousCommunity> getAutonomousCommunitiesList() {
        return autonomousCommunitiesList;
    }

    public void setAutonomousCommunitiesList(List<AutonomousCommunity> autonomousCommunitiesList) {
        this.autonomousCommunitiesList = autonomousCommunitiesList;
    }

    public void setConfiguration(AutonomousCommunity aU, EventPlanner eP)
    {
        isConfigured = true;
        autonomousCommunityConfigured=aU;
        eventPlannerConfigured=eP;
    }
}
