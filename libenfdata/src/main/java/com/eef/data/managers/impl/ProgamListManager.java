package com.eef.data.managers.impl;

import com.eef.data.dataelements.AutonomousCommunity;
import com.eef.data.dataelements.EventPlanner;
import com.eef.data.dataelements.Program;
import com.eef.data.eefExceptions.eefException;
import com.eef.data.eeftypes.TypeEefError;
import com.eef.data.managers.IProgramListManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jluis on 16/05/15.
 */
public class ProgamListManager implements IProgramListManager {

    private  List<Program> fullList = null;
    private  List<Program> futureList = null;
    private  List<Program> pastList = null;
    private  List<Program> currentFutureList = null;
    private  Program currentProgram = null;
    private Boolean existCurrent;
    private Boolean existFuture;
    private Boolean existPast;
    AutonomousCommunity au;
    EventPlanner ep;

    public ProgamListManager()
    {
        fullList = new ArrayList<Program>();
        futureList = new ArrayList<Program>();
        pastList = new ArrayList<Program>();
        currentFutureList = new ArrayList<Program>();
        au=null;
        ep=null;
        existCurrent=new Boolean(false);
        existFuture=new Boolean(false);
        existPast=new Boolean(false);
    }

    @Override
    public void loadData(AutonomousCommunity aC, EventPlanner eP) throws eefException {
        DameProgramsForTest dpT = new DameProgramsForTest();
        List<Program> lp=dpT.getPrograms(aC, eP);
        if(lp==null)
            throw  new eefException(TypeEefError.THE_PROGRAM_LIST_IS_NOT_LOADED);
        au=aC;
        ep=eP;
        ep.setProgramList(lp);
        Date cT = new Date();
        for(Program p : lp)
        {
            //Siempre se añade a full list
            fullList.add(p);

            //Se mira si es pasado

            if(cT.after(p.getProgramEndDate())) {
                pastList.add(p);
                existPast = new Boolean(true);
            }
            //Se mira si es el current
            if(cT.after(p.getProgramStartDate()) && cT.before(p.getProgramEndDate())) {
                existCurrent= new Boolean(true);
                currentProgram=p;
                currentFutureList.add(p);
            }

            // se mira si es futuro
            if(cT.before(p.getProgramStartDate())){
                existFuture = new Boolean(true);
                currentFutureList.add(p);
                futureList.add(p);
            }
        }


    }

    @Override
    public Boolean existCurrentProgram() {
        return existCurrent;
    }

    @Override
    public Boolean existFuturetPrograms() {
        return existFuture;
    }

    @Override
    public Boolean existPastPrograms() {
        return existPast;
    }

    @Override
    public Program getCurrentProgram() throws eefException {
        if(!existCurrent)
            throw new eefException(TypeEefError.NO_PROGRAM_FOR_EVENTPLANNER);
        return currentProgram;
    }

    @Override
    public List<Program> getFutureProgram() throws eefException {
        if(!existFuture)
            throw new eefException(TypeEefError.NO_PROGRAM_FOR_EVENTPLANNER);
        return futureList;
    }

    @Override
    public List<Program> getCurrentAndFuturePrograms() throws eefException {
        if(!existCurrent && !existFuture)
            throw new eefException(TypeEefError.NO_PROGRAM_FOR_EVENTPLANNER);
        return currentFutureList;
    }

    @Override
    public List<Program> getFullProgramList() throws eefException {
        if (!existPast && !existCurrent && !existFuture)
            throw new eefException(TypeEefError.NO_PROGRAM_FOR_EVENTPLANNER);
        return fullList;
    }

    @Override
    public EventPlanner getEventPlannerInformation() {
        return ep;
    }

    @Override
    public AutonomousCommunity getAutonomousCommunityInformation() {
        return au;
    }
}
