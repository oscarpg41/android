package com.eef.data.managers.impl;

import com.eef.data.dataelements.AutonomousCommunity;
import com.eef.data.dataelements.DayEvents;
import com.eef.data.dataelements.Event;
import com.eef.data.dataelements.EventPlanner;
import com.eef.data.dataelements.Program;
import com.eef.data.eefExceptions.eefException;
import com.eef.data.eeftypes.TypeEefError;
import com.eef.data.eeftypes.TypeState;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jluis on 16/05/15.
 */
 public class DameProgramsForTest {

    static EventPlanner siguenza;

    static List<Program> siguenzaPrograms;

    static Program sanjuan;
    static Program sanroque;
    static Program santiago;

    public List<Program> getPrograms(AutonomousCommunity aC, EventPlanner eP) throws eefException
    {
        ArrayList<Program> lp = new ArrayList<Program>();
        if(eP.getEventPlannerId()==1) {
            lp.add(crearMecardoMediaval(aC.getAutonomousCommunityId(), eP.getEventPlannerId(), "GS- "));
            lp.add(creaProgramSanJuan(aC.getAutonomousCommunityId(), eP.getEventPlannerId(), "GS- "));
            return lp;
        }
        if(eP.getEventPlannerId()==10) {
            lp.add(creaProgramSantiago(aC.getAutonomousCommunityId(), eP.getEventPlannerId(), "SI- "));
            lp.add(creaProgramSanRoque(aC.getAutonomousCommunityId(), eP.getEventPlannerId(), "SI- "));
            lp.add(creaProgramSanJuan(aC.getAutonomousCommunityId(), eP.getEventPlannerId(), "SI- "));
            return lp;
        }
        if(eP.getEventPlannerId()==11) {
            lp.add(creaProgramSantiago(aC.getAutonomousCommunityId(), eP.getEventPlannerId(), "TO- "));
            return lp;
        }
        if(eP.getEventPlannerId()==12) {
            lp.add(creaProgramSanRoque(aC.getAutonomousCommunityId(), eP.getEventPlannerId(), "AV- "));
            lp.add(creaProgramSanJuan(aC.getAutonomousCommunityId(), eP.getEventPlannerId(), "AV- "));
            return lp;
        }
        if(eP.getEventPlannerId()==13) {
            lp.add(creaProgramSanJuan(aC.getAutonomousCommunityId(), eP.getEventPlannerId(), "AH- "));
            return lp;
        }
        if(eP.getEventPlannerId()==14) {
            lp.add(creaProgramSanRoque(aC.getAutonomousCommunityId(), eP.getEventPlannerId(), "NA- "));
            return lp;
        }
        throw new eefException(TypeEefError.NO_PROGRAM_FOR_EVENTPLANNER);
    }

    private Program crearMecardoMediaval(Integer acId,Integer epId,String pre)
    {
        Program sj = new Program();
        sj.setAutonomousCommunityId(acId);
        sj.setEventPlannerId(epId);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            sj.setProgramStartDate(sdf.parse("2015-06-05"));
            sj.setProgramEndDate(sdf.parse("2015-06-07"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        sj.setProgramId(genId(154, epId));
        sj.setProgramSortDescription("Mercado Barroco 2015");
        sj.setProgramLongDescription("Mercado Barroco 2015. Si existiera Long DEsciption");
        sj.setState(TypeState.SCHEDULED);

        //Creo los eventos del primer día
        List<DayEvents> xdia = new ArrayList<DayEvents>();
        List<Event> fullList = new ArrayList<Event>();
        Event evC=null;
        DayEvents diaC=null;
        List<Event> dia1_l = null;
        SimpleDateFormat sfd = new SimpleDateFormat("dd-MM-yyyy");

        //Dia 2015-06-05
        diaC= new DayEvents();
        dia1_l = new ArrayList<Event>();
        String dia="05-06-2015";
        try
        {
            diaC.setProgramEventDate(sfd.parse(dia));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        evC = createEvent(acId,epId,sj.getProgramId(),1,dia,"17:30:00"
                ,"Apertura de mercado e inicio de actividades"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),2,dia,"17:45:00"
                ,"Pasacalles musical (Jabardeus)"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),3,dia,"18:00:00"
                ,pre+"El Dragosaurio y el cazador recorren las calles"
                ,"");
        evC = createEvent(acId,epId,sj.getProgramId(),4,dia,"18:00:00"
                ,"Exhibición de cetrería en la Pza. de los Dolores"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),5,dia,"18:30:00"
                ,"Los Duendes"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),6,dia,"19:00:00"
                ,"Ronda por el Mercado de Imperial Services"
                ,"");
        evC = createEvent(acId,epId,sj.getProgramId(),7,dia,"19:30:00"
                ,"Pasacalles musical de Jabardeus"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),8,dia,"19:30:00"
                ,"Historias de la Fuente de Palacio"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),9,dia,"20:00:00"
                ,"El Aula de Flandes-Imperial Services"
                ,"");
        evC = createEvent(acId,epId,sj.getProgramId(),10,dia,"20:15:00"
                ,"El Ducado del Rey-Imperial Services"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),11,dia,"20:30:00"
                ,"Exhibición de cetrería en la Plaza de los Dolores"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),12,dia,"21:00:00"
                ,"Ronda por el Mercado-Imperial Services"
                ,"");
        evC = createEvent(acId,epId,sj.getProgramId(),13,dia,"21:30:00"
                ,"Historias de La Fuente de Palacio"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),14,dia,"21:30:00"
                ,"El Engendro de Palacio"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),15,dia,"22:00:00"
                ,"Pasacalles musical (Jabardeus)"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);


        diaC.setListaEventos(dia1_l);
        xdia.add(diaC);


        //Dia 2015-06-06
        diaC= new DayEvents();
        dia1_l = new ArrayList<Event>();
        dia="06-06-2015";
        try
        {
            diaC.setProgramEventDate(sfd.parse(dia));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        evC = createEvent(acId,epId,sj.getProgramId(),16,dia,"11:00:00"
                ,"Apertura de mercado e inicio de actividades"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),17,dia,"11:30:00"
                ,"Ronda de Imperial Services."
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),18,dia,"12:00:00"
                ,"Exhibición de Cetrería en la Plaza de los Dolores"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),19,dia,"12:00:00"
                ,"El Dragosaurio y el Cazador. Plaza de los Dolores y Calle Valenciana"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),20,dia,"12:00:00"
                ,"Pasacalles musical  Jabardeus. Plaza de los Dolores y Calle La Reina"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),21,dia,"12:15:00"
                ,"El Aula de Flandes-Imperial Services"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),22,dia,"12:45:00"
                ,"El Ducado del Rey-Imperial Services"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),23,dia,"13:30:00"
                ,"El Engendro de Palacio. Plaza de los Dolores y Calle La Reina"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),24,dia,"13:30:00"
                ,"Ronda de Imperial Services"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),25,dia,"13:30:00"
                ,"Los Duendes"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),26,dia,"13:45:00"
                ,"Pasacalles musical Jabardeus. Pza. de los Dolores y C/Valenciana"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),27,dia,"14:00:00"
                ,"Exhibición de Cetrería en la Plaza de los Dolores"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),28,dia,"17:30:00"
                ,"Apertura de mercado e inicio de actividades y Ronda de Imperial Services"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),29,dia,"17:45:00"
                ,"Pasacalles musical (Jabardeus)"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),30,dia,"18:00:00"
                ,"El Dragosaurio y el cazador recorren las calles"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),31,dia,"18:00:00"
                ,"Exhibición de cetrería en la Pza de los Dolores"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),32,dia,"18:30:00"
                ,"Los Duendes"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),33,dia,"18:30:00"
                ,"El aula de Flandes-Imperial Services"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),34,dia,"18:30:00"
                ,"El Ducado del Rey-Imperial Services"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),35,dia,"19:00:00"
                ,"Ronda de Imperial Service"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),36,dia,"19:30:00"
                ,"Historias de la Fuente de Palacio"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),37,dia,"20:00:00"
                ,"El Aula de Flandes-Imperial Services"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),38,dia,"20:15:00"
                ,"El Ducado del Rey-Imperial Services"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),39,dia,"20:30:00"
                ,"Exhibición de cetrería en la Pza de los Dolores"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),40,dia,"21:00:00"
                ,"Ronda por el Mercado-Imperial Services"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),41,dia,"21:30:00"
                ,"El Engendro de Palacio"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),42,dia,"21:30:00"
                ,"Historias de la Fuente de Palacio"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),43,dia,"00:00:23"
                ,"Concierto música Celta (Jabardeus)"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);



        diaC.setListaEventos(dia1_l);
        xdia.add(diaC);

        //Dia 2015-06-07
        diaC= new DayEvents();
        dia1_l = new ArrayList<Event>();
        dia="7-06-2015";
        try
        {
            diaC.setProgramEventDate(sfd.parse(dia));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        evC = createEvent(acId,epId,sj.getProgramId(),44,dia,"11:00:00"
                ,"Apertura de mercado e inicio de actividades"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),45,dia,"11:30:00"
                ,"Ronda de Imperial Services."
                ,"");
        evC = createEvent(acId,epId,sj.getProgramId(),46,dia,"12:00:00"
                ,"Exhibición de Cetrería en la Pza de los Dolores"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),47,dia,"12:00:00"
                ,"El Dragosaurio y el Cazador. Pza de los Dolores y C/ Valenciana"
                ,"");
        evC = createEvent(acId,epId,sj.getProgramId(),48,dia,"12:00:00"
                ,"Pasacalles musical Jabardeus. Pza de los Dolores y C/La Reina"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),49,dia,"12:15:00"
                ,"El Aula de Flandes-Imperial Services"
                ,"");
        evC = createEvent(acId,epId,sj.getProgramId(),50,dia,"12:30:00"
                ,"Actuación de Baile grupo Danzart"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),51,dia,"12:45:00"
                ,"El Ducado del Rey-Imperial Services"
                ,"");
        evC = createEvent(acId,epId,sj.getProgramId(),52,dia,"13:30:00"
                ,"El Engendro de Palacio. Pza de los Dolores y C/La Reina"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),53,dia,"13:30:00"
                ,"Ronda de Imperial Services"
                ,"");
        evC = createEvent(acId,epId,sj.getProgramId(),54,dia,"13:30:00"
                ,"Los Duendes"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),55,dia,"13:45:00"
                ,"Pasacalles musical Jabardeus. Pza de los Dolores y C/ Valenciana"
                ,"");
        evC = createEvent(acId,epId,sj.getProgramId(),56,dia,"14:00:00"
                ,"Exhibición de Cetrería en la Pza de los Dolores"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),57,dia,"17:30:00"
                ,"Apertura de mercado e inicio de actividades y Ronda de Imperial Services"
                ,"");
        evC = createEvent(acId,epId,sj.getProgramId(),58,dia,"17:30:00"
                ,"El aula de Flandes de Imperial Services"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),59,dia,"17:30:00"
                ,"El Ducado del Rey de Imperial Services"
                ,"");
        evC = createEvent(acId,epId,sj.getProgramId(),60,dia,"18:00:00"
                ,"El Dragosaurio y el Cazador recorren el mercado"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),61,dia,"18:30:00"
                ,"Pasacalles musical de Jabardeus"
                ,"");
        evC = createEvent(acId,epId,sj.getProgramId(),62,dia,"18:30:00"
                ,"Los Duendes"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),63,dia,"18:30:00"
                ,"Exhibición de Cetrería en la Plaza de los Dolores"
                ,"");
        evC = createEvent(acId,epId,sj.getProgramId(),64,dia,"18:30:00"
                ,"El aula de Flandes de Imperial Services"
                ,"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),65,dia,"18:30:00"
                ,"El Ducado del Rey de Imperial Services"
                ,"");

        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),66,dia,"19:00:00"
                ,"Ronda de Imperial Services"
                ,"");

        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),67,dia,"19:30:00"
                ,"Historias de La Fuente"
                ,"");

        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),68,dia,"19:30:00"
                ,"Pasacalles Musical de Jabardeus"
                ,"");

        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),69,dia,"20:00:00"
                ,"Exhibición de Cetrería en la Pza de los Dolores"
                ,"");

        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),70,dia,"20:30:00"
                ,"El Engendro de Palacio"
                ,"");

        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),71,dia,"21:00:00"
                ,"Exhibición de Cetrería en la Pza de los Dolores"
                ,"");

        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),72,dia,"21:30:00"
                ,"Historias de la Fuente de Palacio"
                ,"");

        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),73,dia,"21:30:00"
                ,"Pasacalles musical de Jabardeus"
                ,"");

        dia1_l.add(evC);
        fullList.add(evC);

        diaC.setListaEventos(dia1_l);
        xdia.add(diaC);

        //Final
        sj.setEventByDay(xdia);
        sj.setFullList(fullList);
        return sj;
    }
    private  Program creaProgramSantiago(Integer acId,Integer epId,String pre)
    {
        Program sj = new Program();
        sj.setAutonomousCommunityId(acId);
        sj.setEventPlannerId(epId);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            sj.setProgramStartDate(sdf.parse("2014-07-16"));
            sj.setProgramEndDate(sdf.parse("2014-07-20"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        sj.setProgramId(genId(0, epId));
        sj.setProgramSortDescription(pre + "Programacion Fiestas de Santiago 2014");
        sj.setProgramLongDescription(pre + "Programación Fiestas de Santiago 2014. Si existiera Long DEsciption");
        sj.setState(TypeState.SCHEDULED);

        //Creo los eventos del primer día
        List<DayEvents> xdia = new ArrayList<DayEvents>();
        List<Event> fullList = new ArrayList<Event>();
        Event evC=null;
        DayEvents diaC=null;
        List<Event> dia1_l = null;
        SimpleDateFormat sfd = new SimpleDateFormat("dd-MM-yyyy");

        //Dia 16-07.2014
        diaC= new DayEvents();
        dia1_l = new ArrayList<Event>();

        try
        {
            diaC.setProgramEventDate(sfd.parse("16-07-2014"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        evC = createEvent(acId,epId,sj.getProgramId(),1,"16-07-2014","19:30:00"
                ,pre+"CHUPINAZO INAGURAL INAGURAL del día de Santiago"
                ,pre+"Inaguración de las fiestas de Santiago");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),2,"16-07-2014","19:30:00"
                ,pre+"Entrega del Manín de Honor y Concierto en Habaneras"
                ,pre+"En la iglesia, entrega del Manín de Honor. A continuación, tradicional concierto de habaneras del Coro Manín de LAstres");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),3,"16-07-2014","23:00:00"
                ,pre+"Gran Verbena"
                ,pre+"Primera Verbena amenizada por el Grupo K-LIBRE y Discoteca LIBRE DECIBELIOS");
        dia1_l.add(evC);
        fullList.add(evC);

        diaC.setListaEventos(dia1_l);
        xdia.add(diaC);

        //Dia  17-07-2014
        diaC= new DayEvents();
        dia1_l = new ArrayList<Event>();

        try
        {
            diaC.setProgramEventDate(sfd.parse("17-07-2014"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        evC = createEvent(acId,epId,sj.getProgramId(),4,"17-07-2014","12:00:00"
                ,pre+"Gran Pasacalles"
                ,pre+"Pasacalles por las Calles de Lastres amenizado por la banda municipal y el grupo de gigantes y Cabezudos de la asociación la Fiesta");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),5,"17-07-2014","17:00:00"
                ,pre+"Misa Cantada Solemne en Honor a San Juán seguida de Gran merienda"
                ,pre+"Misa Solemne e Honor a Sanjuan MISSA TEDEUM LAUDAMUS de Lorenzo Perosi cantada a dos voces y órgano por el coro de Llastrín. Seguidamente se celebrará una gran Merienda campestre con Costillada y actuación del Grupo el Bronc");
        dia1_l.add(evC);
        fullList.add(evC);

        diaC.setListaEventos(dia1_l);
        xdia.add(diaC);

        //Dia  19-07-2014
        diaC= new DayEvents();
        dia1_l = new ArrayList<Event>();

        try
        {
            diaC.setProgramEventDate(sfd.parse("19-07-2014"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        evC = createEvent(acId,epId,sj.getProgramId(),6,"19-07-2014","12:00:00"
                ,pre+"Misa en honor a los difuntos en la Parroquia Local"
                ,pre+"Misa de Gala con asistencia y entrega de corona conmemorativa por parte del Ayuntamiento de Llastrin");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),7,"19-07-2014","17:00:00"
                ,pre+"Juegos Infantiles"
                ,pre+"Juegos infantiles en el Poli para el disfrute de todos los niños asistentes.");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),8,"19-07-2014","19:30:00"
                ,pre+"Final del Torneo de Futbito San Juan"
                ,pre+"La final se disputará en el polideportivo municipal con un sorteo de un Jamón de Bellota durante el descanso del partido entre los asistentes al mismo");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),9,"19-07-2014","23:30:00"
                ,pre+"Gran Bervena en la Plaza del Ayuntamiento"
                ,pre+"Gran Bervena Amenizada por la orquesta VERSION ORIGINAL y el DJ AUTO-MEGABLOC");
        dia1_l.add(evC);
        fullList.add(evC);

        diaC.setListaEventos(dia1_l);
        xdia.add(diaC);

        //Dia  20-07-2014
        diaC= new DayEvents();
        dia1_l = new ArrayList<Event>();

        try
        {
            diaC.setProgramEventDate(sfd.parse("20-07-2014"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        evC = createEvent(acId,epId,sj.getProgramId(),10,"20-07-2014","17:00:00"
                ,pre+"Exhibición de Waterpolo en el polideportivo Municipal"
                ,pre+"Gran partido entre el equipo CN Natación Oviendo y el Campeón de la Liga nacional At. Barceloneta");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),11,"20-07-2014","18:00:00"
                ,pre+"Tradicional Cucaña en el puerto de Lastres"
                ,pre+"Tradicional CUCAÑA en el puerto de Lastres, con premios en metálico. Presentada por el Speaker Gabriel Gallego. Segudamente se proderá a la tradiconal suelta de patos");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),12,"20-07-2014","20:00:00"
                ,pre+"GRAN CASTILLO DE FUEGOS ARTIFICIALES"
                ,pre+"Gran Castillo de Fuegos Artificiales con espectacular traca final a cargo del taller de pirotecnia JUAN CARLOS DEVITA");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),13,"20-07-2014","00:30:00"
                ,pre+"Ultima Verbena de las Fiesta"
                ,pre+"Ultima verbebna de las fiestas amenizada por los grupos CUARTA CALLE y SONORA REAL. Gran fin de fiesta con el tradicional desfile de los Mayos");
        dia1_l.add(evC);
        fullList.add(evC);

        diaC.setListaEventos(dia1_l);
        xdia.add(diaC);


        //Final
        sj.setEventByDay(xdia);
        sj.setFullList(fullList);

        return sj;
    }

    private  Program creaProgramSanRoque(Integer acId,Integer epId,String pre) {
        Program sj = new Program();
        sj.setAutonomousCommunityId(acId);
        sj.setEventPlannerId(epId);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            sj.setProgramStartDate(sdf.parse("2015-05-07"));
            sj.setProgramEndDate(sdf.parse("2015-06-16"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        sj.setProgramId(genId(1, epId));
        sj.setProgramSortDescription(pre + "Programación Fiestas de San Roque 2015");
        sj.setProgramLongDescription(pre + "Programación Fiestas de San Roque 2015. Si existiera Long DEsciption");
        sj.setState(TypeState.SCHEDULED);

        //Creo los eventos del primer día
        List<DayEvents> xdia = new ArrayList<DayEvents>();
        List<Event> fullList = new ArrayList<Event>();
        Event evC = null;
        DayEvents diaC = null;
        List<Event> dia1_l = null;
        SimpleDateFormat sfd = new SimpleDateFormat("dd-MM-yyyy");

        //Final
        sj.setEventByDay(xdia);
        sj.setFullList(fullList);

        //Dia 07-05-2015
        diaC= new DayEvents();
        dia1_l = new ArrayList<Event>();

        try
        {
            diaC.setProgramEventDate(sfd.parse("07-05-2015"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        evC = createEvent(acId,epId,sj.getProgramId(),1,"07-05-2015","20:00:00"
                ,pre+"SI- Desfile de las peñas y Apertura oficial de las fiestas"
                ,pre+"SI- Desde el colegio Público \"San Antonio de Portacelli\" desfile de Peñas y Carrozas con su recorrido tradicional hasta la Plaza Mayor. Pregón y Apertura Oficial de las fiestas a cargo del Escritos José Esteba . Colaboran las Peñas \"el Golpe\" y \"Los Vikingos\"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),2,"07-05-2015","21:30:00"
                ,pre+"SI- Verbena Popular en la alameda"
                ,pre+"SI- Gran Verbena Popular en la Alameda amenizada por la Orquesta \"SHOW BAND\"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),3,"07-05-2015","00:30:00"
                ,pre+"SI- Verbena Popular en la alameda"
                ,pre+"SI- Gran Verbena Popular en la Alameda amenizada por la Orquesta \"SHOW BAND\"");
        dia1_l.add(evC);
        fullList.add(evC);

        diaC.setListaEventos(dia1_l);
        xdia.add(diaC);

        //Dia 15-05-2015
        diaC= new DayEvents();
        dia1_l = new ArrayList<Event>();

        try
        {
            diaC.setProgramEventDate(sfd.parse("14-05-2015"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        evC = createEvent(acId,epId,sj.getProgramId(),4,"14-05-2015","11:00:00"
                ,pre+"SI- Gran Ginkana INFANTIL en la Alameda"
                ,pre+"SI- En el paseo de la Alameda, junto a la Hermita del Humilladero Gran GINKANA INFANTIL. Organiza la peña \"El Apagon\"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),5,"14-05-2015","12:00:00"
                ,pre+"SI- Cucañas Infantiles en la Alameda"
                ,pre+"SI- En el Paseo de la Alameda, Cucañas Infantiles. Carreras de Sacos y juegos de Herradura. Organiza: Jaime Gómez Olaya");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),6,"14-05-2015","13:00:00"
                ,pre+"SI- Gran desfile de GIGANTES Y CABEZUDOS"
                ,pre+"SI- Desde la plaza Mayor, salida de GIGANTES Y CABEZUDOS. Recorrido: Salida de la Plaza Mayor, Calle Medina hasta la Alameda. Regreso a la Plaza Mayor por las calles Humilladero y Cardenal Mendoza, acompañadas por las Charangas de las Peñas \"Chingui- Y Estafa\". Disparo de JUEGOS JAPONESES a su llegada a la Alameda");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),7,"14-05-2015","18:00:00"
                ,pre+"SI- Emocionante Encierro Infantil"
                ,pre+"SI- En el Paseo de las Cruces EMOCIONANTE ENCIERRO INFANTIL a cargo de las Peñas Rampa y Tropezón.");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),8,"14-05-2015","20:00:00"
                ,pre+"SI- Teatro Infantil den la Plazuela de la Carcel"
                ,pre+"SI- En la Plazuela de la Cárcel TEATRO INFANTIL. Cía.: Los Títeres Vivos. Al nalizar se realizará una actividad de globo exia con todos los niños. Patrocina: UTEA rma - Rayet");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),9,"14-05-2015","21:30:00"
                ,pre+"SI- Gran Verbena Popular en la Alameda"
                ,pre+"SI- En el Paseo de La Alameda. VERBENA POPULAR. Amenizada por la Orquesta \"BONANZA\"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),10,"14-05-2015","00:00:00"
                ,pre+"SI- Actuación de CELTAS CORTOS"
                ,pre+"SI- Actuación del GRupo Celtas Cortos en la Plaza Mayor.");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),11,"14-05-2015","00:30:00"
                ,pre+"SI- Verbena Popular en la Alameda"
                ,pre+"SI- En el Paseo de La Alameda. VERBENA POPULAR. Amenizada por la Orquesta \"BONANZA\"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),12,"14-05-2015","02:00:00"
                ,pre+"SI- En el Paseo de Las Cruces. TORO DE FUEGO"
                ,pre+"SI- En el Paseo de Las Cruces. TORO DE FUEGO");
        dia1_l.add(evC);
        fullList.add(evC);
        diaC.setListaEventos(dia1_l);
        xdia.add(diaC);

        //Dia 22-05-2015
        diaC= new DayEvents();
        dia1_l = new ArrayList<Event>();

        try
        {
            diaC.setProgramEventDate(sfd.parse("22-05-2015"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        evC = createEvent(acId,epId,sj.getProgramId(),13,"22-05-2015","07:30:00"
                ,pre+"SI- DIANA FLOREADA. A cargo de las Peñas \"Golpe-Puntazo-Verdugos y Pendón\" "
                ,pre+"SI- DIANA FLOREADA. A cargo de las Peñas \"Golpe-Puntazo-Verdugos y Pendón\" ");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),14,"22-05-2015","08:00:00"
                ,pre+"SI- DESENCAJONAMIENTO de reses bravas"
                ,pre+"SI- En el Paseo de las Cruces. DESENCAJONAMIENTO de las reses que se han de lidiar por la tarde.");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),15,"22-05-2015","11:30:00"
                ,pre+"SI- SOLEMNE  FUNCIÓN  EN HONOR A SAN ROQUE"
                ,pre+"SI-  En la iglesia de las RR. Ursulinas. SOLEMNE  FUNCIÓN  EN HONOR A SAN ROQUE. Patrón de la Ciudad, cantada por la Rondalla Seguntina, con Procesión del Santo acompañado por los Dulzaineros de la Escuela Municipal de Dulzaina y Tambor, asistiendo la Corporación Municipal, Alcaldes Pedáneos, Reina de Fiestas y Damas de Honor.");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),16,"22-05-2015","12:00:00"
                ,pre+"SI- JUEGOS INFANTILES"
                ,pre+"SI- En la Plaza Mayor JUEGOS INFANTILES organizados por la Peña El Pendón.");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),17,"22-05-2015","12:30:00"
                ,pre+"SI- CUCAÑAS INFANTILES"
                ,pre+"SI- En el Paseo de La Alameda. CUCAÑAS  INFANTILES. Tiro de cuerda y juegos de herradura. Organiza: Jaime Gómez Olaya.");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),18,"22-05-2015","13:30:00"
                ,pre+"SI- PASACALLES ESPECIAL DE GIGANTES Y CABEZUDOS, Gigantes de Sigüenza y Gigantes Los Pilucos."
                ,pre+"SI- Desde la Plaza Mayor, PASACALLES ESPECIAL DE GIGANTES Y CABEZUDOS, Gigantes de Sigüenza y Gigantes Los Pilucos. Recorrido: salida de la Plaza Mayor, Calle Medina hasta La Alameda, regreso a la Plaza Mayor por las calles Humilladero y Cardenal Mendoza, acompañados por la charanga de las Peñas \"Apagón y Pendón\". Disparo de JUEGOS JAPONESES a su llegada a La Alameda.");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),19,"22-05-2015","18:00:00"
                ,pre+"SI- Pasacalles festivo previo a la corrida de toros"
                ,pre+"SI- Desde la Plaza de las Pirámides del Pº de la Alameda y hasta la Plaza de Toros Pasacalles acompañados por el Alguacilillo, Corporación, Reina y Damas de Fiestas y Peñas.");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),20,"22-05-2015","18:30:00"
                ,pre+"SI- EXTRAORDINARIA NOVILLADA PICADA. Seis novillos de la ganadería \"Antonio Bañuelos\""
                ,pre+"SI- En la Plaza Municipal de Toros \"Las Cruces\". EXTRAORDINARIA NOVILLADA PICADA. Seis novillos de la ganadería \"Antonio Bañuelos\" Para los NOVILLEROS\n" +
                "- SERGIO CEREZOS\n" +
                "- CARLOS DURAN\n" +
                "- RAFAEL CASTELLANO.\n" +
                "Triunfador Certamen Novilleros 2009.Sevilla\n" +
                "(Cambios de tercio peñas: Chingui - Mendrugo y Apagón)");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),21,"22-05-2015","19:00:00"
                ,pre+"SI- Teatro de Titeres"
                ,pre+"SI- En la Plazuela de la Cárcel TEATRO DE TITERES. Cía.: Los Pilucos.\n" +
                "Patrocina: Font Vella ");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),22,"22-05-2015","21:30:00"
                ,pre+"SI- Verbena Popular en la Alameda"
                ,pre+"SI- En el Paseo de La Alameda. VERBENA POPULAR. Amenizada por la Orquesta \"MUNDO ORQUESTA\"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),23,"22-05-2015","23:30:00"
                ,pre+"SI- En la Plaza Mayor NOCHE DEL DISFRAZ"
                ,pre+"SI- En la Plaza Mayor NOCHE DEL DISFRAZ\n" +
                "Concentración de Peñas junto al Edif. del Obispado, C/ Guadalajara, C/ Serrano Sanz, C/ San Roque y entrada en la Alameda por la puerta de la Ermita del Humilladero y Pº central de la Alameda");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),24,"22-05-2015","00:30:00"
                ,pre+"SI- Actuación de ARRABAL FOLK"
                ,pre+"SI- En la Plaza Mayor, Actuación de ARRABAL FOLK. Patrocina: Ibercaja");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),25,"22-05-2015","00:30:00"
                ,pre+"SI- Verbena Popular en la Alameda"
                ,pre+"SI- En el Paseo de La Alameda. VERBENA POPULAR. Amenizada por la Orquesta  \"MUNDO ORQUESTA\"");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),26,"22-05-2015","02:00:00"
                ,pre+"SI- En el Paseo de Las Cruces. TORO DE FUEGO"
                ,pre+"SI- En el Paseo de Las Cruces. TORO DE FUEGO");
        dia1_l.add(evC);
        fullList.add(evC);
        diaC.setListaEventos(dia1_l);
        xdia.add(diaC);

        //Dia 23-05-2015
        diaC= new DayEvents();
        dia1_l = new ArrayList<Event>();

        try
        {
            diaC.setProgramEventDate(sfd.parse("23-05-2015"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        evC = createEvent(acId,epId,sj.getProgramId(),27,"23-05-2015","07:30:00"
                ,pre+"SI- DIANA FLOREADA"
                ,pre+"SI- DIANA FLOREADA. A cargo de las Peñas \"Apagón y Chingui-Mendrugo\".");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),28,"23-05-2015","07:30:00"
                ,pre+"SI- DESENCAJONAMIENTO de reses bravas"
                ,pre+"SI- En el Paseo de Las Cruces. DESENCAJONAMIENTO de las reses que se han de lidiar por la tarde.");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),29,"23-05-2015","11:00:00"
                ,pre+"SI- PARQUE INFANTIL"
                ,pre+"SI- En el Paseo de La Alameda. PARQUE INFANTIL. Patrocina: Font Vella.");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),30,"23-05-2015","12:00:00"
                ,pre+"SI- CUCAÑAS INFANTILES"
                ,pre+"SI-  CUCAÑAS INFANTILES Final de juegos de herradura\n" +
                "Organiza: Jaime Gómez Olaya.");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),31,"23-05-2015","13:00:00"
                ,pre+"SI- GIGANTES Y CABEZUDOS."
                ,pre+"SI- Desde la Plaza Mayor, salida de GIGANTES Y CABEZUDOS.\n" +
                "Recorrido: salida de la Plaza Mayor, Calle Medina hasta La Alameda, regreso a la Plaza Mayor por las calles Humilladero y Cardenal Mendoza, acompañados por la charanga de las Peñas \"Golpe-Puntazo-Verdugos y Estafa\".\n" +
                "Disparo de JUEGOS JAPONESES a su llegada a La Alameda.");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),32,"23-05-2015","18:00:00"
                ,pre+"SI- Pasacalles festivo previo a la corrida de toros"
                ,pre+"SI- Desde la Plaza de las Pirámides del Pº de la Alameda y hasta la Plaza de Toros Pasacalles acompañados por el Alguacilillo, Corporación, Reina y Damas de Fiestas y Peñas.");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),33,"23-05-2015","18:30:00"
                ,pre+"SI- EXTRAORDINARIA NOVILLADA PICADA"
                ,pre+"SI- En la Plaza Municipal de Toros \"Las Cruces\". EXTRAORDINARIA NOVILLADA PICADA. Seis novillos de la ganadería \"Alipio Pérez Tabernero\" Para los NOVILLEROS:\n" +
                "\n" +
                "- ANTONIO ROSALES.\n" +
                "Figura de los novilleros 2009\n" +
                "- PEDRO MARIN.\n" +
                "Triunfador de la Feria de Fallas. Valencia 2009\n" +
                "- EL NICO.\n" +
                "Triunfador de la Feria del Corpus. Granada\n" +
                "\n" +
                "(Cambios de tercio peñas: Pendón y Golpe-Puntazo-Verdugos)");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),34,"23-05-2015","00:30:00"
                ,pre+"SI- FUEGOS ARTIFICIALES"
                ,pre+"SI- Junto a la Plaza de Toros. FUEGOS ARTIFICIALES.");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),35,"23-05-2015","01:00:00"
                ,pre+"SI- Gran Espectucalo del grupo LOS CHARROS DE ESPAÑA"
                ,pre+"SI- En el Paseo de la Alameda. Espectáculo \"LOS CHARROS DE ESPAÑA\"\n" +
                "Copatrocina: Diputación de Guadalajara y Caja Castilla La Mancha");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),36,"23-05-2015","01:00:00"
                ,pre+"SI- Gran Espectucalo del grupo LOS CHARROS DE ESPAÑA"
                ,pre+"SI- En el Paseo de la Alameda. Espectáculo \"LOS CHARROS DE ESPAÑA\"\n" +
                "Copatrocina: Diputación de Guadalajara y Caja Castilla La Mancha");
        dia1_l.add(evC);
        fullList.add(evC);

        diaC.setListaEventos(dia1_l);
        xdia.add(diaC);

        //Dia 16-06-2015
        diaC= new DayEvents();
        dia1_l = new ArrayList<Event>();

        try
        {
            diaC.setProgramEventDate(sfd.parse("16-06-2015"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        evC = createEvent(acId,epId,sj.getProgramId(),37,"16-06-2015","12:00:00"
                ,pre+"SI- Misa en Honor a la Virgen de la Salud"
                ,pre+"SI- En la catedral Misa Solemne a CArgo del Obispo Emerito con la presencia de la Reina y damas de Honor de las Fiestas");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId,epId,sj.getProgramId(),38,"16-06-2015","22:00:00"
                ,pre+"SI- Grasn Verbena de Fin de Fiesta"
                ,pre+"SI- Gran cierre de las fiestas patronales con una animada Verbena Popular en la alameda amenizada por la Gran Orquesta La Ola");
        dia1_l.add(evC);
        fullList.add(evC);

        diaC.setListaEventos(dia1_l);
        xdia.add(diaC);

        //Final
        sj.setEventByDay(xdia);
        sj.setFullList(fullList);
        return sj;
    }

    private  Program creaProgramSanJuan(Integer acId,Integer epId,String pre) {
        Program sj = new Program();
        sj.setAutonomousCommunityId(acId);
        sj.setEventPlannerId(epId);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            sj.setProgramStartDate(sdf.parse("2015-06-15"));
            sj.setProgramEndDate(sdf.parse("2015-06-29"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        sj.setProgramId(genId(1, epId));
        sj.setProgramSortDescription(pre + "Programación Fiestas de San Juan 2015");
        sj.setProgramLongDescription(pre + "Programacion Fiestas de San Juan 2015. Si existiera Long DEsciption");
        sj.setState(TypeState.SCHEDULED);

        //Creo los eventos del primer día
        List<DayEvents> xdia = new ArrayList<DayEvents>();
        List<Event> fullList = new ArrayList<Event>();
        Event evC = null;
        DayEvents diaC = null;
        List<Event> dia1_l = null;
        SimpleDateFormat sfd = new SimpleDateFormat("dd-MM-yyyy");

        //Final
        sj.setEventByDay(xdia);
        sj.setFullList(fullList);

        //Dia 15-06-2015
        diaC= new DayEvents();
        dia1_l = new ArrayList<Event>();

        try {
            diaC.setProgramEventDate(sfd.parse("15-06-2015"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        evC = createEvent(acId, epId, sj.getProgramId(), 1, "15-06-2015", "20:00:00"
                , pre + "SI- Audición de Alumnos de la Escuela Municipal de Música"
                , pre + "SI- Audición de Alumnos de la Escuela Municipal de Música (alumnos de piano de la profesora Bernadetta Raatz). Lugar: Plazuela de la Cárcel. ");
        dia1_l.add(evC);
        fullList.add(evC);
        diaC.setListaEventos(dia1_l);
        xdia.add(diaC);

        //Dia 16-06-2015
        diaC= new DayEvents();
        dia1_l = new ArrayList<Event>();

        try {
            diaC.setProgramEventDate(sfd.parse("16-06-2015"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        evC = createEvent(acId, epId, sj.getProgramId(), 2, "16-06-2015", "20:00:00"
                , pre + "SI- Audición de Alumnos de la Escuela Municipal de Música"
                , pre + "SI- Audición de Alumnos de la Escuela Municipal de Música  (alumnos de piano de la profesora Elisa Gómez). Lugar: Plazuela de la Cárcel. ");
        dia1_l.add(evC);
        fullList.add(evC);
        diaC.setListaEventos(dia1_l);
        xdia.add(diaC);

        //Dia 20-06-2015
        diaC= new DayEvents();
        dia1_l = new ArrayList<Event>();

        try {
            diaC.setProgramEventDate(sfd.parse("20-06-2015"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        evC = createEvent(acId, epId, sj.getProgramId(), 3, "20-06-2015", "18:30:00"
                , pre + "SI- Espectáculo Por amor al arte. A cargo de Pan y Miguel Moyarga"
                , pre + "SI- Espectáculo Por amor al arte. A cargo de Pan y Miguel Moyarga. Lugar Ermita de Sanroque");
        dia1_l.add(evC);
        fullList.add(evC);
        diaC.setListaEventos(dia1_l);
        xdia.add(diaC);


        //Dia 22-06-2015
        diaC= new DayEvents();
        dia1_l = new ArrayList<Event>();

        try {
            diaC.setProgramEventDate(sfd.parse("22-06-2015"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        evC = createEvent(acId, epId, sj.getProgramId(), 4, "22-06-2015", "19:30:00"
                , pre + "SI- Audición de Alumnos de la Escuela Municipal de Música"
                , pre + "SI- Audición de Alumnos de la Escuela Municipal de Música  (Alumnos de guitarra del profesor Javier Villaverde). Lugar: Plazuela de la Cárcel. ");
        dia1_l.add(evC);
        fullList.add(evC);
        diaC.setListaEventos(dia1_l);
        xdia.add(diaC);

        //Dia 23-06-2015
        diaC= new DayEvents();
        dia1_l = new ArrayList<Event>();

        try {
            diaC.setProgramEventDate(sfd.parse("23-06-2015"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        evC = createEvent(acId, epId, sj.getProgramId(), 5, "23-06-2015", "19:30:00"
                , pre + "SI- Visita del jurado a los barrios inscritos en concurso de Arcos de San Juan."
                , pre + "SI- Visita del jurado a los barrios inscritos en concurso de Arcos de San Juan.");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId, epId, sj.getProgramId(), 6, "23-06-2015", "23:00:00"
                , pre + "SI- Actuación de la Rondalla Seguntina y su Grupo de Baile Virgen de la Mayor."
                , pre + "SI- Actuación de la Rondalla Seguntina y su Grupo de Baile Virgen de la Mayor. Lugar: La Plaza Mayor.");
        dia1_l.add(evC);
        fullList.add(evC);
        evC = createEvent(acId, epId, sj.getProgramId(), 7, "23-06-2015", "00:00:00"
                , pre + "SI- Entrega de premios del Concurso de Arcos"
                , pre + "SI- Entrega de premios del Concurso de Arcos. Lugar: La Plaza Mayor.");
        dia1_l.add(evC);
        fullList.add(evC);

        diaC.setListaEventos(dia1_l);
        xdia.add(diaC);

        //Dia 25-06-2015
        diaC= new DayEvents();
        dia1_l = new ArrayList<Event>();

        try {
            diaC.setProgramEventDate(sfd.parse("25-06-2015"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        evC = createEvent(acId, epId, sj.getProgramId(), 8, "25-06-2015", "20:00:00"
                , pre + "SI- Audición de Alumnos de la Escuela Municipal de Música"
                , pre + "SI- Audición de Alumnos de la Escuela Municipal de Música  (Alumnos de piano de la profesora Antonia Carmona y de violín del profesor Ricardo Dómine). Lugar: Plazuela de la Cárcel. ");
        dia1_l.add(evC);
        fullList.add(evC);
        diaC.setListaEventos(dia1_l);
        xdia.add(diaC);


        //Dia 26-06-2015
        diaC= new DayEvents();
        dia1_l = new ArrayList<Event>();

        try {
            diaC.setProgramEventDate(sfd.parse("26-06-2015"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        evC = createEvent(acId, epId, sj.getProgramId(), 9, "26-06-2015", "21:00:00"
                , pre + "SI- III Festival Folk-Sigüenza con la actuación del  grupo: JARAIZ"
                , pre + "SI- III Festival Folk-Sigüenza con la actuación del  grupo: JARAIZ. Lugarr: Plazuela de la Cárcel. ");
        dia1_l.add(evC);
        fullList.add(evC);
        diaC.setListaEventos(dia1_l);
        xdia.add(diaC);

        //Dia 29-06-2015
        diaC= new DayEvents();
        dia1_l = new ArrayList<Event>();

        try {
            diaC.setProgramEventDate(sfd.parse("29-06-2015"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        evC = createEvent(acId, epId, sj.getProgramId(), 10, "29-06-2015", "20:30:00"
                , pre + "SI- X Festival de Bailes de Salón."
                , pre + "SI- X Festival de Bailes de Salón.\n" +
                "\n" +
                "Organiza: Asociación Sigüenza Baila\n" +
                "\n" +
                "Lugar: Polideportivo Municipal La Salceda.\n" +
                "\n" +
                "Organiza: Ayto. Sigüenza.\n" +
                "\n" +
                "Colabora: Asoc. Dulzaineros de Sigüenza, Rondalla Seguntina, Asoc. Sigüenza Baila. ");
        dia1_l.add(evC);
        fullList.add(evC);
        diaC.setListaEventos(dia1_l);
        xdia.add(diaC);


        //Final
        sj.setEventByDay(xdia);
        sj.setFullList(fullList);
        return sj;
    }

    private Integer genId(Integer ei, Integer pi)
    {
        String a_ei= ei.toString();
        String a_pi = pi<10?"0"+pi.toString():pi.toString();
        return new Integer (Integer.parseInt(a_pi+a_ei));
    }

    private Event createEvent(Integer acId,Integer epId,Integer pId,Integer eventId,String day, String time, String sorT, String LongT )
    {
        Event localEvent = new Event();
        localEvent.setAutonomousCommunityId(acId);
        localEvent.setEventPlannerId(epId);
        localEvent.setProgramId(pId);
        localEvent.setEventId(genId(eventId, pId));
        localEvent.setState(TypeState.ENDED);
        localEvent.setLongText(LongT);
        localEvent.setSortText(sorT);
        SimpleDateFormat sfd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date StartDate = null;
        try {
            StartDate = sfd.parse(day+" "+time);
            localEvent.setStartDate(StartDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return localEvent;
    }
}
