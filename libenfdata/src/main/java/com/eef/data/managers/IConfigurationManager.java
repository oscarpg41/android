package com.eef.data.managers;

import com.eef.data.dataelements.AutonomousCommunity;
import com.eef.data.dataelements.EventPlanner;
import com.eef.data.eefExceptions.eefException;
import com.eef.data.eeftypes.TypeConfiguration;

import java.util.List;

/**
 * Created by jluis on 28/02/15.
 */
public interface IConfigurationManager {

    /*
        Returns the configuration type used in the com.o3j.es.estamosenfiestas.factory to obtain the configuration.
     */
    TypeConfiguration checkConfigurationType();

    /*
        Returns true if the APK configuration has been deployed with the autonomous community available
     */
    public Boolean isCommunityAutonomousEnabled();

    /*
        This method returns the autonomous Community List to be showed as a list only with the items name
     */
    public List<AutonomousCommunity> getAutonomousCommunityList() throws eefException;

    /*
        Returns the event planner list. It returns the complete event planner list when
        the autonomous community is not configured or the event planner list when a Community has been set in the configuration
     */
    public List<EventPlanner> getEventPlannerList() throws eefException;

    /*
        Set the autonomous community configuration changes including the new Event Planner selected
         If the AutonomousCommunity is not enabled returns an exception
     */
    public void changeAutonomousCommunityConfigured(AutonomousCommunity newAutonomousCommunity,EventPlanner newEventPlanner) throws eefException;

    /*
        Set the ne event planner configured
     */
    public void changeEventPlannerConfigured(EventPlanner newEventPlanner) throws eefException;

    /*
        Returns the autonomous Community configured or throws an exception id it's not
        available or is not configured
     */
    public AutonomousCommunity getAutonomousCommunityConfigured() throws eefException;


    /*
        Returns the event planner which is configured as active or throws an exception if there are
        not a event planner configured
     */
    public EventPlanner getEventPlannerConfigured() throws  eefException;

    /*
        Persist the configuration
     */
    public void persistsConfiguration() throws eefException;

    /*
        Returns true if the configuration is not persisted.
     */

    public boolean configurationToPersist();
    /*
        Returns a Autonomous Configuration if you send and Id or throws an exception
        if the community is not available or the id is not a valid community id
     */
    public AutonomousCommunity getAutonomousCommunityById(Integer id) throws eefException;

    /*
        Returns the Event planner by Id.  If the Community is active only search
        in the community event planner list.
        If the id is not a valid id of the event planner list throws and exception
     */
    public EventPlanner getEventPlannerById(Integer id) throws eefException;


    /*Informs if the configuration is right or not

     */
    public boolean isRightConfigured();



}
