package com.eef.data.managers;

import com.eef.data.dataelements.AutonomousCommunity;
import com.eef.data.dataelements.EventPlanner;

import java.util.List;

/**
 * Created by jluis on 21/02/15.
 */
public interface IEventPlannerListManager {

    public List<EventPlanner> getEventPlanner();

    public List<EventPlanner> getA();

    public void saveEventPlannerList(List<EventPlanner> list);

}
